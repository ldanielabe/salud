<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user
 * @property string $ruta
 * @property string $titulo
 * @property string $descripcion
 * @property string $url
 * @property int $orden
 * @property string $updated_at
 * @property string $created_at
 * @property User $user
 */
class Slide extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'ruta', 'titulo', 'descripcion', 'url', 'orden', 'updated_at', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
