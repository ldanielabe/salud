<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_antecedente
 * @property int $id_estudiante
 * @property string $detalle
 * @property Antecedente $antecedente
 * @property Estudiante $estudiante
 */
class FichaSalud extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_antecedente', 'id_estudiante', 'detalle'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function antecedente()
    {
        return $this->belongsTo('App\Antecedente', 'id_antecedente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estudiante()
    {
        return $this->belongsTo('App\Estudiante', 'id_estudiante');
    }
}
