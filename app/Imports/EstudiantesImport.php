<?php

namespace App\Imports;

use App\Estudiante;
use Maatwebsite\Excel\Concerns\ToModel;

class EstudiantesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Estudiante([

        'nombre'=>$row[0] ." ". $row[1],
        'apellidos'=>$row[2] ." ". $row[3],
        'documento'=>$row[4],
        'fechaNacimiento'=>$row[5],
        'grado'=>$row[6],//int
        'jornada'=>$row[7],
        'genero'=>$row[8],
        'area_residencia'=>$row[9],
        'estrato'=>$row[10],
        'id_institucion'=>auth()->user()->institucio->id,
        //'updated_at'=>''+date('Y-m-d H:i:s'),
        //'created_at'=>''+date('Y-m-d H:i:s'),
        
        ]);
    }
}
