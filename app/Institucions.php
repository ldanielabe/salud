<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre_insti
 * @property string $direccion
 * @property int $telefono
 * @property string $mision
 * @property string $vision
 * @property User[] $users
 */
class Institucions extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['nombre_insti', 'direccion', 'telefono', 'mision', 'vision'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'id_institucion');
    }
}
