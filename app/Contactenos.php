<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactenos extends Model
{
    //
    public function institucio()
    {
        return $this->belongsTo('App\Institucions', 'institucion');
    }
}
