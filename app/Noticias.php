<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user
 * @property string $ruta
 * @property string $titulo
 * @property string $contenido
 * @property string $fecha
 * @property string $updated_at
 * @property string $created_at
 * @property User $user
 */
class Noticias extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'ruta', 'titulo', 'contenido', 'fecha', 'updated_at', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
