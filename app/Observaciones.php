<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_estudiante
 * @property string $nombre_problema
 * @property string $detalles
 * @property int $valor
 * @property string $estado
 * @property string $tipo_falta
 * @property Estudiante $estudiante
 */
class Observaciones extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_estudiante', 'nombre_problema', 'detalles', 'valor', 'estado', 'tipo_falta'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estudiante()
    {
        return $this->belongsTo('App\Estudiante', 'id_estudiante');
    }
}
