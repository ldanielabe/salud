<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 * @property string $documento
 * @property string $celular
 * @property string $fechaNacimiento
 * @property string $direccion
 * @property string $grupo_sanguineo
 * @property int $peso
 * @property int $altura
 * @property string $estado_nutricional
 * @property string $barrio
 * @property int $grado
 * @property string $jornada
 * @property string $imagen
 * @property string $password
 * @property string $area_residencia
 * @property string $regimen_salud
 * @property int $id_institucion
 * @property string $updated_at
 * @property string $created_at
 * @property FichaSalud[] $fichaSaluds
 * @property Observacione[] $observaciones
 */
class Estudiante extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    protected $fillable = ['nombre', 'apellidos', 'email', 'documento', 'celular', 'fechaNacimiento', 'direccion', 'grupo_sanguineo', 'peso', 'altura', 'estado_nutricional', 'barrio', 'grado', 'jornada', 'imagen', 'password', 'area_residencia', 'regimen_salud', 'id_institucion'];//, 'updated_at', 'created_at'
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fichaSaluds()
    {
        return $this->hasMany('App\FichaSalud', 'id_estudiante');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function observaciones()
    {
        return $this->hasMany('App\Observacione', 'id_estudiante');
    }
}
