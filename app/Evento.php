<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user
 * @property string $titulo
 * @property string $introduccion
 * @property string $contenido
 * @property string $fecha
 * @property string $hora
 * @property string $lugar
 * @property int $orden
 * @property string $ruta
 * @property string $updated_at
 * @property string $created_at
 * @property User $user
 */
class Evento extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'titulo', 'introduccion', 'contenido', 'fecha', 'hora', 'lugar', 'orden', 'ruta', 'updated_at', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }
}
