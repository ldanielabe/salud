<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_antecedentes
 * @property string $tipo
 * @property Antecedente $antecedente
 * @property FichaSalud[] $fichaSaluds
 */
class AntecedentesTipo extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_antecedentes', 'tipo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function antecedente()
    {
        return $this->belongsTo('App\Antecedente', 'id_antecedentes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fichaSaluds()
    {
        return $this->hasMany('App\FichaSalud', 'id_antecedentes_tipo');
    }
}
