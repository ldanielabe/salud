<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * @property int $id
 * @property int $id_rol
 * @property int $id_institucion
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property int $documento
 * @property int $celular
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Institucion $institucion
 * @property Rol $rol
 * @property Evento[] $eventos
 * @property Noticia[] $noticias
 * @property Slide[] $slides
 */
class User extends Authenticatable 
{

    

use  Notifiable;
    /**
     * @var array
     */
    protected $fillable = ['id_rol', 'id_institucion', 'nombre', 'apellidos', 'email', 'email_verified_at', 'password', 'documento', 'celular','id_estudiante', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institucio()
    {
        return $this->belongsTo('App\Institucions', 'id_institucion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rol()
    {
        return $this->belongsTo('App\Rol', 'id_rol');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventos()
    {
        return $this->hasMany('App\Evento', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function noticias()
    {
        return $this->hasMany('App\Noticia', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slides()
    {
        return $this->hasMany('App\Slide', 'id_user');
    }
    public function estudiante()
    {
        return $this->belongsTo('App\Estudiante', 'id_estudiante');
    }

    public function sendPasswordResetNotification($token){
        $this->notify(new MailResetPasswordToken($token));
    }
}
