<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;

class AntecedentesTipoController extends Controller{

    public function datagrid_rel() {
        try { 
        
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('antecedentes_tipos');
           // $crud->setSkin('bootstrap-v4');
            //$crud->setThemePath('/my/custom/theme/path/')
            $crud->setSubject('tipos antecedentes ', 'Agrega los tipos de antecedentes, actualiza su informacion y elimina');
            $crud->setPrimaryKey('id', 'antecedentes_tipos');
           
            //$crud->setRelation('id_antecedente', 'antecedentes','{tipo}',['id']);
            
            
            $crud->columns(array('tipo','id_antecedentes'));
            $crud->editFields(['tipo','id_antecedentes']);
            $crud->addFields(['tipo','id_antecedentes']);
            $crud->requiredFields(['tipo','id_antecedentes']);
          
            $crud->unsetDelete(); //Elimine btnDelete
           
            $crud->displayAs('id_antecedente', 'Antecedentes');
            
           /* $antecedente = DB::table('antecedentes')->get();
    
            $valores = [];
            foreach ($antecedente as $var) {
                $valores[$var->id] = [$var->tipo];
            }
            
            $crud->fieldType('id_antecedente', 'dropdown_search',$valores);*/
           
         
    
            $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
        }
    
        return $this->_show_output($output);     
        }
        
        public function datagrid()
        {
         try { 
            
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('antecedentes_tipos');
           // $crud->setSkin('bootstrap-v4');
            //$crud->setThemePath('/my/custom/theme/path/')
            $crud->setSubject('tipos antecedentes', 'Agrega los tipos de antecedentes, actualiza su informacion y elimina');
            $crud->setPrimaryKey('id', 'antecedentes_tipos');
           
            $crud->columns(array('tipo','id_antecedentes'));
            $crud->editFields(['tipo','id_antecedentes']);
            $crud->addFields(['tipo','id_antecedentes']);
            $crud->requiredFields(['tipo','id_antecedentes']);
          
          
            $crud->unsetDelete(); //Elimine btnDelete
            
            $crud->displayAs('id_antecedentes', 'Antecedentes');   
           
            $antecedente = DB::table('antecedentes')->get();
    
            $val = [];
            foreach ($antecedente as $var) {
                $val[$var->id] = [$var->tipo];
            }
            
            $crud->fieldType('id_antecedentes', 'dropdown_search',$val);
           
             
            $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
        }
    
        return $this->_show_output($output);     
        }
       
    
            private function _getGroceryCrudEnterprise() {
            $database = $this->_getDatabaseConnection();
            $config = config('grocerycrud');
            $crud = new GroceryCrud($config, $database);
            return $crud;
        }
    
        
        private function _getDatabaseConnection() {
            $databaseConnection = config('database.default');
            $databaseConfig = config('database.connections.' . $databaseConnection);
    
            return [
                'adapter' => [
                    'driver' => 'Pdo_Mysql',
                    'database' => env('DB_DATABASE'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'charset' => 'utf8'
                ]
            ];
        }
    
        private function _show_output($output) {
            
            if ($output->isJSONResponse) {
                return response($output->output, 200)
                      ->header('Content-Type', 'application/json')
                      ->header('charset', 'utf-8');
            }
          
            $css_files = $output->css_files;
            $js_files = $output->js_files;
            $output = $output->output;
            $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
            $userId = Auth::id();
            $allUser = User::all();
            $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
            $layout_grocery="";
            if(auth()->user()->id_rol==1){
                $layout_grocery='layouts.admin_grocery';
            }else if(auth()->user()->id_rol==2){
                $layout_grocery='layouts.profesor_grocery';
            }else if(auth()->user()->id_rol==3){
                $layout_grocery='layouts.rector_grocery';
            }else if(auth()->user()->id_rol==4){
                $layout_grocery='layouts.evaluador_grocery';
            } else if(auth()->user()->id_rol==5){
                $layout_grocery='layouts.autoridad_grocery';
            }else if(auth()->user()->id_rol==6){
                $layout_grocery='layouts.acudiente_grocery';
            }else if(auth()->user()->id_rol==7){
                $layout_grocery='layouts.admin2_grocery';
            }

            return view('grocery.crud', [
                'output' => $output,
                'css_files' => $css_files,
                'js_files' => $js_files,
                'instituciones'=>$instituciones,
                'titulo'=>'Antecedentes',
                'layout_grocery' => $layout_grocery,
                "allUser"=>$allUser,
                "notificacion"=>$notificacion
        
            ]);
        }
}
