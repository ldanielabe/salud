<?php

namespace App\Http\Controllers\Autoridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Visitas;
use App\Contactenos;
use Auth;
use Illuminate\Support\Facades\Hash;

class AutoridadController extends Controller
{
   
    public function dashboard(){
        $now = new \DateTime();
        $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
        $date = explode('-', $now->format('Y-m-d'), 3); 
        $mes = Visitas::whereYear('fecha', '=', $date[0])
        ->whereMonth('fecha', '=', $date[1])
        ->get()->sum('cantidad');
        $todo = Visitas::all()->sum('cantidad');
        $user = User::all()->count();
        $usuarios=User::offset(0)->limit(5)->get(); 
        $contactenos = Contactenos::orderBy('id','ASC')->paginate(1);

        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));

        return view('autoridad.perfil',['instituciones'=>$instituciones,"hoy"=>$visit->cantidad,
        "mes"=>$mes,"todo"=>$todo,"user"=>$user,"usuarios"=>$usuarios,"contactenos"=>$contactenos]);
          
        }
            

}
