<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Estudiante;
use App\Institucions;
use PDF;
use DateTime;
use Illuminate\Support\Facades\Crypt;
class EditarFichaController extends Controller
{
   
    public function editar($id)
    {
        $id=decrypt($id);
        $estudiante=Estudiante::find($id);

        $inst=Estudiante::find($id)->id_institucion;
              if(auth()->user()->id_rol != 1 && auth()->user()->id_institucion != $inst){
              return redirect()->route('perfil');
        }
       
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $ficha=DB::select('SELECT * FROM formulario_ficha_salud WHERE id_estudiante='.$id.'');
       
        return view('evaluador.historicoFicha',["estudiante"=>$estudiante,"ficha"=>$ficha,"allUser"=>$allUser,"notificacion"=>$notificacion]);   
 
    }
    public function crear($id)
    {
        $id =  Crypt::decrypt($id);
        $estudiante=Estudiante::find($id);
       
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $dominios=DB::select('SELECT * from dominios where estado="Activo"');
        $preguntas=DB::select('SELECT preguntas.id,preguntas.id_dominio,preguntas.pregunta,preguntas.tipo_input FROM preguntas WHERE preguntas.activo=1 GROUP BY preguntas.id_dominio,preguntas.id,preguntas.pregunta,preguntas.tipo_input');
        $respuestas=DB::select('SELECT respuestas_select_dinamico.respuesta, respuestas_select_dinamico.id,respuestas_select_dinamico.id_preguntas,respuestas_select_dinamico.riesgo FROM respuestas_select_dinamico GROUP BY respuestas_select_dinamico.id_preguntas,respuestas_select_dinamico.id, respuestas_select_dinamico.respuesta,respuestas_select_dinamico.riesgo');
       return view('evaluador.aggFicha',["estudiante"=>$estudiante,"allUser"=>$allUser,"notificacion"=>$notificacion,"dominios"=>$dominios,"preguntas"=>$preguntas,"respuestas"=>$respuestas],compact('preguntas','dominios','respuestas'));   
 
    }

    public function reportes($id)
    {
        try {
        $id=decrypt($id);
        $userId = Auth::id();
        $allUser = User::all();
        $dominios=DB::select('SELECT * from dominios');
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin_grocery';
        }else if(auth()->user()->id_rol==2){
            $layout_grocery='layouts.profesor_grocery';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector_grocery';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador_grocery';
        } else if(auth()->user()->id_rol==5){
            $layout_grocery='layouts.autoridad_grocery';
        }else if(auth()->user()->id_rol==6){
            $layout_grocery='layouts.acudiente_grocery';
        }else if(auth()->user()->id_rol==7){
            $layout_grocery='layouts.admin2_grocery';
        }
     
        return view('evaluador.reporte',["instituciones"=>$instituciones,"layout_grocery"=>$layout_grocery,"allUser"=>$allUser,"notificacion"=>$notificacion,"dominios"=>$dominios],compact('id'));   
    }catch(\Exception $e){
        return redirect()->route('perfil');
    }
    }
    public function reporteAjax($activo,$id_pregunta,$id_colegio)
    {
       
            $userId=$id_colegio;
        if(auth()->user()->id_rol==1 || auth()->user()->id_institucion==$id_colegio){

       
        $respuestas=DB::select('SELECT e.nombre,e.apellidos,r.respuestas,r.created_at FROM estudiantes as e INNER JOIN formulario_ficha_salud as f on f.id_estudiante=e.id INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id WHERE e.activo='.$activo.' and e.id_institucion='.$userId.' and r.id_preguntas='.$id_pregunta.'');
        $data = [
            "status" => "200",
            "respuestas" => $respuestas,
          
        ];
        return response()->json($data);
    }else{
        $data = [
            "status" => "403",
            "respuestas" => "colegio no corresponde al asociado a su perfil",
          
        ];
        return response()->json($data);
    } 
    }
    public function reporteAjaxDominio($id_dominio)
    {
        
        $preguntas=DB::select('SELECT * from preguntas where preguntas.id_dominio='.$id_dominio.'');
        $data = [
            "status" => "200",
            "preguntas" => $preguntas,
          
        ];
        return response()->json($data);
    }

    public function recordatorio($id)
    {
        try {
        $id=decrypt($id);
        $id_colegio=auth()->user()->institucio->id;
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $dominios=DB::select('SELECT * from dominios');
        $observaciones=DB::select('SELECT id_dominio,observaciones_dominio_ficha.id as id_observacion,estudiantes.nombre,estudiantes.apellidos,formulario_ficha_salud.observaciones as observaciones_globales,observaciones_dominio_ficha.descripcion as observacion_dominio FROM observaciones_dominio_ficha INNER JOIN formulario_ficha_salud on formulario_ficha_salud.id=observaciones_dominio_ficha.id_formulario_ficha_salud INNER JOIN estudiantes on estudiantes.id=formulario_ficha_salud.id_estudiante where estudiantes.id_institucion='.$id_colegio.' and resuelto=0 and observaciones_dominio_ficha.descripcion IS NOT NULL and observaciones_dominio_ficha.descripcion !="Ninguna"');
        $layout_grocery="";
        if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador';
        } 
        return view('evaluador.recordatorio',['layout_grocery' => $layout_grocery,"allUser"=>$allUser,"notificacion"=>$notificacion,"dominios"=>$dominios,"observaciones"=>$observaciones]);   
    }catch(\Exception $e){
        return redirect()->route('perfil');
    }
    }
    public function recordatorioObservador($id)
    {
        try {
            $id=decrypt($id);
        $institucion = Institucions::where('id',$id)->get();
      
        $id_colegio= $institucion[0]->id;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $tipos=DB::select('SELECT id,tipo_falta FROM observaciones WHERE observaciones.privado=0 GROUP BY observaciones.tipo_falta');
        $observaciones=DB::select('SELECT observaciones.id,estudiantes.nombre,estudiantes.apellidos,observaciones.tipo_falta,observaciones.detalles,observaciones.created_at FROM observaciones INNER JOIN estudiantes on estudiantes.id=observaciones.id WHERE observaciones.privado=0 and estudiantes.id_institucion='.$id_colegio.' and observaciones.resuelto=0');
        $layout_grocery="";
    }catch(\Exception $e){
        return redirect()->route('perfil');
    }
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin';
        }else if(auth()->user()->id_rol==2){
            $layout_grocery='layouts.profesor';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador';
        } else if(auth()->user()->id_rol==5){
            $layout_grocery='layouts.autoridad';
        }

        return view('evaluador.recordatorioObservador',['layout_grocery' => $layout_grocery,"instituciones"=>$instituciones,'institucion'=>$institucion,"allUser"=>$allUser,"notificacion"=>$notificacion,"tipos"=>$tipos,"observaciones"=>$observaciones]);   

    }
    public function updateObservador($id){
        DB::table('observaciones')->where('id',$id)->update(array(
            'resuelto'=>1,
        ));
        $data = [
            "status" => "200",
           
          
        ];
        
        return response()->json($data);
    }
    public function updateObservaciones($id){
        DB::table('observaciones_dominio_ficha')->where('id',$id)->update(array(
            'resuelto'=>1,
        ));
        $data = [
            "status" => "200",
           
          
        ];
        
        return response()->json($data);
    }
    public function historialPDF($id)
    {
        $estudiante=Estudiante::find($id);
       
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $ficha=DB::select('SELECT preguntas.id_dominio as id_dominio, preguntas.id as id_pregunta,respuestas.id as id_respuesta,preguntas.pregunta,preguntas.tipo_input,respuestas.respuestas,formulario_ficha_salud.id as id_ficha,formulario_ficha_salud.observaciones,formulario_ficha_salud.created_at FROM formulario_ficha_salud INNER JOIN respuestas on respuestas.id_formulario_ficha_salud=formulario_ficha_salud.id INNER JOIN preguntas on preguntas.id=respuestas.id_preguntas WHERE formulario_ficha_salud.id='.$id.' GROUP BY preguntas.id,respuestas.id, preguntas.id_dominio,preguntas.pregunta,preguntas.tipo_input,respuestas.respuestas,formulario_ficha_salud.id,formulario_ficha_salud.observaciones,formulario_ficha_salud.created_at');
        $dominios=DB::select('SELECT id,nombre from dominios where estado="Activo" order by id asc');
        $observaciones=DB::select('SELECT * from observaciones_dominio_ficha where id_formulario_ficha_salud='.$id.'');
        $data = [
            "status" => "200",
            "ficha" => $ficha,
            "dominios" => $dominios,
            "observaciones" => $observaciones,
        ];
        
        return response()->json($data);
       
    }
    public function guardarObsDominio($id_ficha,$id_dominio,$valor,$valorizacion)
    {
        if($valor=="1"){
            DB::table('observaciones_dominio_ficha')->insert([
                'id_dominio'=>$id_dominio,
                'id_formulario_ficha_salud'=>$id_ficha,
                'valorizacion'=>$valorizacion
            ]);
        }else{
            DB::table('observaciones_dominio_ficha')->insert([
                'id_dominio'=>$id_dominio,
                'id_formulario_ficha_salud'=>$id_ficha,
                'descripcion'=>$valor,
                'valorizacion'=>$valorizacion
            ]);
        }
      

        $data = [
            "status" => "200",
            
        ];
        
        return response()->json($data);

    }
    public function crearFicha($id_estudiante)
    {

       DB::table('formulario_ficha_salud')->insert([
            'id_estudiante'=>$id_estudiante,
        ]);
        $c =DB::select('SELECT formulario_ficha_salud.id FROM formulario_ficha_salud WHERE created_at in(select MAX(created_at) as maximo from formulario_ficha_salud WHERE id_estudiante='.$id_estudiante.')');
        $data = [
            "status" => "200",
            "ficha"=>$c
          
        ];
        
        return response()->json($data);



    }
    public function guardarObsGlobal($value,$id){
        DB::table('formulario_ficha_salud')->where('id',$id)->update(array(
            'observaciones'=>$value,
        ));
        $data = [
            "status" => "200",
           
          
        ];
        
        return response()->json($data);
    }
    public function updateCrecimiento($id,$altura,$peso,$crecimiento){
        DB::table('estudiantes')->where('id',$id)->update(array(
            'altura'=>$altura,
            'peso'=>$peso,
            'estado_nutricional'=>$crecimiento,
        ));
        $data = [
            "status" => "200",
           
          
        ];
        
        return response()->json($data);
    }
    public function guardarFicha($id_ficha,$id_preguntas,$valores)
    {


        DB::table('respuestas')->insert([
            'respuestas'=>$valores,
            'id_preguntas'=>$id_preguntas,
            'id_formulario_ficha_salud'=>$id_ficha,
            
        ]);

        $data = [
            "status" => "200",

          
        ];
        
        return response()->json($data);



    }
    public function export($id)
    {
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        return view('evaluador.exportFicha',["allUser"=>$allUser,"notificacion"=>$notificacion]);   
 
    }

    public function generar_pdf_ficha_salud($id){
        $estudiante=Estudiante::find($id);
        $ficha=DB::select("SELECT p.id,p.pregunta,p.tipo_input,p.id_dominio,r.respuestas, f.id_estudiante,f.observaciones,f.created_at as fech,count(*) as cantidad 
        FROM formulario_ficha_salud as f 
        INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id 
        INNER JOIN preguntas as p on p.id=r.id_preguntas INNER JOIN dominios as d on d.id=p.id_dominio 
        INNER JOIN estudiantes as e on e.id=f.id_estudiante 
        INNER JOIN(SELECT f.id,MAX(f.created_at) fecha FROM formulario_ficha_salud as f GROUP BY f.id_estudiante)ultimo on ultimo.fecha=f.created_at and e.id=".$id."
        group by r.id_preguntas,r.respuestas");
        $dominios=DB::select('SELECT id,nombre from dominios where estado="Activo" order by id asc');
        
        $institucion=DB::select(DB::raw('SELECT *  FROM institucions AS i WHERE i.id='.$estudiante->id_institucion));
        $fecha_nacimiento = new DateTime($estudiante->fechaNacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fecha_nacimiento);
       
        $data = [
            'fecha'=>$hoy->format('d-m-Y H:i:s'),
            'estudiante' => $estudiante,
            'edad' => $edad->y,
            'ficha' => $ficha,
            'dominios' => $dominios,
            'institucion'=> $institucion 
        ];

    /*$pdf = \PDF::loadView('pdf.formulario_ficha_salud', $data)->setOptions([
                      'isRemoteEnabled'=>TRUE,
                      'isHtml5ParserEnabled'=>TRUE,
                      'logOutputFile' => storage_path('logs/log.htm'),
                      'tempDir' => storage_path('logs/')
                  ]);
       
        
        $contxt = stream_context_create([
        'ssl' => [
        'verify_peer' => FALSE,
        'verify_peer_name' => FALSE,
        'allow_self_signed'=> TRUE
        ]
        ]);
 
        $pdf->getDomPDF()->setHttpContext($contxt);
        */

        $pdf = \PDF::setOptions([
            'logOutputFile' => storage_path('logs/log.htm'),
            'tempDir' => storage_path('logs/')
        ])->loadView('pdf.formulario_ficha_salud', $data);
        //$pdf = \PDF::loadView('pdf.formulario_ficha_salud', $data);
        $nombre_ruta = 'ficha_salud_'.$id.'.pdf';
      //  $path = public_path().'/documentos'.'/'. $nombre_ruta;
        
        return $pdf->download($nombre_ruta);
    }

    public function buscar_pdf_ficha_salud($id,$id_ficha){
        $estudiante=Estudiante::find($id);
        $ficha=DB::select("SELECT p.id,p.pregunta,p.tipo_input,p.id_dominio,r.respuestas, f.id_estudiante,f.observaciones,f.created_at as fech 
        FROM formulario_ficha_salud as f 
        INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id 
        INNER JOIN preguntas as p on p.id=r.id_preguntas 
        INNER JOIN dominios as d on d.id=p.id_dominio 
        INNER JOIN estudiantes as e on e.id=f.id_estudiante
        WHERE f.id=".$id_ficha);
        
        $dominios=DB::select('SELECT id,nombre from dominios where estado="Activo" order by id asc');
        
        $institucion=DB::select(DB::raw('SELECT *  FROM institucions AS i WHERE i.id='.$estudiante->id_institucion));
        
        $fecha_nacimiento = new DateTime($estudiante->fechaNacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fecha_nacimiento);
       
        $data = [
            'fecha'=>$hoy->format('d-m-Y H:i:s'),
            'estudiante' => $estudiante,
            'edad' => $edad->y,
            'ficha' => $ficha,
            'dominios' => $dominios,
            'institucion'=> $institucion 
        ];

       

        $pdf = \PDF::setOptions([
            'logOutputFile' => storage_path('logs/log.htm'),
            'tempDir' => storage_path('logs/')
        ])->loadView('pdf.formulario_ficha_salud', $data);
        
       // $pdf = \PDF::loadView('pdf.formulario_ficha_salud', $data);
        
        $nombre_ruta = 'ficha_salud_'.$id.'.pdf';
       // $path = public_path().'/documentos'.'/'. $nombre_ruta;
        
        return $pdf->download($nombre_ruta);
    }

}
