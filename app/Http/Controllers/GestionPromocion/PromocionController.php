<?php

namespace App\Http\Controllers\GestionPromocion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Evento;
use App\User;
use App\Directorio;
use App\Slide;
use App\Noticias;
use App\Visitas;
use App\Contactenos;
use App\Mensajes;
use App\Dominios;
use App\Prevencions;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use GroceryCrud\Core\GroceryCrud;

class PromocionController extends Controller
{

  public function datagrid_rel(){
    try { 
         
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('prevencions');
         
            $crud->setSubject('prevencion', 'Agrega nuevos documentos de interes, actualiza su informacion y elimina');
            $crud->setPrimaryKey('id', 'prevencions');
            $crud->where([
                'id_user = '.auth()->user()->id
            ]);
     
            $crud->columns(array('tipo','titulo','contenido','url','id_user','privado' ));
            $crud->editFields(['tipo','titulo','contenido','url','id_user','privado']);
            $crud->addFields(['tipo','titulo','contenido','url','id_user','privado' ]);
            $crud->requiredFields(['tipo','titulo','url','privado' ]);
            $crud->setFieldUpload("url","assets/grocery-crud/images/chosen","/assets/grocery-crud/images/chosen");
         
            $crud->uniqueFields(['titulo','url']);
            //$crud->unsetDelete(); //Elimine btnDelete
            
    
            $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
    }
    
return $this->_show_output($output);     
}
        
public function datagrid(){
    try { 
          
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('prevencions');
            $crud->setSubject('prevencion', 'Agrega nuevos Administrador, actualiza su informacion y elimina');
            $crud->setPrimaryKey('id', 'prevencions');
            $crud->where([
                'id_user = '.auth()->user()->id
            ]);
            $crud->columns(array('tipo','titulo','contenido','url','privado' ));
            $crud->editFields(['tipo','titulo','contenido','url','privado' ]);
            $crud->addFields(['tipo','titulo','contenido','url','privado' ]);
            $crud->requiredFields(['tipo','titulo','contenido','url','privado' ]);
    
            $crud->uniqueFields(['titulo','url']);
            $crud->displayAs('url', 'Subir documento');
            //$crud->unsetDelete(); //Elimine btnDelete
            $crud->setFieldUpload("url","assets/grocery-crud/images/chosen","/assets/grocery-crud/images/chosen");
       

             $tipo_prevencion = DB::table('dominios')->get();
    
             $valores = [];
             foreach ($tipo_prevencion as $variable) {
                 $valores[$variable->id] = [$variable->nombre];
             }
            
             $crud->fieldType('tipo', 'dropdown_search',$valores);
             $crud->fieldType('privado','checkbox_boolean');
             
    
            $crud->callbackBeforeInsert(function($stateParameters){
                $stateParameters->data['id_user']=auth()->user()->id;
                return $stateParameters;
             });

            $crud->callbackBeforeUpdate(function($stateParameters){
                $stateParameters->data['id_user']=auth()->user()->id;
                return $stateParameters;
             });
             
            $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
    }
    
return $this->_show_output($output);     
}

private function _getGroceryCrudEnterprise() {
    $database = $this->_getDatabaseConnection();
    $config = config('grocerycrud');
    $crud = new GroceryCrud($config, $database);
    return $crud;
}


private function _getDatabaseConnection() {
    $databaseConnection = config('database.default');
    $databaseConfig = config('database.connections.' . $databaseConnection);

    return [
        'adapter' => [
            'driver' => 'Pdo_Mysql',
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset' => 'utf8'
        ]
    ];
}

private function _show_output($output) {
    
    if ($output->isJSONResponse) {
        return response($output->output, 200)
              ->header('Content-Type', 'application/json')
              ->header('charset', 'utf-8');
    }
  
    $css_files = $output->css_files;
    $js_files = $output->js_files;
    $output = $output->output;
    $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
    $userId = Auth::id();
    $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
       

$layout_grocery="";
if(auth()->user()->id_rol==1){
    $layout_grocery='layouts.admin_grocery';
}else if(auth()->user()->id_rol==2){
    $layout_grocery='layouts.profesor_grocery';
}else if(auth()->user()->id_rol==3){
    $layout_grocery='layouts.rector_grocery';
}else if(auth()->user()->id_rol==4){
    $layout_grocery='layouts.evaluador_grocery';
} else if(auth()->user()->id_rol==5){
    $layout_grocery='layouts.autoridad_grocery';
}else if(auth()->user()->id_rol==6){
    $layout_grocery='layouts.acudiente_grocery';
}else if(auth()->user()->id_rol==7){
    $layout_grocery='layouts.admin2_grocery';
}


return view('grocery.crud', [
    'output' => $output,
    'css_files' => $css_files,
    'js_files' => $js_files,
    'titulo'=>'Documentos de interes',
    'layout_grocery' => $layout_grocery,
    "instituciones"=>$instituciones,
    "allUser"=>$allUser,
    "notificacion"=>$notificacion
]);

}
   /************************************* */
    public function documentos(){
        
        $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $tipo_prevencion= Dominios::all();
       // $prueba=DB::select('ALTER TABLE visitas ADD pruebaaaa VARCHAR(200) NULL AFTER fecha');
      // dd($prueba);
        $documentos= Prevencions::all();
    
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin';
        }else if(auth()->user()->id_rol==2){
            $layout_grocery='layouts.profesor';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador';
        } else if(auth()->user()->id_rol==5){
            $layout_grocery='layouts.autoridad';
        }else if(auth()->user()->id_rol==6){
            $layout_grocery='layouts.acudiente';
        }else if(auth()->user()->id_rol==7){
            $layout_grocery='layouts.admin2';
        }

        return view('admin.prevencion',['layout_grocery' => $layout_grocery,"instituciones"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion,"tipo_prevencion"=>$tipo_prevencion,"documentos"=>$documentos]);
   }

   public function documentos_inicio(){
        
   
   // $prueba=DB::select('ALTER TABLE visitas ADD pruebaaaa VARCHAR(200) NULL AFTER fecha');
  // dd($prueba);
    $tipo_prevencion= DB::select('SELECT * FROM prevencions INNER JOIN dominios on dominios.id=prevencions.tipo where prevencions.privado=0 GROUP BY prevencions.tipo');

    return view('documentos_interes',["tipo_prevencion"=>$tipo_prevencion]);
}

}
