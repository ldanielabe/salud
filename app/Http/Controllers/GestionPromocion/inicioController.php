<?php

namespace App\Http\Controllers\GestionPromocion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Evento;
use App\User;
use App\Directorio;
use App\Slide;
use App\Noticias;
use App\Visitas;
use App\Contactenos;
use App\Mensajes;
use App\Institucions;
use App\Prevencions;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;

class inicioController extends Controller
{
   
    public function inicio(){
        $now = new \DateTime();
        $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
        if($visit == null){
            $visita=new Visitas;
            $visita->cantidad = 1;
            $visita->fecha = $now->format('Y-m-d');
            $visita->save();
        }else{
            $visita = Visitas::find($visit->id);
            $visita->cantidad += 1;
            $visita->save();
        }
        $date = explode('-', $now->format('Y-m-d'), 3); 

        $mes = Visitas::whereYear('fecha', '=', $date[0])
        ->whereMonth('fecha', '=', $date[1])
        ->get()->sum('cantidad');

        $todo = Visitas::all()->sum('cantidad');

        $eventos=Evento::all();
        $slides=Slide::all();
        $noticias=Noticias::offset(0)->limit(3)->get(); 
        $institucions=Institucions::all();
        return view('inicio',["eventos"=>$eventos,"slides"=>$slides,"noticias"=>$noticias,
        "hoy"=>$visit,"mes"=>$mes,"todo"=>$todo,"institucions"=>$institucions]);
    }
    public function inicioAdmin(){
        $now = new \DateTime();
        $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
        if($visit==null){
            $visit="";
        }
        $date = explode('-', $now->format('Y-m-d'), 3); 
        $mes = Visitas::whereYear('fecha', '=', $date[0])
        ->whereMonth('fecha', '=', $date[1])
        ->get()->sum('cantidad');
        $todo = Visitas::all()->sum('cantidad');
        $user = User::all()->count();
        $usuarios=User::offset(0)->limit(3)->get(); 
        $contactenos = Contactenos::orderBy('id','ASC')->paginate(5);
       
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
       // $allUser = DB::select('SELECT * FROM users WHERE online=1 and id !='.$userId.' ');
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
     
        return view('admin.inicio',["hoy"=>$visit->cantidad,
        "mes"=>$mes,"todo"=>$todo,"user"=>$user,"usuarios"=>$usuarios,"contactenos"=>$contactenos,"instituciones"=>$instituciones,"notificacion"=>$notificacion],compact('allUser'));
    }

    public function contactenos_rector(){
        $id =  auth()->user()->institucio->id;
        $userId = Auth::id();
        $contactenos = Contactenos::where('institucion',$id)->orderBy('id','ASC')->paginate(5);
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
       
        return view('rector.contactenos',["contactenos"=>$contactenos,"notificacion"=>$notificacion],compact('allUser'));
    }

    public function inicioAdministrador(){
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
      
        return view('administrador.perfil',["notificacion"=>$notificacion],compact('allUser'));
    }
    public function inicioAutoridad(){
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        return view('autoridad.perfil',["notificacion"=>$notificacion,"instituciones"=>$instituciones],compact('allUser'));
    }
   
    public function  deleteMensaje($id){
        $contactenos= Contactenos::destroy($id);
        if($contactenos>0){
         return response()->json([
             'success' => '1'
         ]);
        }else{
         return response()->json([
             'success' => '0'
         ]);
        }
      
     }
//eventos
    public function mostrarEventos(){
        $eventos=Evento::orderBy('id','DESC')->paginate(1);
        $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector';
            $eventos=Evento::where('id_user',$userId)->orderBy('id','DESC')->paginate(1);
        }
        return view('admin.eventos',["layout_grocery"=>$layout_grocery,"instituciones"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('eventos'));
    }

    public function registrarEventoS(Request $request){

     if($archivo=$request->file('imagen')){
        $evento=new Evento;
        $nombre=$archivo->getClientOriginalName();
        $archivo->move('imagesEventos',$nombre);
        $evento->ruta=$nombre;
     }
       
        $evento->titulo = $request->titulo;
        $evento->introduccion = $request->introduccion;
        $evento->contenido = $request->contenido;
        $evento->id_user=auth()->user()->id;
        $evento->save();


        return redirect()->route('eventos');
    }
    public function deleteEvento($id){
        try {
            $evento = Evento::find($id);
       unlink(public_path('imagesEventos/'.$evento->ruta));
       $event= Evento::destroy($id);
       if($event>0){
        return response()->json([
            'success' => '1'
        ]);
       }else{
        return response()->json([
            'success' => '0'
        ]);
       }
        } catch(\Exception $e){
            
            $event= Evento::destroy($id);
            if($event>0){
                return response()->json([
                    'success' => '1'
                ]);
               }else{
                return response()->json([
                    'success' => '0'
                ]);
               }
        }
     
     
    }

     public function updateEventos(Request $request, $id){
        try {
        $evento = Evento::find($id);
        if($archivo=$request->file('imagen')){
            
            unlink(public_path('imagesEventos/'.$evento->ruta));
            $nombre=$archivo->getClientOriginalName();
            $archivo->move('imagesEventos',$nombre);
            $evento->ruta=$nombre;
         }
            $evento->id_user=auth()->user()->id;
            $evento->titulo = $request->titulo;
            $evento->introduccion = $request->introduccion;
            $evento->contenido = $request->contenido;
            $evento->save();
    
    
            return redirect()->route('eventos');
        }catch(\Exception $e){
            return redirect()->route('eventos');
    }
    }

    public function carrusel_eventos(){
        $eventos=Evento::all();
        return view('carrusel',["eventos"=>$eventos]);
    }
    public function carrusel_documentos($id){
        $documentos=Prevencions::where('tipo',$id)->where('privado','0')->get();
      
        return view('documentos_carrusel',["documentos"=>$documentos]);
    }

//directorio 
    public function mostrarDirectorio(){
        $directorio = Directorio::orderBy('id','DESC')->paginate(1);
        $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');

        return view('admin.directorio',["instituciones"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('directorio'));
    }

    public function registrarDirectorio(Request $request){
        try {
        if($archivo=$request->file('imagen')){
            $directorio=new Directorio;
            $imag=$archivo->getClientOriginalName();
            $archivo->move('imagesDirectorio',$imag);
            $directorio->imagen=$imag;
         }
           
            $directorio->nombre = $request->nombre;
            $directorio->descripcion = $request->descripcion;
            $directorio->id_user=auth()->user()->id;
            $directorio->save();
       
            return redirect()->route('directorio');
        }catch(\Exception $e){
            return redirect()->route('directorio');
    }
    }

    public function eliminarDirectorio($id){
        try {
        $directorio = Directorio::find($id);
        unlink(public_path('imagesDirectorio/'.$directorio->imagen));
        $event= Directorio::destroy($id);
        if($event>0){
         return response()->json([
             'success' => '1'
         ]);
        }else{
         return response()->json([
             'success' => '0'
         ]);
        }
    }catch(\Exception $e){
        return redirect()->route('directorio');
}
    }

    public function actualizarDirectorio(Request $request,$id){

        $directorio=Directorio::find($id);
        if($archivo=$request->file('imagen')){
            $imag=$archivo->getClientOriginalName();
            $archivo->move('imagesDirectorio',$imag);
            $directorio->imagen=$imag;
         }
            $directorio->id_user=auth()->user()->id;
            $directorio->nombre = $request->nombre;
            $directorio->descripcion = $request->descripcion;
            $directorio->save();
       
            return redirect()->route('directorio');
    }

    public function carrusel_directorio(){
        $dir=Directorio::all();
        return view('carrusel_directorio',["directorio"=>$dir]);
    }

//slide
public function slide(){
    $slides=Slide::all();
    $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
    $userId = Auth::id();
    $allUser = User::all();
$notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');

    return view('admin.slide',["slides"=>$slides,"instituciones"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion]);
}
public function registrarSlide(Request $request){
    try{
    $slide=new Slide;
    if($archivo=$request->file('imagen')){
       
       $nombre=$archivo->getClientOriginalName();
       $archivo->move('imagesSlide',$nombre);
       $slide->ruta=$nombre;
    }
      
       $slide->url = $request->link;
       $slide->titulo = $request->titulo;
       $slide->id_user=auth()->user()->id;
       $slide->save();


       return redirect()->route('slide');
    }catch(\Exception $e){
        return redirect()->route('slide');
}
   }
   public function deleteSlide($id){
    $slide = Slide::find($id);

    if($slide->ruta != null) {
        unlink(public_path('imagesSlide/'.$slide->ruta));
        $slide= Slide::destroy($id);
        if($slide>0){
         return 1;
        }else{
         return 0;
        }
    } else {
       

        return 2;
    }
  
  
 }


 //noticias
 public function noticias(){
    $noticias=Noticias::orderBy('id','DESC')->paginate(1);
    $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
    $userId = Auth::id();
    $allUser = User::all();
$notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');

    return view('admin.noticias',["instituciones"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('noticias'));
}

public function registrarNoticia(Request $request){
    $now = new \DateTime();
   
    if($archivo=$request->file('imagen')){
       $noticia=new Noticias;
       $nombre=$archivo->getClientOriginalName();
       $archivo->move('imagesNoticias',$nombre);
       $noticia->ruta=$nombre;
    }

       $noticia->titulo = $request->titulo;
       $noticia->contenido = $request->contenido;
       $noticia->id_user=auth()->user()->id;
       $noticia->fecha = $now->format('d-m-Y H:i:s');
       $noticia->save();


       return redirect()->route('noticias');
   }
   public function deleteNoticia($id){
    $noticia = Noticias::find($id);
    unlink(public_path('imagesNoticias/'.$noticia->ruta));
    $noti= Noticias::destroy($id);
    if($noti>0){
     return 1;
    }else{
     return 0;
    }
  
 }
 public function updateNoticia(Request $request, $id){
       
    $noticia = Noticias::find($id);
    if($archivo=$request->file('imagen')){
        
        unlink(public_path('imagesNoticias/'.$noticia->ruta));
        $nombre=$archivo->getClientOriginalName();
        $archivo->move('imagesNoticias',$nombre);
        $noticia->ruta=$nombre;
     }
        $noticia->id_user=auth()->user()->id;
        $noticia->titulo = $request->titulo;
        $noticia->contenido = $request->contenido;
        $noticia->save();


        return redirect()->route('noticias');
}

public function noticiasPaginadas(){
       $noticias = Noticias::orderBy('id','DESC')->paginate(3);
        return view('noticias',compact('noticias'));
}

public function noticiasInfo($id){
    $noticia = Noticias::find($id);
    return view('noticias_Info',["noticia"=>$noticia]);
}
public function eventoInfo($id){
    $noticia = Evento::find($id);
    $u=User::find($noticia->id_user);
    $ins=null;
    if($u->id_institucion!=null){
        $ins=$u->institucio->nombre_insti;
    }
    
    return view('evento_info',["noticia"=>$noticia,"ins"=>$ins]);
}

public function contactenos(Request $request){
       $contactenos=new Contactenos;
    //   dd($request->institucion);
if ($request->institucion=="Alcaldia de Sardinata") {
   $ins=null;
}else{
    $ins=$request->institucion;
}
       $contactenos->nombre = $request->nombre;
       $contactenos->correo = $request->correo;
       $contactenos->institucion= $ins;
       $contactenos->mensaje= $request->mensaje;
       $contactenos->save();


       return redirect()->route('ini');
   }



   public function historialMensaje($id){
    $userId = Auth::id();
    $historial=DB::select('SELECT * from mensajes m where m.id_user_mensaje='.$id.' and m.id_user='.$userId.' ');
    $historial2=DB::select('SELECT * from mensajes m  where m.id_user_mensaje='.$userId.' and m.id_user='.$id.' ');
    for($i=0;$i<sizeof($historial2);$i++){
      
        if($historial2[$i]->leido==0){
            $mensaje = Mensajes::find($historial2[$i]->id);
            $mensaje->leido = 1;
            $mensaje->save();
        }
    }
    $data = [
        "status" => "200",
        "historial" => $historial,
        "historial2" => $historial2,
    ];
    
    return response()->json($data);

   }
   public function enviarMensaje($id,$mensaje){
  
        $userId = Auth::id();
       
            $mensajess=new \App\Mensajes;
        
            $mensajess->id_user = $userId;
            $mensajess->leido = 0;
            $mensajess->mensaje = $mensaje;
            $mensajess->id_user_mensaje = $id;
            
            $mensajess->save();
            $data = [
                "status" => "200",
                
            ];
            
            return response()->json($data);
   
   }



}
