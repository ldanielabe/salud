<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\Estudiante;
use Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EstudiantesImport;
use App\User;

class FormulariosFichaController extends Controller
{

    public function datagrid_rel($id_estudiante,$dominio)
    {
        try {
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('respuestas');
            $crud->setRelation('id_preguntas', 'preguntas', '{pregunta}', ['id']);
            $crud->setRelation('id_estudiante', 'estudiantes', '{nombre}', ['id']);
            

            $columna=DB::select(DB::raw('SELECT * FROM preguntas AS prg WHERE id_dominio='.$dominio));
            $contador=DB::select(DB::raw('SELECT COUNT(prg.id) AS num FROM preguntas AS prg'));
            $valores = [];
          
            foreach ($columna as $variable) {
                $valores[$variable->id]= $variable->pregunta;
            }
            array_push($valores, "id_estudiante");
            $nombre_dominio=DB::select(DB::raw('SELECT d.nombre FROM preguntas AS prg, dominios AS d WHERE prg.id_dominio=d.id AND d.id='.$dominio.' LIMIT 1'));
            $nombre_dominio=$nombre_dominio[0]->nombre;

            $crud->setSubject($nombre_dominio, 'Agrega nuevos datos del dominio '.$nombre_dominio.', actualiza su informacion y elimina');
            
            
            $crud->columns($valores);
            $crud->editFields($valores);
            $crud->addFields($valores);
            
            //$crud->unsetDelete(); //Elimine btnDelete

            $crud->fieldType('id_estudiante', 'dropdown_search', $id_estudiante);
            
            $output = $crud->render();
        } catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
        }
        $nombres = DB::select('SELECT nombre,apellidos FROM estudiantes WHERE id = '.$id_estudiante);
        $nombre=$nombres[0]->nombre." ".$nombres[0]->apellidos;
        
        return $this->_show_output($output, $nombre);
    }
        
    public function datagrid($id_estudiante,$dominio)
    {
        try {
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('respuestas');

            $columna=DB::select(DB::raw('SELECT * FROM preguntas AS prg WHERE id_dominio='.$dominio));
            $contador=DB::select(DB::raw('SELECT COUNT(prg.id) AS num FROM preguntas AS prg'));
            $valores = [];
          
            foreach ($columna as $variable) {
                $valores[$variable->id]= $variable->pregunta;
            }
            array_push($valores, "id_estudiante");
            $nombre_dominio=DB::select(DB::raw('SELECT d.nombre FROM preguntas AS prg, dominios AS d WHERE prg.id_dominio=d.id AND d.id='.$dominio.' LIMIT 1'));
            $nombre_dominio=$nombre_dominio[0]->nombre;

            $crud->setSubject($nombre_dominio, 'Agrega nuevos datos del dominio '.$nombre_dominio.', actualiza su informacion y elimina');
            
            
            $crud->columns($valores);
            $crud->editFields($valores);
            $crud->addFields($valores);
          
            /*FOR************************************************ */
            foreach ($columna as $preguntas) {
                $rta = DB::select(DB::raw('SELECT * FROM respuestas_select_dinamico AS rta, preguntas AS prg WHERE rta.id_preguntas=prg.id AND prg.id='.$preguntas->id));
                
                if ($preguntas->tipo_input=="select") {
                   
                    $valores = [];
                    $i=1;
                    foreach ($rta as $variable) {
                        $valores[$i] = $variable->respuesta;
                        $i++;
                    }
                    
                    $crud->fieldType($preguntas->pregunta, 'dropdown_search', $valores);
                }elseif ($preguntas->tipo_input=="multiselect") {
                       
                    $valores = [];
                    $i=1; 
                    foreach ($rta as $variable) {
                        $valores[$i] = $variable->respuesta;
                        $i++;
                    }
                    
                    $crud->fieldType($preguntas->pregunta, 'multiselect_searchable', $valores);
                } elseif ($preguntas->tipo_input=="si_no") {
                   
                    $crud->fieldType($preguntas->pregunta, 'dropdown_search', [
                        ''=>'',
                        'Si'=>'Si',
                        'No'=>'No',
                    ]);
                }elseif($preguntas->tipo_input=="adecuado_inadecuado"){
                    $crud->fieldType($preguntas->pregunta, 'dropdown_search', [
                        ''=>'',
                        'Adecuado'=>'Adecuado',
                        'Inadecuado'=>'Inadecuado',
                    ]);
                }
            }
       
            $this->id_estudiante=$id_estudiante;
            $crud->callbackBeforeInsert(function ($stateParameters) {
                $stateParameters->data['id_estudiante']=$this->id_estudiante;
                return $stateParameters;
            });

            $estudiante = DB::select('SELECT * FROM estudiantes ');
       
     
            $val = [];
            foreach ($estudiante as $var) {
                $val[$var->id] = $var->antecedente." - ".$var->tipo;
            }
            
            $crud->fieldType('id_estudiante', 'dropdown_search',$val);
           
  
            
            $output = $crud->render();
        } catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
            $output = (object)[
                'isJSONResponse' => true,
                'output' => json_encode(
                    (object)[
                        'message' => $e->getMessage(),
                        'status' => 'failure'
                    ]
                )
            ];
        }
    
        return $this->_show_output($output, $nombre_dominio);
    }

    private function _show_output($output, $nombre_dominio)
    {
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                      ->header('Content-Type', 'application/json')
                      ->header('charset', 'utf-8');
        }
          
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        
    
        return view('grocery.crud', [
        'output' => $output,
        'css_files' => $css_files,
        'js_files' => $js_files,
        "allUser"=>$allUser,
        "notificacion"=>$notificacion,
        'titulo'=>$nombre_dominio,
        'instituciones'=>$instituciones,
        'layout_grocery'=>'layouts.admin_grocery',

            ]);
    }
   
    /******************************************************************************** */

             
    private function _getGroceryCrudEnterprise()
    {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }
    
        
    private function _getDatabaseConnection()
    {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);
    
        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }
    

         
   
}
