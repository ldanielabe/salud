<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\User;
use App\FichaSalud;
use App\Estudiante;
use Auth;

class FichaSaludController extends Controller
{
    public function datagrid_rel($id) {
    try { 
        $id=decrypt($id);
        
        $crud = $this->_getGroceryCrudEnterprise();
        $crud->setTheme('Bootstrap');
        $crud->setTable('ficha_saluds');
        $crud->setSubject('Antecedentes de Salud', 'Agrega la antecedentes de salud del estudiante, actualiza su informacion');
        $crud->setPrimaryKey('id', 'ficha_saluds');
       
        $crud->setRelation('id_antecedentes_tipo', 'antecedentes_tipos','tipo', ['id']);
        $crud->setRelation('id_estudiante', 'estudiantes','{nombre}', ['id']);
        $crud->where([
            'id_estudiante ='.$id
        ]);
        $crud->columns(array('id_antecedentes_tipo','detalle','estado','id_estudiante'));
        $crud->editFields(['id_antecedentes_tipo','detalle','estado','id_estudiante']);
        $crud->addFields(['id_antecedentes_tipo','detalle','estado','id_estudiante']);
        $crud->requiredFields(['id_antecedentes_tipo','detalle','estado']);
      
        $crud->unsetDelete(); //Elimine btnDelete
       
        $crud->displayAs('id_antecedentes_tipo', 'Antecedentes');
        $crud->displayAs('estado', 'Permitir visualizar a otros usuarios');
        $crud->fieldType('estado','checkbox_boolean');
        $antecedente = DB::table('antecedentes_tipos')->get();

        $valores = [];
        foreach ($antecedente as $var) {
            $valores[$var->id] = [$var->tipo];
        }
        
        $crud->fieldType('id_antecedentes_tipo', 'dropdown_search',$valores);
        $crud->fieldType('id_estudiante', 'dropdown_search',$id);
     

        $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
    }

    return $this->_show_output($output,$id);     
    }
    
    public function datagrid($id)
    {
     try { 
        $id=decrypt($id);
        $inst=Estudiante::find($id)->id_institucion;
              if(auth()->user()->id_rol != 1 && auth()->user()->id_institucion != $inst){
              return redirect()->route('perfil');
              }
        $crud = $this->_getGroceryCrudEnterprise();
        $crud->setTheme('Bootstrap');
        $crud->setTable('ficha_saluds');
       // $crud->setSkin('bootstrap-v4');
        //$crud->setThemePath('/my/custom/theme/path/')
        $crud->setSubject('Antecedentes de Salud', 'Agrega los antecedentes de salud del estudiante, actualiza su informacion');
         $crud->setPrimaryKey('id', 'ficha_saluds');
        $crud->where([
            'id_estudiante ='.$id
        ]);
        $crud->columns(array('id_antecedentes_tipo','detalle','estado'));
        $crud->editFields(['id_antecedentes_tipo','detalle','estado']);
        $crud->addFields(['id_antecedentes_tipo','detalle','estado']);
        $crud->requiredFields(['id_antecedentes_tipo','detalle','estado']);
      
        $crud->unsetDelete(); //Elimine btnDelete
        
        $crud->displayAs('id_antecedentes_tipo', 'Antecedentes');  
        $crud->displayAs('estado', 'Permitir visualizar a otros usuarios'); 
        $crud->fieldType('detalle','text');
        $crud->fieldType('estado','checkbox_boolean');
        $antecedente = DB::select('SELECT antipos.id, antecedente, tipo FROM antecedentes_tipos as antipos 
                                   INNER JOIN antecedentes as ant  ON antipos.id_antecedentes = ant.id');
       
        $val = [];
        foreach ($antecedente as $var) {
            $val[$var->id] = $var->antecedente." - ".$var->tipo;
        
        }
        
        $crud->fieldType('id_antecedentes_tipo', 'dropdown_search',$val);
       
      
        $this->id_estudiante=$id;
        $crud->callbackBeforeInsert(function($stateParameters){
            $stateParameters->data['id_estudiante']=$this->id_estudiante;
            return $stateParameters;
         });

        $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
    }

    return $this->_show_output($output,$id);     
    }
   

        private function _getGroceryCrudEnterprise() {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }

    
    private function _getDatabaseConnection() {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);

        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }

    private function _show_output($output,$id) {
        
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                  ->header('Content-Type', 'application/json')
                  ->header('charset', 'utf-8');
        }
      
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $id=$id;
        $nombres = DB::select('SELECT nombre,apellidos FROM estudiantes WHERE id = '.$id);
        $nombre=$nombres[0]->nombre." ".$nombres[0]->apellidos;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $layout_grocery="";
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin_grocery';
        }else if(auth()->user()->id_rol==2){
            $layout_grocery='layouts.profesor_grocery';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector_grocery';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador_grocery';
        } else if(auth()->user()->id_rol==5){
            $layout_grocery='layouts.autoridad_grocery';
        }else if(auth()->user()->id_rol==6){
            $layout_grocery='layouts.acudiente_grocery';
        }else if(auth()->user()->id_rol==7){
            $layout_grocery='layouts.admin2_grocery';
        }
        return view('grocery.crud', [
            'output' => $output,
            'css_files' => $css_files,
            'js_files' => $js_files,
            'id'=>$id,
            'nombre'=>$nombre,
            "instituciones"=>$instituciones,
            "allUser"=>$allUser,
            "notificacion"=>$notificacion,
            'titulo'=>'Antecedentes de salud',
            'layout_grocery' => $layout_grocery,
        ]);
    }
}
