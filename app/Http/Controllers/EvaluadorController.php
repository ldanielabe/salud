<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class EvaluadorController extends Controller
{
       
    public function datagrid_rel($id){
        try { 
            $id=decrypt($id);
            if(auth()->user6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666()->id_rol!=1 &&  auth()->user()->id_institucion!=$id){
                return redirect()->route('perfil');
            }
                $crud = $this->_getGroceryCrudEnterprise();
                $crud->setTheme('Bootstrap');
                $crud->setTable('users');
                $crud->setSubject('user', 'Agrega nuevos Evaluadores, actualiza su informacion y elimina');
                $crud->setPrimaryKey('id', 'users');
                $crud->where([
                    'id_rol = 4',
                    'id_institucion = '.$id
                ]);
                $crud->setRelation('id_institucion', 'institucions','nombre_insti', ['id']);
                $crud->setRelation('id_rol', 'rols','{descripcion}', ['id']);
                
                $crud->columns(array('nombre','apellidos','email','documento','celular','id_rol','id_institucion' ));
                $crud->addFields(['nombre','apellidos','email','documento','celular','id_rol','id_institucion','password']);
                $crud->uniqueFields(['documento','email']);
                $crud->editFields(['nombre','apellidos','email','documento','celular','id_rol','id_institucion','password']);
                $crud->requiredFields(['nombre','apellidos','email','documento','celular','id_institucion','password']);
        
                
                $crud->unsetDelete(); //Elimine btnDelete
                $crud->displayAs('password', 'Contraseña');
                $crud->fieldType('email', 'email');
                $crud->fieldType('password', 'password');
        
                $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                $output = (object)[
                    'isJSONResponse' => true,
                    'output' => json_encode(
                        (object)[
                            'message' => $e->getMessage(),
                            'status' => 'failure'
                        ]
                    )
                ];
        }
        
    return $this->_show_output($output);     
    }
            
    public function datagrid($id){
        try { 
            $id=decrypt($id);
            if(auth()->user()->id_rol!=1 &&  auth()->user()->id_institucion!=$id){
                return redirect()->route('perfil');
            }
                $crud = $this->_getGroceryCrudEnterprise();
                $crud->setTheme('Bootstrap');
                $crud->setTable('users');
                $crud->setSubject('user', 'Agrega nuevos Evaluadores, actualiza su informacion y elimina');
                $crud->setPrimaryKey('id', 'user');
                $crud->where([
                    'id_rol = 4',
                    'id_institucion = '.$id
                ]);
         
              
                $crud->columns(array('nombre','apellidos','email','documento','celular' ));
                $crud->addFields(['nombre','apellidos','email','documento','celular','password']);
                $crud->uniqueFields(['documento','email']);
                $crud->editFields(['nombre','apellidos','email','documento','celular','password']);
                $crud->requiredFields(['nombre','apellidos','email','documento','celular','password']);
        
                
                 $crud->unsetDelete(); //Elimine btnDelete
            
                 $crud->displayAs('password', 'Contraseña');
                 $crud->fieldType('email', 'email');
                 $crud->fieldType('password', 'password');
        
        
                 $crud->callbackAddField('documento', function () {
                    return '<input class="form-control" id="documento" name="documento" class="numeric" minlength="8" maxlength="10" onkeypress="return validaNumericos(event)" required>';
                });
            
                $crud->callbackEditField('documento', function ($fieldValue) {
                    return '<input  class="form-control" id="documento" name="documento"  class="numeric" minlength="8" maxlength="10" value='.$fieldValue.' onkeypress="return validaNumericos(event)" required>';
                });
            
                $crud->callbackAddField('celular', function () {
                    return '<input class="form-control" id="celular" name="celular" class="numeric" minlength="7" maxlength="10" onkeypress="return validaNumericos(event)" required>';
                });
            
                $crud->callbackEditField('celular', function ($fieldValue) {
                    return '<input  class="form-control" id="celular" name="celular"  class="numeric" minlength="7" maxlength="10" value='.$fieldValue.' onkeypress="return validaNumericos(event)" required>';
                });

                $crud->callbackEditField('password', function ($fieldValue) {
                    return '<input  class="form-control" id="password" name="password" type="password" class="password"  value="" required>';
                });
        
        
                $this->num=4;
                $this->id=$id;
                $crud->callbackBeforeInsert(function($stateParameters){
                    $stateParameters->data['password']=Hash::make($stateParameters->data['password']);
                    $stateParameters->data['id_rol']=$this->num;
                    $stateParameters->data['id_institucion']=$this->id;
                    return $stateParameters;
                 });

                $crud->callbackBeforeUpdate(function($stateParameters){
                    $stateParameters->data['password']=Hash::make($stateParameters->data['password']);
                    $stateParameters->data['id_rol']=$this->num;
                    $stateParameters->data['id_institucion']=$this->id;
                    return $stateParameters;
                 });
                 
                $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                $output = (object)[
                    'isJSONResponse' => true,
                    'output' => json_encode(
                        (object)[
                            'message' => $e->getMessage(),
                            'status' => 'failure'
                        ]
                    )
                ];
        }
        
    return $this->_show_output($output);     
    }

    private function _getGroceryCrudEnterprise() {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }

    
    private function _getDatabaseConnection() {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);

        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }

    private function _show_output($output) {
        
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                  ->header('Content-Type', 'application/json')
                  ->header('charset', 'utf-8');
        }
      
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    $layout_grocery="";
    if(auth()->user()->id_rol==1){
        $layout_grocery='layouts.admin_grocery';
    }else if(auth()->user()->id_rol==2){
        $layout_grocery='layouts.profesor_grocery';
    }else if(auth()->user()->id_rol==3){
        $layout_grocery='layouts.rector_grocery';
    }else if(auth()->user()->id_rol==4){
        $layout_grocery='layouts.evaluador_grocery';
    } else if(auth()->user()->id_rol==5){
        $layout_grocery='layouts.autoridad_grocery';
    }else if(auth()->user()->id_rol==6){
        $layout_grocery='layouts.acudiente_grocery';
    }else if(auth()->user()->id_rol==7){
        $layout_grocery='layouts.admin2_grocery';
    }

    return view('grocery.crud', [
        'output' => $output,
        'css_files' => $css_files,
        'js_files' => $js_files,
        'titulo'=>'Evaluadores',
        'layout_grocery' => $layout_grocery,
        "instituciones"=>$instituciones,
        "allUser"=>$allUser,
        "notificacion"=>$notificacion
    ]);
  
      
    

    }

}
