<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Estudiante;
use Auth;
use Illuminate\Contracts\Encryption\DecryptException;


class ObservacionesController extends Controller{
    public function datagrid_rel($id)
    {
   
    try { 
        $id=decrypt($id);
        
        $crud = $this->_getGroceryCrudEnterprise();
        $crud->setTheme('Bootstrap');
        $crud->setTable('observaciones');
       // $crud->setSkin('bootstrap-v4');
        //$crud->setThemePath('/my/custom/theme/path/')
        $crud->setSubject('Observacion', 'Agrega las observaciones del estudiante, actualiza su informacion y elimina');
        $crud->setPrimaryKey('id', 'observaciones');
       
       
        $crud->setRelation('id_estudiante', 'estudiantes','{nombre}', ['id']);
        $crud->where([
            'id_estudiante ='.$id
        ]);
        $crud->columns(array('tipo_falta','detalles','privado','id_estudiante'));
       
        $crud->editFields(['tipo_falta','detalles','privado','id_estudiante']);
        $crud->addFields(['tipo_falta','detalles','privado','id_estudiante']);
        $crud->requiredFields(['tipo_falta','detalles','privado','id_estudiante']);
       
        $crud->unsetDelete(); //Elimine btnDelete
       
        $crud->fieldType('id_estudiante', 'dropdown_search',$id);

        $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
       
    } 

    return $this->_show_output($output,$id);     
    }
    
    public function datagrid($id)
    { 
        
     try { 
        $id=decrypt($id);
   
       $inst=Estudiante::find($id)->id_institucion;
        if(auth()->user()->id_rol != 1 && auth()->user()->id_institucion != $inst){
        return redirect()->route('perfil');
        } 
        $crud = $this->_getGroceryCrudEnterprise();
        $crud->setTheme('Bootstrap');
        $crud->setTable('observaciones');
       // $crud->setSkin('bootstrap-v4');
        //$crud->setThemePath('/my/custom/theme/path/')
        $crud->setSubject('Ficha', 'Agrega las observaciones del estudiante, actualiza su informacion y elimina');
        $crud->setPrimaryKey('id', 'observaciones');
        $crud->where([
            'id_estudiante ='.$id
        ]);
        $crud->columns(array('tipo_falta','detalles','privado'));
        
        $crud->editFields(['tipo_falta','detalles','privado']);
        $crud->addFields(['tipo_falta','detalles','privado']);
        $crud->requiredFields(['tipo_falta','detalles','privado']);
        if(auth()->user()->id_rol==6){
            $crud->unsetAdd();
            $crud->unsetEdit();
        }
      
        $crud->unsetDelete(); //Elimine btnDelete
        $crud->fieldType('detalle','text'); 

        $crud->fieldType('tipo_falta', 'dropdown_search',[
            ''=>'',
            'Matoneo'=>'Matoneo',
            'Comportamiento'=>'Comportamiento',
            'DisfunciÃ³n familiar'=>'DisfunciÃ³n familiar',
            'Bajo desempeÃ±o escolar '=>'Bajo desempeÃ±o escolar ',
            'Bullying'=>'Bullying',
            'Baja autoestima'=>'Baja autoestima',
            'Relaciones familiares conflictivas'=>'Relaciones familiares conflictivas',
            'Identidad de orientaciÃ³n sexual '=>'Identidad de orientaciÃ³n sexual',
            'Consumo de sustancias'=>'Consumo de sustancias',
            'Otro'=>'Otro'
        ]);
        
        $crud->fieldType('privado','checkbox_boolean');
        $this->id_estudiante=$id;
        $crud->callbackBeforeInsert(function($stateParameters){
            $stateParameters->data['id_estudiante']=$this->id_estudiante;
            return $stateParameters;
         });

        $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
          
    }

    return $this->_show_output($output,$id);     
    }
   

        private function _getGroceryCrudEnterprise() {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }

    
    private function _getDatabaseConnection() {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);

        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }

    private function _show_output($output,$id) {
      try{

 
        if(auth()->user()->id_rol==6 && auth()->user()->id_estudiante!=$id){
            return redirect()->route('acudiente/perfil');
        }
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                  ->header('Content-Type', 'application/json')
                  ->header('charset', 'utf-8');
        }
      
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $nombres = DB::select('SELECT nombre,apellidos FROM estudiantes WHERE id = '.$id);
        $nombre=$nombres[0]->nombre." ".$nombres[0]->apellidos;
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    }catch(\Exception $e){
        return redirect()->route('perfil');
    } 
        if(auth()->user()->id_rol==1){
            $layout_grocery='layouts.admin_grocery';
        }else if(auth()->user()->id_rol==2){
            $layout_grocery='layouts.profesor_grocery';
        }else if(auth()->user()->id_rol==3){
            $layout_grocery='layouts.rector_grocery';
        }else if(auth()->user()->id_rol==4){
            $layout_grocery='layouts.evaluador_grocery';
        } else if(auth()->user()->id_rol==5){
            $layout_grocery='layouts.autoridad_grocery';
        }else if(auth()->user()->id_rol==6){
            $layout_grocery='layouts.acudiente_grocery';
        }else if(auth()->user()->id_rol==7){
            $layout_grocery='layouts.admin2_grocery';
        }
        return view('grocery.crud', [
            'output' => $output,
            'css_files' => $css_files,
            'js_files' => $js_files,
            'nombre'=>$nombre,
            'titulo'=>"Observaciones - ". $nombre,
            'layout_grocery'=>$layout_grocery,
            "instituciones"=>$instituciones,
            "allUser"=>$allUser,
            "notificacion"=>$notificacion
        ]);
    } 
}