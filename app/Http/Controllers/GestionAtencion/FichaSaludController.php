<?php

namespace App\Http\Controllers\GestionAtencion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use GroceryCrud\Core\GroceryCrud;

class FichaSaludController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header("Content-Type: text/html;charset=utf-8");
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $dominios=DB::select('SELECT * from dominios');
       return view('admin.addFicha',["dominios"=>$dominios,"instituciones"=>$instituciones,"allUser"=>$allUser,"notificacion"=>$notificacion]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }
    public function guardar($id,$preguntas,$tipo_input,$respuestas,$riesgo,$id_pregunta)
    {
      
        $data = [
            "status" => "500",
        ];

        if($preguntas!="1"&&$tipo_input!="1"){
            DB::table('preguntas')->insert([
                'pregunta'=>urldecode($preguntas),
                'tipo_input'=>$tipo_input,
                'id_dominio'=>($id+1)
            ]);
            $data = [
                "status" => "200",
            ];
        }
        if($respuestas!="1"){
           DB::table('respuestas_select_dinamico')->insert([
                'respuesta'=>$respuestas,
                'riesgo'=>$riesgo,
                'id_preguntas'=>$id_pregunta
            ]);
            $data = [
                "status" => "200",
            ];
        }
       
        return response()->json($data);
    }

    public function all($dominio,$pregunta)
    {
        
        $dominio=$dominio+1;
        $preguntas=DB::select('SELECT * FROM preguntas AS prg WHERE prg.id_dominio='.$dominio);
        $respuestas=DB::select('SELECT * FROM preguntas AS prg, respuestas_select_dinamico AS rta WHERE prg.id_dominio='.$dominio.' AND rta.id_preguntas=prg.id AND prg.id='.$pregunta);
      
     
        $data = [
            "status" => "200",
            "preguntas" => $preguntas,
            "respuestas" => $respuestas,
          
        ];
        
        return response()->json($data);
    }

  
    public function editarPreguntas($id,$preguntas){
        $data = ["status" => "500"];

        if($preguntas!=""||$preguntas!=null){

            DB::table('preguntas')->where('id',$id)->update(['pregunta'=>urldecode($preguntas)]);
           
            $data = [
                "status" => "200",
            ];
        }
      
       
        return response()->json($data);
    }

    public function editarRespuestas($respuestas,$riesgo,$id_rta){
        $data = ["status" => "500"];
         if($respuestas!=""||$respuestas!=null){
           DB::table('respuestas_select_dinamico')->where('id',$id_rta)->update([
                'respuesta'=>$respuestas,
                'riesgo'=>$riesgo
            ]);
            $data = [
                "status" => "200",
            ];
        }
       
        return response()->json($data);
    }




    public function datagrid_rel(){
        try { 
             
                $crud = $this->_getGroceryCrudEnterprise();
                $crud->setTheme('Bootstrap');
                $crud->setTable('dominios');
                $crud->setSubject('dominio', 'Agrega nuevos Dominios, actualiza su informacion y elimina');
                
                $crud->columns(array('nombre','descripcion','estado'));
                $crud->editFields(['nombre','descripcion','estado']);
                $crud->addFields(['nombre','descripcion','estado']);
                $crud->requiredFields(['nombre']);
        
                $crud->uniqueFields(['nombre']);
                $crud->unsetDelete(); //Elimine btnDelete
              
        
                $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                $output = (object)[
                    'isJSONResponse' => true,
                    'output' => json_encode(
                        (object)[
                            'message' => $e->getMessage(),
                            'status' => 'failure'
                        ]
                    )
                ];
        }
        
    return $this->_show_output($output);     
    }
            
    public function datagrid(){
        try { 
              
                $crud = $this->_getGroceryCrudEnterprise();
                $crud->setTheme('Bootstrap');
                $crud->setTable('dominios');
                $crud->setSubject('dominio', 'Agrega nuevos Dominios, actualiza su informacion y elimina');
                
                $crud->columns(array('nombre','descripcion','estado'));
                $crud->editFields(['nombre','descripcion','estado']);
                $crud->addFields(['nombre','descripcion','estado']);
                $crud->requiredFields(['nombre']);
        
                $crud->uniqueFields(['nombre']);
                $crud->unsetDelete(); //Elimine btnDelete
        
                $crud->fieldType("estado", 'dropdown_search', [
                    'Activo'=>'Activo',
                    'Inactivo'=>'Inactivo',
                ]);
                $output = $crud->render();
        }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                $output = (object)[
                    'isJSONResponse' => true,
                    'output' => json_encode(
                        (object)[
                            'message' => $e->getMessage(),
                            'status' => 'failure'
                        ]
                    )
                ];
        }
        
    return $this->_show_output($output);     
    }

    private function _getGroceryCrudEnterprise() {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }

    
    private function _getDatabaseConnection() {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);

        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }

    private function _show_output($output) {
        
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                  ->header('Content-Type', 'application/json')
                  ->header('charset', 'utf-8');
        }
      
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    $layout_grocery="";
    if(auth()->user()->id_rol==1){
        $layout_grocery='layouts.admin_grocery';
    }else if(auth()->user()->id_rol==2){
        $layout_grocery='layouts.profesor_grocery';
    }else if(auth()->user()->id_rol==3){
        $layout_grocery='layouts.rector_grocery';
    }else if(auth()->user()->id_rol==4){
        $layout_grocery='layouts.evaluador_grocery';
    } else if(auth()->user()->id_rol==5){
        $layout_grocery='layouts.autoridad_grocery';
    }else if(auth()->user()->id_rol==6){
        $layout_grocery='layouts.acudiente_grocery';
    }else if(auth()->user()->id_rol==7){
        $layout_grocery='layouts.admin2_grocery';
    }

    return view('grocery.crud', [
        'output' => $output,
        'css_files' => $css_files,
        'js_files' => $js_files,
        'titulo'=>'Dominios',
        'layout_grocery' => $layout_grocery,
        "instituciones"=>$instituciones,
        "allUser"=>$allUser,
        "notificacion"=>$notificacion
    ]);
    }
}
