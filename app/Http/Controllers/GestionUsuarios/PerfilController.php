<?php

namespace App\Http\Controllers\GestionUsuarios;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use App\Evento;

use App\Directorio;
use App\Slide;
use App\Noticias;
use App\Visitas;
use App\Contactenos;
use App\Estudiante;
use App\Institucions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
class PerfilController extends Controller
{
    function __construct() {	
        session(['c' => '0']);
	}

    public function perfilAdmin(){
        $id=auth()->user()->id_institucion;
       
            $institucion = Institucions::find($id); 
        
            $userId = Auth::id();
            $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $c=session('c');
        $inst =DB::select(DB::raw('SELECT *  FROM institucions'));
        if(auth()->user()->id_rol==4){
            return view('evaluador.perfil',["allUser"=>$allUser,"notificacion"=>$notificacion]);
       
        }else if(auth()->user()->id_rol==5){
            $now = new \DateTime();
            $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
            $date = explode('-', $now->format('Y-m-d'), 3); 
            $mes = Visitas::whereYear('fecha', '=', $date[0])
            ->whereMonth('fecha', '=', $date[1])
            ->get()->sum('cantidad');
            $todo = Visitas::all()->sum('cantidad');
            $user = User::all()->count();
            $usuarios=User::offset(0)->limit(5)->get(); 
            $contactenos = Contactenos::orderBy('id','ASC')->paginate(1);
            $inst = Institucions::where('id',$id)->get();
            $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
            return view('autoridad.perfil',['instituciones'=>$instituciones,"hoy"=>$visit->cantidad,
            "mes"=>$mes,"todo"=>$todo,"user"=>$user,"usuarios"=>$usuarios,"contactenos"=>$contactenos,"inst"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion]);
            
       
        }
        else if(auth()->user()->id_rol==1){
            return view('admin.perfil',["instituciones"=>$instituciones,"allUser"=>$allUser,"notificacion"=>$notificacion]);
       
        }else if(auth()->user()->id_rol==2){
            return view('profesor.perfil',["instituciones"=>$instituciones,"allUser"=>$allUser,"notificacion"=>$notificacion]);
       
        }else if(auth()->user()->id_rol==3){
            return view('rector.perfil',["allUser"=>$allUser,"notificacion"=>$notificacion]);
             
        }else if(auth()->user()->id_rol==7){
            return view('administrador.perfil',["allUser"=>$allUser,"notificacion"=>$notificacion]);
             
       
        }
      
    }

    public function actualizarPerfil(Request $request){

        $userId = Auth::id();
        $user = User::find($userId);
        
        if($request->password==""){

            $user->nombre = $request->nombres;
            $user->apellidos = $request->apellidos;
            $user->email = $request->correo;
            $user->celular = $request->celular;
            $user->documento = $request->documento;
            $user->save();
        }else{
            $user->password = Hash::make($request->password);
            $user->nombre = $request->nombres;
            $user->apellidos = $request->apellidos;
            $user->email = $request->correo;
            $user->celular = $request->celular;
            $user->documento = $request->documento;
            $user->save();
        }
    //
        return redirect()->route('perfil');
    }

    public function rectorInicio(){
        $id=auth()->user()->institucio->id;
        $profesores=DB::select('SELECT count(*) as cantidad FROM users WHERE users.id_institucion='.$id.' and users.id_rol=2');
        $evaluadores=DB::select('SELECT count(*) as cantidad FROM users WHERE users.id_institucion='.$id.' and users.id_rol=4');
        $acudientes=DB::select('SELECT count(*) as cantidad FROM users WHERE users.id_institucion='.$id.' and users.id_rol=6');
        $estudiantes=DB::select('SELECT count(*) as cantidad FROM estudiantes WHERE estudiantes.id_institucion='.$id.'');
        $profesores=$profesores[0]->cantidad;
        $evaluadores=$evaluadores[0]->cantidad;
        $acudientes=$acudientes[0]->cantidad;
        $estudiantes=$estudiantes[0]->cantidad;
           $userId = Auth::id();
            $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
       
        return view('rector.inicio',["allUser"=>$allUser,"notificacion"=>$notificacion,"profesores"=>$profesores,"evaluadores"=>$evaluadores,"acudientes"=>$acudientes,"estudiantes"=>$estudiantes],compact('profesores','evaluadores','acudientes','estudiantes'));
       
   }

   public function profesorInicio(){
    $id=auth()->user()->institucio->id;
    $userId = Auth::id();
    $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    $acudientes=DB::select('SELECT count(*) as cantidad FROM users WHERE users.id_institucion='.$id.' and users.id_rol=6');
    $estudiantes=DB::select('SELECT count(*) as cantidad FROM estudiantes WHERE estudiantes.id_institucion='.$id.'');
   
    return view('profesor.perfil',["allUser"=>$allUser,"notificacion"=>$notificacion,"acudientes"=>$acudientes,"estudiantes"=>$estudiantes]);
       
   }

   public function perfilAcudiente(){
    $userId = Auth::id();
    $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
   
    return view('acudiente.perfil',["allUser"=>$allUser,"notificacion"=>$notificacion]);
      
   }



}