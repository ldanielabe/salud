<?php

namespace App\Http\Controllers\Auth;

use App\Estudiante;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255|unique:estudiantes',
            'documento' => 'required|string|max:20|unique:estudiantes',
            'celular' => 'required|string|max:10',
            'fechaNacimiento' => 'required|string|max:20',
            'direccion' => 'required|string|max:50',
            'barrio' => 'required|string|max:20',
            'institucion' => 'required|string|max:50',
            'grado' => 'required|string|max:2',
            'jornada' => 'required|string|max:20',
            'contrasena' => 'required|string|min:6|confirmed',
            'contrasena_confirm' => 'required|string|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return Estudiante::create([
            'nombre' => $data['nombre'],
            'correo' => $data['correo'],
            'documento' => $data['documento'],
            'celular' => $data['celular'],
            'fechaNacimiento' => $data['fechaNacimiento'],
            'direccion' => $data['direccion'],
            'barrio' => $data['barrio'],
            'institucion' => $data['institucion'],
            'grado' => $data['grado'],
            'jornada' => $data['jornada'],
            'contrasena' => Hash::make($data['contrasena']),
            'contrasena_confirm' => Hash::make($data['contrasena_confirm']),
        ]);
    }
}
