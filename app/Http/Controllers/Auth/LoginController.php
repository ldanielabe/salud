<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
  
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo;
public function redirectTo()
    {
        $userId = Auth::id();
        $userOnline = User::find($userId);
        $userOnline->online=1;
        $userOnline->save();
       
        switch(Auth::user()->id_rol){
            case 1:
                return '/admin';
                break;
            case 2:
                return '/profesor/dashboard';
                break;
            case 3:
                return '/rector/dashboard';
                break;
            case 4:
                $id=encrypt(auth()->user()->id_institucion);
                return '/antecedentesVista/'.$id;
                break;
            case 5:
                return '/autoridad/dashboard';
                break;
            case 6:
                return '/acudiente/perfil';
                break;
                case 7:
                    return '/administrador/dashboard';
                    break;
           /* case 7://Estudiante
                $this->redirectTo = '/home';
                return $t*/
            default:
                return '/login';
        }
         
        // return $next($request);
    } 
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout() {
       
        $userId = Auth::id();
        $userOnline = User::find($userId);
        $userOnline->online=0;
        $userOnline->save();
        Auth::logout();
        return redirect('/');
      }
}
