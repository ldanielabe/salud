<?php

namespace App\Http\Controllers\GestionPrevencion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Evento;
use App\User;
use App\Directorio;
use App\Slide;
use App\Noticias;
use App\Visitas;
use App\Contactenos;
use App\Estudiante;
use App\Institucions;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use PDF;

class prevencionController extends Controller
{
    public function inicioEvaluador($id){
        try {
        $id=decrypt($id);
        if($id==0){
            $id=auth()->user()->id_institucion;
            
        }
       
    $inst = Institucions::where('id',$id)->get();
    $userId = Auth::id();
    $allUser = User::all();
$notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');

    $datos= new prevencionController();
    $edad=$datos->edadInstitucion($id,"0");
    $grado=$datos->gradoInstitucion($id);
    $genero=$datos->generoInstitucion($id,"0");
    $regimen=$datos->regimenInstitucion($id,"0");
    $jornada=$datos->jornadaInstitucion($id,"0");
    $residencia=$datos->residenciaInstitucion($id,"0");
    $barrio=$datos->barrioInstitucion($id,"0");
    $patologicos=$datos->patologicosInstitucion($id);
    $quirurgicos=$datos->quirurgicosInstitucion($id);
    $traumaticos=$datos->traumaticosInstitucion($id);
    $alergicos=$datos->alergicosInstitucion($id);
}catch(\Exception $e){
    return redirect()->route('perfil');
}
    if(auth()->user()->id_rol==4 && auth()->user()->id_institucion==$id){
        return view('estadisticas-antecedentes',["layout"=>"layouts.evaluador","allUser"=>$allUser,"inst"=>$inst,"notificacion"=>$notificacion],compact('edad','grado','regimen','jornada','residencia','barrio','patologicos','quirurgicos','traumaticos','alergicos','genero','id'));
    }else if(auth()->user()->id_rol==5 && auth()->user()->id_institucion==$id){
        $now = new \DateTime();
        $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
        $date = explode('-', $now->format('Y-m-d'), 3); 
        $mes = Visitas::whereYear('fecha', '=', $date[0])
        ->whereMonth('fecha', '=', $date[1])
        ->get()->sum('cantidad');
        $todo = Visitas::all()->sum('cantidad');
        $user = User::all()->count();
        $usuarios=User::offset(0)->limit(5)->get(); 
        $contactenos = Contactenos::orderBy('id','ASC')->paginate(1);

        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        return view('estadisticas-antecedentes',['layout'=>'layouts.autoridad','instituciones'=>$instituciones,"hoy"=>$visit->cantidad,
        "mes"=>$mes,"todo"=>$todo,"user"=>$user,"usuarios"=>$usuarios,"contactenos"=>$contactenos,"inst"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('edad','grado','regimen','jornada','residencia','barrio','patologicos','quirurgicos','traumaticos','alergicos','genero','id'));
        
   
    }
    else if(auth()->user()->id_rol==1){
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
      
        return view('estadisticas-antecedentes',['layout'=>'layouts.admin',"instituciones"=>$instituciones,"inst"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('edad','grado','regimen','jornada','residencia','barrio','patologicos','quirurgicos','traumaticos','alergicos','genero','id'));
        
   
    }
    else if(auth()->user()->id_rol==3 && auth()->user()->id_institucion==$id){
        return view('estadisticas-antecedentes',['layout'=>'layouts.rector',"allUser"=>$allUser,"inst"=>$inst,"notificacion"=>$notificacion],compact('edad','grado','regimen','jornada','residencia','barrio','patologicos','quirurgicos','traumaticos','alergicos','genero','id'));
        
   
    }else{
        return redirect()->route('perfil');
    }
  }


  public function graficas($id){
    try {
        $id=decrypt($id);
    $inst = Institucions::where('id',$id)->get();
    $userId = Auth::id();
    $allUser = User::all();
    $dominios=DB::select('SELECT * from dominios');
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
}catch(\Exception $e){
    return redirect()->route('perfil');
}
    if(auth()->user()->id_rol==1){
        $layout_grocery='layouts.admin';
    }else if(auth()->user()->id_rol==2 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.profesor';
    }else if(auth()->user()->id_rol==3 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.rector';
    }else if(auth()->user()->id_rol==4 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.evaluador';
    } else if(auth()->user()->id_rol==5 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.autoridad';
    }else if(auth()->user()->id_rol==6 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.acudiente';
    }else if(auth()->user()->id_rol==7 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.admin2';
    }else{
        return redirect()->route('perfil');
    }


    return view('evaluador.graficas',["id"=>$id,"instituciones"=>$instituciones,"layout_grocery"=>$layout_grocery,"allUser"=>$allUser,"notificacion"=>$notificacion,"institucion"=>$inst,"dominios"=>$dominios],compact('id'));
    

  }


  public function generar_pdf_graficas(Request $request){

    $pdf=\PDF::loadHTML($request->contenedor);
    $pdf->loadHtml("Daniela data: <br>");
    $pdf->render();
    return $pdf->stream('estadisticas.pdf');
    
  }

  public function observador($id){
    try {
        $id=decrypt($id);
    $inst = Institucions::where('id',$id)->get();
    $userId = Auth::id();
    $allUser = User::all();
    $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
    
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
    $observaciones=DB::select(DB::raw('SELECT observaciones.tipo_falta, COUNT(*) as cantidad FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.'  GROUP BY observaciones.tipo_falta '));
    $total=DB::select(DB::raw('SELECT f.id_estudiante,MAX(f.created_at) fecha  FROM observaciones as f INNER JOIN estudiantes on estudiantes.id=f.id_estudiante and estudiantes.id_institucion='.$id.' GROUP BY f.id_estudiante '));
}catch(\Exception $e){
    return redirect()->route('perfil');
}  
    if(auth()->user()->id_rol==1 ){
        $layout_grocery='layouts.admin';
    }else if(auth()->user()->id_rol==2 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.profesor';
    }else if(auth()->user()->id_rol==3 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.rector';
    }else if(auth()->user()->id_rol==4 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.evaluador';
    } else if(auth()->user()->id_rol==5 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.autoridad';
    }else if(auth()->user()->id_rol==6 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.acudiente';
    }else if(auth()->user()->id_rol==7 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.admin2';
    }else{
        return redirect()->route('perfil');
    }



    return view('evaluador.observador',["instituciones"=>$instituciones,"layout_grocery"=>$layout_grocery,"allUser"=>$allUser,"notificacion"=>$notificacion,"institucion"=>$inst],compact('observaciones','total','id'));
    

  }
  public function observadorReporte($id){
    try {
        $id=decrypt($id);
    $inst = Institucions::where('id',$id)->get();
    $userId = Auth::id();
    $allUser = User::all();
    $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
    
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
}catch(\Exception $e){
    return redirect()->route('perfil');
} 
    if(auth()->user()->id_rol==1){
        $layout_grocery='layouts.admin';
    }else if(auth()->user()->id_rol==2 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.profesor';
    }else if(auth()->user()->id_rol==3 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.rector';
    }else if(auth()->user()->id_rol==4 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.evaluador';
    } else if(auth()->user()->id_rol==5 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.autoridad';
    }else if(auth()->user()->id_rol==6 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.acudiente';
    }else if(auth()->user()->id_rol==7 && auth()->user()->id_institucion==$id){
        $layout_grocery='layouts.admin2';
    }else{
        return redirect()->route('perfil');
    }


    return view('evaluador.reporteObservador',["instituciones"=>$instituciones,"layout_grocery"=>$layout_grocery,"allUser"=>$allUser,"notificacion"=>$notificacion,"institucion"=>$inst],compact('id'));
    

  }
  public function observadorAjax($id,$inicio,$fin,$grado,$estado){
    $inst = Institucions::where('id',$id)->get();
    if ($inicio=="0") {
        $observaciones=DB::select(DB::raw('SELECT observaciones.tipo_falta, COUNT(*) as cantidad FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.' and estudiantes.grado='.$grado.' and estudiantes.activo='.$estado.'  GROUP BY observaciones.tipo_falta '));
        $total=DB::select(DB::raw('SELECT f.id_estudiante,MAX(f.created_at) fecha  FROM observaciones as f INNER JOIN estudiantes on estudiantes.id=f.id_estudiante and estudiantes.id_institucion='.$id.' and estudiantes.grado='.$grado.' and estudiantes.activo='.$estado.' GROUP BY f.id_estudiante '));
      
    }else{
     
        $observaciones=DB::select(DB::raw('SELECT observaciones.tipo_falta, COUNT(*) as cantidad FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.' and estudiantes.grado='.$grado.' and estudiantes.activo='.$estado.' and observaciones.created_at>="'.$inicio.'" and observaciones.created_at<="'.$fin.'"  GROUP BY observaciones.tipo_falta '));
        $total=DB::select(DB::raw('SELECT f.id_estudiante,MAX(f.created_at) fecha  FROM observaciones as f INNER JOIN estudiantes on estudiantes.id=f.id_estudiante and estudiantes.id_institucion='.$id.' and estudiantes.grado='.$grado.' and estudiantes.activo='.$estado.' and f.created_at>="'.$inicio.'" and f.created_at<="'.$fin.'" GROUP BY f.id_estudiante '));
      
    }
   
    $data = [
        "status" => "200",
        "observaciones" => $observaciones,
        "total" => $total,
    ];
    return response()->json($data);

  }
  public function observadorReporteAjax($id,$inicio,$fin,$grado,$estado,$falta){
    $inst = Institucions::where('id',$id)->get();
    if ($inicio=="0" && $grado=="0") {
        $observaciones=DB::select(DB::raw('SELECT estudiantes.nombre, estudiantes.apellidos,observaciones.detalles,observaciones.created_at as fecha FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.' and estudiantes.activo='.$estado.' and observaciones.tipo_falta="'.$falta.'" and privado=0'));
      
    } else if ($inicio=="0" && $grado!="0") {
        $observaciones=DB::select(DB::raw('SELECT estudiantes.nombre, estudiantes.apellidos,observaciones.detalles,observaciones.created_at as fecha FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.' and estudiantes.activo='.$estado.' and observaciones.tipo_falta="'.$falta.'" and privado=0 and estudiantes.grado='.$grado.''));
      
    }else{
     
        $observaciones=DB::select(DB::raw('SELECT estudiantes.nombre, estudiantes.apellidos,observaciones.detalles,observaciones.created_at as fecha FROM observaciones INNER JOIN estudiantes on observaciones.id_estudiante=estudiantes.id and estudiantes.id_institucion='.$id.' and estudiantes.activo='.$estado.' and observaciones.tipo_falta="'.$falta.'" and privado=0 and estudiantes.grado='.$grado.' and observaciones.created_at>="'.$inicio.'" and observaciones.created_at<="'.$fin.'"'));
      
    }
   
    $data = [
        "status" => "200",
        "observaciones" => $observaciones,
    ];
    return response()->json($data);

  }
public function estadisticasAjax($id_dominio,$grado,$id_colegio){
    $preguntas=DB::select('SELECT * from preguntas WHERE preguntas.id_dominio='.$id_dominio.'');
    if($grado!="0"){
        $estadisticas=DB::select('SELECT p.id,p.pregunta,p.descripcion_grafica,r.respuestas ,count(*) as cantidad,p.tipo_input FROM formulario_ficha_salud as f INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id INNER JOIN preguntas as p on p.id=r.id_preguntas INNER JOIN dominios as d on d.id=p.id_dominio INNER JOIN estudiantes as e on e.id=f.id_estudiante INNER JOIN(SELECT f.id_estudiante,MAX(f.created_at) fecha FROM formulario_ficha_salud as f GROUP BY f.id_estudiante)ultimo on ultimo.fecha=f.created_at and d.id='.$id_dominio.' and e.id_institucion='.$id_colegio.' and e.grado='.$grado.' group by r.id_preguntas,r.respuestas,p.id,p.pregunta,p.descripcion_grafica,p.tipo_input');
    }else{
        $estadisticas=DB::select('SELECT p.id,p.pregunta,p.descripcion_grafica,r.respuestas ,count(*) as cantidad FROM formulario_ficha_salud as f INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id INNER JOIN preguntas as p on p.id=r.id_preguntas INNER JOIN dominios as d on d.id=p.id_dominio INNER JOIN estudiantes as e on e.id=f.id_estudiante INNER JOIN(SELECT f.id_estudiante,MAX(f.created_at) fecha FROM formulario_ficha_salud as f GROUP BY f.id_estudiante)ultimo on ultimo.fecha=f.created_at and d.id='.$id_dominio.' and e.id_institucion='.$id_colegio.' group by r.id_preguntas,r.respuestas,p.id,p.pregunta,p.descripcion_grafica');
   
    }
    $data = [
        "status" => "200",
        "estadisticas" => $estadisticas,
        "preguntas" => $preguntas,
    ];
    return response()->json($data);


}
public function estadisticasAjaxRango($id_dominio,$grado,$id_colegio,$inicio,$fin){
    $preguntas=DB::select('SELECT * from preguntas WHERE preguntas.id_dominio='.$id_dominio.'');
    if($grado!="0"){
        $estadisticas=DB::select('SELECT p.id,p.pregunta,p.descripcion_grafica,r.respuestas ,count(*) as cantidad,p.tipo_input FROM formulario_ficha_salud as f INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id INNER JOIN preguntas as p on p.id=r.id_preguntas INNER JOIN dominios as d on d.id=p.id_dominio INNER JOIN estudiantes as e on e.id=f.id_estudiante INNER JOIN(SELECT f.id_estudiante,MAX(f.created_at) fecha FROM formulario_ficha_salud as f where f.created_at>="'.$inicio.'" and f.created_at<="'.$fin.'" GROUP BY f.id_estudiante)ultimo on ultimo.fecha=f.created_at and d.id='.$id_dominio.' and e.id_institucion='.$id_colegio.' and e.grado='.$grado.' group by r.id_preguntas,r.respuestas,p.id,p.pregunta,p.descripcion_grafica,p.tipo_input');
    }else{
        $estadisticas=DB::select('SELECT p.id,p.pregunta,p.descripcion_grafica,r.respuestas ,count(*) as cantidad FROM formulario_ficha_salud as f INNER JOIN respuestas as r on r.id_formulario_ficha_salud=f.id INNER JOIN preguntas as p on p.id=r.id_preguntas INNER JOIN dominios as d on d.id=p.id_dominio INNER JOIN estudiantes as e on e.id=f.id_estudiante INNER JOIN(SELECT f.id_estudiante,MAX(f.created_at) fecha FROM formulario_ficha_salud as f where f.created_at>="'.$inicio.'" and f.created_at<="'.$fin.'" GROUP BY f.id_estudiante)ultimo on ultimo.fecha=f.created_at and d.id='.$id_dominio.' and e.id_institucion='.$id_colegio.' group by r.id_preguntas,r.respuestas,p.id,p.pregunta,p.descripcion_grafica');
   
    }
    $data = [
        "status" => "200",
        "estadisticas" => $estadisticas,
        "preguntas" => $preguntas,
    ];
    return response()->json($data);


}







    
    public function dominiosEvaluador($id){
        if($id==0){
            $id=auth()->user()->id_institucion;
            
        }
        $datos= new prevencionController();
        $estadoHigiene=$datos->estadoHigieneInstitucion(null,null,$id);
        $practicasCuidado=$datos->practicasCuidado(null,null,$id);
        $cuidadores=$datos->cuidadores(null,null,$id);
        $fumadores=$datos->fumadores(null,null,$id);
        $alcoholicos=$datos->consumidores_alcohol(null,null,$id);
        $raciones_comida=$datos->raciones_comida(null,null,$id);
        $anemia=$datos->anemia(null,null,$id);
        $salud_oral=$datos->salud_oral(null,null,$id);
        $cepillado=$datos->cepillado(null,null,$id);
        $alt_gastrointestinales=$datos->alt_gastrointestinales(null,null,$id);
        $estado_nutricional=$datos->estado_nutricional($id);
        $userId = Auth::id();
        $allUser = User::all();
    $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');

        //dd($alt_gastrointestinales);
        if(auth()->user()->id_rol==4){
            return view('evaluador.dominios',["allUser"=>$allUser,"notificacion"=>$notificacion],compact('estadoHigiene','practicasCuidado','cuidadores','fumadores','alcoholicos','raciones_comida','anemia','salud_oral','cepillado','alt_gastrointestinales','estado_nutricional'));
        }else if(auth()->user()->id_rol==5){
            $now = new \DateTime();
            $visit=Visitas::where('fecha', $now->format('Y-m-d'))->first();
            $date = explode('-', $now->format('Y-m-d'), 3); 
            $mes = Visitas::whereYear('fecha', '=', $date[0])
            ->whereMonth('fecha', '=', $date[1])
            ->get()->sum('cantidad');
            $todo = Visitas::all()->sum('cantidad');
            $user = User::all()->count();
            $usuarios=User::offset(0)->limit(5)->get(); 
            $contactenos = Contactenos::orderBy('id','ASC')->paginate(1);
            $inst = Institucions::where('id',$id)->get();
            $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
            return view('autoridad.dominioUno',['instituciones'=>$instituciones,"hoy"=>$visit->cantidad,
            "mes"=>$mes,"todo"=>$todo,"user"=>$user,"usuarios"=>$usuarios,"contactenos"=>$contactenos,"inst"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('estadoHigiene','practicasCuidado','cuidadores','fumadores','alcoholicos','raciones_comida','anemia','salud_oral','cepillado','alt_gastrointestinales','estado_nutricional'));
            
       
        }
        else if(auth()->user()->id_rol==1){
            $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
            $inst = Institucions::where('id',$id)->get();
           
            return view('admin.dominioUno',["instituciones"=>$instituciones,"inst"=>$inst,"allUser"=>$allUser,"notificacion"=>$notificacion],compact('estadoHigiene','practicasCuidado','cuidadores','fumadores','alcoholicos','raciones_comida','anemia','salud_oral','cepillado','alt_gastrointestinales','estado_nutricional'));
            
       
        }
        else if(auth()->user()->id_rol==3){
            return view('rector.dominioUno',["allUser"=>$allUser,"notificacion"=>$notificacion],compact('estadoHigiene','practicasCuidado','cuidadores','fumadores','alcoholicos','raciones_comida','anemia','salud_oral','cepillado','alt_gastrointestinales','estado_nutricional'));
            
       
        }
       }

       
      
     

    public function edadInstitucion($id,$grado){
        if($grado!="0"){
            $fechas=DB::select('SELECT YEAR(fechaNacimiento) AS fecha, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY year(fechaNacimiento)');
            
            $now = new \DateTime();
            $now->format('Y-m-d');
            $date = explode('-', $now->format('Y-m-d'), 3); 
            $edad=[];
            $cantidad=[];
            //$colegio=auth()->user()->institucio->nombre_insti;
            $num=0;
            $total=0;
            foreach($fechas as $var){
                $edad[$num]=($date[0]-$var->fecha).' años';
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",Total Estudiantes:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            return compact('edad','cantidad','colegio');
        }else{
            $fechas=DB::select('SELECT YEAR(fechaNacimiento) AS fecha, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 GROUP BY year(fechaNacimiento)');
            $now = new \DateTime();
            $now->format('Y-m-d');
            $date = explode('-', $now->format('Y-m-d'), 3); 
            $edad=[];
            $cantidad=[];
            //$colegio=auth()->user()->institucio->nombre_insti;
            $num=0;
            $total=0;
            foreach($fechas as $var){
                $edad[$num]=($date[0]-$var->fecha).' años';
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            return compact('edad','cantidad','colegio');
        }
     
    }

    public function gradoInstitucion($id){

        $grados=DB::select('SELECT grado, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 GROUP BY grado');
        $grado=[];
        $cantidad=[];
        $num=0;
        $total=0;
        foreach($grados as $var){
            $grado[$num]=$var->grado.' grado';
            $cantidad[$num]=$var->cantidad; 
            $total+=$var->cantidad;
            $num++; 
        }
        if($id==0){
            $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=$inst[0]->nombre_insti.",total:".$total;
      
        }
        
        return compact('grado','cantidad','colegio');

    }

    public function generoInstitucion($id,$grado){
        if($grado!="0"){
            $genero=DB::select('SELECT genero, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY genero');
            $gen=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($genero as $var){
                $gen[$num]=$var->genero;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('gen','cantidad','colegio');
        }else{
            $genero=DB::select('SELECT genero, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 GROUP BY genero');
            $gen=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($genero as $var){
                $gen[$num]=$var->genero;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('gen','cantidad','colegio');
        }
       

    }

    public function regimenInstitucion($id,$grado){
             if($grado!="0"){
                $regimen=DB::select('SELECT regimen_salud, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY regimen_salud');
                $tipo=[];
                $cantidad=[];
                $num=0;
                $total=0;
                foreach($regimen as $var){
                    $tipo[$num]=$var->regimen_salud;
                    $cantidad[$num]=$var->cantidad; 
                    $total+=$var->cantidad;
                    $num++; 
                }
                if($id==0){
                    $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
              
                }else{
                    $inst = Institucions::where('id',$id)->get();
                    $colegio=$inst[0]->nombre_insti.",total:".$total;
              
                }
                
                return compact('tipo','cantidad','colegio');
             }else{
                $regimen=DB::select('SELECT regimen_salud, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 GROUP BY regimen_salud');
                $tipo=[];
                $cantidad=[];
                $num=0;
                $total=0;
                foreach($regimen as $var){
                    $tipo[$num]=$var->regimen_salud;
                    $cantidad[$num]=$var->cantidad; 
                    $total+=$var->cantidad;
                    $num++; 
                }
                if($id==0){
                    $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
              
                }else{
                    $inst = Institucions::where('id',$id)->get();
                    $colegio=$inst[0]->nombre_insti.",total:".$total;
              
                }
                
                return compact('tipo','cantidad','colegio');
             }
        
            }
    public function jornadaInstitucion($id,$grado){
        if($grado!="0"){
            $jornada=DB::select('SELECT jornada, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY jornada');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($jornada as $var){
                $tipo[$num]=$var->jornada;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('tipo','cantidad','colegio');
        }else{
            $jornada=DB::select('SELECT jornada, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1  GROUP BY jornada');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($jornada as $var){
                $tipo[$num]=$var->jornada;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('tipo','cantidad','colegio');
        }      
        
        }

        public function residenciaInstitucion($id,$grado){
          if($grado!="0"){
            $recidencia=DB::select('SELECT area_residencia, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY area_residencia');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($recidencia as $var){
                $tipo[$num]=$var->area_residencia;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('tipo','cantidad','colegio');
          }else{
            $recidencia=DB::select('SELECT area_residencia, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1  GROUP BY area_residencia');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=0;
            foreach($recidencia as $var){
                $tipo[$num]=$var->area_residencia;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total;
          
            }
            
            return compact('tipo','cantidad','colegio');
          }
    
    }

    public function barrioInstitucion($id,$grado){
      if($grado!="0"){
        $barrio=DB::select('SELECT barrio, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1 and grado='.$grado.' GROUP BY barrio');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=0;
        foreach($barrio as $var){
            $tipo[$num]=$var->barrio;
            $cantidad[$num]=$var->cantidad; 
            $total+=$var->cantidad;
            $num++; 
        }
        if($id==0){
            $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=$inst[0]->nombre_insti.",total:".$total;
      
        }
        
        return compact('tipo','cantidad','colegio');
      }else{
        $barrio=DB::select('SELECT barrio, count(*) as cantidad FROM estudiantes WHERE id_institucion='.$id.' AND activo=1  GROUP BY barrio');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=0;
        foreach($barrio as $var){
            $tipo[$num]=$var->barrio;
            $cantidad[$num]=$var->cantidad; 
            $total+=$var->cantidad;
            $num++; 
        }
        if($id==0){
            $colegio=auth()->user()->institucio->nombre_insti.",total:".$total;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=$inst[0]->nombre_insti.",total:".$total;
      
        }
        
        return compact('tipo','cantidad','colegio');
      }

}

    public function patologicosInstitucion($id){
        $patologicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=1 GROUP by a.tipo');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=1 GROUP by s.id) as t');
        foreach($patologicos as $var){
            $tipo[$num]=$var->tipo;
            $cantidad[$num]=$var->cantidad; 
            $num++; 
        }
        if($id==0){
            $colegio="Total Estudiantes:".$total[0]->cantidad;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=",Total Estudiantes:".$total[0]->cantidad;
      
        }
        
        return compact('tipo','cantidad','colegio');

    }

    public function quirurgicosInstitucion($id){
        $quirurgicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=2 GROUP by a.tipo');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=2 GROUP by s.id) as t');
        foreach($quirurgicos as $var){
            $tipo[$num]=$var->tipo;
            $cantidad[$num]=$var->cantidad; 
            $num++; 
        }
        if($id==0){
            $colegio="Total Estudiantes:".$total[0]->cantidad;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=",Total Estudiantes:".$total[0]->cantidad;
      
        }
        
        return compact('tipo','cantidad','colegio');

    }

    public function traumaticosInstitucion($id){
        $traumaticos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=3 GROUP by a.tipo');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=3 GROUP by s.id) as t');
        foreach($traumaticos as $var){
            $tipo[$num]=$var->tipo;
            $cantidad[$num]=$var->cantidad; 
            $num++; 
        }
        if($id==0){
            $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio="Total Estudiantes::".$total[0]->cantidad;
      
        }
        
        return compact('tipo','cantidad','colegio');

    }
  
    public function alergicosInstitucion($id){
        $alergicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=4 GROUP by a.tipo');
        $tipo=[];
        $cantidad=[];
        $num=0;
        $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=3 GROUP by s.id) as t');
        foreach($alergicos as $var){
            $tipo[$num]=$var->tipo;
            $cantidad[$num]=$var->cantidad; 
            $num++; 
        }
        if($id==0){
            $colegio="Total Estudiantes:".$total[0]->cantidad;
      
        }else{
            $inst = Institucions::where('id',$id)->get();
            $colegio=",Total Estudiantes:".$total[0]->cantidad;
      
        }
        
        return compact('tipo','cantidad','colegio');

    }

    public function antecedentesRango($inicio,$fin,$id){
        
        if($id==0){
          $id=auth()->user()->id_institucion;
            
        }
        
        $datos= new prevencionController();
        $patologicos=$datos->patologicosRango($inicio,$fin,$id,"0");
        $quirurgicos=$datos->quirurgicosRango($inicio,$fin,$id,"0");
        $traumaticos=$datos->traumaticosRango($inicio,$fin,$id,"0");
        $alergicos=$datos->alergicosRango($inicio,$fin,$id,"0");
        $data = [
            "status" => "200",
            "patologicos" => $patologicos,
            "quirurgicos" => $quirurgicos,
            "traumaticos" => $traumaticos,
            "alergicos" => $alergicos
        ];
        
        return response()->json($data);

    }

    public function reporteAntecedentesGrado($inicio,$fin,$grado,$id){
        $datos= new prevencionController();
        $edad=$datos->edadInstitucion($id,$grado);
        $genero=$datos->generoInstitucion($id,$grado);
        $regimen=$datos->regimenInstitucion($id,$grado);
        $jornada=$datos->jornadaInstitucion($id,$grado);
        $residencia=$datos->residenciaInstitucion($id,$grado);
        $barrio=$datos->barrioInstitucion($id,$grado);
        if($inicio=="0"){
            $hoy = new \DateTime();
            $hoy=$hoy->format('Y-m-d H:i:s');;
          
            $patologicos=$datos->patologicosRango("2010-01-01",$hoy,$id,$grado);
            $quirurgicos=$datos->quirurgicosRango("2010-01-01",$hoy,$id,$grado);
            $traumaticos=$datos->traumaticosRango("2010-01-01",$hoy,$id,$grado);
            $alergicos=$datos->alergicosRango("2010-01-01",$hoy,$id,$grado);
        }else{
            $patologicos=$datos->patologicosRango($inicio,$fin,$id,$grado);
            $quirurgicos=$datos->quirurgicosRango($inicio,$fin,$id,$grado);
            $traumaticos=$datos->traumaticosRango($inicio,$fin,$id,$grado);
            $alergicos=$datos->alergicosRango($inicio,$fin,$id,$grado);
        }
        $data = [
            "status" => "200",
            "edad" => $edad,
            "grado" => $grado,
            "genero" => $genero,
            "regimen" => $regimen,
            "jornada" => $jornada,
            "residencia" => $residencia,
            "barrio" => $barrio,
            "patologicos" => $patologicos,
            "quirurgicos" => $quirurgicos,
            "traumaticos" => $traumaticos,
            "alergicos" => $alergicos
        ];
        
        return response()->json($data);
    }

    public function patologicosRango($inicio,$fin,$id,$grado){
        if($grado!="0"){
            $patologicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=1 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
      
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.grado='.$grado.' and s.id_institucion='.$id.' and an.id=1 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($patologicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            return compact('tipo','cantidad','colegio');
        }else{
            $patologicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=1 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
      
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=1 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($patologicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            return compact('tipo','cantidad','colegio');
        }
       
    }

    public function quirurgicosRango($inicio,$fin,$id,$grado){
        if($grado!="0"){
            $quirurgicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=2 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=2 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($quirurgicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            
            return compact('tipo','cantidad','colegio');
    
        }else{
            $quirurgicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=2 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=2 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($quirurgicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            
            return compact('tipo','cantidad','colegio');
    
        }
       
    }

    public function traumaticosRango($inicio,$fin,$id,$grado){
        if($grado!=""){
            $traumaticos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=3  and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=3 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($traumaticos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            
            return compact('tipo','cantidad','colegio');
        }else{
            $traumaticos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=3 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=3 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($traumaticos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            
            return compact('tipo','cantidad','colegio');
        }
      

    }
  
    public function alergicosRango($inicio,$fin,$id,$grado){
        if($grado!="0"){
            $alergicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=4 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and s.grado='.$grado.' and an.id=4 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($alergicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            return compact('tipo','cantidad','colegio');
        }else{
            $alergicos=DB::select('SELECT a.tipo ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=4 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by a.tipo');
            $tipo=[];
            $cantidad=[];
            $num=0;
            $total=DB::select('SELECT count(*) as cantidad from (SELECT s.id ,count(*) as cantidad FROM estudiantes s INNER JOIN ficha_saluds f on s.id=f.id_estudiante INNER JOIN antecedentes_tipos a on f.id_antecedentes_tipo=a.id INNER JOIN antecedentes an on a.id_antecedentes = an.id and s.activo=1 and s.id_institucion='.$id.' and an.id=4 and f.updated_at>="'.$inicio.'" and f.updated_at<="'.$fin.'" GROUP by s.id) as t');
            foreach($alergicos as $var){
                $tipo[$num]=$var->tipo;
                $cantidad[$num]=$var->cantidad; 
                $total+=$var->cantidad;
                $num++; 
            }
            if($id==0){
                $colegio=auth()->user()->institucio->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }else{
                $inst = Institucions::where('id',$id)->get();
                $colegio=$inst[0]->nombre_insti.",total:".$total[0]->cantidad." Rango: ".$inicio." Hasta: ".$fin;
          
            }
            return compact('tipo','cantidad','colegio');
        }
     

    }

    
    
}