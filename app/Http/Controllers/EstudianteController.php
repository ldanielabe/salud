<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GroceryCrud\Core\GroceryCrud;
use Illuminate\Support\Facades\DB;
use App\Estudiante;
use Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EstudiantesImport;
use App\User;

class EstudianteController extends Controller
{

    public function __construct()
{
    ini_set('max_execution_time', 300);
}


    public function datagrid_rel($id)
    {
        try { 
        $id=decrypt($id);
        if(auth()->user()->id_institucion!=$id){
            return redirect()->route('perfil');
        }
    
     
            $crud = $this->_getGroceryCrudEnterprise();
            $crud->setTheme('Bootstrap');
            $crud->setTable('estudiantes');
            $crud->setSubject('estudiante', 'Agrega nuevos Estudiantes, actualiza su informacion y elimina.');
            $crud->setPrimaryKey('id', 'user');
            $crud->where([
                'id_institucion = '.$id
            ]);
          
            $crud->setRelation('id_institucion', 'institucions','{nombre_insti}', ['id']);
          
            $institucion = DB::table('institucions')->get();
    
            $valores = [];
            foreach ($institucion as $variable) {
                $valores[$variable->id] = [$variable->nombre_insti];
            }
           
            $crud->fieldType('id_institucion', 'dropdown_search',$valores);

            $crud->columns(array('nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion', 'genero','grupo_sanguineo', 'barrio','grado','jornada','area_residencia','regimen_salud'));
            $crud->uniqueFields(['documento','email']);
            $crud->addFields(['nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion','genero' , 'grupo_sanguineo', 'barrio','grado','jornada','area_residencia','regimen_salud','imagen']);
            $crud->editFields(['nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion', 'genero','grupo_sanguineo', 'barrio','grado','jornada','area_residencia','regimen_salud','imagen']);
            $crud->requiredFields(['nombre','apellidos','documento', 'fechaNacimiento','genero' , 'barrio','grado','jornada']);
            $crud->fieldType('id', 'hidden');
            $crud->fieldType('direccion', 'string');
      
            $crud->displayAs('fechaNacimiento', 'Fecha de Nacimiento');
        
             
   
             $crud->unsetDelete(); //Elimine btnDelete
    
             $crud->displayAs('id_institucion', 'Institucion');
             $crud->displayAs('grupo_sanguineo', 'Grupo Sanguineo y RH');
             $crud->displayAs('estado_nutricional', 'Estado Nutricional');
             $crud->fieldType('email', 'email');
             $crud->fieldType('fechaNacimiento', 'date');
             $crud->setFieldUpload("imagen","assets/grocery-crud/images/chosen","/assets/grocery-crud/images/chosen");
            
          
            
            $crud->changeFieldType('documento', 'integer');
          


            $output = $crud->render();
       
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
    }

    return $this->_show_output($output);     
    }
    
        
    public function datagrid($id){
    try { 
        $id=decrypt($id);
        if(auth()->user()->id_rol!=1 && auth()->user()->id_institucion!=$id){
            return redirect()->route('perfil');
        }
        $crud = $this->_getGroceryCrudEnterprise();
        $crud->setTheme('Bootstrap');
        $crud->setTable('estudiantes');
        $crud->setSubject('estudiante', 'Agrega nuevos Estudiantes, actualiza su informacion y elimina.');
        $crud->setPrimaryKey('id', 'estudiante');
        $crud->where([
            'id_institucion = '.$id
        ]);

    
        $crud->columns(array('nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion', 'genero','grupo_sanguineo' , 'barrio','grado','jornada','area_residencia','regimen_salud'));
        $crud->uniqueFields(['documento','email']);
        $crud->addFields(['nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion', 'genero','grupo_sanguineo' , 'barrio','grado','jornada','area_residencia','regimen_salud','imagen']);
        $crud->editFields(['nombre','apellidos','email','documento','celular', 'fechaNacimiento','direccion', 'genero','grupo_sanguineo' , 'barrio','grado','jornada','area_residencia','regimen_salud','imagen']);
        $crud->requiredFields(['nombre','apellidos','documento', 'fechaNacimiento','genero' , 'barrio','grado','jornada']);
        $crud->fieldType('id', 'hidden');
        
        $crud->fieldType('direccion', 'string');
        $crud->fieldType('grado', 'dropdown_search',[
            ''=>'',
            '1'=>'Primero',
            '2'=>'Segundo',
            '3'=>'Tercero',
            '4'=>'Cuarto',
            '5'=>'Quinto',
            '6'=>'Sexto',
            '7'=>'Septimo',
            '8'=>'Octavo',
            '9'=>'Noveno',
            '10'=>'Decimo',
            '11'=>'Once',

        ]);

        $crud->fieldType('jornada', 'dropdown_search',[
            ''=>'',
            'Mañana'=>'MAÑANA',
            'Tarde'=>'TARDE',
            'Unica'=>'UNICA',
        ]);
        
        $crud->fieldType('genero', 'dropdown_search',[
            ''=>'',
            'Femenino'=>'FEMENINO', 
            'Masculino'=>'MASCULINO', 
        ]);
        
    

        $crud->fieldType('grupo_sanguineo', 'dropdown_search',[
            ''=>'',
            'O+'=>'O+',
            'O-'=>'O-',
            'A+'=>'A+',
            'A-'=>'A-',
            'B+'=>'B+',
            'B+'=>'B+',
            'AB+'=>'AB+',
            'AB-'=>'AB-'
        ]);

       
        $crud->fieldType('area_residencia', 'dropdown_search',[
            ''=>'',
            'Urbano'=>'URBANO',
            'Rural'=>'RURAL'
        ]);

        $crud->fieldType('regimen_salud', 'dropdown_search',[
            ''=>'',
            'Subsidiado'=>'Subsidiado',
            'Contributivo'=>'Contributivo'
        ]);

        $crud->fieldType('barrio', 'dropdown_search',[
            ''=>'',
            'Las Mercedes'=>'Las Mercedes',
            'San Martín de la Loba'=>'San Martín de la Loba',
            'Luis Vero'=>'Luis Vero',
            'El Carmen de Sardinata'=>'El Carmen de Sardinata',
            'La Victoria'=>'La Victoria',
            'Centro de Sardinata'=>'Centro de Sardinata',
        ]);

        $crud->displayAs('fechaNacimiento', 'Fecha de Nacimiento');
        
         $crud->unsetDelete(); //Elimine btnDelete
       
         $crud->displayAs('id_institucion', 'Institucion');
         $crud->displayAs('password', 'Contraseña');
         $crud->displayAs('grupo_sanguineo', 'Grupo Sanguineo');
         $crud->displayAs('estado_nutricional', 'Estado Nutricional');
         $crud->fieldType('email', 'email');
         $crud->fieldType('fechaNacimiento', 'date');
         
         $crud->setFieldUpload("imagen","assets/grocery-crud/images/chosen","/assets/grocery-crud/images/chosen");
       
         $this->id=$id;
         $crud->callbackBeforeInsert(function($stateParameters){
            $stateParameters->data['id_institucion']=$this->id;
            return $stateParameters;
         });

         $crud->callbackBeforeUpdate(function($stateParameters){
            $stateParameters->data['id_institucion']=$this->id;
            return $stateParameters;
         });

         
        $crud->setActionButton('Observaciones','fa fa-eye',function($row){
            
            return '/observacion/'.encrypt($row->id);
        });

        if(auth()->user()->id_rol==4||auth()->user()->id_rol==2){
        $crud->setActionButton('Antecedentes','fa fa-list-alt',function($row){
            
            return '/antecedentes/'.encrypt($row->id);
        });}
        if(auth()->user()->id_rol==4){
        $crud->setActionButton('Ficha de salud','fa fa-list-alt',function($row){
            
            return '/ficha_salud/'.encrypt($row->id);
        });
        }

        $crud->setActionButton('Acudiente','fa fa-list-alt',function($row){
            
            return '/acudiente/'.encrypt($row->id);
        });
        
     
    $crud->callbackAddField('documento', function () {
        return '<input class="form-control" id="documento" name="documento" class="numeric" maxlength="10" onkeypress="return validaNumericos(event)" required>';
    });

    $crud->callbackEditField('documento', function ($fieldValue) {
        return '<input  class="form-control" id="documento" name="documento"  class="numeric" maxlength="10" value='.$fieldValue.' onkeypress="return validaNumericos(event)" required>';
    });

    $crud->callbackAddField('celular', function () {
        return '<input class="form-control" id="celular" name="celular" class="numeric" maxlength="10" onkeypress="return validaNumericos(event)" >';
    });

    $crud->callbackEditField('celular', function ($fieldValue) {
        return '<input  class="form-control" id="celular" name="celular"  class="numeric" maxlength="10" value='.$fieldValue.' onkeypress="return validaNumericos(event)" >';
    });

      
        $output = $crud->render();
    }catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
        $output = (object)[
            'isJSONResponse' => true,
            'output' => json_encode(
                (object)[
                    'message' => $e->getMessage(),
                    'status' => 'failure'
                ]
            )
        ];
    }

    return $this->_show_output($output);     
    }

    

        private function _getGroceryCrudEnterprise() {
        $database = $this->_getDatabaseConnection();
        $config = config('grocerycrud');
        $crud = new GroceryCrud($config, $database);
        return $crud;
    }

    
    private function _getDatabaseConnection() {
        $databaseConnection = config('database.default');
        $databaseConfig = config('database.connections.' . $databaseConnection);

        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'database' => env('DB_DATABASE'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'charset' => 'utf8'
            ]
        ];
    }

    private function _show_output($output) {
        
        if ($output->isJSONResponse) {
            return response($output->output, 200)
                  ->header('Content-Type', 'application/json')
                  ->header('charset', 'utf-8');
        }
      
        $css_files = $output->css_files;
        $js_files = $output->js_files;
        $output = $output->output;
        $instituciones=DB::select(DB::raw('SELECT *  FROM institucions'));
        $userId = Auth::id();
        $allUser = User::all();
        $notificacion=DB::select('SELECT id_user,nombre, count(*) as cantidad from (SELECT mensajes.leido,users.nombre,mensajes.mensaje,mensajes.id_user,mensajes.id_user_mensaje,mensajes.id from mensajes INNER JOIN users  on users.id=mensajes.id_user where mensajes.id_user_mensaje='.$userId.' and mensajes.leido=0) t GROUP BY nombre');
        $layout_grocery="";
    if(auth()->user()->id_rol==1){
        $layout_grocery='layouts.admin_grocery';
    }else if(auth()->user()->id_rol==2){
        $layout_grocery='layouts.profesor_grocery';
    }else if(auth()->user()->id_rol==3){
        $layout_grocery='layouts.rector_grocery';
    }else if(auth()->user()->id_rol==4){
        $layout_grocery='layouts.evaluador_grocery';
    } else if(auth()->user()->id_rol==5){
        $layout_grocery='layouts.autoridad_grocery';
    }else if(auth()->user()->id_rol==6){
        $layout_grocery='layouts.acudiente_grocery';
    }else if(auth()->user()->id_rol==7){
        $layout_grocery='layouts.admin2_grocery';
    }
   
    return view('grocery.crud', [
        'output' => $output,
        'css_files' => $css_files,
        'js_files' => $js_files,
        'titulo'=>'Estudiantes',
        'layout_grocery' => $layout_grocery,
        "instituciones"=>$instituciones,
        "allUser"=>$allUser,
        "notificacion"=>$notificacion
    ]);
        
    }

   

    public function subir(Request $request){

        set_time_limit(300);
        $file=$request->file('archivo');
        $id=auth()->user()->institucio->id_institucion;

        //dd(auth()->user());
        Excel::import(new EstudiantesImport,$file);


        return back()->with('message','Importacion de estudiantes exitosa');
    }

  
}
