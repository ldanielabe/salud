<?php

namespace App\Http\Middleware;

use Closure;

class evaluador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()){
            if (auth()->user()->id_rol=="3" || auth()->user()->id_rol=="4" || auth()->user()->id_rol=="2" || auth()->user()->id_rol=="1" || auth()->user()->id_rol=="6" || auth()->user()->id_rol=="7") {
                return $next($request);
            }else{
               
                switch(auth()->user()->id_rol){
                    case 1:
                        return redirect('/admin');
                        break;
                    case 2:
                        return redirect('/profesor/dashboard');
                        break;
                    case 3:
                        return redirect('/rector/dashboard');
                        break;
                    case 4:
                        return redirect('/antecedentesVista/0');
                        break;
                    case 5:
                        return redirect('/autoridad/dashboard');
                        break;
                    case 6:
                        return redirect('/acudiente/perfil');
                        break;
                        case 7:
                            return redirect('/administrador/dashboard');
                            break;
                    default:
                        return redirect('/login');
                }   
            }
            
        }
        

        return redirect('/');
    }
    
}
