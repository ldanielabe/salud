@extends('layouts.acudiente')

@section('content')
<br>

<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary text-md-center">
                  <p class="card-category">Editar Perfil</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('act_perfil')}}"  >
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        @if(auth()->user()->institucio !=null)
                        <div class="form-group">
                          <label class="bmd-label-floating">Institucion Educativa</label>
                        <input type="text" class="form-control" name="institucion" value="{{auth()->user()->institucio->nombre_insti}}" disabled>
                        </div>
                        @endif
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo</label>
                          <input type="email" class="form-control" name="correo" value="{{ auth()->user()->email }}">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Contraseña Nueva</label>
                          <input type="password" class="form-control" name="password">
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <input type="text" class="form-control" name="nombres" value="{{ auth()->user()->nombre }}">
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <input type="text" class="form-control" name="apellidos" value="{{ auth()->user()->apellidos }}">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Documento</label>
                          <input type="number" class="form-control" name="documento"  maxlength="10" value="{{ auth()->user()->documento }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                          type = "number"
                          maxlength = "10">
                        </div>
                      </div>
                   

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Celular</label>
                          <input type="number" class="form-control" name="celular"  maxlength="10" value="{{ auth()->user()->celular }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                          type = "number"
                          maxlength = "10">
                        </div>
                      </div>
                    </div>
                   
                    <button type="submit" class="btn btn-primary pull-right" onclick="md.showNotification('top','right','actualizando')">Actualizar Peril</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           
          </div>
        </div>
      </div>




@endsection