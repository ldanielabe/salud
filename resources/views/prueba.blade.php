<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>Roller - Responsive HTML5 Template</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 
    	Roller Template
		http://www.templatemo.com/preview/templatemo_424_roller
    -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
   
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">	
    <link href="./assets/css/font-awesome.min.css" rel="stylesheet"><!-- FontAwesome Icons -->
	<link href="./assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/templatemo_style.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project 3219155922--wp - 
                          corrdinadora academico karina- 3102637630-->
    <link href="./assets/demo/demo.css" rel="stylesheet" />
    <link href="./assets/css/app.css" rel="stylesheet" />
</head>
<body>
	<div id="responsive-menu" >
        <ul class="menu-holder">
            <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="#about"><i class="fa fa-cogs"></i>About</a></li>
            <li><a href="#services"><i class="fa fa-list"></i>Services</a></li>
            <li><a href="#portfolio"><i class="fa fa-briefcase"></i>Portfolio</a></li>
            <li><a href="#testimonials"><i class="fa fa-comment"></i>Testimonials</a></li>
            <li><a href="#contact"><i class="fa fa-envelope"></i>Contact</a></li>
        </ul>
    </div>
	<div class="templatemo-header tm-orange-bg-transparent" style="    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 900;">
		<div class="templatemo-header-inner">					
			<div class="container">
				<h1 class="templatemo-logo text-uppercase pull-left">Logo</h1>
				<nav class="hidden-xs templatemo-nav pull-right text-uppercase">
					<ul class="menu-holder" style="font-size: 13px; font-weight: bold;">
						<li><a href="#" class="active">Home</a></li>
                        <li class="">
                                <a data-scroll href="#noticias" style=" width:100%;">
                                <i class="material-icons">accessibility</i> Noticias
                                </a>
                        </li>
                        <li >
                                <a  data-scroll href="#eventos" style=" width:100%;">
                                <i class="material-icons">calendar_today</i> Eventos
                                </a>
                                </li>
                                <li >
                                <a  data-scroll href="#directorio" style=" width:100%;">
                                <i class="material-icons">account_balance_wallet</i> Directorio
                                </a>
                                </li>
                                
                                <li >
                                    <a  data-scroll href="#contactanos" style=" width:100%;">
                                        <i class="material-icons">contacts</i> Contactenos
                                    </a>
                                    </li>
                                    
                                
                               
                               
                             
                      
                                <li >
                                  <a href="{{ route('login') }}" >
                                  <i class="material-icons">account_circle</i>Ingresar
                                  </a>
                                </li>
                      
                                <li >
                                  <a    target="_blank" data-original-title="Follow us on YouTube">
                                    <i class="fa fa-youtube"></i>
                                  </a>
                                </li>
                      
                      
                                <li >
                                  <a   target="_blank" data-original-title="Like us on Facebook">
                                    <i class="fa fa-facebook-square"></i>
                                  </a>
                                </li>
					</ul>
				</nav>
				<div class="text-right visible-xs">
                    <a href="#" id="mobile_menu"><span class="fa fa-bars"></span></a>
                </div>			
			</div>
		</div> <!-- templatemo-header-inner -->
    </div> <!-- templatemo-header -->
<br>
<br>
<br>



    
	<section class="tm-dark-bg tm-section" id="home">
	
             
   
		 <div class=" header-filter clear-filter " id="inicio" 
   data-parallax="true" >

    <div >
        <!-carousel page-header -->
    <div class="section_slider" id="carousel" >
      <div class="" id="inicio" style="height: 40rem;">
         <!-- Carousel Card -->
         <div class="card card-raised card-carousel" style="margin: 0; ">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" style="margin: 0; height: 25rem !important; margin-top: 4.5rem;">

                <div class="carousel-item active">
                <a href="{{ $slides[0]->url}}" target="_self" tabindex="-1">
                    <img class="d-block w-100" src="./imagesSlide/{{ $slides[0]->ruta }}" style="margin: 0; height: 25rem !important;" alt="First slide">
                </a>  
                  <div class="carousel-caption d-none d-md-block">
                      <h4>
                        <i class="material-icons">location_on</i> {{$slides[0]->titulo}}
                      </h4>
                    </div>
                  </div>

                  @for ($i = 1; $i < sizeof($slides); $i++) 
                 
                  <div class="carousel-item">
                  <a href="{{ $slides[$i]->url}}" target="_self" tabindex="-1">
                  <img class="d-block w-100" src="./imagesSlide/{{ $slides[$i]->ruta }}" style="margin: 0; height: 25rem !important;" alt="Next slide">
                  </a> 
                  <div class="carousel-caption d-none d-md-block">
                      <h4>
                        <i class="material-icons">location_on</i> {{$slides[$i]->titulo}}
                      </h4>
                    </div>
                  </div>
                   
                  @endfor

               
                </div>


                </div>
              </div>
            </div>
            <!-- End Carousel Card -->
      </div>
    </div>
    <!--         end carousel -->
        
   
            			
		</div>		
    </section>
    

	<section class="templatemo-about tm-section" id="about">
        <img src="./assets/img/tm-border-black-top.png" alt="Border" class="tm-border">	
        <div class="container">
  <div class="" id="noticias" style="">
    
        <div class="section ">
          <div class="container">
    
          <div style="width:100%; background:#5c5b5d; text-align: center; color:#000; font-weight: bold; ">
          <h1><strong>NOTICIAS</strong></h1>
      </div>
    
      @if(sizeof($noticias) <= 2)
    
        <div class="row">
          
              @for ($i = 0; $i < sizeof($noticias); $i++) 
              <div class="col-12 col-md-6">
              <div class="card noti" style="width: 300px; height: 100%;  margin-left: auto;
              margin-right: auto; ">
                        <img class="card-img-top" src="./imagesNoticias/{{$noticias[$i]->ruta}}"  alt="Card image cap">
                        <div  class="card-body"  style="height: 30px;">
                <p class="card-text">{{ $noticias[$i]->titulo}}</p>
                <a href="">ver mas...</a>
                </div>
                
                </div>
                
              </div>
    
              @endfor
          
        </div>
    
    
    
      @else 
      <div class="row">
          <div class="col">
              <div class="col-12 col-md-12">
                  <div  class="card noti" style=" margin-left: auto;
                  margin-right: auto;">
                        <img class="card-img-top" src="./imagesNoticias/{{$noticias[0]->ruta}}" alt="Card image cap">
                        <div class="card-body" >
                          <p class="card-text">{{ $noticias[0]->titulo}}</p>
                          <a href="">ver mas...</a>
                        </div>
                        
                </div>
               
              </div>
    
                  <div class="col-12 col-md-12">
                    <div class="card noti"  style=" margin-left: auto;
                    margin-right: auto;">
                      <img class="card-img-top" src="./imagesNoticias/{{$noticias[1]->ruta}}" alt="Card image cap">
                      <div class="card-body" >
                    <p class="card-text">{{ $noticias[1]->titulo}}</p>
                    <a href="">ver mas...</a>
                  </div>
              </div>
            </div>
      
          </div>
    
          <div class="col">
            <div class="row">
    
                <div class="col-12 col-md-12">
    
                @for ($i = 2; $i < sizeof($noticias); $i++) 
                    
                      <div class="col-12 col-md-12">
                      <div class="card noti" style="width: 300px; height: 100%; margin-left: auto;
                      margin-right: auto;">
                          <img class="card-img-top" src="./imagesNoticias/{{$noticias[$i]->ruta}}"  alt="Card image cap" style="width: 300px; height: 150px;">
                          <div  class="card-body"  >
                          <p class="card-text">{{ $noticias[$i]->titulo}}</p>
                          <a href="">ver mas...</a>
                          </div>
                        </div>
                        
                      </div>
                
               @endfor
          </div>   
        </div> 
        <div class="col-12 col-md-12">
             
               
    
            <div style="text-aling:center">
            <a href="{{ route('news') }}"><h3>Ver todas las noticias</h3></a>
         </div> 
       
          </div>
        </div>
        
      </div>
            
    
             
    
             
    
    
              </div>
            
            </div>
             
         
          
          </div>
         
    </div>
      @endif  
    
    
                
           
                </div>
        </div>
        </div>
                 
      </div>
     
      </div>
          <!--  Noticias -->

		
					
        </div>		
        


	</section> <!-- about -->

    	<section class="tm-section tm-orange-bg" id="contact">
		<img src="./assets/img/tm-border-white-top.png" alt="Border" class="tm-border">
		<div class="container text-center">

			  <div class="" id="eventos">
    
    <div class="section ">
      <div class="container">

      <div style="width:100%; background:#5c5b5d; text-align: center; color:#fff; font-weight: bold; margin-top:2rem;">
      <h1><strong>EVENTOS</strong></h1>
  </div>

  <!--  carousel -->
  <iframe src="/carrusel" width="100%" style="background:#eee; overflow: hidden !important; overflow-y: hidden !important; border: #eee;overflow-x: hidden; height: 45rem !important;" scrolling="no"></iframe>

	
            
		</div>
       
    </section> <!-- services -->
    
	
  
	<section class="tm-section" id="testimonials">
		<img src="./assets/img/tm-border-black-top.png" alt="Border" class="tm-border">
		<div class="container">
			<h1 class="text-uppercase text-center">Testimonials</h1>
			<div class="text-center margin-top-50 tm-testimonial-images-container">

			
        <div class="section" id="directorio">
          <div class="container" style="">
        
        
          <!--  carousel -->
          <iframe src="/carruselDirectorio" width="100%" heigth="70%" style="background:#eee; overflow: hidden !important; overflow-y: hidden !important; border: #eee;overflow-x: hidden; height: 45rem !important;" scrolling="no"></iframe>
        
        
            
          </div>
        </div>
				
			
				
			</div>
			<p class="text-center margin-top-50 tm-comment"><em>This is a place holder for different description text.</em></p>
			<p class="text-uppercase text-center tm-author tm-orange-text">Author Name</p>
			<hr class="tm-author-name-underline">
		</div>
  </section> <!-- testimonials -->
	<section class="templatemo-services tm-section tm-orange-bg-transparent" id="services">
		<img src="./assets/img/tm-border-white-top.png" alt="Border" class="tm-border">
		<div class="container">
			<h1 class="text-uppercase text-center">Contact Us</h1>
			<p class="text-center margin-top-50">Phasellus sed rutrum quam. Aenean mollis nec metus quis fringilla. Aliquam interdum risus quis sem rhoncus, vel iaculis lorem pulvinar. Maecenas in lorem non quam bibendum auctor vel lobortis sem.</p>
			<div class="row margin-top-50">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	            	<div id="map-canvas"></div>	
	            </div>
	            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		            <form action="#" method="post" class="tm-contact-form">
		                <div class="form-group">
		                    <input type="text" id="contact_name" class="form-control" placeholder="NAME..." />
		                </div>
		                <div class="form-group">
		                    <input type="text" id="contact_email" class="form-control" placeholder="EMAIL..." />
		                </div>
		                <div class="form-group">
		                    <input type="text" id="contact_subject" class="form-control" placeholder="SUBJECT..." />
		                </div>
		                <div class="form-group">
		                    <textarea id="contact_message" class="form-control" rows="8" placeholder="WRITE A MESSAGE..."></textarea>
		                </div>
		                <button type="submit" class="btn text-uppercase tm-dark-bg tm-orange-text tm-send-btn">Send</button>
		            </form>
	            </div>
            </div>
		</div>
		<img src="./assets/img/tm-border-black-bottom.png" alt="Border" class="tm-border">
  </section> <!-- contact -->
  
	<footer class="text-right tm-dark-bg">		
		<p class="text-uppercase container small">Copyright &copy; 2084 <a href="#">Company Name</a></p>
	</footer>
	<script src="./assets/js/jquery-1.11.1.min.js"></script> 
    <script src="./assets/js/templatemo_script.js"></script>
    <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  
</body>
</html>