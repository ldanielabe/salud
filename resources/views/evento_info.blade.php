<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    ASIE
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-kit.css?v=2.0.6') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project 3219155922--wp - 
                        corrdinadora academico karina- 3102637630-->
  <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet"><!-- FontAwesome Icons -->
  <link href="{{ asset('assets/css/templatemo_style.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


</head>
<body>





<div class="container ">
    <div class="section animate__animated animate__slideInLeft">
        <a href="{{ url('/carrusel')}}">Atras</a>
        <div class="row">
        @if($ins!=null)
        <h2 style="text-align: center;font-weight: bold;">Evento en el colegio {{$ins}}</h2>
        @endif
        <div class="col-12 col-md-6"> 
        <img  src="../imagesEventos/{{$noticia->ruta}}"  style="width: 500px; height: 250px;" alt="">

        </div>
        <div class="col-12 col-md-6"> 
        
        <h2 style="text-align: justify;font-weight: bold;">{{$noticia->titulo}}</h2>
        </div>
        <div class="col-12">
        <br> 
        <p style="text-align: justify;">{{$noticia->contenido}}</p>

        </div>
        </div>
      
       

       
    </div>



    

  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/material-kit.js?v=2.0.6') }}" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  <script src="{{ asset('assets/js/templatemo_script.js') }}"></script>
  <script>
    $(document).ready(function() {
      function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    //alert(docViewTop);
    var docViewBottom = docViewTop + $(window).height();
    //alert(docViewBottom);
    var elemTop = $(elem).offset().top;
    //alert(elemTop);
    var elemBottom = elemTop + $(elem).height();
    //alert(elemBottom);   
    return ((elemBottom < docViewBottom) && (elemTop > docViewTop));
  }
      $(window).scroll(function() {
        $('.scroll-animations .animated').each(function() {
          if (isScrolledIntoView(this) === true) {
            $(this).addClass('animate__animated animate__slideInLeft');
          }
        });

        $('.animar').each(function() {
          if (isScrolledIntoView(this) === true) {
            //alert("a");
            $(this).addClass('animate__animated animate__fadeInUpBig');
          }
        });

      });
      



      
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
   
    function myFunction(titulo){
      document.title = 'ASIE::'+titulo;
}


  </script>
</body>

</html>