@extends($layout_grocery)

@section('content')
<div class="content">
    <div class="container-fluid" >
    <h2 id="nombre" style="text-align: center;font-weight: bold;">Graficas del Observador de los estudiantes</h2>
    <br>
<select id="selectGrado" class="custom-select">
        <option  value="-1">(Opcional) Seleccione un Grado</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
    </select>
    <br>
    <br>
    <select id="activo" class="custom-select">
        <option  value="-1">¿Estudiantes Activos o Inactivos?</option>
        <option value="0">Inactivos</option>
        <option value="1">Activos</option>
       
    </select>
<br>
<br>

<div class="row" style="">
    

    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Inicio:</label>
        <br>
        <input type="date" id="inicio" class="form-control" >
      </div>
    </div>
   

    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Fin:</label>
        <br>
        <input type="date" id="fin" class="form-control" >
      </div>
    </div>
    <div class="col-md-12" style="text-align:center;">
      <button class="btn btn-primary" onclick="filtrar();">Filtrar</button>
    </div>
   
  </div>
  <br>
   
   <div id="reporte">

   </div>
<br>

    <div class="row" id="contenedor">
    <div class="col-md-12 animate__animated animate__backInLeft container" ><div class="chart-container card" style="position: relative; height:90%; width:90%; text-align: center; margin-left: auto;margin-right: auto;"><div id="grafica" ></div></div></div>
      </div>
  


    </div>
    </div>

<script>
var observaciones = {!! json_encode($observaciones) !!};
var total = {!! json_encode($total) !!};

var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+total.length+' Estudiantes agrupados por tipo de observacion. <p> '+
'<h5 style="font-size: 0.6rem;">Nota: La información entregada en la presente grafica, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.</h5>';

              document.getElementById('reporte').innerHTML="";
              document.getElementById('reporte').innerHTML=html;
var datos=[];
              var label=[""];
              html="";
            
for (let i = 0; i < observaciones.length; i++) {
  
    if(i==0){
                    datos=[observaciones[i].cantidad];
                    label=[observaciones[i].tipo_falta];
                  }else{
                    datos=datos.concat([observaciones[i].cantidad]);
                    label=label.concat([observaciones[i].tipo_falta]);
                  }
}

var options = {
                    series: [{
                      name: 'Cantidad',
                    data: datos
                  }],
                    chart: {
                    height: 350,
                    type: 'bar',
                    events: {
                      click: function(chart, w, e) {
                        // console.log(chart, w, e)
                      }
                    }
                  },
                  plotOptions: {
                    bar: {
                      columnWidth: '45%',
                      distributed: true
                    }
                  },
                  dataLabels: {
                    enabled: true,
                    formatter: function (val) {
                      return val ;
                    },
                  },
                  legend: {
                    show: false
                  },
                  xaxis: {
                    categories: label,
                    position: 'top',
                    axisBorder: {
                      show: false
                    },
                    axisTicks: {
                      show: false
                    },
                    crosshairs: {
                      fill: {
                        type: 'gradient',
                        gradient: {
                          colorFrom: '#D8E3F0',
                          colorTo: '#BED1E6',
                          stops: [0, 100],
                          opacityFrom: 0.4,
                          opacityTo: 0.5,
                        }
                      }
                    },
                    tooltip: {
                      enabled: true,
                    }
                  },

                  title: {
                    text: 'Grafica del observador de todos los estudiantes clasificados por tipo de falta',
                    floating: true,
                    offsetY: 330,
                    align: 'center',
                    style: {
                      color: '#444'
                    }
                  }
                  };


                  var mychart = new ApexCharts(document.querySelector('#grafica'), options);
                  mychart.render();  
                  var URLactual = document.location+'';
var res=URLactual.split("/");
var id_colegio={!! json_encode($id) !!};;

function filtrar(){
var grado=document.getElementById("selectGrado").value;
var activo=document.getElementById("activo").value;
var inicio=document.getElementById("inicio").value; 
var fin=document.getElementById("fin").value;
if(grado!="-1" && activo !="-1" && inicio =="" && fin ==""){
    $.ajax({
          type: "GET",
            url: "/observadorAjax/"+id_colegio+"/0/0/"+grado+"/"+activo,
            data: {},
            success: function (data) {
              var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.total.length+' Estudiantes agrupados por tipo de observacion y grado '+grado+'. Nota: La información entregada en la presente grafica, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              document.getElementById('reporte').innerHTML=html; 
              datos=[];
               label=[""];                 
for (let i = 0; i < data.observaciones.length; i++) {
  
  if(i==0){
                  datos=[data.observaciones[i].cantidad];
                  label=[data.observaciones[i].tipo_falta];
                }else{
                  datos=datos.concat([data.observaciones[i].cantidad]);
                  label=label.concat([data.observaciones[i].tipo_falta]);
                }
}

var options = {
                  series: [{
                    name: 'Cantidad',
                  data: datos
                }],
                  chart: {
                  height: 350,
                  type: 'bar',
                  events: {
                    click: function(chart, w, e) {
                      // console.log(chart, w, e)
                    }
                  }
                },
                plotOptions: {
                  bar: {
                    columnWidth: '45%',
                    distributed: true
                  }
                },
                dataLabels: {
                  enabled: true,
                  formatter: function (val) {
                    return val ;
                  },
                },
                legend: {
                  show: false
                },
                xaxis: {
                  categories: label,
                  position: 'top',
                  axisBorder: {
                    show: false
                  },
                  axisTicks: {
                    show: false
                  },
                  crosshairs: {
                    fill: {
                      type: 'gradient',
                      gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                      }
                    }
                  },
                  tooltip: {
                    enabled: true,
                  }
                },

                title: {
                  text: 'Grafica del observador de todos los estudiantes clasificados por tipo de falta',
                  floating: true,
                  offsetY: 330,
                  align: 'center',
                  style: {
                    color: '#444'
                  }
                }
                };

                mychart.destroy();
                 mychart = new ApexCharts(document.querySelector('#grafica'), options);
                mychart.render();  

             
              }         
          });
}else if(inicio !="" && fin !="" && grado!="-1" && activo !="-1"){
  
    if(document.getElementById("inicio").value>document.getElementById("fin").value){
    md.showNotification('top','right','La fecha de inicio no puede ser mayor a la fecha fin');
    }else{
        $.ajax({
          type: "GET",
            url: "/observadorAjax/"+id_colegio+"/"+inicio+"/"+fin+"/"+grado+"/"+activo,
            data: {},
            success: function (data) {
              var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.total.length+' Estudiantes agrupados por tipo de observacion, con grado '+grado+' y rango de fechas desde el '+inicio+' hasta '+fin+'. Nota: La información entregada en la presente grafica, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              document.getElementById('reporte').innerHTML=html;
                    
                      datos=[];
                      label=[];
for (let i = 0; i < data.observaciones.length; i++) {
  
  if(i==0){
                  datos=[data.observaciones[i].cantidad];
                  label=[data.observaciones[i].tipo_falta];
                }else{
                  datos=datos.concat([data.observaciones[i].cantidad]);
                  label=label.concat([data.observaciones[i].tipo_falta]);
                }
}

var options = {
                  series: [{
                    name: 'Cantidad',
                  data: datos
                }],
                  chart: {
                  height: 350,
                  type: 'bar',
                  events: {
                    click: function(chart, w, e) {
                      // console.log(chart, w, e)
                    }
                  }
                },
                plotOptions: {
                  bar: {
                    columnWidth: '45%',
                    distributed: true
                  }
                },
                dataLabels: {
                  enabled: true,
                  formatter: function (val) {
                    return val ;
                  },
                },
                legend: {
                  show: false
                },
                xaxis: {
                  categories: label,
                  position: 'top',
                  axisBorder: {
                    show: false
                  },
                  axisTicks: {
                    show: false
                  },
                  crosshairs: {
                    fill: {
                      type: 'gradient',
                      gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                      }
                    }
                  },
                  tooltip: {
                    enabled: true,
                  }
                },

                title: {
                  text: 'Grafica del observador de todos los estudiantes clasificados por tipo de falta',
                  floating: true,
                  offsetY: 330,
                  align: 'center',
                  style: {
                    color: '#444'
                  }
                }
                };

                mychart.destroy();
                 mychart = new ApexCharts(document.querySelector('#grafica'), options);
                mychart.render();  

             
              }         
          });
    }
}else{
md.showNotification('top','right','Faltan datos por completar');
}


}

</script>


@endsection