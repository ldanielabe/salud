@extends($layout_grocery)

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<div class="content">
    <div class="container-fluid" >
    <h2 id="nombre" style="text-align: center;font-weight: bold;">REPORTES GLOBALES POR PREGUNTA</h2>
    <br>
    <select id="selectDominio" class="custom-select" onchange="dominios()">
        <option  value="-1">Seleccione un domonio</option>
        @for ($i = 0; $i < sizeof($dominios); $i++)
        <option value="{{$dominios[$i]->id}}">{{$dominios[$i]->nombre}}</option>
        @endfor
    </select>
    <br>
    <br>
    <select id="selectPregunta" class="custom-select">
        <option  value="-1">Seleccione un dominio primero</option>
    </select>
    <br>
    <br>

    <select id="activo" class="custom-select">
        <option  value="-1">¿Estudiantes Activos o Inactivos?</option>
        <option value="1">Activos</option>
        <option value="0">Inactivos</option>
       
    </select>
    <br>
    <br>
    <div class="row">
    <div class="col-md-6" style="height: 50px;text-align:center;">
    <button class="btn" style="background: #bfbfbf;" onclick="filtrar();">Filtrar</button>
    </div>
    <div class="col-md-6" style="height: 50px;text-align:center;">
    <button class="btn" style="background: #bfbfbf;" id="exportar" onclick="exportar();" disabled>Exportar</button>
    </div>
    </div>
    
   <br>
   
    <div id="reporte">

    </div>
    <br>
    <div id="pdf" class="table-responsive" style="overflow:scroll;
     height:400px;
     width:100%;">
					<table class="table ">
				  <thead class="thead-dark">
					<tr>
				
                      <th scope="col">Nombres</th>
                      <th scope="col">Apellidos</th>
					  <th scope="col">Respuestas</th>
					  <th scope="col">Fecha</th>
					</tr>
				  </thead>
				  <tbody id="datos">
					
				
					
				  </tbody>
				</table>
</div>

    </div>
</div>

<script>

function exportar(){
  var sel = document.getElementById('pdf');
  console.log(sel);
  var mywindow = window.open('newFile.html', 'PRINT', 'height=800,width=800');
var con=0;
            mywindow.document.write('<html><head><title>' + 'Sistema de Ayuda a la Salud Escolar en las Instituciones Educativas'  + '</title>');
            mywindow.document.write('</head><body >');
   
            mywindow.document.write(sel.innerHTML);
            
            mywindow.document.write('</body></html>');

mywindow.document.close(); // necessary for IE >= 10
mywindow.focus(); // necessary for IE >= 10*/

mywindow.print();
mywindow.close();
}

var id_colegio={!! json_encode($id) !!};
function dominios(){
  var sel = document.getElementById('selectDominio');
  $('#exportar').attr("disabled", true);
  if(sel.value!="-1"){
    $.ajax({
          type: "GET",
            url: "/reporteAjaxDominio/"+sel.value,
            data: {},
            success: function (data) {
             
              var html="";
              document.getElementById('selectPregunta').innerHTML="";
              $('#selectPregunta').append(html);
                for (let index = 0; index < data.preguntas.length; index++) {
                     html +='<option value="'+data.preguntas[index].id+'">'+data.preguntas[index].pregunta+'</option>' ;
                    
                }
                $('#selectPregunta').append(html);
              }         
          });
  }
}
function filtrar(){
    var sel2 = document.getElementById('selectDominio');
    var activo = document.getElementById('activo');
    
    var sel = document.getElementById('selectPregunta');
    if(sel.value=="-1"){
        md.showNotification('top','right','No has seleccionado ningun dominio');
    }if(activo.value=="-1"){
        md.showNotification('top','right','No has seleccionado el estado de los estudiantes');
    }
   
    else{
      $('#exportar').attr("disabled", false);
        $.ajax({
          type: "GET",
            url: "/reporteAjax/"+activo.value+"/"+sel.value+"/"+id_colegio,
            data: {},
            success: function (data) {
             if(data.status=="403"){
              md.showNotification('top','right','Accion no permitida');
             }else{
              var html="";
              document.getElementById('datos').innerHTML="";
              $('#datos').append(html);
                for (let index = 0; index < data.respuestas.length; index++) {
                     html +='<tr><td>'+data.respuestas[index].nombre+'</td><td>'+data.respuestas[index].apellidos+'</td><td>'+data.respuestas[index].respuestas+'</td><td>'+data.respuestas[index].created_at+'</td></tr>' ;
                    
                }
                $('#datos').append(html);
                var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.respuestas.length+' Respuestas. Nota: La información entregada en la presente tabla, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              $('#reporte').append(html);
              }  
            }       
          });
        
    }

}

</script>



@endsection
