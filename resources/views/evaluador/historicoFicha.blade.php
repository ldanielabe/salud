@extends('layouts.evaluador')

@section('content')


<div class="content">
    <div class="container-fluid" >
    <h2 id="nombre" style="text-align: center;font-weight: bold;">Historial del Estudiante {{$estudiante->nombre}}</h2>
    <br>
    <br>
    <div class="row">
    @for ($i = 0; $i < sizeof($ficha); $i++)
    <div class="col-12 col-md-4 animate__animated animate__fadeInUpBig">
            
            <div class="card noti" style="width: 80%; height: 90%; margin-left: auto;
            margin-right: auto; margin-bottom: 2px; border-color: #2196F3!important;color: #000!important;
    background-color: #ddffff!important;">
                <img class="card-img-top" src="../imagesNoticias/estudiante.png"  alt="Card image cap" style="width: 150px; height: 150px;display:block;
margin:auto;">
                <div  class="card-body"  >
                <p class="card-text"></p>
                <p >Ficha de Salud de la fecha {{$ficha[$i]->created_at}}</p>
                <p>Observaciones: {{$ficha[$i]->observaciones}}</p>
                <a href="{{ url('/buscar_pdf_ficha_salud/') }}/{{$estudiante->id}}/{{$ficha[$i]->id}}" >Ver mas...</a>
                </div>
              </div>
   <br>
    </div>
    @endfor
    @if(sizeof($ficha)<=0)
    <br>
    <div class="alert alert-danger" role="alert" style="width: 100%;
    text-align: center;font-size: 18px;
    font-weight: bold;">No se ha valorizado al estudiante</div>
    </div>
    @endif
    
<br>
<br>

<div class="col-md-12" style="height: 50px;text-align:center;">
      <a class="btn btn-primary" href="{{ route('nueva_ficha',Crypt::encrypt($estudiante->id))}} " >Nueva Evaluación</a>
    </div>

    </div>
    </div>
</div>
    
<br>
<br>


<script>
function exportar(id,estudiante){
    
    $.ajax({
        type: "GET",
          url: "/historialPDF/"+id,
          data: {},
          success: function (data) {
        
            console.log(data);
            var final;
            var mywindow = window.open('newFile.html', 'PRINT', 'height=800,width=800');
var con=0;
            mywindow.document.write('<html><head><title>' + 'Sistema de Ayuda a la Salud Escolar en las Instituciones Educativas'  + '</title>');
            mywindow.document.write('</head><body >');

            mywindow.document.write('<h1 style="text-align: center;">'+'Ficha de salud del estudiante '+ estudiante +'</h1>');
            mywindow.document.write('<p style="text-align: center;">'+'Fecha: </p>');

            for(let k = 0; k < data.dominios.length; k++) {
                mywindow.document.write('<h3 >'+data.dominios[k].nombre+'</h3>');

            for (let i = 0; i < data.ficha.length; i++) {
                if(data.dominios[k].id==data.ficha[i].id_dominio){
con++;
                
                var pregunta=data.ficha[i].pregunta;
                mywindow.document.write('<h4>'+con+". "+data.ficha[i].pregunta+'</h4>');
               if(data.ficha[i].tipo_input=="select" || data.ficha[i].tipo_input=="si_no"){

                    mywindow.document.write('<p>'+data.ficha[i].respuestas+'</p>');
                
                }else if(data.ficha[i].tipo_input=="crecimiento"){

                    mywindow.document.write('<p>'+data.ficha[i].respuestas+'</p>');
                }
                else{

               while(pregunta==data.ficha[i+1].pregunta){
               
                mywindow.document.write('<p>'+data.ficha[i].respuestas+'</p>');
                i++;
               }
               mywindow.document.write('<p>'+data.ficha[i].respuestas+'</p>');

                }
         
            }    
            }
           
  

  for (let j = 0; j < data.observaciones.length; j++) {
    if(data.observaciones[j].id_dominio==data.dominios[k].id){
          
  mywindow.document.write('<p >'+'Observaciones del dominio:' +'</p>');
        mywindow.document.write('<p >'+data.observaciones[j].descripcion+'</p>');
     
        mywindow.document.write('<p >'+data.observaciones[j].valorizacion+'</p>');
        break;
    }
  }
  


          }




            final=data.ficha[data.ficha.length-1].id_dominio;  
      
  
mywindow.document.write('<h3 >'+'Observaciones Globales: ' +'</h3>');
 
mywindow.document.write('<p >'+data.ficha[final].observaciones +'</p>');



            mywindow.document.write('</body></html>');

mywindow.document.close(); // necessary for IE >= 10
mywindow.focus(); // necessary for IE >= 10*/

mywindow.print();
mywindow.close();


           // md.showNotification('top','right','Dominios Actualizados');
            }         
        });


}

</script>





@endsection