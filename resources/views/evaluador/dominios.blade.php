@extends('layouts.evaluador')

@section('content')
<style>
html {
  scroll-behavior: smooth;
  touch-action: auto;
}
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 20%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

span.apexcharts-tooltip-text-label, .apexcharts-tooltip-title, .apexcharts-xaxistooltip.apexcharts-xaxistooltip-top.apexcharts-theme-light.apexcharts-active {
    display: none;
}

</style>


<div class="content" style="background-color: #E3E3E3;">
<br>
<h2 style="text-align: center;font-weight: bold;">Dominios {{auth()->user()->institucio->nombre_insti}}</h2>
<div class="container-fluid">

  <div class="row" style="margin-left:20px;padding-left:20px;">
      <div class="col-sm-1 col-md-">
      </div>
    <div class="col-12 col-sm-4 col-md-4">

     <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle btn btn-info animate__animated animate__bounceInUp" title="PROMOCIÓN DE LA SALUD" style="background-color: #9c27b0;width: 140px; " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dominio 1
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="font-size: 16px;">
          <a class="dropdown-item" href="#grafica1"  style="font-size: 16px;">Estado general de la higiene del escolar</a>
          <a class="dropdown-item" href="#grafica2"  style="font-size: 16px;">Practicas de cuidado</a>
          <a class="dropdown-item" href="#grafica3" style="font-size: 16px;">Cuidadores del menor cuando decae la salud</a>
          <a class="dropdown-item" href="#grafica4" style="font-size: 16px;">Presencia de fumadores en el hogar</a>
          <a class="dropdown-item" href="#grafica5" style="font-size: 16px;">Presencia de consumidores de alcohol</a>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-3 col-md-3">
        <div class="dropdown">
            <button title="NUTRICIÓN" class="btn btn-secondary dropdown-toggle btn btn-info animate__animated animate__bounceInUp" title="PROMOCIÓN DE LA SALUD" style="background-color: #9c27b0;width: 140px; " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dominio 2
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="font-size: 16px;">
              <a class="dropdown-item" href="#grafica6"  style="font-size: 16px;">Frecuencia de raciones de comidas</a>
              <a class="dropdown-item" href="#grafica7"  style="font-size: 16px;">Problemas de crecimiento</a>
              <a class="dropdown-item" href="#grafica8"  style="font-size: 16px;">Precensia de anemia</a>
              <a class="dropdown-item" href="#grafica9" style="font-size: 16px;">Salud oral</a>
              <a class="dropdown-item" href="#grafica10"  style="font-size: 16px;">Frecuencia de cepillado al dia en los escolares</a>
            </div>
          </div>
    </div>
    <div class="col-sm-1 col-md-">
      </div>
    <div class="col-12 col-sm-3 col-md-3">
       
            <div class="dropdown">
                <button title="ELIMINACION E INTERCAMBIO" class="btn btn-secondary dropdown-toggle btn btn-info animate__animated animate__bounceInUp" title="PROMOCIÓN DE LA SALUD" style="background-color: #9c27b0;width: 140px; " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dominio 3
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="font-size: 16px;">
                  <a class="dropdown-item" href="#grafica11"  style="font-size: 16px;">Presencia de alteraciones gastrointetinales y urinarias en escolares</a>
                  <a class="dropdown-item" href="#grafica12"  style="font-size: 16px;">Practicas de cuidado del menor</a>
                </div>
              </div>
       
    </div>
    </div>

  <br>


  
<div class="row">
    <div id="grafica1" class="col-md-12 animate__animated animate__backInLeft container" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart1" ></div>
    </div>
    
    </div>
</div>
<br>
<div class="row">
    <div id="grafica2" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart2" ></div>
    </div>
    
    </div>
    </div>
    <br>
    <div class="row">
    <div id="grafica3" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart3" ></div>
    </div>
    
    </div>
  </div>
  <br>
  <div class="row">
    <div id="grafica4" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart" ></div>
    </div>
    
    </div>
  
    <div id="grafica5" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart2" ></div>
    </div>
    </div>
    </div>
    <br>
    <div class="row">

    
    <div id="grafica6" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart4" ></div>
    </div>
    
    </div>
</div>
<br>
<div class="row">
    <div id="grafica7" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart5" ></div>
    </div>
    
    </div>

    <div id="grafica8" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart6" ></div>
    </div>
    
    </div>
    </div>
<br>

    <div class="row">
    <div id="grafica9" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart7" ></div>
    </div>
    
    </div>
    </div>
    <br>
    <div class="row">
    <div id="grafica10" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart8" ></div>
    </div>
    
    </div>
    </div>
    <br>
    <div class="row">
    <div id="grafica11" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart9" ></div>
    </div>
    
    </div>
    <div id="grafica12" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart3" ></div>
    </div>
    
    </div>

  </div>
<br>
<br>
  <div class="row" style="margin-left:20px;padding-left:20px;">
    

  <div class="col-md-2">
    
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label class="bmd-label">Rango Inicio:</label>
        <br>
        <input type="month" id="inicio" class="form-control" >
      </div>
    </div>
    <div class="col-md-2">
    
    </div>

    <div class="col-md-3">
      <div class="form-group">
        <label class="bmd-label">Rango Fin:</label>
        <br>
        <input type="month" id="fin" class="form-control" >
      </div>
    </div>
    <div class="col-md-12" style="height: 50px;text-align:center;">
      <button class="btn btn-primary" onclick="enviarDatos()">Enviar</button>
    </div>
   
    <div class="col-md-12">
      <br>
      <div class="alert alert-success" style="position: relative;padding:10px;   ">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        
        <p style="text-align: center">Escoja un rango de fecha de mes a mes</p> 
      </div>
    </div>

   
  </div>


</div>


<script>



var oidos = {!! json_encode($estadoHigiene['oidos']) !!};
var ojos = {!! json_encode($estadoHigiene['ojos']) !!};
var fosas_nasales = {!! json_encode($estadoHigiene['fosas_nasales']) !!};
var cavidad_oral = {!! json_encode($estadoHigiene['cavidad_oral']) !!};
var manos_pies = {!! json_encode($estadoHigiene['manos_pies']) !!};
var vestimenta = {!! json_encode($estadoHigiene['vestimenta']) !!};
var colegio_higiene = {!! json_encode($estadoHigiene['colegio']) !!};
var datos;
var label;
var datos2;
var label2;
var practicas_cuidado = {!! json_encode($practicasCuidado['practicas_cuidado']) !!};
var practicas_colegio = {!! json_encode($practicasCuidado['colegio']) !!};
var cuidadores = {!! json_encode($cuidadores['cuidadores']) !!};
var cuidadores_colegio = {!! json_encode($cuidadores['colegio']) !!};
var presencia_fumadores = {!! json_encode($fumadores['presencia_fumadores']) !!};
var presencia_fumadores_colegio = {!! json_encode($fumadores['colegio']) !!};
var presencia_alcoholicos = {!! json_encode($alcoholicos['presencia_alcoholicos']) !!};
var presencia_alcoholicos_colegio = {!! json_encode($alcoholicos['colegio']) !!};
var desayuno = {!! json_encode($raciones_comida['desayuno']) !!};
var media_manana = {!! json_encode($raciones_comida['media_manana']) !!};
var almuerzo = {!! json_encode($raciones_comida['almuerzo']) !!};
var onces = {!! json_encode($raciones_comida['onces']) !!};
var cena = {!! json_encode($raciones_comida['cena']) !!};
var raciones_colegio = {!! json_encode($raciones_comida['colegio']) !!};
var anemia = {!! json_encode($anemia['anemia']) !!};
var anemia_colegio = {!! json_encode($anemia['colegio']) !!};
var gingivitis = {!! json_encode($salud_oral['gingivitis']) !!};
var caries = {!! json_encode($salud_oral['caries']) !!};
var dolor = {!! json_encode($salud_oral['dolor']) !!};
var salud_oral_colegio = {!! json_encode($salud_oral['colegio']) !!};
var cepillado = {!! json_encode($cepillado['cepillado']) !!};
var cepillado_colegio = {!! json_encode($cepillado['colegio']) !!};
var alt_gastrointestinales = {!! json_encode($alt_gastrointestinales) !!};
var estado_nutricional = {!! json_encode($estado_nutricional['estado_nutricional']) !!};
var estado_nutricional_colegio = {!! json_encode($estado_nutricional['colegio']) !!};


if(oidos.length>1){
  datos= [oidos[0].cantidad,oidos[1].cantidad];
}else{
  if(oidos.length==0){
    datos= [0,0];
  }else{
  if(oidos[0].oidos=="Adecuado"){
    datos= [oidos[0].cantidad,0];
  }else{
    datos= [0,oidos[0].cantidad];
  }
  }
}

if(ojos.length>1){
  datos2= [ojos[0].cantidad,ojos[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(ojos.length==0){
    datos= [0,0];
  }else{
  if(ojos[0].ojos=="Adecuado"){
    datos2= [ojos[0].cantidad,0];
  }else{
    datos2= [0,ojos[0].cantidad];
  }
  }
 
  datos= datos.concat(datos2);
 
}

if(fosas_nasales.length>1){
  datos2= [fosas_nasales[0].cantidad,fosas_nasales[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(fosas_nasales.length==0){
    datos= [0,0];
  }else{
  if(fosas_nasales[0].fosas_nasales=="Adecuado"){
    datos2= [fosas_nasales[0].cantidad,0];
  }else{
    datos2= [0,fosas_nasales[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}

if(cavidad_oral.length>1){
  datos2= [cavidad_oral[0].cantidad,cavidad_oral[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(cavidad_oral.length==0){
    datos= [0,0];
  }else{
  if(cavidad_oral[0].cavidad_oral=="Adecuado"){
    datos2= [cavidad_oral[0].cantidad,0];
  }else{
    datos2= [0,cavidad_oral[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}
if(manos_pies.length>1){
  datos2= [manos_pies[0].cantidad,manos_pies[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(manos_pies.length==0){
    datos= [0,0];
  }else{
  if(manos_pies[0].manos_pies=="Adecuado"){
    datos2= [manos_pies[0].cantidad,0];
  }else{
    datos2= [0,manos_pies[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}
if(vestimenta.length>1){
  datos2= [vestimenta[0].cantidad,vestimenta[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(vestimenta.length==0){
    datos= [0,0];
  }else{
  if(vestimenta[0].vestimenta=="Adecuado"){
    datos2= [vestimenta[0].cantidad,0];
  }else{
    datos2= [0,vestimenta[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}

//-----------------------------

var colors=["#2689d6","#26e7a6","#febc3b","#ff6178","#ff6178"];
 
        var options = {
          series: [{
          name: 'cantidad',
          data: datos

        }],
          annotations: {
          points: [{
            x: 'Bananas',
            seriesIndex: 0,
            label: {
              borderColor: '#775DD0',
              offsetY: 0,
              style: {
                color: '#fff000',
                background: '#115DD1',
              },
              text: 'Bananas are good',
            }
          }]
        },
        chart: {
          height: 350,
          type: 'bar',
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '50%',
            endingShape: 'rounded'  
          }
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
          width: 2
        },
        
        grid: {
          row: {
            colors: ['#fff', '#f2f2f2']
          }
        },
        xaxis: {
          labels: {
            rotate: -45
          },
          categories:  ["Oidos Adecuados", "Oidos Inadecuados","Ojos Adecuados", "Ojos Inadecuados","Fosas Adecuados", "Fosas Inadecuados","Cavidad Adecuados", "Cavidad Inadecuados","Manos Adecuados", "Manos Inadecuados","Vestimenta Adecuados", "Vestimenta Inadecuados"], 
          title: {
          text: 'Estado del higiene escolar '+ colegio_higiene,
          floating: true,
          align: 'center',
          style: {
            color: '#444'
          }
        },
          tickPlacement: 'on'
        },
        yaxis: {
         
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        },
        tooltip: {
          y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              if(value==1){
                return value + " estudiante tiene: " + w.config.xaxis.categories[dataPointIndex];
              }else{
                console.log(w);
                return value + " estudiantes tienen: " +  w.config.xaxis.categories[dataPointIndex];
              }
              
            }
          }
        }
        };

        var mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
        mychart1.render();

//------------------------------------------------

var label_p=['si','no'];
var datos_p=[0,0];
var cantidad_c=0;
for (let index = 0; index < alt_gastrointestinales.practicas.length; index++) {
    cantidad_c+= alt_gastrointestinales.practicas[index].cantidad;
 
  
}


for (let index = 0; index < alt_gastrointestinales.practicas.length; index++) {
    if(index==0){
        if(alt_gastrointestinales.practicas[index].practicas_cuidado_escolar==1){
          label_p= ["Si"];
        }else{
          label_p= ["No"];
        }
     
        datos_p =[alt_gastrointestinales.practicas[index].cantidad];
    }else{
      if(alt_gastrointestinales.practicas[index].practicas_cuidado_escolar==1){
          label2= ["Si"];
        }else{
          label2= ["No"];
        }
      let datos2 =[alt_gastrointestinales.practicas[index].cantidad];
      datos_p=datos_p.concat(datos2);
      label_p=label_p.concat(label2);
    }
    
  }

  var options = {
          series: datos_p,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {
          text: ' Practicas de cuidado en el escolar, Total:'+cantidad_c
        },
        labels: label_p,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }],
        tooltip: {
          y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              if(value==1){
                return value + " estudiante: " + w.config.xaxis.categories[dataPointIndex];
              }else{
                console.log(w);
                return value + " estudiantes: " +  w.config.xaxis.categories[dataPointIndex];
              }
              
            }
          }
        }
        };
        var piechart3 = new ApexCharts(document.querySelector("#pie-chart3"), options);
        piechart3.render();



        var URLactual = document.location+'';
var res=URLactual.split("/");
var u=res[res.length-1].split("#");
var id=u[0];




//----------------------------------------------------
function enviarDatos(){
  if(document.getElementById("inicio").value>document.getElementById("fin").value){
    md.showNotification('top','right','La fecha de inicio no puede ser mayor a la fecha fin');
  }else{
    var inicio=document.getElementById("inicio").value+"-01";
    var fin=document.getElementById("fin").value+"-01";
    $.ajax({
        type: "GET",
          url: "/dominiosRango/"+inicio+"/"+fin+"/"+id,
          data: {},
          success: function (data) {
            practicas_cuidado=data.practicasCuidado.practicas_cuidado;
            practicas_colegio=data.practicasCuidado.colegio;
            dominioUno();
             oidos = data.higiene.oidos;
             ojos = data.higiene.ojos;
             fosas_nasales = data.higiene.fosas_nasales;
             cavidad_oral = data.higiene.cavidad_oral;
             manos_pies = data.higiene.manos_pies;
             vestimenta = data.higiene.vestimenta;
             colegio_higiene = data.higiene.colegio;
             cuidadores = data.cuidadores.cuidadores;
             cuidadores_colegio = data.cuidadores.colegio;
             dominioUnoCuidadores();
             presencia_fumadores = data.fumadores.presencia_fumadores;
             presencia_fumadores_colegio = data.fumadores.colegio;
             dominioUnoFumadores();
             presencia_alcoholicos = data.alcoholicos.presencia_alcoholicos;
             presencia_alcoholicos_colegio = data.alcoholicos.colegio;
             dominioUnoAlcoholicos();
             desayuno = data.raciones_comida.desayuno;
             media_manana = data.raciones_comida.media_manana;
             almuerzo = data.raciones_comida.almuerzo;
             onces = data.raciones_comida.onces;
             cena = data.raciones_comida.cena;
             raciones_colegio = data.raciones_comida.colegio;
             dominioDosRaciones();
             anemia = data.anemia.anemia;
             anemia_colegio = data.anemia.colegio;
             dominioDosAnemia();
             gingivitis = data.salud_oral.gingivitis;
             caries = data.salud_oral.caries;
             dolor = data.salud_oral.dolor;
             salud_oral_colegio = data.salud_oral.colegio;
             dominioDosSaludOral();
             cepillado = data.cepillado.cepillado;
             cepillado_colegio = data.cepillado.colegio;
             dominioDosCepillado();
             alt_gastrointestinales= data.alt_gastrointestinales;
             dominioTresAlt();
             datos=0;
             datos2=0;
if(oidos.length>1){
  datos= [oidos[0].cantidad,oidos[1].cantidad];
}else{
  if(oidos.length==0){
    datos= [0,0];
  }else{
  if(oidos[0].oidos=="Adecuado"){
    datos= [oidos[0].cantidad,0];
  }else{
    datos= [0,oidos[0].cantidad];
  }
  }
}

if(ojos.length>1){
  datos2= [ojos[0].cantidad,ojos[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(ojos.length==0){
    datos= [0,0];
  }else{
  if(ojos[0].ojos=="Adecuado"){
    datos2= [ojos[0].cantidad,0];
  }else{
    datos2= [0,ojos[0].cantidad];
  }
  }
 
  datos= datos.concat(datos2);
 
}

if(fosas_nasales.length>1){
  datos2= [fosas_nasales[0].cantidad,fosas_nasales[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(fosas_nasales.length==0){
    datos= [0,0];
  }else{
  if(fosas_nasales[0].fosas_nasales=="Adecuado"){
    datos2= [fosas_nasales[0].cantidad,0];
  }else{
    datos2= [0,fosas_nasales[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}

if(cavidad_oral.length>1){
  datos2= [cavidad_oral[0].cantidad,cavidad_oral[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(cavidad_oral.length==0){
    datos= [0,0];
  }else{
  if(cavidad_oral[0].cavidad_oral=="Adecuado"){
    datos2= [cavidad_oral[0].cantidad,0];
  }else{
    datos2= [0,cavidad_oral[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}
if(manos_pies.length>1){
  datos2= [manos_pies[0].cantidad,manos_pies[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(manos_pies.length==0){
    datos= [0,0];
  }else{
  if(manos_pies[0].manos_pies=="Adecuado"){
    datos2= [manos_pies[0].cantidad,0];
  }else{
    datos2= [0,manos_pies[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}
if(vestimenta.length>1){
  datos2= [vestimenta[0].cantidad,vestimenta[1].cantidad];
  datos=datos.concat(datos2);
}
else{
  if(vestimenta.length==0){
    datos= [0,0];
  }else{
  if(vestimenta[0].vestimenta=="Adecuado"){
    datos2= [vestimenta[0].cantidad,0];
  }else{
    datos2= [0,vestimenta[0].cantidad];
  }
  }
  datos= datos.concat(datos2);
}

mychart1.destroy();

var options = {
          series: [{
          name: 'cantidad',
          data: datos

        }],
          annotations: {
          points: [{
            x: 'Bananas',
            seriesIndex: 0,
            label: {
              borderColor: '#775DD0',
              offsetY: 0,
              style: {
                color: '#fff000',
                background: '#115DD1',
              },
              text: 'Bananas are good',
            }
          }]
        },
        chart: {
          height: 350,
          type: 'bar',
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '50%',
            endingShape: 'rounded'  
          }
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
          width: 2
        },
        
        grid: {
          row: {
            colors: ['#fff', '#f2f2f2']
          }
        },
        xaxis: {
          labels: {
            rotate: -45
          },
          categories:  ["Oidos Adecuados", "Oidos Inadecuados","Ojos Adecuados", "Ojos Inadecuados","Fosas Adecuados", "Fosas Inadecuados","Cavidad Adecuados", "Cavidad Inadecuados","Manos Adecuados", "Manos Inadecuados","Vestimenta Adecuados", "Vestimenta Inadecuados"], 
          title: {
          text: 'Estado del higiene escolar '+ colegio_higiene,
          floating: true,
          align: 'center',
          style: {
            color: '#444'
          }
        },
          tickPlacement: 'on'
        },
        yaxis: {
         
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        },
        tooltip: {
          y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
              if(value==1){
                return value + " estudiante: " + w.config.xaxis.categories[dataPointIndex];
              }else{
                console.log(w);
                return value + " estudiantes: " +  w.config.xaxis.categories[dataPointIndex];
              }
              
            }
          }
        }
        };


         mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
        mychart1.render();
     
        
          md.showNotification('top','right','Dominios Actualizados');
            }         
        });
  }




}
</script>
<script src="{{ asset('js/dominios/dominioUno.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/dominios/dominioDos.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/dominios/dominioTres.js')}}" type="text/javascript"></script>

@endsection