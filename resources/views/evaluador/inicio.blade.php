@extends('layouts.evaluador')

@section('content')
<style>
html {
  scroll-behavior: smooth;
  touch-action: auto;
}
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 20%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}


</style>

<div class="content" style="background-color: #E3E3E3;">
<br>
<h2 style="text-align: center;font-weight: bold;">Estadisticas {{$inst[0]->nombre_insti}}</h2>
<div class="container-fluid">

  
  <select id="selectGrado" class="custom-select" onchange="grados()">
        <option  value="-1">(Opcional) Seleccione un Grado</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="1">6</option>
        <option value="2">7</option>
        <option value="3">8</option>
        <option value="4">9</option>
        <option value="5">10</option>
        <option value="5">11</option>
    </select>
    
    <div class="row" style="">
    

 
    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Inicio:</label>
        <br>
        <input type="month" id="inicio" class="form-control" >
      </div>
    </div>
   

    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Fin:</label>
        <br>
        <input type="month" id="fin" class="form-control" >
      </div>
    </div>

    <div class="col-md-12" style="height: 50px;text-align:center;">
      <button class="btn btn-primary" onclick="enviarDatos()">Enviar</button>
    </div>
   
   

   
  </div>
 
    
  </div>

  <br>
<div class="row " >
    <div id="grafica1" class="col-md-6 animate__animated animate__backInLeft container " >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart" ></div>
    </div>
     
    </div>
  
    <div id="grafica2" class="col-md-6 animate__animated animate__backInLeft container " >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart2" ></div>
    </div>
    
    </div>
</div>
<br>
<br>
<div class="row">
    <div id="grafica3" class="col-md-6 animate__animated animate__backInLeft"  >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart"></div>
    </div>
    
    </div>
    <div id="grafica4" class="col-md-6 animate__animated animate__backInLeft" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart2"></div>
    </div>
    
    </div>
</div>
<br>
<br>
<div class="row">
    <div id="grafica5" class="col-md-6" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart3"></div>
    </div>
    
    </div>

    <div id="grafica6" class="col-md-6"  >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="pie-chart4"></div>
    </div>
    
    </div>
</div>
<br>
<br>
<div class="row">
    <div id="grafica7" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart3"></div>
    </div>
    
    </div>
</div>
    <br>
    <div class="row">
    <div id="grafica8" class="col-md-12"  >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart4"></div>
    </div>
    
    </div>

</div>
<br>
<br>
<div class="row">
    <div id="grafica9" class="col-md-12"  >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart5"></div>
    </div>
    
    </div>
    </div>
    <br>
    <div class="row">
    <div id="grafica10" class="col-md-12"  >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart6"></div>
    </div>
    
    </div>

   

  </div>
  <br>
<br>
<div class="row">
<div id="grafica11" class="col-md-12" >
      <div class="chart-container card" style="position: relative; height:100%; width:90%; text-align: center; margin-left: auto;margin-right: auto;">
        <div id="bar-chart7"></div>
    </div>
    
    </div>

</div>
<br>
<br>
 


</div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm;



  var edad = {!! json_encode($edad['edad']) !!};
  var cantidad = {!! json_encode($edad['cantidad']) !!};
  var colegio = {!! json_encode($edad['colegio']) !!};
  var grado = {!! json_encode($grado['grado']) !!};
  var cantidad_grado = {!! json_encode($grado['cantidad']) !!};
  var regimen = {!! json_encode($regimen['tipo']) !!};
  var cantidad_regimen = {!! json_encode($regimen['cantidad']) !!};
  var jornada = {!! json_encode($jornada['tipo']) !!};
  var cantidad_jornada = {!! json_encode($jornada['cantidad']) !!};
  var colegio_jornada = {!! json_encode($jornada['colegio']) !!};
  var residencia = {!! json_encode($residencia['tipo']) !!};
  var cantidad_residencia = {!! json_encode($residencia['cantidad']) !!};
  var colegio_residencia = {!! json_encode($residencia['colegio']) !!};
  var barrio = {!! json_encode($barrio['tipo']) !!};
  var cantidad_barrio = {!! json_encode($barrio['cantidad']) !!};
  var colegio_barrio = {!! json_encode($barrio['colegio']) !!};
  var patologicos = {!! json_encode($patologicos['tipo']) !!};
  var cantidad_patologicos = {!! json_encode($patologicos['cantidad']) !!};
  var colegio_patologicos = {!! json_encode($patologicos['colegio']) !!};
  var quirurgicos = {!! json_encode($quirurgicos['tipo']) !!};
  var cantidad_quirurgicos = {!! json_encode($quirurgicos['cantidad']) !!};
  var colegio_quirurgicos = {!! json_encode($quirurgicos['colegio']) !!};
  var traumaticos = {!! json_encode($traumaticos['tipo']) !!};
  var cantidad_traumaticos = {!! json_encode($traumaticos['cantidad']) !!};
  var colegio_traumaticos = {!! json_encode($traumaticos['colegio']) !!};
  var alergicos = {!! json_encode($alergicos['tipo']) !!};
  var cantidad_alergicos = {!! json_encode($alergicos['cantidad']) !!};
  var colegio_alergicos = {!! json_encode($alergicos['colegio']) !!};
  var genero = {!! json_encode($genero) !!};
  
var colors=["#2689d6","#26e7a6","#febc3b","#ff6178","#ff6178"];
  var options = {
          series: [{
            name: 'Cantidad',
          data: cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: edad,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'F edad,'+colegio,
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };


        var chart1 = new ApexCharts(document.querySelector("#bar-chart"), options);
        chart1.render();
//-----------------
var options2 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_grado
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: grado,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'F grados,'+colegio,
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };


        var chart2 = new ApexCharts(document.querySelector("#bar-chart2"), options2);
        chart2.render();

//--------------------------

var options3 = {
          series: genero.cantidad,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {
          text: 'Frecuencia por genero,'
        },
        labels: genero.gen,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };
        var chart3 = new ApexCharts(document.querySelector("#pie-chart"), options3);
        chart3.render();

//-----------------------------------------------------------

var options = {
          series: cantidad_regimen,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {
          text: 'Regimen de salud,'
        },
        labels: regimen,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };

        var chart4 = new ApexCharts(document.querySelector("#pie-chart2"), options);
        chart4.render();

//-----------------------------------------------------------

var options = {
          series: cantidad_jornada,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {   
        text: 'Proporcion segun jornada,'
        },
        labels: jornada,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };

        var chart5 = new ApexCharts(document.querySelector("#pie-chart3"), options);
        chart5.render();
  
//------------------------------------------------------------------------


var options = {
          series: cantidad_residencia,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {   
          text: 'Area de residencia,'
        },
        labels: residencia,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };

        var chart6 = new ApexCharts(document.querySelector("#pie-chart4"), options);
        chart6.render();

//-------------------------------------
var options4 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_barrio
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: barrio,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Barrio de residencia ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        var chart7 = new ApexCharts(document.querySelector("#bar-chart3"), options4);
        chart7.render();
//--------------------------------------------------------------------------

var options5 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_patologicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: patologicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes patologicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        var myChart1 = new ApexCharts(document.querySelector("#bar-chart4"), options5);
        myChart1.render();

//------------------------------------------------------

var options6 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_quirurgicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: quirurgicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes quirurgicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        var myChart2 = new ApexCharts(document.querySelector("#bar-chart5"), options6);
        myChart2.render();
//---------------------


var options7 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_traumaticos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: traumaticos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes traumaticos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        var myChart3 = new ApexCharts(document.querySelector("#bar-chart6"), options7);
        myChart3.render();
//----------------------------------------------------------



var options8 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_alergicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: alergicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia de antecedentes alergicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        var myChart4 = new ApexCharts(document.querySelector("#bar-chart7"), options8);
        myChart4.render();
//----------------------------------------------------------------------------
var URLactual = document.location+'';
var res=URLactual.split("/");
var u=res[res.length-1].split("#");
var id=u[0];
function enviarDatos(){
  if(document.getElementById("inicio").value>document.getElementById("fin").value){
    md.showNotification('top','right','La fecha de inicio no puede ser mayor a la fecha fin');
  }else if(document.getElementById("inicio").value!="" && document.getElementById("fin").value!=""){
var inicio=document.getElementById("inicio").value+"-01";
var fin=document.getElementById("fin").value+"-01";
     $.ajax({
        type: "GET",
          url: "/antecedentesRango/"+inicio+"/"+fin+"/"+id,
          data: {},
          success: function (data) {
      

           patologicos = data.patologicos.tipo;
           cantidad_patologicos =data.patologicos.cantidad;
           colegio_patologicos = data.patologicos.colegio;
           
var options5 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_patologicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: patologicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes patologicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart1.destroy();
         myChart1 = new ApexCharts(document.querySelector("#bar-chart4"), options5);
      
        myChart1.render();
         //----------------------------------------
          
     quirurgicos = data.quirurgicos.tipo;
     cantidad_quirurgicos = data.quirurgicos.cantidad;
     colegio_quirurgicos = data.quirurgicos.colegio;
      
    
var options6 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_quirurgicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: quirurgicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes quirurgicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart2.destroy();
        myChart2 = new ApexCharts(document.querySelector("#bar-chart5"), options6);
        myChart2.render();
        //-----------------------------------------------
          
     traumaticos = data.traumaticos.tipo;
     cantidad_traumaticos = data.traumaticos.cantidad;
     colegio_traumaticos = data.traumaticos.colegio;
   
var options7 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_traumaticos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: traumaticos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes traumaticos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart3.destroy();
       myChart3 = new ApexCharts(document.querySelector("#bar-chart6"), options7);
        myChart3.render();
  //---------------------------------------
     alergicos = data.alergicos.tipo;
     cantidad_alergicos = data.alergicos.cantidad;
     colegio_alergicos = data.alergicos.colegio; 
    
var options8 = {
          series: [{
            name: 'Cantidad',
          data: cantidad_alergicos
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: alergicos,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
    title: {
  text: 'Frecuencia de antecedentes alergicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart4.destroy();
        myChart4 = new ApexCharts(document.querySelector("#bar-chart7"), options8);
        myChart4.render();
        
          md.showNotification('top','right','Antecedentes Actualizados');
            }         
        });

}else{
  md.showNotification('top','right','Escoja un rango de mes a mes');

}
}

function grados(){
  var grado = document.getElementById('selectGrado').value;
  if(grado=="-1"){
  grado=0;
  }else if(grado!="-1" && document.getElementById("inicio").value =="" && document.getElementById("fin").value ==""){
    $.ajax({
          type: "GET",
            url: "/reporteAntecedentesGrado/0/0/"+grado+"/"+id,
            data: {},
            success: function (data) {
              var options = {
          series: [{
            name: 'Cantidad',
          data: data.edad.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.edad.edad,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia edad, grado '+grado,
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };

       chart1.destroy();
        chart1 = new ApexCharts(document.querySelector("#bar-chart"), options);
        chart1.render();
       
              console.log(data);

        
var options3 = {
          series: data.genero.cantidad,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {
          text: 'Frecuencia por genero,'
        },
        labels: data.genero.gen,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };
        chart3.destroy();
       chart3 = new ApexCharts(document.querySelector("#pie-chart"), options3);
        chart3.render();

//-----------------------------------------------------------

var options = {
          series: data.regimen.cantidad,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {
          text: 'Regimen de salud,'
        },
        labels: data.regimen.tipo,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };
        chart4.destroy();
        chart4 = new ApexCharts(document.querySelector("#pie-chart2"), options);
        chart4.render();

//-----------------------------------------------------------

var options = {
          series: data.jornada.cantidad,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {   
        text: 'Proporcion segun jornada,'
        },
        labels: data.jornada.tipo,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };
        chart5.destroy();
         chart5 = new ApexCharts(document.querySelector("#pie-chart3"), options);
        chart5.render();
  
//------------------------------------------------------------------------


var options = {
          series: data.residencia.cantidad,
          chart: {
          width: '100%',
          type: 'pie',
        },
        plotOptions: {
          pie: {
            dataLabels: {
              offset: -5
            }
          }
        },
        title: {   
          text: 'Area de residencia,'
        },
        labels: data.residencia.tipo,
        responsive: [{
          breakpoint: 280,
          options: {
            chart: {
              width: 100
            },
            legend: {
              show: false
            }
          }
        }]
        };
        chart6.destroy();
         chart6 = new ApexCharts(document.querySelector("#pie-chart4"), options);
        chart6.render();

//-------------------------------------
var options4 = {
          series: [{
            name: 'Cantidad',
          data: data.barrio.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.barrio.tipo,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Barrio de residencia ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        chart7.destroy();
         chart7 = new ApexCharts(document.querySelector("#bar-chart3"), options4);
        chart7.render();
//--------------------------------------------------------------------------

var options5 = {
          series: [{
            name: 'Cantidad',
          data: data.patologicos.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.patologicos.tipo,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes patologicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart1.destroy();
         myChart1 = new ApexCharts(document.querySelector("#bar-chart4"), options5);
        myChart1.render();

//------------------------------------------------------

var options6 = {
          series: [{
            name: 'Cantidad',
          data: data.quirurgicos.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.quirurgicos.tipo,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes quirurgicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart2.destroy();
         myChart2 = new ApexCharts(document.querySelector("#bar-chart5"), options6);
        myChart2.render();
//---------------------


var options7 = {
          series: [{
            name: 'Cantidad',
          data: data.traumaticos.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.traumaticos.tipo,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia antecedentes traumaticos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart3.destroy();
         myChart3 = new ApexCharts(document.querySelector("#bar-chart6"), options7);
        myChart3.render();
//----------------------------------------------------------



var options8 = {
          series: [{
            name: 'Cantidad',
          data: data.alergicos.cantidad
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: data.alergicos.tipo,
          position: 'top',
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
   
        title: {
          text: 'Frecuencia de antecedentes alergicos ',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };
        myChart4.destroy();
        myChart4 = new ApexCharts(document.querySelector("#bar-chart7"), options8);
        myChart4.render();

              }         
          });
          md.showNotification('top','right','Datos Actualizados');

  }
}

</script>


@endsection