@extends($layout_grocery)

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 100%;
  padding-left: 32px;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
hr {
    height: 10px;
  width: 98%;
  background-color: #9c27b0;
  
}
.horizontal {
    background-color: #9c27b0;
    color: rgba(0, 0, 0, 1);
}
.horizontalDos {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    height: 10px;
   
    width: calc(100% + 2px);
}
</style>
<div class="content">
    <div class="container-fluid" >
   <br>
   <h2 id="nombre" style="text-align: center;font-weight: bold;">Observador de los estudiantes agrupadas por tipo, {{ $institucion[0]->nombre_insti }}</h2>
   <br>

   @for ($i = 0; $i < sizeof($tipos); $i++)

<div class="horizontal horizontalDos"> </div>
    <div class="card " style="margin-top: -0.5px;">
        <h4 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$tipos[$i]->id}}" aria-expanded="true" aria-controls="collapseOne" style="color:#000;font-weight: bold;">
        <p style="paddin: 10px;">{{ $tipos[$i]->tipo_falta }}</p>
        </button>
      </h4> 
    </div>
    <div id="collapse{{$tipos[$i]->id}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="">
         <ul class="list-group">
         @for ($k = 0; $k < sizeof($observaciones); $k++)
         @if($observaciones[$k]->tipo_falta == $tipos[$i]->tipo_falta)
            <li class="alert alert-warning" role="alert" style="font-weight: bold;">
            <p>{{$observaciones[$k]->nombre}} {{$observaciones[$k]->apellidos}}</p>
            <p>Observacion: {{$observaciones[$k]->detalles}}</p>
            <p>Fecha: {{$observaciones[$k]->created_at}}</p>
            @if(auth()->user()->rol->id != "5" && auth()->user()->rol->id != "1")
            <a class="btn" href="javascript:void(0)" onclick="resuelta('{{$observaciones[$k]->id}}');" style="background: #5a5a5a;">¿Marcar como resuelta?</a>
            @endif
            </li>
          @endif
         @endfor

          
        </ul>
</div>
    </div>
    @endfor

</div>
    </div>

    <script>
   function resuelta(id){


              Swal.fire({
  title: '¿Marcar como resuelta?',
  showDenyButton: true,
  showCancelButton: true,
  confirmButtonText: `Si`,
  cancelButtonText: `No`,
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
    Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Observacion Resuelta',
                showConfirmButton: false,
                timer: 1500
            })
            $.ajax({
          type: "GET",
            url: "/updateObservador/"+id,
            data: {},
            success: function (data) {
           console.log(data);
              
            }         
          });
            setTimeout(function(){ window.location.reload();  }, 3000);
            
  } else if (result.isDenied) {
    Swal.fire('Ninguna observacion se ha resuelto', '', 'info')
  }
})
        

        }
    
    </script>
@endsection