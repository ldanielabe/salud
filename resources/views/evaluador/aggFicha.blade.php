@extends('layouts.evaluador')

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 100%;
  padding-left: 32px;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
hr {
    height: 10px;
  width: 98%;
  background-color: #9c27b0;
  
}
.horizontal {
    background-color: #9c27b0;
    color: rgba(0, 0, 0, 1);
}
.horizontalDos {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    height: 10px;
   
    width: calc(100% + 2px);
}
</style>
<div class="content">
    <div class="container-fluid" >
   <br>
   <h2 id="nombre" style="text-align: center;font-weight: bold;">Ficha de Salud del Estudiante {{$estudiante->nombre}}</h2>
   <br>
    @for ($i = 0; $i < sizeof($dominios); $i++)

    <div class="horizontal horizontalDos"> </div>
        <div class="card " style="margin-top: -0.5px;">
       
            <h3 style="font-weight: bold;" id="dominio{{$dominios[$i]->id}}">{{ $dominios[$i]->nombre }}</h3>
            <p style="paddin: 10px;">{{ $dominios[$i]->descripcion }}</p>
        </div>

    @for ($j = 0; $j < sizeof($preguntas); $j++)
    @if($preguntas[$j]->id_dominio == $dominios[$i]->id)
        <div class="card" id="valirdar{{$preguntas[$j]->id}}">
          <h4>{{$preguntas[$j]->pregunta}}</h4>
        <form id="pregunta{{$preguntas[$j]->id}}" action=""> 
          @if($preguntas[$j]->tipo_input=="select")
          @for ($k = 0; $k < sizeof($respuestas); $k++)
          @if($respuestas[$k]->id_preguntas==$preguntas[$j]->id)
          <div class="form-check form-check-inline" >
        
          <input class="" type="radio" name="inlineRadioOptions" id="{{$respuestas[$k]->id}}"  value="{{$respuestas[$k]->respuesta}}" checked="checked">
          <label class="form-check-label" style="color:#4a4949;">{{$respuestas[$k]->respuesta}}</label>
          
           <input type="hidden" id="riesgoPregunta{{$respuestas[$k]->id}}" value="{{$respuestas[$k]->riesgo}}">
          </div>
          @endif
          @endfor
          @elseif($preguntas[$j]->tipo_input=="si_no")
         
         
         <div class="form-check form-check-inline" id="pregunta{{$preguntas[$j]->id}}">
           <input class="" type="radio" name="inlineRadioOptions"  value="Si" checked="checked">
           <label class="form-check-label" style="color:#4a4949;">Si</label>
         </div>
         <div class="form-check form-check-inline">
           <input class="" type="radio" name="inlineRadioOptions"  value="No">
           <label class="form-check-label" style="color:#4a4949;">No</label>
         </div>
         @elseif($preguntas[$j]->tipo_input=="crecimiento")
         
        
          <table class="table">
            <tbody>
              <tr> 
               
                <td><input  class="form-control" id="peso" type="text" name="peso" size="6" class="numericonly" placeholder="Peso en KG" required> (ejemplo: 76)</td>
              </tr> 
                <tr>
                <td><input class="form-control" id="altura" type="text" name="altura" size="6" class="numericonly"  placeholder="Altura en metros" required> (ejemplo: 1.80)</td>
                </tr> 
                <tr>  
                  <td><input type="button" class="btn" value="Calcular" onclick="calcula_imc();" style="color: #000;
    background-color: #9c27b0;
    font-weight: bold;"></td>
                </tr> 
            </tbody>
          </table> 
         
          <div id="contenedor"   style="display:none;"> 
          
          </div>
          @elseif($preguntas[$j]->tipo_input=="input")
         
          <div class="form-check" id="pregunta{{$preguntas[$j]->id}}">
              <input type="text" class="form-control" name=""  value="">
          </div>
       
         @else
         
         @for ($k = 0; $k < sizeof($respuestas); $k++)
          @if($respuestas[$k]->id_preguntas==$preguntas[$j]->id)
         <div class="form-check" id="pregunta{{$preguntas[$j]->id}}">
            <input type="checkbox" class="" id="respuesta{{$respuestas[$k]->id}}" value="{{$respuestas[$k]->respuesta}}" checked="checked">
            <label class="form-check-label" for="exampleCheck1" style="color:#4a4949;">{{$respuestas[$k]->respuesta}}</label>
        </div>
        @endif
          @endfor
          @endif
        </div>
    @endif
    </form>
    @endfor
    <div id="valorizacion{{$dominios[$i]->id}}">
    </div>
    <br>
    <div class="form-group">
    <textarea class="form-control" id="obsDominio{{$dominios[$i]->id}}" rows="3" placeholder="¿Alguna Observacion?"></textarea>
  </div>
    @endfor
 <form id="" action="">
 <div class="form-group">
  
    <br>
    <textarea class="form-control" id="obsGlobales" rows="3" placeholder="Si tiene alguna observacion global, por favor escribela."></textarea>
  </div>
 </form>
    </div>

</div>
<div id="resumen" style="display: none;">

<div class="horizontal horizontalDos"> </div>
        <div class="card " style="margin-top: -0.5px;">
       
            <h5 style="font-weight: bold;" >Resumen de las valorizaciones:</h5>
          
        </div>
</div>
<br>


<div style="width:100%; height:100%;margin: -20px -50px;
    position: relative;left: 45%;">
<button class="btn btn-info" onclick="probar();">Guardar Datos</button>
                                                                 
<a id="generar-pdf" class="btn btn-material col-md-3" type="button" href="{{ url('/generar_pdf_ficha_salud/') }}/{{$estudiante->id}}" style="display: none">
        <i class="material-icons">add_circle_outline</i> Generar PDF</a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/jspdf.umd.min.js"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script>
<script>

var data = {!! json_encode($preguntas) !!};
var dominios = {!! json_encode($dominios) !!};
var data3= {!! json_encode($respuestas) !!};
var entro=0;
var crecimiento="";
var altura="";
var peso="";
function probar(){
 
if(entro==0){
  validarDatos();
}else{
 // enviarDatos();
  
    
}
}
//-----------------

function valorizacion(){
  var total=0;
  document.getElementById("resumen").style.display  = "block";
  for (let k = 0; k < dominios.length; k++) {
    total=0;
    for (let i = 0; i < data.length; i++) {
      if(data[i].id_dominio==dominios[k].id){

    var formulario = document.getElementById("pregunta"+data[i].id);
   
   
    var id_pregunta=data[i].id;
    if(data[i].tipo_input=="select"){
     
    for(var j=0; j<formulario.elements.length; j++) {
    var elemento = formulario.elements[j];
      if(elemento.checked){

      total+=parseInt(document.getElementById("riesgoPregunta"+elemento.id).value);
     // console.log(total);
       //riesgoPregunta
        break;
      }
    }
    
    }else if(data[i].tipo_input=="si_no"){
      
      for(var j=0; j<formulario.elements.length; j++) {
    var elemento = formulario.elements[j];
      if(elemento.checked){
        if(elemento.value=="Si"){
          total+=1;
        }
        //console.log(total);
       //
        break;
      }
    }
    

    }
      }
  //
}
if(total==0){
  var mens='La valorización del dominio '+dominios[k].nombre+' es Adecuada';
  var html='<div class="alert alert-success" role="alert" style="text-transform: lowercase;" >'+mens+' </div><input type="hidden" id="valDominio'+dominios[k].id+'" value="'+mens+'">';
 
  var elem=document.getElementById('valorizacion'+dominios[k].id);
  if(elem!=null){
    elem.innerHTML = "";
  $('#valorizacion'+dominios[k].id).append(html);
  $('#resumen').append(html);
  }
  
  
    
}else{
  var mens='La valorización del dominio '+dominios[k].nombre+' es Alterado';
  var html='<div class="alert alert-danger" role="alert" style="text-transform: lowercase;" >'+mens+'</div><input type="hidden" id="valDominio'+dominios[k].id+'" value="'+mens+'">';
 
  var elem=document.getElementById('valorizacion'+dominios[k].id);
  if(elem!=null){
    document.getElementById('valorizacion'+dominios[k].id).innerHTML = "";
  $('#valorizacion'+dominios[k].id).append(html);
  $('#resumen').append(html);
  }
}

  }

  

}

//-----------------------------

function validarDatos(){
  var mensaje=0;
  
  for (let i = 0; i < data.length; i++) {
    var formulario = document.getElementById("pregunta"+data[i].id);
    var validar=0;
    var check=0;
    if(data[i].tipo_input=="select" || data[i].tipo_input=="si_no"){
    
    for(var j=0; j<formulario.elements.length; j++) {
    var elemento = formulario.elements[j];
      if(elemento.checked){
        validar=1;
      
        document.getElementById("valirdar"+data[i].id).style.border="1px solid green";
      }
    }
    if(validar==0){
      
      document.getElementById("valirdar"+data[i].id).style.border="1px solid red";
      mensaje++;
    }
    }else if(data[i].tipo_input=="input" ){
      if(formulario.elements[0].value!=""){
      validar=1;
      document.getElementById("valirdar"+data[i].id).style.border="1px solid green";
      }else{
        document.getElementById("valirdar"+data[i].id).style.border="1px solid red";
      mensaje++;
      }
    }
    else if(data[i].tipo_input=="crecimiento"){
      this.peso = document.getElementById("peso");
   this.altura = document.getElementById("altura");
        if (peso.value.length == 0){ 
          md.showNotification('top','right','El peso es obligatorio');
          mensaje++;
        } 
        else if (altura.value.length == 0){ 
          md.showNotification('top','right','La altura es obligatoria');
          mensaje++;
        }else if(this.crecimiento==""){
          md.showNotification('top','right','Debe presionar el boton de calcular');
          mensaje++;
        }
        else{
          document.getElementById("valirdar"+data[i].id).style.border="1px solid green";
        } 


    }else{

      for(var j=0; j<formulario.elements.length; j++) {
        var elemento = formulario.elements[j];
        if(elemento.type == "checkbox") {
          if(elemento.checked) {
        check++;
          }
        }
      }
      if(check==0){
        document.getElementById("valirdar"+data[i].id).style.border="1px solid red";
      mensaje++;
      }else{
        document.getElementById("valirdar"+data[i].id).style.border="1px solid green";
      }

    }
  
}
if(mensaje!=0){
 
  md.showNotification('top','right','Faltan preguntas por responder.');
  
}else{
  
  $('#generar-pdf').css("display","inline");
  valorizacion();
  enviarDatos();
this.entro++;
}
 
// console.log($('input:radio[id=respuesta72]:checked').val());
}
//---------------------
var URLactual = document.location+'';
var res=URLactual.split("/");
var id_estudiante=res[res.length-1];
var id_ficha=0;

function guardar(id_f){

  for (let i = 0; i < data.length; i++) {
      var formulario = document.getElementById("pregunta"+data[i].id);
      var id_pregunta=data[i].id;
      if(data[i].tipo_input=="select" || data[i].tipo_input=="si_no"){
      
      for(var j=0; j<formulario.elements.length; j++) {
      var elemento = formulario.elements[j];
        if(elemento.checked){
          if(data[i].tipo_input=="select" || data[i].tipo_input=="si_no"){
            //console.log(elemento.value);
          }
          var str=  elemento.value.replace(/[^\w\s]/gi, '');
        
        
          $.ajax({
          type: "GET",
            url: "/guardarFicha/"+id_f+"/"+id_pregunta+"/"+str,
            data: {},
            success: function (data) {
             
           // console.log(data);
          
             // md.showNotification('top','right','Dominios Actualizados');
              }         
          });
          break;
        }
      }
      
      }
      else if(data[i].tipo_input=="input" ){
      if(formulario.elements[0].value!=""){
        var str=  formulario.elements[0].value.replace(/[^\w\s]/gi, '');
        //console.log(str);
        $.ajax({
          type: "GET",
            url: "/guardarFicha/"+id_f+"/"+id_pregunta+"/"+str,
            data: {},
            success: function (data) {
             // console.log(data);
             // md.showNotification('top','right','Dominios Actualizados');
              }         
          });
      }
    }
      else if(data[i].tipo_input=="crecimiento"){
        $.ajax({
          type: "GET",
            url: "/guardarFicha/"+id_f+"/"+id_pregunta+"/"+this.crecimiento,
            data: {},
            success: function (data) {
             // console.log(data);
             // md.showNotification('top','right','Dominios Actualizados');
              }         
          });
          this.peso = document.getElementById("peso");
   this.altura = document.getElementById("altura");
          $.ajax({
          type: "GET",
            url: "/updateCrecimiento/"+id_estudiante+"/"+altura.value.length+"/"+peso.value.length+"/"+crecimiento,
            data: {},
            success: function (data) {
             // console.log(data);
             // md.showNotification('top','right','Dominios Actualizados');
              }         
          });

      }
      else{
        var datos = [];
       
        for(var j=0; j<formulario.elements.length; j++) {
          var elemento = formulario.elements[j];
          if(elemento.type == "checkbox") {
            if(elemento.checked) {
              var str=  elemento.value.replace(/[^\w\s]/gi, '');
      //  console.log(str);
              $.ajax({
          type: "GET",
            url: "/guardarFicha/"+id_f+"/"+id_pregunta+"/"+str,
            data: {},
            success: function (data) {
            
             //console.log(data);
              }         
          });
            }
          }
        }
    
  
      }
  //observaciones por dominio
  
  
  
      if(i == data.length-1){
        Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Formulario guardado!',
    showConfirmButton: false,
    timer: 1500
  })
  
  
      }
    
  }
  
  
      for (let p = 0; p < dominios.length; p++) {
        
       var value = document.getElementById("obsDominio"+dominios[p].id).value;
       var valDominio = document.getElementById("valDominio"+dominios[p].id);
      // console.log(valDominio.value);
       if(value==""){
       value="1";
       }
        $.ajax({
          type: "GET",
            url: "/guardarObsDominio/"+id_f+"/"+dominios[p].id+"/"+value+"/"+valDominio.value,
            data: {},
            success: function (data) {
            
             //console.log(data);
              }         
          });
       }
      
      
  //observaciones globales
  if(document.getElementById("obsGlobales").value!=""){
    
    $.ajax({
  type: "GET",
  url: "/guardarObsGlobal/"+document.getElementById("obsGlobales").value+"/"+id_f,
  data: {},
  success: function (data) {
 
   //console.log(data);
    }         
  });
  }
}

function enviarDatos(){
  $.ajax({
        type: "GET",
          url: "/crearFicha/"+id_estudiante,
          data: {},
          success: function (data) {
            this.id_ficha=data.ficha[0].id;
          
            guardar(this.id_ficha);
           // md.showNotification('top','right','Dominios Actualizados');
            }         
        });

 
}
//------------------
function exportar(){
 // window.location.href = "{{URL::to('fichaExport/')}}"+"/"+id_estudiante;
 var mywindow = window.open('', 'PRINT', 'height=800,width=800');

mywindow.document.write('<html><head><title>' + 'Sistema de Ayuda a la Salud Escolar en las Instituciones Educativas'  + '</title>');
mywindow.document.write('</head><body >');

mywindow.document.write('<h1 style="text-align: center;">'+document.getElementById("nombre").innerHTML +'</h1>');
var f = new Date();
mywindow.document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
var q=0;
 var c=0;
 mywindow.document.write('<h3 >'+document.getElementById("dominio1").innerHTML +'</h3>');
 var final;
for (let i = 0; i < data.length; i++) {
 
 
  
    var formulario = document.getElementById("pregunta"+data[i].id);
    mywindow.document.write('<h4>'+(i+1)+". "+data[i].pregunta+'</h4>');
    var id_pregunta=data[i].id;
    if(data[i].tipo_input=="select" || data[i].tipo_input=="si_no"){
    
    for(var j=0; j<formulario.elements.length; j++) {
    var elemento = formulario.elements[j];
      if(elemento.checked){
        
        mywindow.document.write('<p>'+elemento.value+'</p>');
        //elemento.value
        break;
      }
    }
    
    }else if(data[i].tipo_input=="crecimiento"){

      mywindow.document.write('<p>'+this.crecimiento+'</p>');
    }
    else{
      var datos = [];
     
      for(var j=0; j<formulario.elements.length; j++) {
        var elemento = formulario.elements[j];
        if(elemento.type == "checkbox") {
          if(elemento.checked) {
            mywindow.document.write('<p>'+elemento.value+'</p>');
      
          }
        }
      }
  

    }
    if( i+1 < data.length && data[i].id_dominio!=data[i+1].id_dominio ){
  
      mywindow.document.write('<p >' +document.getElementById("valorizacion"+data[i].id_dominio).innerHTML +'</p>');
      mywindow.document.write('<p >'+'Observaciones del dominio:' +'</p>');
      mywindow.document.write('<p >'+document.getElementById("obsDominio"+data[i].id_dominio).value +'</p>');
    mywindow.document.write('<h3 >'+document.getElementById("dominio"+data[i+1].id_dominio).innerHTML +'</h3>');
  final=data[i+1].id_dominio;
  }
  
}

mywindow.document.write('<p >' +document.getElementById("valorizacion"+final).innerHTML +'</p>');
      mywindow.document.write('<p >'+'Observaciones del dominio:' +'</p>');
      mywindow.document.write('<p >'+document.getElementById("obsDominio"+final).value +'</p>');
mywindow.document.write('<h3 >'+'Observaciones Globales: ' +'</h3>');
 
mywindow.document.write('<p >'+document.getElementById("obsGlobales").value +'</p>');







mywindow.document.write('</body></html>');

mywindow.document.close(); // necessary for IE >= 10
mywindow.focus(); // necessary for IE >= 10*/

mywindow.print();
mywindow.close();
window.location.href = "{{URL::to('ficha_salud/')}}"+"/"+id_estudiante;
return true;
}
//----------------------------------

function calcula_imc(){ 
   this.peso = document.getElementById("peso");
   this.altura = document.getElementById("altura");
  var imc_c = document.getElementById("contenedor"); 
if (peso.value.length == 0){ 
  md.showNotification('top','right','El peso es obligatorio');
 
return 0; 
} 
if (altura.value.length == 0){ 
  md.showNotification('top','right','La altura es obligatoria');
 
return 0; 
} 

var imc = peso.value/Math.pow(altura.value,2);  
imc = Math.round(imc*100)/100; 

   
    var alert;
    if (imc < 18.5) {
      this.crecimiento = 'Bajo peso';
        alert = "alert alert-danger";
    } else if (imc > 18.5 && imc < 24.9) {
      this.crecimiento = 'Peso normal';
        alert = "alert alert-success";
    } else if (imc > 25 && imc < 26.9) {
      this.crecimiento = 'Sobrepeso grado I (Riesgo)';
        alert = "alert alert-warning";
    }else if (imc > 27 && imc < 29.9) {
      this.crecimiento = 'Sobrepeso grado II (Riesgo)';
        alert = "alert alert-warning";
    }else if (imc > 30 && imc < 34.9) {
      this.crecimiento = 'Obesidad de tipo I (Riesgo moderado)';
        alert = "alert alert-danger";
    }else if (imc > 35 && imc < 39.9) {
      this.crecimiento = 'Obesidad de tipo II (Riesgo severo)';
        alert = "alert alert-danger";
    }else  if (imc > 40 && imc < 49.9) {
      this.crecimiento = 'Obesidad de tipo III (Riesgo muy severo)';
        alert = "alert alert-danger";
    }else if (imc > 50) {
      this.crecimiento = 'Obesidad de tipo IV (Riesgo extremo)';
        alert = "alert alert-danger";
    }

    var html = '<p> Para un peso de ' + peso.value + ' kilogramos y una talla de ' + altura.value +
        ' metros, su IMC es: ' + imc + '<p>' +
        '<div class="'+alert+'" role="alert">{{$estudiante->nombre}} se encuentra en la clasificación de: ' + this.crecimiento + '</div>';
    imc_c.innerHTML = html;


document.getElementById("contenedor").style.display='block'; 
} 
$(document).ready(function(){ 
$('.numericonly').keydown(function(e) { 
if ((e.keyCode < 45 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 190 && e.keyCode != 110) 
e.preventDefault(); 
}); 
});



</script>
@endsection