@extends($layout_grocery)

@section('content')
<div class="content">
    <div class="container-fluid" >
    <h2 id="nombre" style="text-align: center;font-weight: bold;">Reporte del Observador de los Estudiantes</h2>
    <h2 id="nombre" style="text-align: center;font-weight: bold;">{{$institucion[0]->nombre_insti}}</h2>
    
    <select id="falta" class="custom-select" >
        <option  value="-1">(Seleccione) Tipo de observacion</option>
        <option value="Matoneo">Matoneo</option>
        <option value="Comportamiento">Comportamiento</option>
        <option value="Disfunción familiar">Disfunción familiar</option>
        <option value="Bajo desempeño escolar">Bajo desempeño escolar</option>
        <option value="Bullying">Bullying</option>
        <option value="Baja autoestima">Baja autoestima</option>
        <option value="Relaciones familiares conflictivas">Relaciones familiares conflictivas</option>
        <option value="Identidad de orientación sexual">Identidad de orientación sexual</option>
        <option value="Consumo de sustancias">Consumo de sustancias</option>
        <option value="Otros">Otros</option>
      
    </select>
    <br>
    <br>

<select id="grado" class="custom-select">
        <option  value="-1">(Opcional) Seleccione un Grado</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
    </select>
    <br>
    <br>
    <select id="activo" class="custom-select">
        <option  value="-1">¿Estudiantes Activos o Inactivos?</option>
        <option value="0">Inactivos</option>
        <option value="1">Activos</option>
       
    </select>
    <div class="row" style="">
    

    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Inicio:</label>
        <br>
        <input type="date" id="inicio" class="form-control" >
      </div>
    </div>
   

    <div class="col-md-6">
      <div class="form-group">
        <label class="bmd-label">Rango Fin:</label>
        <br>
        <input type="date" id="fin" class="form-control" >
      </div>
    </div>
  </div>
  <br>
    <div class="row">
    <div class="col-md-6" style="height: 50px;text-align:center;">
    <button class="btn"  onclick="reporte();">Filtrar</button>
    </div>
    <div class="col-md-6" style="height: 50px;text-align:center;">
    <button class="btn" style="background: #bfbfbf;" id="exportar" onclick="exportar();" >Exportar</button>
    </div>
    </div>
    <br>
    <div id="reporte">

    </div>

    <br>
    <div id="pdf" class="table-responsive" style="overflow:scroll;
     height:400px;
     width:100%;">
					<table class="table ">
				  <thead class="thead-dark">
					<tr>
				
                      <th scope="col">Nombres</th>
                      <th scope="col">Apellidos</th>
					  <th scope="col">Detalles</th>
					  <th scope="col">Fecha</th>
					</tr>
				  </thead>
				  <tbody id="datos">
					
				
					
				  </tbody>
				</table>
</div>


    </div>
    </div>

    <script>
    var URLactual = document.location+'';
var res=URLactual.split("/");
var id_colegio={!! json_encode($id) !!};



function exportar(){
  var sel = document.getElementById('pdf');
  console.log(sel);
  var mywindow = window.open('newFile.html', 'PRINT', 'height=800,width=800');
var con=0;
            mywindow.document.write('<html><head><title>' + 'Sistema de Ayuda a la Salud Escolar en las Instituciones Educativas'  + '</title>');
            mywindow.document.write('</head><body >');
   
            mywindow.document.write(sel.innerHTML);
            
            mywindow.document.write('</body></html>');

mywindow.document.close(); // necessary for IE >= 10
mywindow.focus(); // necessary for IE >= 10*/

mywindow.print();
mywindow.close();
}



    function reporte(){
  var falta = document.getElementById('falta').value;
  var grado = document.getElementById('grado').value;
  var activo = document.getElementById('activo').value;
 
var inicio=document.getElementById("inicio").value; 
var fin=document.getElementById("fin").value;
  
  if (falta!="-1" && activo!="-1" && inicio =="" && fin =="" && grado=="-1") {
    $.ajax({
          type: "GET",
            url: "/observadorReporteAjax/"+id_colegio+"/0/0/0/"+activo+"/"+falta,
            data: {},
            success: function (data) {
              console.log(data);
              var html="";
              document.getElementById('datos').innerHTML="";
              $('#datos').append(html);
                for (let index = 0; index < data.observaciones.length; index++) {
                     html +='<tr><td>'+data.observaciones[index].nombre+'</td><td>'+data.observaciones[index].apellidos+'</td><td>'+data.observaciones[index].detalles+'</td><td>'+data.observaciones[index].fecha+'</td></tr>' ;
                    
                }
                $('#datos').append(html);
                var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.observaciones.length+' Estudiantes con observaciones de '+falta+'. Nota: La información entregada en la presente tabla, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              $('#reporte').append(html);
              }         
          });
  }else if (falta!="-1" && activo!="-1" && inicio =="" && fin =="" && grado!="-1") {
    $.ajax({
          type: "GET",
            url: "/observadorReporteAjax/"+id_colegio+"/0/0/"+grado+"/"+activo+"/"+falta,
            data: {},
            success: function (data) {
              console.log(data);
              var html="";
              document.getElementById('datos').innerHTML="";
              $('#datos').append(html);
                for (let index = 0; index < data.observaciones.length; index++) {
                     html +='<tr><td>'+data.observaciones[index].nombre+'</td><td>'+data.observaciones[index].apellidos+'</td><td>'+data.observaciones[index].detalles+'</td><td>'+data.observaciones[index].fecha+'</td></tr>' ;
                    
                }
                $('#datos').append(html);
                var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.observaciones.length+' Estudiantes con observaciones de '+falta+' y grado '+grado+'. Nota: La información entregada en la presente tabla, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              $('#reporte').append(html);
              }         
          });
  }else if (falta!="-1" && activo!="-1" && inicio !="" && fin !="" && grado!="-1") {
    $.ajax({
          type: "GET",
            url: "/observadorReporteAjax/"+id_colegio+"/"+inicio+"/"+fin+"/"+grado+"/"+activo+"/"+falta,
            data: {},
            success: function (data) {
              console.log(data);
              var html="";
              document.getElementById('datos').innerHTML="";
              $('#datos').append(html);
                for (let index = 0; index < data.observaciones.length; index++) {
                     html +='<tr><td>'+data.observaciones[index].nombre+'</td><td>'+data.observaciones[index].apellidos+'</td><td>'+data.observaciones[index].detalles+'</td><td>'+data.observaciones[index].fecha+'</td></tr>' ;
                    
                }
                $('#datos').append(html);
                var html='<p style="text-align: justify;font-weight: bold;">Se han encontrado '+data.observaciones.length+' Estudiantes con observaciones de '+falta+', grado '+grado+' y rango de fechas desde el '+inicio+' hasta '+fin+'. Nota: La información entregada en la presente tabla, '+
                'es suministrada directamente por la institución y es responsabilidad de la institución y del usuario asegurar la confidencialidad de la información acá suministrada. '+
                'Toda la información esta amparada bajo la Ley Estatutaria 1581 de 2012. Por la cual se dictan disposiciones generales para la protección de datos personales. Resaltando lo descrito en los Artículos 2,5,6 y 7 de la presente ley.<p>';
              document.getElementById('reporte').innerHTML="";
              $('#reporte').append(html);
              }         
          });
  }
  else{
    md.showNotification('top','right','Selecciona un tipo de observacion y un estado.');
  }
    }
    
    
    </script>
@endsection