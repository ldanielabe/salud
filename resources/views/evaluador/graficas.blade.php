@extends($layout_grocery)

@section('content')
<style>
html {
    scroll-behavior: smooth;
    touch-action: auto;
}

.card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    width: 20%;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
}

span.apexcharts-tooltip-text-label, .apexcharts-tooltip-title, .apexcharts-xaxistooltip.apexcharts-xaxistooltip-top.apexcharts-theme-light.apexcharts-active {
    display: none;
}

</style>

<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
   
<div class="content">
    <div class="container-fluid">
        <h2 id="nombre" style="text-align: center;font-weight: bold;">Estadisticas Por Dominios
            {{$institucion[0]->nombre_insti}}</h2>
        <br>
        <select id="selectDominio" class="custom-select" onchange="dominios()">
            <option value="-1">Seleccione un dominio</option>
            @for ($i = 0; $i < sizeof($dominios); $i++) <option value="{{$dominios[$i]->id}}">{{$dominios[$i]->nombre}}
                </option>
                @endfor
        </select>
        <br>
        <br>
        <select id="selectGrado" class="custom-select" onchange="dominios()">
            <option value="-1">(Opcional) Seleccione un Grado</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
        </select>
        <br>

        <div class="row" style="">


            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label">Rango Inicio:</label>
                    <br>
                    <input type="month" id="inicio" class="form-control">
                </div>
            </div>


            <div class="col-md-6">
                <div class="form-group">
                    <label class="bmd-label">Rango Fin:</label>
                    <br>
                    <input type="month" id="fin" class="form-control">
                </div>
            </div>
            <div class="col-md-12" style="text-align:center;">
                <button class="btn btn-primary" onclick="dominios()">Enviar</button>
            </div>

        </div>
    
      
      
    </div>

    <div class="row" id="contenedor">

    </div>



</div>
</div>



<script>

var id_colegio ={!! json_encode($id) !!};

function dominios() {
    var sel = document.getElementById('selectDominio');
    var grado = document.getElementById('selectGrado');
    if (sel.value != "-1" && document.getElementById("inicio").value == "" && document.getElementById("fin").value ==
        "") {
        if (grado.value == "-1") {
            grado = "0";
        } else {
            grado = grado.value;
        }
        $.ajax({
            type: "GET",
            url: "/estadisticasAjax/" + sel.value + "/" + grado + "/" + id_colegio,
            data: {},
            success: function(data) {

                var datos = 0;
                var label = "";
                var html;
                document.getElementById('contenedor').innerHTML = "";
                for (let index = 0; index < data.preguntas.length; index++) {
                    datos = [];
                    label = [""];
                    html = "";
                    for (let i = 0; i < data.estadisticas.length; i++) {
                        if (data.preguntas[index].id == data.estadisticas[i].id) {

                            if (datos == 0) {
                                datos = [data.estadisticas[i].cantidad];
                                label = [data.estadisticas[i].respuestas];
                            } else {
                                datos = datos.concat([data.estadisticas[i].cantidad]);
                                label = label.concat([data.estadisticas[i].respuestas]);
                            }
                        }
                    }
                    html =
                        '<br><br><br> <div class="col-md-6 animate__animated animate__backInLeft container" ><div class="chart-container card" style="position: relative; height:90%; width:90%; text-align: center; margin-left: auto;margin-right: auto;"><div id="grafica' +
                        data.preguntas[index].id + '" ></div></div></div><br>';
                    $('#contenedor').append(html);

                    if (data.preguntas[index].tipo_input == "select") {
                        var options = {
                            series: [{
                                name: 'Cantidad',
                                data: datos
                            }],
                            chart: {
                                height: 350,
                                type: 'bar',
                                events: {
                                    click: function(chart, w, e) {
                                        // console.log(chart, w, e)
                                    }
                                }
                            },
                            plotOptions: {
                                bar: {
                                    columnWidth: '45%',
                                    distributed: true
                                }
                            },
                            dataLabels: {
                                enabled: true,
                                formatter: function(val) {
                                    return val;
                                },
                            },
                            legend: {
                                show: false
                            },
                            xaxis: {
                                categories: label,
                                position: 'top',
                                axisBorder: {
                                    show: false
                                },
                                axisTicks: {
                                    show: false
                                },
                                crosshairs: {
                                    fill: {
                                        type: 'gradient',
                                        gradient: {
                                            colorFrom: '#D8E3F0',
                                            colorTo: '#BED1E6',
                                            stops: [0, 100],
                                            opacityFrom: 0.4,
                                            opacityTo: 0.5,
                                        }
                                    }
                                },
                                tooltip: {
                                    enabled: true,
                                }
                            },
                            tooltip: {
                            y: {
                                formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                                if(value==1){
                                    return value + " estudiante: " + w.config.xaxis.categories[dataPointIndex];
                                }else{
                                    console.log(w);
                                    return value + " estudiantes: " +  w.config.xaxis.categories[dataPointIndex];
                                }
                                
                                }
                            }
                            },

                            title: {
                                text: data.preguntas[index].descripcion_grafica,
                                floating: true,
                                offsetY: 330,
                                align: 'center',
                                style: {
                                    color: '#444'
                                }
                            }
                        };


                        var mychart = new ApexCharts(document.querySelector('#grafica' + data.preguntas[
                            index].id + ''), options);
                        mychart.render();
                    } else {
                        var options = {
                            series: datos,
                            chart: {
                                width: '100%',
                                type: 'pie',
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        offset: -5
                                    }
                                }
                            },
                            title: {
                                text: '' + data.preguntas[index].descripcion_grafica
                            },
                            labels: label,
                            responsive: [{
                                breakpoint: 280,
                                options: {
                                    chart: {
                                        width: 100
                                    },
                                    legend: {
                                        show: false
                                    }
                                }
                            }],
                            dataLabels: {
                            formatter(val, opts) {
                                const name = opts.w.globals.labels[opts.seriesIndex]
                                return [name, val.toFixed(1) + '%']
                            }
                            },
                            tooltip: { enabled: true, custom: function ({ series, seriesIndex, dataPointIndex, w }) {
                            console.log(w);
                            if(series[seriesIndex]==1){
                                return series[seriesIndex]+" estudiante: "+w.config.labels[seriesIndex]; 
                                }else{
                                return series[seriesIndex]+" estudiantes: "+w.config.labels[seriesIndex]; 
                                }
                            
                            } 
                            }
                        };
                        piechart = new ApexCharts(document.querySelector('#grafica' + data.preguntas[index]
                            .id + ''), options);
                        piechart.render();
                    }
                }

            }
        });
    } else if (document.getElementById("inicio").value != "" && document.getElementById("fin").value != "") {
        if (document.getElementById("inicio").value > document.getElementById("fin").value) {
            md.showNotification('top', 'right', 'La fecha de inicio no puede ser mayor a la fecha fin');
        } else {
            var inicio = document.getElementById("inicio").value + "-01";
            var fin = document.getElementById("fin").value + "-01";
            if (grado.value == "-1") {
                grado = "0";
            } else {
                grado = grado.value;
            }
            $.ajax({
                type: "GET",
                url: "/estadisticasAjaxRango/" + sel.value + "/" + grado + "/" + id_colegio + "/" + inicio +
                    "/" + fin,
                data: {},
                success: function(data) {

                    var datos = 0;
                    var label = "";
                    var html;
                    document.getElementById('contenedor').innerHTML = "";
                    for (let index = 0; index < data.preguntas.length; index++) {
                        datos = [];
                        label = [""];
                        html = "";
                        for (let i = 0; i < data.estadisticas.length; i++) {
                            if (data.preguntas[index].id == data.estadisticas[i].id) {

                                if (datos == 0) {
                                    datos = [data.estadisticas[i].cantidad];
                                    label = [data.estadisticas[i].respuestas];
                                } else {
                                    datos = datos.concat([data.estadisticas[i].cantidad]);
                                    label = label.concat([data.estadisticas[i].respuestas]);
                                }
                            }
                        }
                        html =
                            '<br><br><br> <div class="col-md-6 animate__animated animate__backInLeft container" ><div class="chart-container card" style="position: relative; height:90%; width:90%; text-align: center; margin-left: auto;margin-right: auto;"><div id="grafica' +
                            data.preguntas[index].id + '" ></div></div></div><br>';
                        $('#contenedor').append(html);
                        console.log(datos);
                        console.log(label);
                        if (data.preguntas[index].tipo_input == "select") {
                            var options = {
                                series: [{
                                    name: 'Cantidad',
                                    data: datos
                                }],
                                chart: {
                                    height: 350,
                                    type: 'bar',
                                    events: {
                                        click: function(chart, w, e) {
                                            // console.log(chart, w, e)
                                        }
                                    }
                                },
                                plotOptions: {
                                    bar: {
                                        columnWidth: '45%',
                                        distributed: true
                                    }
                                },
                                dataLabels: {
                                    enabled: true,
                                    formatter: function(val) {
                                        return val;
                                    },
                                },
                                legend: {
                                    show: false
                                },
                                xaxis: {
                                    categories: label,
                                    position: 'top',
                                    axisBorder: {
                                        show: false
                                    },
                                    axisTicks: {
                                        show: false
                                    },
                                    crosshairs: {
                                        fill: {
                                            type: 'gradient',
                                            gradient: {
                                                colorFrom: '#D8E3F0',
                                                colorTo: '#BED1E6',
                                                stops: [0, 100],
                                                opacityFrom: 0.4,
                                                opacityTo: 0.5,
                                            }
                                        }
                                    },
                                    tooltip: {
                                        enabled: true,
                                    }
                                },
                            tooltip: {
                            y: {
                                formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                                if(value==1){
                                    return value + " estudiante: " + w.config.xaxis.categories[dataPointIndex];
                                }else{
                                    console.log(w);
                                    return value + " estudiantes: " +  w.config.xaxis.categories[dataPointIndex];
                                }
                                
                                }
                            }},
                                title: {
                                    text: data.preguntas[index].descripcion_grafica,
                                    floating: true,
                                    offsetY: 330,
                                    align: 'center',
                                    style: {
                                        color: '#444'
                                    }
                                }
                            };


                            var mychart = new ApexCharts(document.querySelector('#grafica' + data.preguntas[
                                index].id + ''), options);
                            mychart.render();
                        } else {
                            var options = {
                                series: datos,
                                chart: {
                                    width: '100%',
                                    type: 'pie',
                                },
                                plotOptions: {
                                    pie: {
                                        dataLabels: {
                                            offset: -5
                                        }
                                    }
                                },
                                title: {
                                    text: '' + data.preguntas[index].descripcion_grafica
                                },
                                labels: label,
                                responsive: [{
                                    breakpoint: 280,
                                    options: {
                                        chart: {
                                            width: 100
                                        },
                                        legend: {
                                            show: false
                                        }
                                    }
                                }],
                                dataLabels: {
                                formatter(val, opts) {
                                    const name = opts.w.globals.labels[opts.seriesIndex]
                                    return [name, val.toFixed(1) + '%']
                                }
                                },
                                tooltip: { enabled: true, custom: function ({ series, seriesIndex, dataPointIndex, w }) {
                                console.log(w);
                                if(series[seriesIndex]==1){
                                    return series[seriesIndex]+" estudiante: "+w.config.labels[seriesIndex]; 
                                    }else{
                                    return series[seriesIndex]+" estudiantes: "+w.config.labels[seriesIndex]; 
                                    }
                                
                                } 
                                }
                            };
                            piechart = new ApexCharts(document.querySelector('#grafica' + data.preguntas[
                                index].id + ''), options);
                            piechart.render();
                        }
                    }

                }
            });
        }
    } else {
        md.showNotification('top', 'right', 'No has seleccionado ningun dominio');
    }
}


function generarPdf() {
    contenedor = document.getElementById('contenedor').innerHTML;
    var pdf = new jsPDF('p', 'pt', 'letter');
        source = $('#contenedor')[0];

        specialElementHandlers = {
            '#bypassme': function (element, renderer) {
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };

        pdf.fromHTML(
            source, 
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, 
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
                pdf.save('Prueba.pdf');
            }, margins
        );
    
    
}
</script>
@endsection