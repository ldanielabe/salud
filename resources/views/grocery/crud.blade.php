@extends($layout_grocery)


@section('head')
<meta charset="UTF-8">
<title>{{$titulo}}</title>

<meta name="csrf-token" content="{{ csrf_token() }}">

@foreach ($css_files as $css_file)
<link rel="stylesheet" href="{{ asset($css_file) }}">
@endforeach

@endsection
@section('content')



<div class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
             
                <div class="card">
                    <div class="card-header card-header-primary text-md-center">
                        <p class="card-category">{{$titulo}}</p>
                    </div>
                    <div class="card-body">
                        <div style="padding: 5px">
                            {!! $output !!}
                        </div>
                    </div>
                </div>

@if($titulo=="Estudiantes" && auth()->user()->rol->id != 1)
                <div class="card">
                <div class="alert alert-warning" role="alert" style="font-size: 14px;">
                                Seleccione un archivo tipo de archivo excel para subir informacion. Ejemplo:
                                </div>
                <img src="../documentos/datos.PNG" class="img-thumbnail" style="width: 80%;
    height: 90%;
    display: block;
    margin: auto;">
		     <div class="card-body">
                        <div class="formulario" >
                            <form action="{{ route('subir')}}"  method="POST"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                              
                                @if(Session::has('message'))
                                <p class="alert alert-success" style="font-size: 14px;">{{Session::get('message')}}</p>
                                @endif
                                <div class="row">
                                <div class="col-12">
                                <input type="file" name="archivo" style="font-size: 14px;" required>
                                </div>
                                <br>

                                <div class="col-12">
                                <button type="submit" class="btn btn-info" style="font-size: 14px;">Importar Estudiantes</button>
                                
                                </div>
                                </div>
                            
                              
                               
                            </form>
                        </div>
                    </div>
                </div>
@endif
            </div>
        </div>
    </div>
</div>


@endsection


@section('js')
@foreach ($js_files as $js_file)
<script src="{{ asset($js_file) }}"></script>
@endforeach
<script>
if (typeof $ !== 'undefined') {
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
}
</script>
@endsection