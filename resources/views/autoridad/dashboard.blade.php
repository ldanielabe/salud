@extends('layouts.autoridad')

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_copy</i>
                  </div>
                  <p class="card-category">Visitantes hoy</p>
                  <h3 class="card-title">{{$hoy}}
                    <small></small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">date_range</i> Todos
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">store</i>
                  </div>
                  <p class="card-category">Ultimo mes</p>
                  <h3 class="card-title">{{$mes}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> 
                  </div>
                </div>
              </div>
            </div>

           
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">info_outline</i>
                  </div>
                  <p class="card-category">Desde el principio</p>
                  <h3 class="card-title">{{$todo}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-user"></i>
                  </div>
                  <p class="card-category">Total de usuarios</p>
                  <h3 class="card-title">{{$user}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Mensajes De Visitantes</h4>
                  <p class="card-category">Todos los mensajes</p>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Colegio</th>
                      <th>Correo</th>
                      <th>Mensaje</th>
                      <th>Eliminar</th>
                    </thead>
                    <tbody>
                      @forelse($contactenos as $c) 
                      <tr>
                      <td>{{$c->id}}</td>
                        <td>{{$c->nombre}}</td>
                        <td>{{$c->institucion}}</td>
                        <td>{{$c->correo}}</td>
                        <td>{{$c->mensaje}}</td>
                        <td>                                              
                          <a class="btn btn-primary"  role="button" onclick="eliminar({{$c->id}})" style="color:#fff; background-color:#fd9108;">Eliminar</a> 
                        </td>
                      </tr>
                      @empty

                      <div id="evento" class="alert alert-danger" role="alert">
                        no se ha registrado ningun mensaje <a href="#" class="alert-link"></a>. 
                      </div>
                      @endforelse  
  
                    </tbody>
                  </table>
                  <hr>
                  <div class="row">
                                 
                    <div col-12 style="text-align: center; margin-left: auto;
                    margin-right: auto;">
                        {{ $contactenos->render()}}
                    </div>
                
                </div>
                </div>
              </div>
            </div>
        
            
          </div>



         
        </div>
      </div>

      <script>

        function eliminar(id){
          
          Swal.fire({
          title: '¿Estas seguro?',
          text: "Quieres eliminar el mensaje!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'si'
        }).then((result) => {
          if (result.value) {
            Swal.fire(
              'Deleted!',
              'Mensaje Eliminado con exito',
              'success'
            ),
          borrar(id);
          }
        })
        
        };
        function borrar(id) {
                    $.ajax({
                        type: "GET",
                        url: "/deleteMensaje/"+id,
                        data: {id:id},
                        success: function (data) {
                  console.log(data);
                  location.reload();
                            }         
                    });
              
            }
        
        </script>
@endsection