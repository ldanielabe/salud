<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
  <link rel="shortcut icon" href="favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    ASIE
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-kit.css?v=2.0.6') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project 3219155922--wp - 
                        corrdinadora academico karina- 3102637630-->
  <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet"><!-- FontAwesome Icons -->
  <link href="{{ asset('assets/css/templatemo_style.css') }}" rel="stylesheet" type="text/css">
  <link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
/>
</head>

<body class="index-page sidebar-collapse">
  
  <div class="header-top" ng-style="main.topBar" ng-if="main.gov.isActive" style="background-color: rgb(1, 93, 202);"><div class="content_all">
    <a class="header-linkWrap" href="https://www.gov.co/" target="_blank">
      <img class="header-logoGov" ng-src="https://www.micolombiadigital.gov.co/sites/superadmin/content/files/000804/40161.jpg" alt="" src="https://www.micolombiadigital.gov.co/sites/superadmin/content/files/000804/40161.jpg"></a>
    </div>
  </div>


  <nav class=" navbar navbar-expand-lg fixed-top navbar-color-on-scroll " color-on-scroll="120" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" data-scroll href="{{ route('ini') }}"> <img src="{{ asset('logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
   

          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="#noticias" onclick="myFunction('Noticias')" style=" width:100%;margin-right:60px;margin-left:-20px;">
            <i class="fa fa-fw fa-book"></i>Noticias
          </a>
          </li>
          
          <li class="nav-item">
          <a class="" data-scroll href="#eventos" onclick="myFunction('Eventos')" style=" width:100%;color:#fff;margin: 4px;margin-right:15px;">
          <i class="fa fa-fw fa-calendar"></i> Eventos
          </a>
          </li>
          <li class="nav-item">
          <a class="" data-scroll href="#directorio" onclick="myFunction('Directorio')" style=" width:100%;color:#fff;margin-right:15px;">
          <i class="fa fa-fw fa-address-book"></i> Directorio
          </a>
          </li>

          <li class="nav-item">
              <a class="" data-scroll href="#contactenos" onclick="myFunction('Contactenos')" style=" width:100%;color:#fff;margin-right:5px;">
                  <i class="fa fa-fw fa-id-card"></i> Contactenos
              </a>
              </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}" style=" width:100%;color:#fff;">
              
              <i class="fa fa-fw fa-user"  ></i>Ingresar
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Siguenos en YouTube">
              <i class="fa fa-youtube"></i>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>

        


        </ul>
      </div>
    </div>
  </nav>

 
  @yield('content')

  <div class="row" data-background-color="black" style="background-color: #1d1d1d; padding: 10px;">
      <!-- About -->
     
 
    </div>

  
    <div class="row" data-background-color="black" style="background-color: #1d1d1d; padding: 10px;">
      <!-- About -->
      <div class="col-md-3 col-sm-4 " style=" text-align: center;  display: inline-block;">
        <div class="footer-main">
          <a href="./"><img id="logo-footer" class="img-responsive" src="./logo_ingsistemas_vertical_invertido.png" alt="Logo Pie de Página" style="width: 160px; height: 180px;"></a>
        </div>
      </div>

      <div class="col-md-3 col-sm-4 " style=" text-align: center;  display: inline-block;">
          <div class="posts">
            <div class="headline" style="color:#fff;"><h2>Visitantes</h2></div>
            <ul class="list-unstyled latest-list">
              <li style="color:#fff">
                 </li>
                <br>
              <li style="color:#fff">
                Último mes: {{$mes}} </li>
                <br>
              <li style="color:#fff">
                Desde el principio: {{$todo}} </li>
            </ul>
          </div>
        </div>

      <div class="col-md-3 col-sm-4 " style=" text-align: center;  display: inline-block;">
          <div class="footer-main">
            <a href="./"><img id="logo-footer" class="img-responsive" src="./Logo-nuevo-vertical.png" alt="Logo Pie de Página" style="width: 200px; height: 169px;"></a>
          </div>
        </div>

        <div class="col-md-3 col-sm-4 " style=" text-align: center;  display: inline-block;">
            <div class="footer-main">
              <a href="./"><img id="logo-footer" class="img-responsive" src="./escudo_colombia.png" alt="Logo Pie de Página" style="width: 126px; height: 127px;"></a>
            </div>
          </div>

 
    </div>

  <footer class="footer" data-background-color="black">





    <div class="container">


<br>
      <nav class="float-left">
        <ul>
          <li>
            <a href="#">
              Quienes Somos
            </a>
          </li>
          <li>
            <a href="#">
              Entidades
            </a>
          </li>
      
          <li>
            <a href="#">
              Contactenos
            </a>
          </li>

      
        
        </ul>
      </nav>
      <div class="float-right">
            <a href="https://ww2.ufps.edu.co/" target="_blank">Build by:</a> Universidad Francisco de Paula Santander.
            <script>
                document.write(new Date().getFullYear())
            </script> 
           <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
            Cúcuta 
          <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
      </div>
    </div>
  </footer>



  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/material-kit.js?v=2.0.6') }}" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  <script src="{{ asset('assets/js/templatemo_script.js') }}"></script>
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
   
    function myFunction(titulo){
      document.title = 'ASIE::'+titulo;
}
  </script>
</body>

</html>