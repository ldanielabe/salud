<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">

  <link rel="shortcut icon" href="favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    AppSalud
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-kit.css?v=2.0.6') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />

  
  @yield('head')

</head>
<body>

<nav class=" navbar navbar-expand-lg fixed-top navbar-color-on-scroll " color-on-scroll="120" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
      <a class="navbar-brand" data-scroll href="{{ route('ini') }}"> <img src="{{ asset('logo.png') }}"></a>
      
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
   

          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="/noticias" style=" width:100%;">
          <i class="material-icons">accessibility</i> Noticias
          </a>
          </li>
          
          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="/eventos" style=" width:100%;">
          <i class="material-icons">calendar_today</i> Eventos
          </a>
          </li>
          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll  href="/contenidoDigital" style=" width:100%;">
          <i class="material-icons">account_balance_wallet</i> Contenido Digital
          </a>
          </li>
          
       
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Siguenos en YouTube">
              <i class="fa fa-youtube"></i>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>


        


        </ul>

        <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesi&oacute;n
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
            @endguest
        </ul>
      </div>
    </div>
  </nav>
  
@yield('content')

<footer class="footer" data-background-color="black">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="#">
              Quienes Somos
            </a>
          </li>
          <li>
            <a href="#">
              Entidades
            </a>
          </li>
          <li>
            <a href="#">
              Mision y vision
            </a>
          </li>
          <li>
            <a href="#">
              Contactenos
            </a>
          </li>
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script><i class="material-icons">favorite</i>
        <a href="#" target="_blank">Build by:</a> Daniela Buitrago and Sami Arevalo.
      </div>
    </div>
  </footer>



  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/material-kit.js?v=2.0.6') }}" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
  


  </script>
  @yield('js')
</body>
</html>