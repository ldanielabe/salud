<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        ASIE:Rector
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{ asset('assets/css-admin/material-dashboard.css?v=2.1.2') }}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('assets/demo-admin/demo.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ asset('sweetalert2.all.min.js') }}"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
    <link href="{{asset('js/dominios/chat.css')}}" rel="stylesheet" />
    @yield('head')

</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="purple" data-background-color="white"
            data-image="{{asset('assets/img/sidebar-1.jpg')}}">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo"><a href="" class="simple-text logo-normal" style="font-weight: bold;">
                    {{ auth()->user()->nombre }}
                </a>
                <h3 class="txt-rol" >Rector
                </h3>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active  ">
                        <a class="nav-link" href="{{ url('/rector/dashboard') }}">
                            <i class="material-icons">inicio</i>
                            <p>Inicio</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/perfil') }}">
                            <i class="material-icons">person</i>
                            <p>Perfil</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="#recordatorio" aria-expanded="false" data-toggle="collapse" class="nav-link" href="">
                        <i class="material-icons">warning</i>
                            <p>Recordatorio</p>
                        </a>
                        <ul id="recordatorio" class="collapse list-unstyled ">
                        <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/recordatorio/') }}/{{encrypt(auth()->user()->institucio->id)}}">
                            <i class="material-icons">warning</i>
                            <p>Ficha de Salud</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/recordatorioObservador/') }}/{{encrypt(auth()->user()->institucio->id)}}">
                            <i class="material-icons">warning</i>
                            <p>Observador del Estudiante</p>
                        </a>
                    </li>
                        
                           
                             
                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/contactenos_rector') }}">
                            <i class="material-icons">message</i>
                            <p>Mensajes Contactenos</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/reportes/') }}/{{encrypt(auth()->user()->id_institucion)}}">
                            <i class="material-icons">receipt</i>
                            <p>Reporte Ficha</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                                                <a class="nav-link" href="{{ url('/observador/') }}/{{encrypt(auth()->user()->institucio->id)}}">
                                                    <i class="material-icons">receipt</i>
                                                    <p>Estadisticas Observador </p>
                                                </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a class="nav-link" href="{{ url('/observadorReporte/') }}/{{encrypt(auth()->user()->institucio->id)}}">
                                                    <i class="material-icons">receipt</i>
                                                    <p>Reporte Observador </p>
                                                </a>
                                            </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/estadisticas/') }}/{{encrypt(auth()->user()->id_institucion)}}">
                            <i class="material-icons">equalizer</i>
                            <p>Estadisticas Ficha</p>
                        </a>
                    </li>
                    <li class="nav-item "> <a class="nav-link"
                            href="{{ url('/antecedentesVista/') }}/{{ encrypt(auth()->user()->id_institucion)}}">
                            <i class="fa fa-fw fa-folder"></i>
                            <p>Estadisticas Antecedentes</p>
                        </a></li>
                    <li class="nav-item ">
                        <a href="#estadisticas" aria-expanded="false" data-toggle="collapse" class="nav-link" href="">
                            <i class="material-icons">face</i>
                            <p>Usuarios</p>
                        </a>
                        <ul id="estadisticas" class="collapse list-unstyled ">
                            <li style="padding-top:-2px;"> <a class="nav-link"
                                    href="{{ url('/estudiante/') }}/{{ encrypt(auth()->user()->id_institucion)}}"
                                    style="margin-top:-10px;">
                                    <i class="fa fa-fw fa-folder"></i>
                                    <p>Estudiantes</p>
                                </a></li>
                            <li style="padding-top:-2px;"> <a class="nav-link"
                                    href="{{ url('/profesores/') }}/{{ encrypt(auth()->user()->id_institucion)}}"
                                    style="margin-top:-10px;">
                                    <i class="fa fa-fw fa-folder"></i>
                                    <p>Profesores</p>
                                </a></li>
                            <li style="padding-top:-2px;"> <a
                                    href="{{ url('/evaluador/') }}/{{ encrypt(auth()->user()->id_institucion)}}" class="nav-link"
                                    style="margin-top:-10px;">
                                    <i class="fa fa-fw fa-folder"></i>
                                    <p>Evaluadores</p>
                                </a></li>
                           
                                <li style="padding-top:-2px;"> <a
                                    href="{{ url('/administrador/') }}/{{ encrypt(auth()->user()->id_institucion)}}" class="nav-link"
                                    href="" style="margin-top:-10px;">
                                    <i class="fa fa-fw fa-folder"></i>
                                    <p>Administrador</p>
                                </a></li>
                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/eventos') }}">
                            <i class="material-icons">library_books</i>
                            <p>Eventos</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                    <a class="dropdown-item" href="{{ route('logout') }}" style="font-size: 18px;"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                      <i class="material-icons">person</i>
                            <p>Salir</p>
                                    </a>
                    </li>
                </ul>

            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <a class="navbar-brand" href="javascript:;"></a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">

                        <ul class="navbar-nav">

                            <li class="nav-item dropdown">
                                @if(sizeof($notificacion)<= 0) <a class="nav-link" href="http://example.com"
                                    id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="material-icons">notifications</i>

                                    <span class="notification">0</span>
                                    <p class="d-lg-none d-md-block">
                                        Some Actions
                                    </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right"
                                        aria-labelledby="navbarDropdownMenuLink">
                                        <buttom class="btn btn-secondary" data-toggle="modal   style=" text-transform:
                                            none; color: #005afd; width: 100%; text-align: left; height: 40px;">No
                                            tienes notificaciones</buttom>
                                        @else
                                        <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">notifications</i>

                                            <span class="notification">{{sizeof($notificacion)}}</span>
                                            <p class="d-lg-none d-md-block">
                                                Some Actions
                                            </p>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right"
                                            aria-labelledby="navbarDropdownMenuLink">
                                            @for ($i = 0; $i < sizeof($notificacion); $i++) <buttom
                                                class="btn btn-secondary" data-toggle="modal"
                                                onClick="historialMensaje({{ $notificacion[$i]->id_user }},'{{ $notificacion[$i]->nombre }}');"
                                                data-target="#exampleModalCenter" style="    text-transform: none;
    color: #005afd;
    width: 100%;
    text-align: left;
    height: 40px;">Mensaje de {{ $notificacion[$i]->nombre }}</buttom>


                                                @endfor

                                                @endif
                                        </div>
                            </li>
                            <li class="nav-item dropdown">



                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="material-icons">person</i>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"
                                    style="font-size: 14px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}" style="font-size: 18px;"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesi&oacute;n
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>




                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->


            @yield('content')




            <footer class="footer">
                <div class="container-fluid">
                    <nav class="float-left">
                        <ul>
                            <li>

                            </li>
                            <li>

                            </li>
                            <li>

                            </li>
                            <li>

                            </li>
                        </ul>
                    </nav>
                    <div class="copyright float-right">
                        &copy;
                        <script>
                        document.write(new Date().getFullYear())
                        </script>, todos los derechos reservados.
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <!--   Core JS Files   -->

    <script src="{{ asset('assets/js-admin/core/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js-admin/core/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js-admin/core/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ asset('assets/js-admin/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/dominios/chat.js')}}" type="text/javascript"></script>
    <!-- Plugin for the momentJs  -->
    <script src="{{ asset('assets/js-admin/plugins/moment.min.js') }}"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="{{ asset('assets/js-admin/plugins/sweetalert2.js') }}"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.validate.min.js') }}"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.bootstrap-wizard.js') }}"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-selectpicker.js') }}"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-datetimepicker.min.js') }}"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.dataTables.min.js') }}"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-tagsinput.js') }}"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ asset('assets/js-admin/plugins/jasny-bootstrap.min.js') }}"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{ asset('assets/js-admin/plugins/fullcalendar.min.js') }}"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{ asset('assets/js-admin/plugins/jquery-jvectormap.js') }}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('assets/js-admin/plugins/nouislider.min.js') }}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{ asset('assets/js-admin/plugins/arrive.min.js') }}"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Chartist JS -->
    <script src="{{ asset('assets/js-admin/plugins/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/js-admin/material-dashboard.js?v=2.1.2') }}" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('assets/demo-admin/demo.js') }}"></script>

    <script src="{{ asset('assets/js-admin/eventos.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js-admin/slide.js') }}" type="text/javascript"></script>
    <script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function(event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $(
                        '.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function() {
                        $sidebar_img_container.css('background-image', 'url("' +
                            new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $(
                        '.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                        'img').data('src');

                    $full_page_background.fadeOut('fast', function() {
                        $full_page_background.css('background-image', 'url("' +
                            new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr(
                        'src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                        'img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' +
                        new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function() {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function() {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function() {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });

    function validaNumericos(event) {
        if (event.charCode >= 48 && event.charCode <= 57) {
            return true;
        }
        return false;
    }
    </script>
    <script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();

    });
    </script>
    @yield('js')
</body>

</html>