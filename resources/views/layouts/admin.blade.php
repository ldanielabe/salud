<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
    <link rel="shortcut icon">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>
        ASIE:Super Administrador
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <style>
    html {
        scroll-behavior: smooth;
    }
    </style>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset('assets/css-admin/material-dashboard.css')}}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/demo-admin/demo.css')}}" rel="stylesheet" />

    <script src="{{asset('https://cdn.jsdelivr.net/npm/sweetalert2@9')}}"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

    <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" />

    <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css-admin/style.css')}}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    <link href="{{asset('js/dominios/chat.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <style>

    </style>

</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="purple" data-background-color="white"
            data-image="{{asset('assets/img/sidebar-1.jpg')}}">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
        <div class="logo"><a href="" class="simple-text logo-normal" style="font-weight: bold;">
                    {{ auth()->user()->nombre }}
                </a>
                <h3 class="txt-rol" >
                       Super Administrador
                </h3>
        </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active  ">
                        <a class="nav-link" href="{{ url('/admin') }}">
                            <i class="material-icons">inicio</i>
                            <p>Inicio</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/perfil') }}">
                            <i class="material-icons">person</i>
                            <p>Perfil</p>
                        </a>
                    </li>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/editarFicha') }}">
                            <i class="material-icons">library_books</i>
                            <p>Editar Ficha Salud</p>
                        </a>
                    </li>

                    <li lass="nav-item ">
                        <a href="{{ url('/autoridad') }}" class="nav-link" href="">
                            <i class="material-icons">add_location_alt</i>
                            <p>Autoridades</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/instituciones') }}">
                            <i class="material-icons">foundation</i>
                            <p>Agregar Colegios</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="#estadisticas" aria-expanded="false" data-toggle="collapse" class="nav-link" href="">
                            <i class="material-icons">house</i>
                            <p>Colegios</p>
                        </a>
                        <ul id="estadisticas" class="collapse list-unstyled ">
                            @foreach($instituciones as $institucion)
                            <li class="nav-item " style="">
                                <a href="#dashvariants{{$institucion->id}}" aria-expanded="false" data-toggle="collapse"
                                    class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">library_books</i>
                                    <span class="badge badge-pill badge-warning"></span>
                                    <p>{{$institucion->nombre_insti}}</p>
                                </a>

                                <ul id="dashvariants{{$institucion->id}}" class="collapse list-unstyled ">
                                    <li class="nav-item " style="background-color:#E3E3E3;">
                                        <a href="#users{{$institucion->id}}" aria-expanded="false"
                                            data-toggle="collapse" class="nav-link" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">library_books</i>
                                            <span class="badge badge-pill badge-warning"></span>
                                            <p>Usuarios</p>
                                        </a>
                                        <ul id="users{{$institucion->id}}" class="collapse list-unstyled ">
                                            <li style="padding-top:-2px;"> <a class="nav-link"
                                                    href="{{ url('/estudiante/') }}/{{encrypt($institucion->id)}}"
                                                    style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Estudiantes</p>
                                                </a></li>
                                            <li style="padding-top:-2px;"> <a class="nav-link"
                                                    href="{{ url('/profesores/') }}/{{encrypt($institucion->id)}}"
                                                    style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Profesores</p>
                                                </a></li>
                                            <li style="padding-top:-2px;"> <a
                                                    href="{{ url('/rector/') }}/{{encrypt($institucion->id)}}" class="nav-link"
                                                    style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Rectores</p>
                                                </a></li>
                                            <li style="padding-top:-2px;"> <a
                                                    href="{{ url('/evaluador/') }}/{{encrypt($institucion->id)}}"
                                                    class="nav-link" style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Evaluadores</p>
                                                </a>
                                          
                                                <li style="padding-top:-2px;"> <a
                                                    href="{{ url('/administrador/') }}/{{encrypt($institucion->id)}}"
                                                    class="nav-link" href="" style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Administrador</p>
                                                </a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item " style="background-color:#E3E3E3;">
                                        <a href="#estadisticas{{$institucion->id}}" aria-expanded="false"
                                            data-toggle="collapse" class="nav-link" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">library_books</i>
                                            <span class="badge badge-pill badge-warning"></span>
                                            <p>Estadisticas</p>
                                        </a>
                                        <ul id="estadisticas{{$institucion->id}}" class="collapse list-unstyled ">
                                            <li style="padding-top:-2px;"> <a class="nav-link"
                                                    href="{{ url('/antecedentesVista/') }}/{{encrypt($institucion->id)}}"
                                                    style="margin-top:-10px;">
                                                    <i class="fa fa-fw fa-folder"></i>
                                                    <p>Estadisticas Antecedentes</p>
                                                </a></li>
                                                <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/recordatorioObservador/') }}/{{encrypt($institucion->id)}}">
                            <i class="material-icons">warning</i>
                            <p>Observador del Estudiante</p>
                        </a>
                    </li>
                                            <li class="nav-item ">
                                                <a class="nav-link"
                                                    href="{{ url('/estadisticas/') }}/{{encrypt($institucion->id)}}">
                                                    <i class="material-icons">equalizer</i>
                                                    <p>Estadisticas Ficha</p>
                                                </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a class="nav-link" href="{{ url('/reportes/') }}/{{encrypt($institucion->id)}}">
                                                    <i class="material-icons">receipt</i>
                                                    <p>Reportes Ficha</p>
                                                </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a class="nav-link" href="{{ url('/observador/') }}/{{encrypt($institucion->id)}}">
                                                    <i class="material-icons">receipt</i>
                                                    <p>Estadisticas Observador </p>
                                                </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a class="nav-link" href="{{ url('/observadorReporte/') }}/{{encrypt($institucion->id)}}">
                                                    <i class="material-icons">receipt</i>
                                                    <p>Reporte Observador </p>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </li>





                            @endforeach
                        </ul>
                    </li>
                    <li class="nav-item ">
                        <a href="#documentos" aria-expanded="false" data-toggle="collapse" class="nav-link"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">note_add</i>
                            <p>Documentos de Interes</p>
                        </a>
                        <ul id="documentos" class="collapse list-unstyled ">
                            <li style="padding-top:-2px;"> <a class="nav-link" href="{{ url('/agregarDocumentos') }}"
                                    style="margin-top:-10px;">
                                    <i class="material-icons">addchart</i>
                                    <p>Agregar</p>
                                </a></li>
                            <li style="padding-top:-2px;"> <a class="nav-link" href="{{ url('/promocion') }}"
                                    style="margin-top:-10px;">
                                    <i class="material-icons">list</i>
                                    <p>Listar</p>
                                </a></li>
                        </ul>
                    </li>




                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/eventos') }}">
                            <i class="material-icons">library_books</i>
                            <p>Eventos</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/noticias') }}">
                            <i class="material-icons">bubble_chart</i>
                            <p>Noticias</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/slide') }}">
                            <i class="material-icons">bubble_chart</i>
                            <p>Slide</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/directorio')}}">
                            <i class="material-icons">location_ons</i>
                            <p>Directorio</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                    <a class="dropdown-item" href="{{ route('logout') }}" style="font-size: 18px;"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                      <i class="material-icons">person</i>
                            <p>Salir</p>
                                    </a>
                    </li>


                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <a class="navbar-brand" href="javascript:;"></a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse  justify-content-end">

                        <ul class="navbar-nav">

                            <li class="nav-item dropdown">
                                @if(sizeof($notificacion)<= 0) <a class="nav-link" href="http://example.com"
                                    id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="material-icons">notifications</i>

                                    <span class="notification">0</span>
                                    <p class="d-lg-none d-md-block">
                                        Some Actions
                                    </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right"
                                        aria-labelledby="navbarDropdownMenuLink">
                                        <buttom class="btn btn-secondary" data-toggle="modal   style=" text-transform:
                                            none; color: #005afd; width: 100%; text-align: left; height: 40px;">No
                                            tienes notificaciones</buttom>
                                        @else
                                        <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">notifications</i>

                                            <span class="notification">{{sizeof($notificacion)}}</span>
                                            <p class="d-lg-none d-md-block">
                                                Some Actions
                                            </p>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right"
                                            aria-labelledby="navbarDropdownMenuLink">
                                            @for ($i = 0; $i < sizeof($notificacion); $i++) <buttom
                                                class="btn btn-secondary" data-toggle="modal"
                                                onClick="historialMensaje({{ $notificacion[$i]->id_user }},'{{ $notificacion[$i]->nombre }}');"
                                                data-target="#exampleModalCenter" style="    text-transform: none;
    color: #005afd;
    width: 100%;
    text-align: left;
    height: 40px;">Mensaje de {{ $notificacion[$i]->nombre }}</buttom>


                                                @endfor

                                                @endif
                                        </div>
                            </li>

                            <li class="nav-item dropdown">



                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="material-icons">person</i>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"
                                    style="font-size: 14px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}" style="font-size: 18px;"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesi&oacute;n
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>




                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->


            @yield('content')




      
        </div>
    </div>

    <div class="fixed-plugin">
        <div class="dropdown show-dropdown">
            <a href="#" data-toggle="dropdown">
                <i class="fa fa-comments fa-3x"> </i>
            </a>

            <ul class="dropdown-menu contenedor">

                <li class="header-title"> Color Menu</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger active-color">
                        <div class="badge-colors ml-auto mr-auto">
                            <span class="badge filter badge-purple" data-color="purple"></span>
                            <span class="badge filter badge-azure" data-color="azure"></span>
                            <span class="badge filter badge-green" data-color="green"></span>
                            <span class="badge filter badge-warning" data-color="orange"></span>
                            <span class="badge filter badge-danger" data-color="danger"></span>
                            <span class="badge filter badge-rose active" data-color="rose"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>


                </li>
                <li class="header-title" style="color:green;">Contactos En Linea</li>
                <table class="table">
                    <thead class="thead-success">
                        <tr>

                            <div>
                                <h5 style="text-align:center;">Tipo de Usuarios</h5>

                                <a href="javascript:void(0)" class="switch-trigger active-color">
                                    <form>
                                        <input id="searchTerm" class="form-control" placeholder="Usuario a Buscar"
                                            type="text" onkeyup="doSearch()" />
                                    </form>
                                </a>




                            </div>

                        </tr>
                    </thead>
                </table>
                <div id="table-wrapper">
                    <div id="table-scroll">
                        <table id="datos" style="overflow:scroll;position:relative;text-align: center;
    width: 100%;">

                            <tr>
                                <th scope="col" style="text-align: center;">Usuario</th>

                            </tr>
                            @forelse($allUser as $user)
                            @if($user->id != Auth::id())
                            <tr>

                                <td><button type="button" class="btn btn-secondary" data-toggle="modal"
                                        onClick="historialMensaje({{ $user->id }},'{{ $user->nombre }}');"
                                        data-target="#exampleModalCenter" style="    text-transform: none;
    color: #005afd;
    background-color: #c1c1c1;
    width: 100%;
    text-align: center;
    height: 40px;">{{$user->nombre}}</button></td>

                            </tr>
                            @endif
                            @empty

                            @endforelse
                            <tr class='noSearch hide'>
                                <td colspan="5"></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>



            </ul>
        </div>
    </div>
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="nombre" style="padding-left: 137px;">Chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="chat_window">
                        <div class="top_menu">

                        </div>
                        <ul class="messages">


                        </ul>
                        <div class="bottom_wrapper clearfix">
                            <div class="message_input_wrapper">
                                <input maxlength="30" class="message_input" placeholder="Escribe tu mensaje..." />
                            </div>
                            <div class="send_message">
                                <div class="icon"></div>
                                <div class="text">
                                    <buttom>Enviar</buttom>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="message_template">
                        <li class="message">
                             <div class="avatar"><img src="https://www.w3schools.com/w3images/avatar2.png" alt="Avatar" class="avatar">
                                <p id="me" style="    text-align: center;
    padding: 15;"></p>
                            </div>
                            <div class="text_wrapper">
                                <div class="text"></div>
                            </div>
                        </li>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>


    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js-admin/core/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js-admin/core/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js-admin/core/bootstrap-material-design.min.js')}}"></script>
    <script src="{{ asset('assets/js-admin/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

    <script src="{{ asset('js/dominios/chat.js')}}" type="text/javascript"></script>

    <!-- Plugin for the momentJs  -->
    <script src="{{ asset('assets/js-admin/plugins/moment.min.js')}}"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="{{ asset('assets/js-admin/plugins/sweetalert2.js')}}"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.validate.min.js')}}"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.bootstrap-wizard.js')}}"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-selectpicker.js"')}}></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src=" {{ asset('assets/js-admin/plugins/bootstrap-datetimepicker.min.js')}}"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="{{ asset('assets/js-admin/plugins/jquery.dataTables.min.js')}}"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-tagsinput.js')}}"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ asset('assets/js-admin/plugins/jasny-bootstrap.min.js')}}"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{ asset('assets/js-admin/plugins/fullcalendar.min.js')}}"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{ asset('assets/js-admin/plugins/jquery-jvectormap.js')}}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('assets/js-admin/plugins/nouislider.min.js')}}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{ asset('assets/js-admin/plugins/arrive.min.js')}}"></script>
    <!--  Google Maps Plugin    -->
    <!-- Chartist JS -->
    <script src="{{ asset('assets/js-admin/plugins/chartist.min.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js-admin/plugins/bootstrap-notify.js')}}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/js-admin/material-dashboard.js?v=2.1.2')}}" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('assets/demo-admin/demo.js')}}"></script>

    <script src="{{ asset('assets/js-admin/eventos.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js-admin/directorio.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js-admin/slide.js')}}" type="text/javascript"></script>

    <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js">
    </script>

    <script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function(event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function() {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $(
                        '.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function() {
                        $sidebar_img_container.css('background-image', 'url("' +
                            new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $(
                        '.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                        'img').data('src');

                    $full_page_background.fadeOut('fast', function() {
                        $full_page_background.css('background-image', 'url("' +
                            new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr(
                        'src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                        'img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' +
                        new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function() {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function() {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function() {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
    </script>
    <script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();

    });
    </script>
    <script>

    </script>


</body>

</html>