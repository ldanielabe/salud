<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  Aplicativo de Ayuda a la Salud en Instituciones Educativas (ASIE)
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-kit.css?v=2.0.6') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project 3219155922--wp - 
                        corrdinadora academico karina- 3102637630-->
  <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet"><!-- FontAwesome Icons -->
  <link href="{{ asset('assets/css/templatemo_style.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

 
<style>

.animar {
  display: inline-flexbox;
}



/* Click animations styles */

.click-animations {

  padding-bottom: 20px;
  background: #f6f8fa;
}




.click-animations input:focus,
.click-animations textarea:focus {
  border-color: #30cc8b;
}

.click-animations button {
  display: block;
  margin: 20px auto;
  padding: 10px;
  font-size: 20px;
  border: none;
  outline: none;
  background: #30cc8b;
  color: #fff;
  cursor: pointer;
  transition: 0.2s;
}

.click-animations button:active {
  transform: scale(0.95);
}
.form-error {
  border-color: #F46036 !important;
}

.funky-animations {
  text-align: center;
}

.funky-animations h3 {
  font-size: 2em;
}

.funky-animations div {
  margin: 120px auto;
  padding-top: 6px;
  height: 100px;
  width: 320px;
  border: 4px solid #f6f8fa;
}

.funky-animations h4 {
  font-size: 4.5em;
  font-weight: 400;
  /* Fallback color */
  color: #30cc8b;
  background: -webkit-linear-gradient(left, #69bcf4, #30cc8b);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  cursor: pointer;
}
.card2 {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 20%;
}

.card2:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

</style>
</head>
  <body class="index-page sidebar-collapse">
  <nav class=" navbar navbar-expand-lg fixed-top navbar-color-on-scroll " color-on-scroll="120" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" data-scroll href="{{ route('ini') }}"> <img src="{{ asset('logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
   

          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="#noticias" onclick="myFunction('Noticias')" style=" width:100%;margin-right:60px;margin-left:-20px;">
            <i class="fa fa-fw fa-book"></i>Noticias
          </a>
          </li>
          
          <li class="nav-item">
          <a class="" data-scroll href="#eventos" onclick="myFunction('Eventos')" style=" width:100%;color:#fff;margin: 4px;margin-right:15px;">
          <i class="fa fa-fw fa-calendar"></i> Eventos
          </a>
          </li>
          <li class="nav-item">
          <a class="" data-scroll href="#directorio" onclick="myFunction('Directorio')" style=" width:100%;color:#fff;margin-right:15px;">
          <i class="fa fa-fw fa-address-book"></i> Directorio
          </a>
          </li>

          <li class="nav-item">
              <a class="" data-scroll href="#contactenos" onclick="myFunction('Contactenos')" style=" width:100%;color:#fff;margin-right:5px;">
                  <i class="fa fa-fw fa-id-card"></i> Contactenos
              </a>
              </li>
              <li class="nav-item">
              <a class="" data-scroll  href="{{ route('documentos_interes') }}" style=" width:100%;color:#fff;margin-right:5px;">
                  <i class="fa fa-fw fa-book"></i> Documentos
              </a>
              </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}" style=" width:100%;color:#fff;">
              
              <i class="fa fa-fw fa-user"  ></i>Ingresar
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Siguenos en YouTube">
              <i class="fa fa-youtube"></i>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>

        


        </ul>
      </div>
    </div>
  </nav>

  <div class=" header-filter clear-filter " id="inicio" style="background-color: #fff;"
   data-parallax="true" >

    <div >
        <!-carousel page-header -->
    <div class="section_slider" id="carousel" >
      <div class="" id="inicio" style="height: 25rem;">
         <!-- Carousel Card -->
         <div class="card card-raised card-carousel" style="margin: 0; ">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">

                <div class="carousel-item active">
                @empty(!$slides)
                <a href="{{ $slides[0]->url}}" target="_self" tabindex="-1">
                    <img class="d-block w-100 slidecito" src="./imagesSlide/{{ $slides[0]->ruta }}"  alt="First slide">
                </a>  
                  <div class="carousel-caption d-none d-md-block">
                      <h4>
                        <i class="material-icons">location_on</i> {{$slides[0]->titulo}}
                      </h4>
                    </div>
                  </div>
                  @else
                     
                  @endempty
                  @for ($i = 1; $i < sizeof($slides); $i++) 
                 
                  <div class="carousel-item">
                  <a href="{{ $slides[$i]->url}}" target="_self" tabindex="-1">
                  <img class="d-block w-100 slidecito" src="./imagesSlide/{{ $slides[$i]->ruta }}"  alt="Next slide">
                  </a> 
                  <div class="carousel-caption d-none d-md-block">
                      <h4>
                        <i class="material-icons">location_on</i> {{$slides[$i]->titulo}}
                      </h4>
                    </div>
                  </div>
                   
                  @endfor

               



                </div>
              </div>
            </div>
            <!-- End Carousel Card -->
      </div>
    </div>
    <!--         end carousel -->
        
    </div>
             
  </div>
  

  <div  id="noticias" style="margin-top: 5rem;">
    
    <div class="section ">
      <div class="container">

      <div style="width:100%;  text-align: center; color:#000; font-weight: bold; margin-top:2rem;">
      <h1 class="text-uppercase text-center animate__animated animate__slideInLeft" style="color:#000; font-weight: bold;">NOTICIAS</h1> 
      
  </div>

  
      <div class="col animate__animated animate__slideInLeft">
        <div class="row">
        @foreach($noticias as $noticia)
       
            <div class="col-12 col-md-4 animate__animated animate__fadeInUpBig">
            
                    <div class="card noti" style="width: 300px; height: 100%; margin-left: auto;
                    margin-right: auto; margin-bottom: 2px;">
                        <img class="card-img-top" src="./imagesNoticias/{{$noticia->ruta}}"  alt="Card image cap" style="width: 300px; height: 150px;">
                        <div  class="card-body"  >
                        <p class="card-text">{{ $noticia->titulo}}</p>
                        <a href="{{ url('/noticiasInfo/') }}/{{ $noticia->id}}">ver mas...</a>
                      
                        </div>
                      </div>
           
            </div>
            
            @endforeach
 
       
        <div class="col-12">
        <div style="text-aling:center">
        <br>
         <br>
         <br>
          <a href="{{ route('news') }}"><h3 style="    text-align: end;">Ver todas las noticias</h3></a>
       </div>
        </div>
         </div>
        
         </div>
    
    </div> 
    
    </div>
    
  </div>
        

         

         


          </div>
        
        </div>
         
     
      
      </div>
     
</div>
 


            
       
            </div>
    </div>
    </div>
             
  </div>
 
  </div>
      <!--  Noticias -->



 
  <!--  Eventos -->
 
  <section class="tm-section tm-orange-bg" >
      <img src="./assets/img/tm-border-white-top.png" alt="Border" class="tm-border">
  <div class="" >
    
    <div class="section" id="eventos">
      <div class="container ">

      <div class="scroll-animations " style="width:100%;  text-align: center; color:#fff; font-weight: bold; margin-top: -28px;" >
      <h1 class="text-uppercase text-center animated" style="color:#fff; font-weight: bold;" >EVENTOS</h1>    
      <br>
      <img src="./assets/img/tm-border-white-bottom.png" width="100%" >	
      

      <!--  carousel -->
      
      <iframe class="" src="{{ url('/carrusel')}}" width="100%" style="background:#eee; overflow: hidden !important; overflow-y: hidden !important; border: #eee;overflow-x: hidden; height: 30rem !important;" scrolling="no"></iframe>
      <img src="./assets/img/tm-border-white-top.png" alt="Border" class="tm-border-event">
      </div>
      </div>
    </div>
      <img src="./assets/img/tm-border-white-bottom.png" width="100%" class="tm-border">
    </div>
</section>

<section class="tm-section" id="testimonials" style="padding: 0px;">

<div class="section " id="directorio">
  <div class="container scroll-animations " style="">
  <div  class="animated" style="width:100%; text-align: center; color:#000; font-weight: bold;">
  <h1 class="text-uppercase text-center" style="color:#000; font-weight: bold;">DIRECTORIO</h1>               
  
  </div>
  </div>
  <!--  carousel -->
  <iframe src="{{ url('/carruselDirectorio')}}" width="100%" heigth="60%" style="background:#eee; overflow: hidden !important; overflow-y: hidden !important; border: #eee;overflow-x: hidden; height: 17rem !important;" scrolling="no"></iframe>

</div>

</section>


<!--contactenos -->

<section class="templatemo-services tm-section tm-orange-bg-transparent" style="padding: 0px;" >
    <img src="./assets/img/tm-border-white-top.png" >
  	<div class="scroll-animations" style="" id="contactenos">
        <h1 class="text-uppercase text-center animated" style="color:#FFF; font-weight: bold;" >CONTACTENOS</h1>
	<br>
			<div class="row animar">
      <div class="col-1"></div>
				<div class="col-md-5 col-12  card" >

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7900.321323833737!2d-72.80431387576957!3d8.085092476715989!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e67aa26c39092d1%3A0xd86a95ed3691d218!2sSardinata%2C%20Norte%20de%20Santander!5e0!3m2!1ses!2sco!4v1585411336426!5m2!1ses!2sco" width="90%" height="100%" style="margin-top:2px;"></iframe>
                

            
                
              </div>
              <div class="col-1"></div>
	            <div class=" col-md-4 col-12  click-animations card" >
                <form method="POST" enctype="multipart/form-data"  action="{{ route('contactenos')}}" style="*::placeholder {color:#909;}">
                  {{ csrf_field() }}
                    
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nombre" placeholder="Escriba Su Nombre" >
                        
                      </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Correo</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" name="correo" aria-describedby="emailHelp" placeholder="Correo" minlength="10" maxlength="50"  required>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Colegio:</label>
                    <select class="form-control form-control-sm form-group" name="institucion"  ">
                      <option>Seleccione</option>
                      <option >Alcaldia de Sardinata</option>
                      @foreach($institucions as $ins)
                      <option value="{{$ins->id}}">{{$ins->nombre_insti}}</option>
                      @endforeach
                    </select>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Mensaje</label>
                        <textarea id="contact_message" class="form-control" name="mensaje" rows="8" placeholder="Mensaje" minlength="5" maxlength="200"  required></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" style="background-color:#1d1d1d;">Enviar</button>
                    </div>
                    
                  </form>
     
	            </div>
              <div class="col-1"></div>
            </div>
		</div>
		<img src="./assets/img/tm-border-black-bottom.png" alt="Border" class="tm-border">
  </section> <!-- contact -->


  <div class="row " data-background-color="black" style="background-color: #1d1d1d; padding: 10px;">
      <!-- About -->
      <div class="col-md-3 col-sm-4 scroll-animations " style=" text-align: center;  display: inline-block;">
        <div class="footer-main animar ">
          <a href="#"><img id="logo-footer" class="img-responsive" src="./logo_ingsistemas_vertical_invertido.png" alt="Logo Pie de Página" style="width: 160px; height: 180px;"></a>
        </div>
      </div>

      <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
          <div class="posts">
            <div class="headline" style="color:#fff;"><h2>Visitantes</h2></div>
            <ul class="list-unstyled latest-list">
              <li style="color:#fff">
                 </li>
                <br>
              <li style="color:#fff">
                Último mes: {{$mes}} </li>
                <br>
              <li style="color:#fff">
                Desde el principio: {{$todo}} </li>
            </ul>
          </div>
        </div>

      <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
          <div class="footer-main">
            <a href="./"><img id="logo-footer" class="img-responsive" src="./Logo-nuevo-vertical.png" alt="Logo Pie de Página" style="width: 200px; height: 169px;"></a>
          </div>
        </div>

        <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
            <div class="footer-main">
              <a href="./"><img id="logo-footer" class="img-responsive" src="./logo5.png" alt="Logo Pie de Página" style="width: 126px; height: 127px;"></a>
            </div>
          </div>

 
    </div>
    
    <div class="row" data-background-color="black" style="background-color: #1d1d1d; padding: 10px;">
      <!-- About -->
      <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
        <div class="footer-main">
          <a href="./"><img id="logo-footer" class="img-responsive" src="./logo1.png" alt="Logo Pie de Página" style="width: 160px; height: 180px;margin-top:15px;"></a>
        </div>
      </div>

      <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
        <div class="footer-main">
          <a href="./"><img id="logo-footer" class="img-responsive" src="./logo2.png" alt="Logo Pie de Página" style="width: 160px; height: 180px;margin-top:15px;"></a>
        </div>
      </div>
   
      <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
          <div class="footer-main">
            <a href="./"><img id="logo-footer" class="img-responsive" src="./logo3.png" alt="Logo Pie de Página" style="width: 200px; height: 169px;margin-top:15px;"></a>
          </div>
        </div>

        <div class="col-md-3 col-sm-4 animar" style=" text-align: center;  display: inline-block;">
            <div class="footer-main">
              <a href="./"><img id="logo-footer" class="img-responsive" src="./logo4.png" alt="Logo Pie de Página" style="width: 126px; height: 127px;margin-top:15px;"></a>
            </div>
          </div>

 
    </div>

  <footer class="footer" data-background-color="black"  style="background-color: #1d1d1d;">





    <div class="container">


<br>



      <nav class="float-left" style="background-color: #1d1d1d;">
        <ul>
          <li>
            <a href="#">
              Quienes Somos
            </a>
          </li>
          <li>
            <a href="#">
              Entidades
            </a>
          </li>
      
          <li>
            <a href="#">
              Contactenos
            </a>
          </li>
          
        </ul>
      </nav>
      <div class="float-right" data-background-color="black"  style="background-color: #1d1d1d;">
            <a href="https://ww2.ufps.edu.co/" target="_blank">Build by:</a> Universidad Francisco de Paula Santander.
            <script>
                document.write(new Date().getFullYear())
            </script> 
           <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
            Cúcuta 
          <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
      </div>
    </div>
  </footer>



  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/material-kit.js?v=2.0.6') }}" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  <script src="{{ asset('assets/js/templatemo_script.js') }}"></script>
  <script>
    $(document).ready(function() {
      function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    //alert(docViewTop);
    var docViewBottom = docViewTop + $(window).height();
    //alert(docViewBottom);
    var elemTop = $(elem).offset().top;
    //alert(elemTop);
    var elemBottom = elemTop + $(elem).height();
    //alert(elemBottom);   
    return ((elemBottom < docViewBottom) && (elemTop > docViewTop));
  }
      $(window).scroll(function() {
        $('.scroll-animations .animated').each(function() {
          if (isScrolledIntoView(this) === true) {
            $(this).addClass('animate__animated animate__slideInLeft');
          }
        });

        $('.animar').each(function() {
          if (isScrolledIntoView(this) === true) {
            //alert("a");
            $(this).addClass('animate__animated animate__fadeInUpBig');
          }
        });

      });
      



      
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
   
    function myFunction(titulo){
      document.title = 'ASIE::'+titulo;
}


  </script>
</body>

</html>