@extends('layouts.rector')

@section('content')

<h2 style="text-align: center;font-weight: bold;"> {{auth()->user()->institucio->nombre_insti}}</h2>
<br>
<div class="">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_copy</i>
                  </div>
                  <p class="card-category">Cantidad Estudiantes</p>
                  <p class="card-title" id="uno">
                    {{$estudiantes}}
                  </p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  <i class="material-icons">date_range</i> Todos
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">store</i>
                  </div>
                  <p class="card-category">Cantidad Profesores</p>
                  <h3 class="card-title" id="dos">{{$profesores}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> 
                  </div>
                </div>
              </div>
            </div>

           
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">info_outline</i>
                  </div>
                  <p class="card-category">Cantidad Evaluadores</p>
                  <h3 class="card-title" id="tres">{{$evaluadores}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-user"></i>
                  </div>
                  <p class="card-category">Cantidad Acudientes</p>
                  <h3 class="card-title" id="cuatro">{{$acudientes}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
          </div>


        
        </div>
      </div>
<div class="row">
      <div id="grafica2" class="col-md-12" >
      <div class="chart-container" style="position: absolote; height:80%; width:60%; text-align: center; margin-left: auto;margin-right: auto;">
        <canvas id="bar-chart" ></canvas>
    </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
$(document).ready(function() {
var uno = {!! json_encode($estudiantes) !!};
var dos={!! json_encode($profesores) !!};
var tres={!! json_encode($evaluadores) !!};
var cuatro={!! json_encode($acudientes) !!};
var label_religion=["Estudiantes","Profesores","Evaluadores","Acudientes"];
var datos_religion=[uno,dos,tres,cuatro];

  
     mychart5=new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
          labels:label_religion ,
          datasets: [
            {
              label: "Cantidad",
              backgroundColor: [
            '#fc9309',
            '#4ba64f',
            '#e73e3a',
            '#15bbcf',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
    
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
    
          ],
          borderWidth: 1,
              data: datos_religion
            }
          ], 
        
        },
        options: {
            scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          },
            legend: { display: false },
            title: {
              
              display: true,
              text: 'Usuarios  '
            }
          }
    });

});




</script>





@endsection