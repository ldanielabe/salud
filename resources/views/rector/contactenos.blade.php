@extends('layouts.rector')

@section('content')

<br>
<br>
<br>
<br>


<div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Mensajes De Visitantes</h4>
                  <p class="card-category">Todos los mensajes</p>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                  
                      <th>Nombre</th>
                      <th>Colegio</th>
                      <th>Correo</th>
                      <th>Mensaje</th>
                      <th>Eliminar</th>
                    </thead>
                    <tbody>
                      @forelse($contactenos as $c) 
                      <tr>
                      
                        <td>{{$c->nombre}}</td>
                        <td>{{$c->institucio->nombre_insti}}</td>
                        <td>{{$c->correo}}</td>
                        <td>{{$c->mensaje}}</td>
                        <td>                                              
                          <a class="btn btn-primary"  role="button" onclick="eliminar({{$c->id}})" style="color:#fff;">Eliminar</a> 
                        </td>
                      </tr>
                      @empty

                      <div id="evento" class="alert alert-danger" role="alert">
                        no se ha registrado ningun mensaje <a href="#" class="alert-link"></a>. 
                      </div>
                      @endforelse  
  
                    </tbody>
                  </table>
                  <hr>
                  <div class="row">
                                 
                    <div col-12 style="text-align: center; margin-left: auto;
                    margin-right: auto;">
                        {{ $contactenos->render()}}
                    </div>
                
                </div>
                </div>
              </div>
            </div>
        
            
          </div>


 
      <script>

        function eliminar(id){
          
          Swal.fire({
          title: '¿Estas seguro?',
          text: "Quieres eliminar el mensaje!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'si'
        }).then((result) => {
          if (result.value) {
            Swal.fire(
              'Deleted!',
              'Mensaje Eliminado con exito',
              'success'
            ),
          borrar(id);
          }
        })
        
        };
        function borrar(id) {
                    $.ajax({
                        type: "GET",
                        url: "/deleteMensaje/"+id,
                        data: {id:id},
                        success: function (data) {
                  console.log(data);
                  location.reload();
                            }         
                    });
              
            }
        
        </script>
        
        <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>


@endsection