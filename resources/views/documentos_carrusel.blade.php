<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- head -->
    <meta charset="utf-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Autoplay usage demo">
    <meta name="author" content="Daniela Buitrago">


    <!-- links carrusel card -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,300italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('carousel/assets/css/docs.theme.min.css')}}">

    <link rel="stylesheet" href="{{ asset('carousel/assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('carousel/assets/owlcarousel/assets/owl.theme.default.min.css')}}">
 
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('carousel/assets/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="shortcut icon" href="{{ asset('carousel/assets/ico/favicon.png')}}">
    <link rel="shortcut icon" href="favicon.ico">

    <script src="{{ asset('carousel/assets/vendors/jquery.min.js')}}"></script>
    <script src="{{ asset('carousel/assets/owlcarousel/owl.carousel.js')}}"></script>
	
  </head>
  <body>

    <!-- header -->
    <header class="header">
     
    </header>

   
    <!--  Demos -->
    <section id="demos">
      <div class="row">
        <div class="large-12 columns">
          <div class="owl-carousel owl-theme">
            @forelse($documentos as $doc) 
            <div class="col-md-4 col-12">
                          <div class="card" style="width: 15rem;">
                                <img class="card-img-top" src="../imagesEventos/documentos.jpg" class="img-thumbnail" style="height:130px;">
                               <h2 id="title-event"><strong>{{$doc->titulo}}</strong></h2>
                                <div class="card-body">
                                <a href="/assets/grocery-crud/images/chosen/{{$doc->url}}" target="_blank">Mirar Documento.</a>
              
                                  <p class="card-text">{{$doc->contenido}}</p>
                                 
                                  <br>
                                </div>
                          </div>
            </div>
             
            @empty
            @endforelse  
          </div>
         
          <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 5,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: false,
                    loop: true
                  },
                  1000: {
                    items: 3,
                    nav: true,
                    loop: true,
                    margin: 1
                  }
                }
              });
              $('.play').on('click', function() {
                owl.trigger('play.owl.autoplay', [1000])
              })
              $('.stop').on('click', function() {
                owl.trigger('stop.owl.autoplay')
              })
            })
          </script>
        </div>
      </div>
    </section>

    <!-- vendors -->
    <script src="{{ asset('carousel/assets/vendors/highlight.js')}}"></script>
    <script src="{{ asset('carousel/assets/js/app.js')}}"></script>
  </body>
</html>