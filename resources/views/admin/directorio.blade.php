@extends('layouts.admin')

@section('content')
<br>
<div class="content">
    <div class="container-fluid">
		<div class="card">
<div id="seccionDirectorio" class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
	
	<button id="btnAgregarDirectorio" class="btn btn-info btn-lg">Agregar Directorio</button>

	<div id="agregarDirectorio" style="display:none">
		
		<form method="post" enctype="multipart/form-data" action="{{ route('registrarDirectorio')}}">
			{{ csrf_field() }}
			<input name="nombre" id="nombre" type="text" placeholder="Nombre del Directorio" class="form-control" required>

			<textarea name="descripcion" id="descripcion" cols="30" rows="5" placeholder="Decripcion del directorio" class="form-control"  maxlength="170" required></textarea>

			<input type="file" name="imagen" class="btn btn-default" id="imagen" required>

			<p>Tamaño recomendado: 800px * 800px, peso máximo 2MB</p>
			<div class="col-md-12" style="text-align: center;">
				<input type="submit" id="botonSlide" value="Guardar" class="btn btn-primary" style="text-aling:center;">
			   
				</div>
		</form>

	</div>
<br>
<br>
<br>

	@forelse($directorio as $dir) 


	<div class="row">
		<div class="col-4">
			<img src="imagesDirectorio/{{ $dir->imagen }}" class="img-thumbnail">
		</div>


	
		<div class="col-8">

	<li id="{{ $dir->id }}" class="bloqueArticulo" >
		<span class="handleArticle">
			
		<a class="button"  onclick="prueba({{$dir->id}})">
			<i class="fa fa-times btn btn-danger"></i>
		</a>
		</span>
		
		<img src="imagesDirectorio/{{ $dir->imagen }}" class="img-thumbnail" style="display: none">
		<h1>{{ $dir->nombre }}</h1>
		<p>{{ $dir->descripcion }}</p>

		<a href="#articulo'.{{ $dir->id }}" data-toggle="modal">
		<button class="fa fa-pencil btn btn-primary"></button>
		</a>

		<hr>

	</li>
</div>

	</div>
	<div id="articulo'.{{ $dir->id }}" class="modal fade">

		<div class="modal-dialog modal-content">

			<div class="modal-header" style="border:1px solid #eee">
			
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			 
			
			</div>
		<div class="content">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
							
							<form method="POST" enctype="multipart/form-data" action="{{ route('actualizarDirectorio',[$dir->id])}}"  >
									{{ csrf_field() }}
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											
											<input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ $dir->nombre }}" requerid>
										</div>
									</div>

									<div class="col-md-12">
											<input type="file" name="imagen" class="btn btn-default" id="subirFoto" >
										<p style="text-align: center">Tamaño recomendado: 800px * 400px, peso máximo 2MB</p>
										   
									</div>
									<div class="col-md-12">
											<div class="form-group">
													<textarea name="descripcion" id="" cols="20" rows="5" placeholder="Descripcion"   class="form-control" required>{{ $dir->descripcion }}</textarea>

													
										</div>
									</div>
								</div>
								<input type="submit"  value="Guardar" class="btn btn-primary">
								
							
							
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
									
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		


		</div>

	</div>

	@empty

	<div id="directorioNot" class="alert alert-danger" role="alert">
	  no se ha registrado ningun directorio <a href="#" class="alert-link"></a>. 
	</div>
	@endforelse   

	<hr>
	<div class="row">
                 
		<div col-12 style="text-align: center; margin-left: auto;
		margin-right: auto;">
				{{ $directorio->render()}}
		</div>

</div>	

		
	</ul>
<br>
<br>
<br>
<!--	<button id="ordenarArticulos" class="btn btn-warning pull-right" style="margin:10px 30px">Ordenar directorios</button>

	<button id="guardarOrdenArticulos" class="btn btn-primary pull-right" style="display:none; margin:10px 30px">Guardar Orden Artículos</button>
-->
</div>

</div>
</div>
</div>

<script>

function prueba(id){
	
  Swal.fire({
  title: 'Estas seguro?',
  text: "Quieres eliminar el directorio!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'si'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    ),
	borrar(id);
  }
})

};
function borrar(id) {
            $.ajax({
                type: "GET",
                url: "/eliminarDirectorio/"+id,
                data: {id:id},
                success: function (data) {
					console.log(data);
					location.reload();
                    }         
            });
			
    }

</script>



@endsection