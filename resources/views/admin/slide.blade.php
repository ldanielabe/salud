@extends('layouts.admin')

@section('content')

<div class="content">
    <div class="container-fluid">
		<div class="card">

<h2 style="">AGREGAR IMAGENES</h2>

            <div id="imgSlide" class="col-lg-10 col-md-12 col-sm-9 col-xs-12">

                <hr>
                
               
                    <span class="fa fa-arrow-down">

						
                        </span>  
                    
                    <ul id="columnasSlide">
                        	<form method="POST" enctype="multipart/form-data" id="contactsForm" action="{{ route('aggSlide')}}"  >
                                {{ csrf_field() }}
                        	
                                <input type="file" name="imagen" class="btn btn-default" id="slidefoto" style="width: 100%" required>
                                <p style="text-align: center">Tamaño recomendado: 1600px * 600px , peso máximo 2MB</p>
                                   
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="bmd-label-floating">Url(opcional)</label>
                                        <input type="text" class="form-control" name="link" >
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label class="bmd-label-floating">Titulo</label>
                                          <input type="text" class="form-control" name="titulo" >
                                        </div>
                                      </div>
                                </div>
                                <div class="col-md-12" style="text-align: center;">
                                    <input type="submit" id="botonSlide" value="Guardar" class="btn btn-primary">
                                </div>
                            
                            </form>
                            <br>
                            <br>
                    @forelse($slides as $slide) 
                        <li id="{{$slide->id}}" class="bloqueSlide">
                        <span class="fa fa-times eliminarSlide" ruta="{{$slide->ruta}}"></span>
                            <img src="imagesSlide/{{ $slide->ruta }}" class="handleImg"></li>
                   
                    @empty
                    </ul>
                    <div id="evento" class="alert alert-danger" role="alert">
                            no se ha registrado ninguna imagen <a href="#" class="alert-link"></a>. 
                            </div>
                            @endforelse   
                   
                </div>
                
                
                
                <div id="textoSlide" class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                
                <hr>
                    
                    <ul id="ordenarTextSlide">
                
                    <!--===============================================
                   
                    $slide = new GestorSlide();
                    $slide -> editorSlideController();
                
                -->
                
                    </ul>
                </div>






        </div>
    </div>
</div>





@endsection