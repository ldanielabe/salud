@extends($layout_grocery)

@section('content')

<div class="content">
    <div class="container-fluid">
		<div class="card">
<div id="seccionArticulos" class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
	
	<button id="btnAgregarArticulo" class="btn btn-info btn-lg">Agregar Evento</button>

	<div id="agregarArtículo" style="display:none">
		
		<form method="post" enctype="multipart/form-data" action="{{ route('reg_evento')}}">
			{{ csrf_field() }}
			<input name="titulo" type="text" placeholder="Nombre del evento" class="form-control" required>

			<textarea name="intro" id="" cols="30" rows="5" maxlength="100" placeholder="Introducción que se mostrara del evento" class="form-control" required></textarea>

			<input type="file" name="imagen" class="btn btn-default" id="subirFoto" required>

			<p>Tamaño recomendado: 800px * 400px, peso máximo 2MB</p>

			<textarea name="contenido" id="contenido" cols="30" rows="10" placeholder="Contenido del Evento" class="form-control" required></textarea>

			<div class="col-md-12" style="text-align: center;">
				<input type="submit" id="botonSlide" value="Guardar" class="btn btn-primary" style="text-aling:center;">
			   
				</div>
		</form>

	</div>
<br>
<br>
<br>

	@forelse($eventos as $evento) 

	<div class="row">
		<div class="col-4">
			<img src="imagesEventos/{{ $evento->ruta }}" class="img-thumbnail">
		</div>


	
		<div class="col-8">

	<li id="{{ $evento->id }}" class="bloqueArticulo" >
		<span class="handleArticle">
			
		<a class="button"  onclick="prueba({{$evento->id}})">
			<i class="fa fa-times btn btn-danger"></i>
		</a>
		</span>
		
		<img src="imagesEventos/{{ $evento->ruta }}" class="img-thumbnail" style="display: none">
		<h1>{{ $evento->titulo }}</h1>
		<div>{!! str_replace('_', ' ', $evento->introduccion) !!}</div>
	<div >
		{!! str_replace('_', ' ', $evento->contenido) !!}
		</div>
		<input type="hidden" value="{{ $evento->contenido }}">
		<a href="#articulo'.{{ $evento->id }}" data-toggle="modal">
		<button class="fa fa-pencil btn btn-primary" ></button>
		</a>

		<hr>

	</li>
</div>

	</div>
	<div id="articulo'.{{ $evento->id }}" class="modal fade">

		<div class="modal-dialog modal-content">

			<div class="modal-header" style="border:1px solid #eee">
			
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			 
			
			</div>
		<div class="content">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
							
							<form method="POST" enctype="multipart/form-data" action="{{ route('updateEventos',[$evento->id])}}"  >
									{{ csrf_field() }}
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											
											<input type="text" class="form-control" name="titulo" placeholder="Titulo" value="{{ $evento->titulo }}" requerid>
										</div>
									</div>
									<div class="col-md-12">
											<div class="form-group">
												
													<textarea class="form-control" name="introduccion" value="" cols="20" rows="5" maxlength="100" placeholder="Introducción que se mostrara del evento"    required>{{$evento->introduccion}}</textarea>
											</div>
									</div>
								
									<div class="col-md-12">
											<input type="file" name="imagen" class="btn btn-default" id="subirFoto" accept="image/jpeg, image/png" >
										<p style="text-align: center">Tamaño recomendado: 800px * 400px, peso máximo 2MB</p>
										   
									</div>
									<div class="col-md-12">
											<div class="form-group">
													<textarea name="contenido" cols="20" rows="5" placeholder="Contenido del Evento"   class="form-control" required>{{ $evento->contenido }}</textarea>

													
										</div>
									</div>
								</div>
								<input type="submit"  value="Guardar" class="btn btn-primary">
								
							
							
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
									
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		


		</div>

	</div>


	@empty

	<div id="evento" class="alert alert-danger" role="alert">
	  no se ha registrado ningun evento <a href="#" class="alert-link"></a>. 
	</div>
	@endforelse   

	<hr>
	<div class="row">
                 
		<div col-12 style="text-align: center; margin-left: auto;
		margin-right: auto;">
				{{ $eventos->render()}}
		</div>

</div>

	<ul id="editarArticulo">
		
	</ul>
<br>
<br>
<br>
<!--	<button id="ordenarArticulos" class="btn btn-warning pull-right" style="margin:10px 30px">Ordenar Eventos</button>

	<button id="guardarOrdenArticulos" class="btn btn-primary pull-right" style="display:none; margin:10px 30px">Guardar Orden Artículos</button>
-->
</div>

</div>
</div>
</div>

<script>

function prueba(id){
	
  Swal.fire({
  title: 'Estas seguro?',
  text: "Quieres eliminar el evento!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'si'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    ),
	borrar(id);
  }
})

};
function borrar(id) {
            $.ajax({
                type: "GET",
                url: "/deleteEvento/"+id,
                data: {id:id},
                success: function (data) {
					console.log(data);
					location.reload();
                    }         
            });
			
    }

</script>




@endsection