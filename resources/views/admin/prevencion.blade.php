@extends($layout_grocery)

@section('content')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<style>



</style>

<div class="container">
<br>
<br>

    <h3 style="text-align: center;font-weight: bold;">DOCUMENTOS DE INTERES</h3>
<br>
    <div class="row">
    <div id="accordion" class="col-md-12 animate__animated animate__backInLeft">
    @forelse($tipo_prevencion as $tipo) 
   
  <div class="carda">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$tipo->id}}" aria-expanded="true" aria-controls="collapseOne" style="color:#000;font-weight: bold;">
        {{$tipo->nombre}}
        </button>
      </h5>
    </div>

    <div id="collapse{{$tipo->id}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="">
         <ul class="list-group">
         @foreach($documentos as $docs) 
         @if($docs->tipo == $tipo->id)
            <li class="alert alert-info" role="alert"><a  href="/assets/grocery-crud/images/chosen/{{$docs->url}}" target="_blank">{{$docs->titulo}}</a></li>
          @endif
         @endforeach

          
        </ul>
</div>
    </div>
  </div>

       
        @empty

	<div id="evento" class="alert alert-danger" role="alert">
	  no se ha registrado ningun tipo <a href="#" class="alert-link"></a>. 
	</div>
	@endforelse   
    </div>


</div>

@endsection