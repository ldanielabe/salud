@extends('layouts.admin')

@section('content')
<style>


.collapsible {
    background-color: #777;
    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
}



.collapsible:after {
    content: '\002B';
    color: white;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "\2212";
}

.card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    width: 95%;
    margin: 1rem;
    padding: 0 !important;
}

.card:hover {
    box-shadow: 0 8px 15px 0 rgba(0, 0, 0, 0.2);
}

hr {
    height: 1px;
    width: 98%;
    background-color: #9c27b0;
}

.horizontal {
    background-color: rgb(121, 15, 187);
    color: rgba(0, 0, 0, 1);
}

.horizontalDos {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    height: 10px;
    width: calc(100% + 2px);
}

a:hover {
    background-color: white !important;
}

.btn-material {
    padding: 0 16px;
    
    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),
        0 1px 5px 0 rgba(0, 0, 0, 0.12);
    color: #fff;
    transition: background-color 15ms linear,
        box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);

    height: 36px;
    line-height: 2.25rem;
    font-family: Roboto, sans-serif;
    font-size: 0.875rem;
    font-weight: 500;
    letter-spacing: 0.06em;
    border-radius: 1rem;
    margin-left: auto;
    margin-right: auto;
    display:block;
    background: repeating-linear-gradient(45deg, #9c27b0, rgb(121, 15, 187) 100px);
    margin-right: 9rem;
}

.btn-material:hover,
.btn-material:focus {
    box-shadow: 0 2px 4px -1px rgba(0, 0, 0, 0.2), 0 4px 5px 0 rgba(0, 0, 0, 0.14),
        0 1px 10px 0 rgba(0, 0, 0, 0.12);
    background: repeating-linear-gradient(45deg, #9c27b0, rgb(121, 15, 187) 100px) !important;
}

.btn-material:active {
    box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),
        0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);
    background: repeating-linear-gradient(45deg, #9c27b0, rgb(121, 15, 187) 100px) !important;
}
</style>


<div class="content">
<a id="btn4" class="btn btn-material col-md-3" type="button" href="{{ route('agregarDominio')}}">
        <i class="material-icons">add_circle_outline</i> Dominio</a>
    <div class="container-fluid">
        <br>
    
   
        <div class="row">
            <div id="accordion" class="col-md-12 animate__animated animate__backInLeft">

                <div class="container">
                    <div class="row">
                        @for ($i = 0; $i < sizeof($dominios); $i++) <div class="card col-5" style="">
                            <div class="horizontal horizontalDos">
                            </div>
                            <a class="dropdown-item" data-toggle="modal" data-target="#ModalDominio{{$i}}"
                                onclick="allFichaSalud({{$i}});" style="font-size: 16px;font-weight: bold;">
                                <h2 style="text-transform: capitalize; font-family: fantasy; font-size: 1rem;"><i
                                        class="material-icons">library_books</i>Dominio {{($i+1)}}: {{ $dominios[$i]->nombre }}</h2>
                            </a>
                            <p style="">{{ $dominios[$i]->descripcion }}</p>

                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>

@for ($i = 0; $i < sizeof($dominios); $i++) 
<div class="modal fade" id="ModalDominio{{$i}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-left: auto;margin-right: auto;">
    <div class="modal-dialog" role="document" style="margin-left: auto;margin-right: 28%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-aling:center;"> {{ $dominios[$i]->nombre }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="input-group col-md-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Preguntas</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">

                        <select class="custom-select" id="pregunta{{$i}}" onclick="allFichaSalud({{$i}})">

                        </select>

                        <input type="text" name="Agregarpreguntas{{$i}}" style="display: none;"
                            id="Agregarpreguntas{{$i}}" class="form-control">
                        <input type="text" name="Editarpreguntas{{$i}}" style="display: none;"
                            id="Editarpreguntas{{$i}}" class="form-control">

                    </div>
                    <div class="col-12 col-md-3">

                    </div>



                    <div class="col-12 col-md-3">

                    </div>
                    <div class="col-12 col-md-6">

                        <select class="custom-select" id="tipo_input{{$i}}" disabled>
                            <option value="" select>Seleccionar tipo respuesta</option>
                            <option value="select">Lista seleccionable</option>
                            <option value="multiselect">Lista con seleccion multiple</option>
                            <option value="si_no">Falso/Verdadero</option>
                            <option value="input">Respuesta escrita</option>
                        </select>

                    </div>
                    <div class="col-12 col-md-3">
                        <button id="botonAgregarpreguntas{{$i}}" class="btn btn-primary btn-fab btn-fab-mini btn-round"
                            onclick="addPreguntas({{$i}})">
                            <i class="material-icons">add</i>
                        </button>

                        <button id="botonEditarpreguntas{{$i}}" class="btn btn-primary btn-fab btn-fab-mini btn-round"
                            onclick="jsFichaSalud('pregunta',{{$i}})">
                            <i class="material-icons">edit</i>
                        </button>

                        <button id="botonConfirmarpreguntas{{$i}}" style="display: none;"
                            onclick="guardarFichaSalud({{$i}})" class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">check</i>
                        </button>

                        <button id="botonEditarConfirmarpreguntas{{$i}}" style="display: none;"
                            onclick="editarFichaSalud('preguntas',{{$i}})"
                            class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">check</i>
                        </button>

                        <button id="botonCerrarpreguntas{{$i}}" style="display: none;" onclick="cerrarpreguntas({{$i}})"
                            class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">clear</i>
                        </button>
                    </div>


                    <hr>

                    <div class="input-group col-md-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Respuestas</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">

                        <select class="custom-select" id="respuesta{{$i}}">
                        </select>
                        <input type="text" name="Agregarrespuesta{{$i}}" style="display: none;"
                            id="Agregarrespuesta{{$i}}" class="form-control">
                        <input type="text" name="Editarrespuesta{{$i}}" style="display: none;"
                            id="Editarrespuesta{{$i}}" class="form-control">
                    </div>

                    <div class="input-group col-md-3" style="display: inherit;justify-content: center;">
                        <label class="input-group-text" for="inputGroupSelect01">Riesgo</label>
                        <input type="checkbox" name="riesgo{{$i}}" class="inline checkbox" id="riesgo{{$i}}" value="0"
                            style="margin: 0.2rem;">

                    </div>

                    <div class="col-12 col-md-9">
                    </div>
                    <div class="col-12 col-md-3">


                        <button id="botonAgregarrespuesta{{$i}}" class="btn btn-primary btn-fab btn-fab-mini btn-round"
                            onclick="addRespuestas({{$i}})">
                            <i class="material-icons">add</i>
                        </button>

                        <button id="botonEditarrespuesta{{$i}}" class="btn btn-primary btn-fab btn-fab-mini btn-round"
                            onclick="jsFichaSalud('respuesta',{{$i}})">
                            <i class="material-icons">edit</i>
                        </button>

                        <button id="botonConfirmarrespuesta{{$i}}" style="display: none;"
                            onclick="guardarFichaSalud({{$i}})" class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">check</i>
                        </button>

                        <button id="botonEditarConfirmarrespuesta{{$i}}" style="display: none;"
                            onclick="editarFichaSalud('respuesta',{{$i}})"
                            class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">check</i>
                        </button>

                        <button id="botoncerrarRespuesta{{$i}}" style="display: none;" onclick="cerrarRespuesta({{$i}})"
                            class="btn btn-primary btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">clear</i>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    @endfor


    </div>
    </div>

    </div>
    </div>

    <script>
   /* var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
*/
    function addPreguntas(id) {
    
          
                $('#tipo_input' + id).empty();
                html_tipo_input = '<option value="" hidden> Selecciona </option>' +
                    '<option value="select">Lista seleccionable</option>' +
                    '<option value="multiselect">Lista con seleccion multiple</option>' +
                    '<option value="si_no">Falso/Verdadero</option>' +
                    '<option value="input">Respuesta escrita</option>';

                $('#tipo_input' + id).append(html_tipo_input);
                
        $('#tipo_input' + id).prop("disabled", false);       
        $('#pregunta' + id).hide();
        $('#botonAgregarpreguntas' + id).hide();
        $('#botonEditarpreguntas' + id).hide();
        $('#Agregarpreguntas' + id).show();
        $('#botonConfirmarpreguntas' + id).show();
        $('#botonCerrarpreguntas' + id).show();

        $('#respuesta' + id).prop("disabled", true);
        $('#botonAgregarrespuesta' + id).prop("disabled", true);
        $('#botonEditarrespuesta' + id).prop("disabled", true);
        $('#Agregarrespuesta' + id).prop("disabled", true);
        $('#botonConfirmarrespuesta' + id).prop("disabled", true);
        $('#botoncerrarRespuesta' + id).prop("disabled", true);
    }

    function cerrarpreguntas(id) {
             
        $('#pregunta' + id).show();
        $('#botonAgregarpreguntas' + id).show();
        $('#botonEditarpreguntas' + id).show();
        $('#Agregarpreguntas' + id).hide();
        $('#Editarpreguntas' + id).hide();
        $('#botonConfirmarpreguntas' + id).hide();
        $('#botonEditarConfirmarpreguntas' + id).hide();
        $('#botonCerrarpreguntas' + id).hide();

        $('#respuesta' + id).prop("disabled", false);
        $('#botonAgregarrespuesta' + id).prop("disabled", false);
        $('#botonEditarrespuesta' + id).prop("disabled", false);
        $('#Agregarrespuesta' + id).prop("disabled", false);
        $('#botonConfirmarrespuesta' + id).prop("disabled", false);
        $('#botoncerrarRespuesta' + id).prop("disabled", false);
        $('#tipo_input' + id).prop("disabled", false);
        $('#riesgo' + id).prop("disabled", false);
    }

    function addRespuestas(id) {
        $('#respuesta' + id).hide();
        //$('#Agregarrespuesta' + id).append("");
        $('#botonAgregarrespuesta' + id).hide();
        $('#botonEditarrespuesta' + id).hide();
        $('#Agregarrespuesta' + id).show();
        $('#botonConfirmarrespuesta' + id).show();
        $('#botoncerrarRespuesta' + id).show();
    }

    function cerrarRespuesta(id) {
        $('#respuesta' + id).show();
        $('#botonAgregarrespuesta' + id).show();
        $('#botonEditarrespuesta' + id).show();
        $('#Agregarrespuesta' + id).hide();
        $('#Editarrespuesta' + id).hide();
        $('#botonConfirmarrespuesta' + id).hide();
        $('#botonEditarConfirmarrespuesta' + id).hide();
        $('#botoncerrarRespuesta' + id).hide();
    }

    function allFichaSalud(dominio) {

        pregunta_select_index = document.getElementById('pregunta' + dominio).value;
        if (pregunta_select_index == "" || pregunta_select_index == null) {
            pregunta_select_index = 0;
        }

      
        $.ajax({
            type: "GET",
            url: "/editarFicha/all/" + dominio + "/" + pregunta_select_index,
            data: {},
            success: function(data) {
            
             for (indice = 0; indice < data.preguntas.length; indice++) {
                    if (pregunta_select_index == data.preguntas[indice].id) {
                        pregunta_select_index = indice;
                        break;
                    }
                }
                
                    console.log(pregunta_select_index+" holi");
                    document.getElementById('pregunta'+dominio).innerHTML = "";
                    var html = "";
                    for (indice = 0; indice < data.preguntas.length; indice++) {
                    if(indice==pregunta_select_index){
                      html += '<option value="' + data.preguntas[indice].id + '" selected>' + data.preguntas[indice].pregunta + '</option>';
                    }else{ 
                        html += '<option value="' + data.preguntas[indice].id + '">' + data.preguntas[indice].pregunta + '</option>';
                    }
                    }

                   $('#pregunta'+dominio).append(html);
               



               

                document.getElementById('tipo_input' + dominio).innerHTML = "";
                $('#tipo_input' + dominio).empty();
                html_tipo_input = "";
                tipo_input = data.preguntas[pregunta_select_index].tipo_input;
                if (tipo_input == "select") {
                    txt_tipo_input = "Lista seleccionable";
                } else if (tipo_input == "multiselect") {
                    txt_tipo_input = "Lista con seleccion multiple";
                } else if (tipo_input == "si_no") {
                    txt_tipo_input = "Falso/Verdadero";

                } else if (tipo_input == "input") {
                    txt_tipo_input = "Respuesta escrita";
                }
                $('#tipo_input' + dominio).empty();
                html_tipo_input = '<option value="'+tipo_input+'" hidden>' + txt_tipo_input + '</option>' ;

                $('#tipo_input' + dominio).append(html_tipo_input);


                document.getElementById('respuesta' + dominio).innerHTML = "";
                rta = "";

                if (tipo_input == "si_no") {
                    rta = '<option value="si">Si</option>' +
                        '<option value="no">No</option>';
                    $('#respuesta' + dominio).append(rta);

                    $('#botonAgregarrespuesta' + dominio).prop("disabled", true);
                    $('#botonEditarrespuesta' + dominio).prop("disabled", true);
                    $('#Agregarrespuesta' + dominio).prop("disabled", true);
                    $('#botonConfirmarrespuesta' + dominio).prop("disabled", true);
                    $('#botoncerrarRespuesta' + dominio).prop("disabled", true);

                } else if (tipo_input == "input") {
                    rta = '<option disabled selected>Lista vacia</option>';
                    $('#respuesta' + dominio).append(rta);

                    $('#botonAgregarrespuesta' + dominio).prop("disabled", true);
                    $('#botonEditarrespuesta' + dominio).prop("disabled", true);
                    $('#Agregarrespuesta' + dominio).prop("disabled", true);
                    $('#botonConfirmarrespuesta' + dominio).prop("disabled", true);
                    $('#botoncerrarRespuesta' + dominio).prop("disabled", true);

                } else if (tipo_input == "select" || tipo_input == "multiselect") {
                    rta = "";
                    for (indice = 0; indice < data.respuestas.length; indice++) {
                        rta += '<option value="' + data.respuestas[indice].id + '">' +
                            data.respuestas[indice].respuesta + '</option>';

                    }
                    $('#respuesta' + dominio).append(rta);
                   
                    $('#botonAgregarrespuesta' + dominio).prop("disabled", false);
                    $('#botonEditarrespuesta' + dominio).prop("disabled", false);
                    $('#Agregarrespuesta' + dominio).prop("disabled", false);
                    $('#botonConfirmarrespuesta' + dominio).prop("disabled", false);
                    $('#botoncerrarRespuesta' + dominio).prop("disabled", false);
                }

            }
        });
    }

    function guardarFichaSalud(id) {

        prg = encodeURIComponent(document.getElementById('Agregarpreguntas' + id).value);
        
        
        rta = document.getElementById('Agregarrespuesta' + id).value;
        tipo_input = document.getElementById('tipo_input' + id).value;

        if ($('#riesgo' + id).is(':checked')) {
            $('#riesgo' + id).attr('value', '1');
        } else {
            $('#riesgo' + id).attr('value', '0');
        }

        riesgo = document.getElementById('riesgo' + id).value;
        id_pregunta = document.getElementById('pregunta' + id).value;

        if ($('#Agregarrespuesta' + id).val().length == 0) {
            rta = 1;
        }
        if ($('#Agregarpreguntas' + id).val().length == 0) {
            prg = 1;
        }
        if ($('#tipo_input' + id).val().length == 0) {
            tipo_input = 1;
        }
        if ($('#riesgo' + id).val().length == 0) {
            riesgo = 1;
        }
        if ($('#pregunta' + id).val() == null) {
            id_pregunta = 1;
        }

        $.ajax({

            type: "GET",
            url: "/editarFicha/" + id + "/" + prg + "/" + tipo_input + "/" + rta + "/" + riesgo + "/" +
                id_pregunta,
            data: {},
            success: function(data) {
                console.log(data);
               
             
             
                if (data.status == 500) {
                    toastr.error('Faltan datos.');
                    $("#Agregarpreguntas"+id).val("");
                    $("#Agregarrespuesta"+id).val("");
                } else if (data.status == 200) {
                    toastr.success('Datos guardados.');
                    cerrarpreguntas(id);
                    cerrarRespuesta(id);
                    allFichaSalud(id);
                    
                    if(id_pregunta==1){
                     
                      allFichaSalud(id);
                    }
                }
            }
        });

    }

    function editarFichaSalud(datos, id) {

        if (datos == 'preguntas') {
            var prg = encodeURIComponent(document.getElementById('Editarpreguntas' + id).value);
            id_pregunta = document.getElementById('pregunta' + id).value;
            $.ajax({

                type: "GET",
                url: "/editarPreguntas/" + id_pregunta + "/" + prg,
                data: {},
                success: function(data) {
                    console.log("editar prg" + id);


                    if (data.status == 500) {
                        toastr.error('Faltan datos.');
                    } else if (data.status == 200) {
                     
                      
                        toastr.success('Datos guardados.');
                        cerrarpreguntas(id);
                        cerrarRespuesta(id);
                        allFichaSalud(id);
                     
                    }
                }
            });
        } else {
            id_rta = document.getElementById('respuesta' + id).value;
            rta = document.getElementById('Editarrespuesta' + id).value;
            riesgo = document.getElementById('riesgo' + id).value;
            $.ajax({

                type: "GET",
                url: "/editarRespuestas/" + rta + "/" + riesgo + "/" + id_rta,
                data: {},
                success: function(data) {
                    console.log("Editar respuestas: " + data);
                    


                    if (data.status == 500) {
                        toastr.error('Faltan datos.');
                    } else if (data.status == 200) {
                       
                        toastr.success('Datos guardados.');
                        cerrarpreguntas(id);
                        cerrarRespuesta(id);
                        allFichaSalud(id);
                    }
                }
            });
        }


    }

    function jsFichaSalud(datos, id) {

        if (datos == 'pregunta') {
            $("#Editarpreguntas" + id).val($("#pregunta" + id + " option:selected").text());
            $('#pregunta' + id).hide();
            $('#botonAgregarpreguntas' + id).hide();
            $('#botonEditarpreguntas' + id).hide();
            $('#Editarpreguntas' + id).show();
            $('#botonEditarConfirmarpreguntas' + id).show();
            $('#botonCerrarpreguntas' + id).show();
            $('#tipo_input' + id).prop("disabled", true);
            $('#riesgo' + id).prop("disabled", true);
        

        } else {
            console.log("edit rta: " + id);
            $("#Editarrespuesta" + id).val($("#respuesta" + id + " option:selected").text());
            $('#respuesta' + id).hide();
            $('#botonAgregarrespuesta' + id).hide();
            $('#botonEditarrespuesta' + id).hide();
            $('#Editarrespuesta' + id).show();
            $('#botonEditarConfirmarrespuesta' + id).show();
            $('#botoncerrarRespuesta' + id).show();
        }
    }
    </script>
    @endsection