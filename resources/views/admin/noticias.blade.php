@extends('layouts.admin')

@section('content')


<div class="content">
    <div class="container-fluid">
		<div class="card">
<div id="seccionArticulos" class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
	
	<button id="btnAgregarArticulo" class="btn btn-info btn-lg">Agregar Noticia</button>

	<div id="agregarArtículo" style="display:none">
		
		<form method="post" enctype="multipart/form-data" action="{{ route('reg_noticia')}}">
			{{ csrf_field() }}
			<input name="titulo" type="text" placeholder="titulo de la noticia" class="form-control" required>

			<input type="file" name="imagen" class="btn btn-default" id="subirFoto" required>

			<p>Tamaño recomendado: 800px * 400px, peso máximo 2MB</p>

			<textarea name="contenido" id="" cols="30" rows="10" placeholder="Contenido de la noticia" class="form-control" required></textarea>

		 <div class="col-md-12" style="text-align: center;">
            <input type="submit" id="botonSlide" value="Guardar" class="btn btn-primary" style="text-aling:center;">
           
            </div>
		</form>

	</div>
<br>
<br>
<br>

	@forelse($noticias as $noticia) 

	<div class="row">
		<div class="col-4">
			<img src="./imagesNoticias/{{ $noticia->ruta }}" class="img-thumbnail">
		</div>


	
		<div class="col-8">

	<li id="{{ $noticia->id }}" class="bloqueArticulo" >
		<span class="handleArticle">
			
		<a class="button"  onclick="prueba({{$noticia->id}})">
			<i class="fa fa-times btn btn-danger"></i>
		</a>
		</span>
		<h1>{{ $noticia->titulo }}</h1>
		<div>{!! str_replace('_', ' ', $noticia->contenido) !!}</div>

	
		<input type="hidden" value="{{ $noticia->contenido }}">
		<a href="#articulo'.{{ $noticia->id }}" data-toggle="modal">
		<button class="fa fa-pencil btn btn-primary"></button>
		</a>

		<hr>

	</li>
</div>

	</div>
	<div id="articulo'.{{ $noticia->id }}" class="modal fade">

		<div class="modal-dialog modal-content">

			<div class="modal-header" style="border:1px solid #eee">
			
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			 
			
			</div>
		<div class="content">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
							
							<form method="POST" enctype="multipart/form-data" action="{{ route('updateNoticias',[$noticia->id])}}"  >
									{{ csrf_field() }}
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											
											<input type="text" class="form-control" name="titulo" placeholder="Titulo" value="{{ $noticia->titulo }}" requerid>
										</div>
									</div>
								
									<div class="col-md-12">
											<input type="file" name="imagen" class="btn btn-default" id="subirFoto" >
										<p style="text-align: center">Tamaño recomendado: 800px * 400px, peso máximo 2MB</p>
										   
									</div>
									<div class="col-md-12">
											<div class="form-group">
													<textarea name="contenido" id="" cols="20" rows="5" placeholder="Contenido del Evento"   class="form-control" required>{{ $noticia->contenido }}</textarea>

													
										</div>
									</div>
								</div>
								<input type="submit"  value="Guardar" class="btn btn-primary">
								
							
							
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
									
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		


		</div>

	</div>


	@empty

	<div id="evento" class="alert alert-danger" role="alert">
	  no se ha registrado ninguna noticia <a href="#" class="alert-link"></a>. 
	</div>
	@endforelse   

	<hr>
	<div class="row">
                 
		<div col-12 style="text-align: center; margin-left: auto;
		margin-right: auto;">
				{{ $noticias->render()}}
		</div>

</div>	

	<ul id="editarArticulo">
		
	</ul>
<br>
<br>
<br>

</div>

</div>
</div>
</div>


<script>

    function prueba(id){
        
      Swal.fire({
      title: 'Estas seguro?',
      text: "Quieres eliminar la noticia!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'si'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Borrando!',
          'Noticia ELiminada',
          'success'
        ),
        borrar(id);
      }
    })
    
    };
    function borrar(id) {
                $.ajax({
                    type: "GET",
                    url: "/deleteNoticia/"+id,
                    data: {id:id},
                    success: function (data) {
                        console.log(data);
                        location.reload();
                        }         
                });
                
        }
    
    </script>





@endsection