<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- head -->
    <meta charset="utf-8">
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Autoplay usage demo">
    <meta name="author" content="Daniela Buitrago">


    <!-- links carrusel card -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,300italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('carousel/assets/css/docs.theme.min.css')}}">

    <link rel="stylesheet" href="{{ asset('carousel/assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('carousel/assets/owlcarousel/assets/owl.theme.default.min.css')}}">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('carousel/assets/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="shortcut icon" href="{{ asset('carousel/assets/ico/favicon.png')}}">
    <link rel="shortcut icon" href="favicon.ico">

    <script src="{{ asset('carousel/assets/vendors/jquery.min.js')}}"></script>
    <script src="{{ asset('carousel/assets/owlcarousel/owl.carousel.js')}}"></script>
	<style>
  .card-img-top{
    height: 10rem;
    width: 8rem; 
  }

  .rowcita{
    margin: 0 auto;
    max-width: 80%;
    width: 100%;
  }


  @media screen and (max-width: 600px) {

    .card-img-top{
    height: 5rem;
    width: 5rem; 
  }

  .rowcita{
    margin: 0;
    padding: 0;
    width: 100%;
  }
 
  }

  </style>
  </head>
  <body>

    <!-- header -->
    <header class="header">
     
    </header>

   
    <!--  Demos -->
    <section id="demos">
      <div class="rowcita">
        <div class="large-12 columns">
          <div class="owl-carousel owl-theme">
            @forelse($directorio as $dir) 
            <div class="col-md-4 col-12">
                          <div class="card" >
                                <img class="card-img-top class="rounded-circle dir"" src="imagesDirectorio/{{ $dir->imagen }}" class="img-thumbnail" style="border-radius: 70%;">
                              
                                <a class="nav-link" rel="tooltip" title="" data-placement="bottom" target="_blank" href="https://ww2.ufps.edu.co/" data-original-title="{{ $dir->descripcion }}">
                                <h3 style="text-align: center; font-w"><strong>{{ $dir->nombre }}</strong></h3>
                               
                                <div class="ripple-container"></div></a>

                          </div>
            </div>
             
            @empty
            @endforelse  
          </div>
          <!--a class="button secondary play">Play</a> 
          <a class="button secondary stop">Stop</a--> 
          
   
          <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 15,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 3,
                    nav: true,
                    loop: true
                  },
                  600: {
                    items: 4,
                    loop: true,
                    nav: false
                  },
                  1000: {
                    items: 6,
                    nav: true,
                    loop: true,
                    margin: 10
                  }
                }
              });
              $('.play').on('click', function() {
                owl.trigger('play.owl.autoplay', [1000])
              })
              $('.stop').on('click', function() {
                owl.trigger('stop.owl.autoplay')
              })
            })
          </script>
        </div>
      </div>
    </section>

    <!-- vendors -->
    <script src="{{ asset('carousel/assets/vendors/highlight.js')}}"></script>
    <script src="{{ asset('carousel/assets/js/app.js')}}"></script>
  </body>
</html>