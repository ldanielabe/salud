<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>



</head>

<body>

    <style>
    .fondo-header {
        background: #f2f2f2;
    }

    .content-padding {
        padding: 37px 37px 37px 37px;
    }

    .fechycot {
        text-align: right;
        padding-top: 16px;
    }

    .text1>label {
        margin-bottom: 0px;
    }

    #inmmobiliarialogo {
        box-shadow: 0px 0px 2px #8a8a8a;
        border-radius: 6px;
    }

    .fpago {
        box-shadow: inset 0px -1px #8a8a8a;
    }

    .fpago2 {
        box-shadow: inset 0px -1px #8a8a8a;
        margin-left: 0px;
    }

    .labelizq>label {
        text-align: right;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
    }

    table tr:nth-child(2n-1) td {
        background: #F5F5F5;
    }

    table th,
    table td {
        text-align: left;
    }

    table th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
    }
    </style>

    <div class="content-padding">
        <div class="row fondo-header">
            <div class="col-sm-4 "><img src="{{ url('Banner-pdf.jpg')}}" style="width:630px;"></div>
            <div class="col-sm-8 fechycot">Fecha: {{$fecha}}</div>
        </div> 

        <div class="row inmobiliaria" style="padding-top:24px;">
            @if($estudiante->imagen!="")
            <div class="logohead1" style="padding-left:20px;">
                <img id="estudiante-foto" src="{{ url('/assets/grocery-crud/images/chosen/'.$estudiante->imagen)}}"
                    style="max-height: 100px; max-width: 90px;">
            </div>
            @endif
            <div class="col-sm-11">
                <h2 id="nombre" style="text-align: center;font-weight: bold;">Historial del Estudiante
                    {{$estudiante->nombre}}</h2>

            </div>

        </div>
    </div>


    <div class="row Fotop" style="display: inline-flex;">
        <div class="col-sm-7 " style="padding-top:20px; padding-left:10px;">
            <img id="logo" style="width:75px;" src="{{ url('logo.png')}}">
        </div>
        <div class="col-sm-5 " style="padding-left:20px;">
            <strong> DirecciÃ³n del estudiante: {{$estudiante->direccion}}</strong>
        </div>

    </div>
    <br>

    <h2>Respuestas del formulario</h2>
    <br>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>No.</th>
                <th>Pregunta</th>
                <th>Respuesta</th>
            </tr>
        </thead>
        <tbody>
            <?php $cont=1;?>
    @for ($i = 0; $i < sizeof($dominios); $i++) 
        @for ($j=0; $j < sizeof($ficha); $j++) 
            @if($dominios[$i]->id==$ficha[$j]->id_dominio)
                
                @if($ficha[$j]->tipo_input=="multiselect"&&$ficha[$j+1]!=null)
                <?php $cont_prg=1;?>
                @while($ficha[$j]->pregunta==$ficha[$j+1]->pregunta)
                <tr>

                    @if($cont_prg==1)
                    <td>{{$cont}}</td>
                    <td>{{$ficha[$j]->pregunta}}</td>
                    <?php $cont++;?>
                    @else
                    <td></td>
                    <td></td>
                    @endif

                    <td>{{$ficha[$j]->respuestas}}</td>
                </tr>
                <?php $j++;?>
                <?php $cont_prg++;?>
                @endwhile
                @else
               
                <tr>
                    <td>{{$cont}}</td>
                    <td>{{$ficha[$j]->pregunta}}</td>
                    <td>{{$ficha[$j]->respuestas}}</td>
                </tr>
                <?php $cont++;?>
               
                @endif

               
            @endif
        @endfor
    @endfor
        </tbody>
        <tfoot>
            <!--tr>
                <td colspan="2"></td>
                <td>Valorizacion</td>
                <td>60/100</td>
            </tr-->
        </tfoot>
    </table>

    <h2>DescripciÃ³n</h2>
    <br>
    <p>El estudiante {{$estudiante->nombre}} {{$estudiante->apellidos}} identificado con la terjeta de identidad No.
        {{$estudiante->documento}}, tiene {{$edad}} aÃ±os, su tipo de sangre es {{$estudiante->grupo_sanguineo}}, tiene
        un estado nutricional {{$estudiante->estado_nutricional}}, se escuentra
        domiciliado en la direcciÃ³n {{$estudiante->area_residencia}} este estudiante esta inscrito en el grado
        {{$estudiante->grado}} en la jornada de la {{$estudiante->jornada}} en el {{$institucion[0]->nombre_insti}}
        ubicado en
        {{$institucion[0]->direccion}};
    </p>

    <br><br>

    <small style="text-align: justify;"><strong> Nota: </strong>  La informaciÃ³n entregada en el presente documento 
        es suministrada directamente por la instituciÃ³n y es responsabilidad del usuario asegurar la confidencialidad de la informaciÃ³n acÃ¡ suministrada recolectada del estudiante, 
        Toda la informaciÃ³n esta amparada bajo la Ley Estatutaria 1581 de 2012, por la cual se dictan disposiciones generales para la protecciÃ³n de datos personales. Resaltando lo descrito en los ArtÃ­culos 2,5,6 y 7 de la presente ley.
        El fin de este documento es ayudar a dar cumplimiento a la Ley 115  Art 14 de 1994, la cual contempla la formaciÃ³n de los ciudadanos mediante el fortalecimiento y expansiÃ³n de los programas pedagÃ³gicos transversales y del programa de Competencias Ciudadanas la cual estipula que toda 
        Entidad Educativa debe aplicar entornos saludables. 

        Nuestra polÃ­tica de tratamiento de informaciÃ³n define, entre otros, los principios que cumpliremos al
        recolectar, almacenar y usar los datos personales de los estudiantes de la institucion, que se traduce en
        actuar con responsabilidad al momento de recopilar la informaciÃ³n personal y proteger su privacidad,
        garantizar la confidencialidad de la ficha de salud conforme lo seÃ±alado en la Ley Estatutaria 1581 de 2012.

        De esta forma, toda ficha de salud tendra su privacidad donde solo los especialistas tendran acceso a esta
        informaciÃ³n en las condiciones que especifique, debe ser obtenida directamente por el interesado en
        las oficinas de la instituciÃ³n, en documento original.

        Si tiene alguna duda puede comunicarce con la Institucion Educativa al telÃ©fono: {{$institucion[0]->telefono}}
    </small>


    </div>



</body>

</html>