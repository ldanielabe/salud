@extends('layouts.app_inicio')

@section('content')
<div class="container container_register">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header text-md-center">{{ __('Registrar') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                 <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                            <label for="nombre" class="col-form-label text-md-left">{{ __('Nombre') }}</label>

                            <div class="">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6 col-md-6 form-group ">
                        
                            <label for="correo" class="col-form-label text-md-left">{{ __('Correo') }}</label>

                            <div class="">
                                <input id="correo" type="email" class="form-control{{ $errors->has('correo') ? ' is-invalid' : '' }}" name="correo" value="{{ old('correo') }}" required>

                                @if ($errors->has('correo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('correo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                </div>
                       

                <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="documento" class="col-form-label text-md-left">{{ __('Documento de identidad') }}</label>

                            <div class="">
                                <input id="documento" type="number"  class="form-control" name="documento"  maxlength="15" required>

                                @if ($errors->has('documento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('documento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-6 col-md-6 form-group ">
                            <label for="celular" class="col-form-label text-md-left">{{ __('Celular') }}</label>

                            <div class="">
                                <input id="celular" type="text" class="form-control" name="celular" maxlength="10" required>
                            </div>
                        </div>
                </div>





                <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="fechaNacimiento" class="col-form-label text-md-left">{{ __('Fecha de Nacimiento') }}</label>

                            <div class="">
                                <input id="fechaNacimiento" type="text" class="form-control" name="fechaNacimiento" maxlength="20" required>

                            </div>
                        </div>

                        <div class="col-6 col-md-6 form-group ">
                            <label for="direccion" class="col-form-label text-md-left">{{ __('Direccion') }}</label>

                            <div class="">
                                <input id="direccion" type="text" class="form-control" name="direccion" required>
                            </div>
                        </div>
                </div>


                <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="barrio" class="col-form-label text-md-left">{{ __('Barrio') }}</label>

                            <div class="">
                                <input id="barrio" type="text" class="form-control" name="barrio" required>

                            </div>
                        </div>

                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="institucion" class="col-form-label text-md-left">{{ __('institucion') }}</label>

                            <div class="">
                                <input id="institucion" type="text" class="form-control" name="institucion" required>

                            </div>
                        </div>                        

                        <!--div class="col-6 col-md-6 form-group ">
                            <label for="institucion" class="col-form-label text-md-left">{{ __('Nombre de Institucion Educativa') }}</label>

                            <div class="">
                               
                                <select id="institucion"  class="form-control" name="institucion" required>
                                    <option value="Nuestra Señora de las Mercedes">Nuestra Señora de las Mercedes</option>
                                    <option value="Alirio Vergel Pacheco">Alirio Vergel Pacheco</option>
                                    <option value="Argelino Durán Quintero">Argelino Durán Quintero</option>
                                </select>
                            </div>
                        </div-->
                </div>



                <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="grado" class="col-form-label text-md-left">{{ __('Grado') }}</label>

                            <div class="">
                                <input id="grado" type="text" class="form-control" name="grado" maxlength="2" required>

                            </div>
                        </div>


                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="jornada" class="col-form-label text-md-left">{{ __('Jornada') }}</label>

                            <div class="">
                                <input id="jornada" type="text" class="form-control" name="jornada" maxlength="7" required>

                            </div>
                        </div>
                        <!--div class="col-6 col-md-6 form-group ">
                            <label for="jornada" class="col-form-label text-md-left">{{ __('Jornada') }}</label>

                            <div class="">
                                <select id="jornada"  class="form-control" name="jornada" required>
                                    <option value="mañana">Mañana</option>
                                    <option value="tarde">Tarde</option>
                                </select>
                            </div-->


                        </div>
                </div>













                <!-- CONTRASEÑA-->
                <div class="row">
                        <div class="col-6 col-md-6 form-group ">
                       
                            <label for="contrasena" class="col-form-label text-md-left">{{ __('Contraseña') }}</label>

                            <div class="">
                                <input id="contrasena" type="password" class="form-control{{ $errors->has('contrasena') ? ' is-invalid' : '' }}" name="contrasena" required>

                                @if ($errors->has('contrasena'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contrasena') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-6 col-md-6 form-group ">
                            <label for="contrasena-confirm" class="col-form-label text-md-left">{{ __('Confirmar contraseña') }}</label>

                            <div class="">
                                <input id="contrasena-confirm" type="password" class="form-control" name="contrasena_confirm" required>
                            </div>
                        </div>
                </div>



                        <div class="form-group row mb-0 text-md-right">
                            <div class="col-md-12 offset-md-12">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
