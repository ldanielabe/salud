<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    AppSalud
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="./assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="./assets/demo/demo.css" rel="stylesheet" />
  <link href="./assets/css/app.css" rel="stylesheet" />

  
  @yield('head')

</head>
<body>

  <nav class=" navbar navbar-expand-lg fixed-top navbar-color-on-scroll " color-on-scroll="120" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" data-scroll href="{{ route('ini') }}"> <img src="{{ asset('logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
   

          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="{{ route('ini') }}" onclick="myFunction('Noticias')" style=" width:100%;margin-right:60px;margin-left:-20px;">
            <i class="fa fa-fw fa-book"></i>Noticias
          </a>
          </li>
          
          <li class="nav-item">
          <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Eventos')" style=" width:100%;color:#fff;margin: 4px;margin-right:15px;">
          <i class="fa fa-fw fa-calendar"></i> Eventos
          </a>
          </li>
          <li class="nav-item">
          <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Directorio')" style=" width:100%;color:#fff;margin-right:15px;">
          <i class="fa fa-fw fa-address-book"></i> Directorio
          </a>
          </li>

          <li class="nav-item">
              <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Contactenos')" style=" width:100%;color:#fff;margin-right:5px;">
                  <i class="fa fa-fw fa-id-card"></i> Contactenos
              </a>
              </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}" style=" width:100%;color:#fff;">
              
              <i class="fa fa-fw fa-user"  ></i>Ingresar
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Siguenos en YouTube">
              <i class="fa fa-youtube"></i>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>

        


        </ul>
      </div>
    </div>
  </nav>

  
<div class="container container_login">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h1 class="text-center" style="color:#000; font-weight: bold;">Iniciar sesión</h1>               
  
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                        <div class="col-md-4 col-4 col-form-label text-md-center"><label for="email" class="">{{ __('Correo Electronico') }}</label>
                        </div>
                            <div class="col-md-7 col-7">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-4 col-4 col-form-label text-md-center"><label for="password">{{ __('Contraseña') }}</label>
                        </div>
                            <div class="col-md-7 col-7">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Ingresar') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Olvidaste tu contraseña?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer" data-background-color="black">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="#">
              Quienes Somos
            </a>
          </li>
          <li>
            <a href="#">
              Entidades
            </a>
          </li>
          <li>
            <a href="#">
              Mision y vision
            </a>
          </li>
          <li>
            <a href="#">
              Contactenos
            </a>
          </li>

          
        </ul>
      </nav>
      <div class="float-right">
            <a href="https://ww2.ufps.edu.co/" target="_blank">Build by:</a> Universidad Francisco de Paula Santander.
            <script>
                document.write(new Date().getFullYear())
            </script> 
          <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
            Cúcuta 
          <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
      </div>
    </div>
  </footer>



  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
   

  </script>
  @yield('js')
</body>
</html>
