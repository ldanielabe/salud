<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon.png') }}">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    ASIE
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/material-kit.css?v=2.0.6') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project 3219155922--wp - 
                        corrdinadora academico karina- 3102637630-->
  <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet"><!-- FontAwesome Icons -->
  <link href="{{ asset('assets/css/templatemo_style.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

<style>
html {
  scroll-behavior: smooth;
  touch-action: auto;
}
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 100%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}


</style>
<style>

.animar {
  display: inline-flexbox;
}



/* Click animations styles */

.click-animations {

  padding-bottom: 20px;
  background: #f6f8fa;
}




.click-animations input:focus,
.click-animations textarea:focus {
  border-color: #30cc8b;
}

.click-animations button {
  display: block;
  margin: 20px auto;
  padding: 10px;
  font-size: 20px;
  border: none;
  outline: none;
  background: #30cc8b;
  color: #fff;
  cursor: pointer;
  transition: 0.2s;
}

.click-animations button:active {
  transform: scale(0.95);
}
.form-error {
  border-color: #F46036 !important;
}

.funky-animations {
  text-align: center;
}

.funky-animations h3 {
  font-size: 2em;
}

.funky-animations div {
  margin: 120px auto;
  padding-top: 6px;
  height: 100px;
  width: 320px;
  border: 4px solid #f6f8fa;
}

.funky-animations h4 {
  font-size: 4.5em;
  font-weight: 400;
  /* Fallback color */
  color: #30cc8b;
  background: -webkit-linear-gradient(left, #69bcf4, #30cc8b);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  cursor: pointer;
}
.card2 {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 20%;
}

.card2:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

</style>
</head>
<body>

<nav class=" navbar navbar-expand-lg fixed-top navbar-color-on-scroll " color-on-scroll="120" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" data-scroll href="{{ route('ini') }}"> <img src="{{ asset('logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
   

          <li class="nav-item">
          <a class="btn btn-info btn-round" data-scroll href="{{ route('ini') }}" onclick="myFunction('Noticias')" style=" width:100%;margin-right:60px;margin-left:-20px;">
            <i class="fa fa-fw fa-book"></i>Noticias
          </a>
          </li>
          
          <li class="nav-item">
          <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Eventos')" style=" width:100%;color:#fff;margin: 4px;margin-right:15px;">
          <i class="fa fa-fw fa-calendar"></i> Eventos
          </a>
          </li>
          <li class="nav-item">
          <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Directorio')" style=" width:100%;color:#fff;margin-right:15px;">
          <i class="fa fa-fw fa-address-book"></i> Directorio
          </a>
          </li>

          <li class="nav-item">
              <a class="" data-scroll href="{{ route('ini') }}" onclick="myFunction('Contactenos')" style=" width:100%;color:#fff;margin-right:5px;">
                  <i class="fa fa-fw fa-id-card"></i> Contactenos
              </a>
              </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}" style=" width:100%;color:#fff;">
              
              <i class="fa fa-fw fa-user"  ></i>Ingresar
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Siguenos en YouTube">
              <i class="fa fa-youtube"></i>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom"  target="_blank" data-original-title="Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>

        


        </ul>
      </div>
    </div>
  </nav>
<br>
<br>
<br>

<div class="container card">
    <div class="section ">

        <br>
        <br>
        <div class="row">
        <div class="col-12 col-md-6"> 
        <img  src="../imagesNoticias/{{$noticia->ruta}}"  style="width: 500px; height: 250px;" alt="">

        </div>
        <div class="col-12 col-md-6"> 
        
        <h2 style="text-align: justify;font-weight: bold;">{{$noticia->titulo}}</h2>
        </div>
        <div class="col-12">
        <br> 
        <p style="text-align: justify;">{{$noticia->contenido}}</p>

        </div>
        </div>
      


       
    </div>


</div>
<br>
<br>

<footer class="footer" data-background-color="black"  style="background-color: #1d1d1d;">





<div class="container">


<br>



  <nav style="background-color: #1d1d1d;">
    <ul>
      <li>
        <a href="#">
          Quienes Somos
        </a>
      </li>
      <li>
        <a href="{{ route('ini') }}">
          Entidades
        </a>
      </li>
  
      <li>
        <a href="{{ route('ini') }}">
          Contactenos
        </a>
      </li>
      
    </ul>
  </nav>
  <div  data-background-color="black"  style="background-color: #1d1d1d;">
        <a href="https://ww2.ufps.edu.co/" target="_blank">Build by:</a> Universidad Francisco de Paula Santander.
        <script>
            document.write(new Date().getFullYear())
        </script> 
       <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
        Cúcuta 
      <img alt="love" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTciIHZpZXdCb3g9IjAgMCAyMSAxNyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+bG92ZTwvdGl0bGU+PHBhdGggZD0iTTE0LjcyNS4wMzJhNS4zMSA1LjMxIDAgMCAwLTQuNjg3IDIuODE0IDUuMzEyIDUuMzEyIDAgMCAwLTEwIDIuNDk4YzAgNC43NjMgNS44MzQgNy4zOTcgMTAgMTEuNTY0IDQuMzA2LTQuMzA2IDEwLTYuNzYgMTAtMTEuNTYzQTUuMzEyIDUuMzEyIDAgMCAwIDE0LjcyNS4wMzJ6IiBmaWxsPSIjRTgyRjJGIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjwvcGF0aD48L3N2Zz4K" class="heart">
  </div>
</div>
</footer>



  <!--   Core JS Files   -->
  <script src="{{ asset('assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/moment.min.js') }}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/material-kit.js?v=2.0.6') }}" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0.0/dist/smooth-scroll.polyfills.min.js"></script>
  <script src="{{ asset('assets/js/templatemo_script.js') }}"></script>
  <script>
    $(document).ready(function() {
      function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    //alert(docViewTop);
    var docViewBottom = docViewTop + $(window).height();
    //alert(docViewBottom);
    var elemTop = $(elem).offset().top;
    //alert(elemTop);
    var elemBottom = elemTop + $(elem).height();
    //alert(elemBottom);   
    return ((elemBottom < docViewBottom) && (elemTop > docViewTop));
  }
      $(window).scroll(function() {
        $('.scroll-animations .animated').each(function() {
          if (isScrolledIntoView(this) === true) {
            $(this).addClass('animate__animated animate__slideInLeft');
          }
        });

        $('.animar').each(function() {
          if (isScrolledIntoView(this) === true) {
            //alert("a");
            $(this).addClass('animate__animated animate__fadeInUpBig');
          }
        });

      });
      



      
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });

    var scroll = new SmoothScroll('a[href*="#"]');
   
    function myFunction(titulo){
      document.title = 'ASIE::'+titulo;
}


  </script>
</body>

</html>