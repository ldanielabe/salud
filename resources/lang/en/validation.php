<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    |  following language lines contain  default error messages used by
    |  validator class. Some of these rules have multiple versions such
    | as  size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute debe ser aceptado.',
    'active_url'           => ' :attribute no es valida la URL.',
    'after'                => ' :attribute debe ser una fecha posterior :date.',
    'after_or_equal'       => ' :attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => ' :attribute solo puede contener letras.',
    'alpha_dash'           => ' :attribute solo puede contener letras, números, guiones y guiones bajos.',
    'alpha_num'            => ' :attribute solo puede contener letras y números.',
    'array'                => ' :attribute debe ser una matriz.',
    'before'               => ' :attribute debe ser una fecha antes :date.',
    'before_or_equal'      => ' :attribute debe ser una fecha antes o igual a :date.',
    'between'              => [
        'numeric' => ' :attribute must be between :min and :max.',
        'file'    => ' :attribute must be between :min and :max kilobytes.',
        'string'  => ' :attribute must be between :min and :max characters.',
        'array'   => ' :attribute must have between :min and :max items.',
    ],
    'boolean'              => ' :attribute field must be true or false.',
    'confirmed'            => ' :attribute confirmation does not match.',
    'date'                 => ' :attribute is not a valid date.',
    'date_format'          => ' :attribute does not match  format :format.',
    'different'            => ' :attribute and :other must be different.',
    'digits'               => ' :attribute must be :digits digits.',
    'digits_between'       => ' :attribute must be between :min and :max digits.',
    'dimensions'           => ' :attribute has invalid image dimensions.',
    'distinct'             => ' :attribute field has a duplicate value.',
    'email'                => ' :attribute must be a valid email address.',
    'exists'               => ' selected :attribute is invalid.',
    'file'                 => ' :attribute must be a file.',
    'filled'               => ' :attribute field must have a value.',
    'gt'                   => [
        'numeric' => ' :attribute must be greater than :value.',
        'file'    => ' :attribute must be greater than :value kilobytes.',
        'string'  => ' :attribute must be greater than :value characters.',
        'array'   => ' :attribute must have more than :value items.',
    ],
    'gte'                  => [
        'numeric' => ' :attribute must be greater than or equal :value.',
        'file'    => ' :attribute must be greater than or equal :value kilobytes.',
        'string'  => ' :attribute must be greater than or equal :value characters.',
        'array'   => ' :attribute must have :value items or more.',
    ],
    'image'                => ' :attribute must be an image.',
    'in'                   => ' selected :attribute is invalid.',
    'in_array'             => ' :attribute field does not exist in :other.',
    'integer'              => ' :attribute must be an integer.',
    'ip'                   => ' :attribute must be a valid IP address.',
    'ipv4'                 => ' :attribute must be a valid IPv4 address.',
    'ipv6'                 => ' :attribute must be a valid IPv6 address.',
    'json'                 => ' :attribute must be a valid JSON string.',
    'lt'                   => [
        'numeric' => ' :attribute must be less than :value.',
        'file'    => ' :attribute must be less than :value kilobytes.',
        'string'  => ' :attribute must be less than :value characters.',
        'array'   => ' :attribute must have less than :value items.',
    ],
    'lte'                  => [
        'numeric' => ' :attribute must be less than or equal :value.',
        'file'    => ' :attribute must be less than or equal :value kilobytes.',
        'string'  => ' :attribute must be less than or equal :value characters.',
        'array'   => ' :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => ' :attribute may not be greater than :max.',
        'file'    => ' :attribute may not be greater than :max kilobytes.',
        'string'  => ' :attribute may not be greater than :max characters.',
        'array'   => ' :attribute may not have more than :max items.',
    ],
    'mimes'                => ' :attribute must be a file of type: :values.',
    'mimetypes'            => ' :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => ' :attribute must be at least :min.',
        'file'    => ' :attribute must be at least :min kilobytes.',
        'string'  => ' :attribute must be at least :min characters.',
        'array'   => ' :attribute must have at least :min items.',
    ],
    'not_in'               => ' :attribute la selección no es válida.',
    'not_regex'            => ' :attribute el formato no es válido.',
    'numeric'              => ' :attribute Tiene que ser un número.',
    'present'              => ' :attribute el campo debe estar presente.',
    'regex'                => ' :attribute formato es invalido.',
    'required'             => ' :attribute El campo es obligatorio.',
    'required_if'          => ' :attribute fEl campo es obligatorio cuando ninguno de :other es :value.',
    'required_unless'      => ' :attribute el campo es obligatorio a menos que :other este en :values.',
    'required_with'        => ' :attribute El campo es obligatorio cuando ninguno de :values este presente.',
    'required_with_all'    => ' :attribute fEl campo es obligatorio cuando ninguno de :values este presente.',
    'required_without'     => ' :attribute El campo es obligatorio cuando ninguno de :values no esta presente.',
    'required_without_all' => ' :attribute El campo es obligatorio cuando ninguno de :values esta presente.',
    'same'                 => ' :attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => ' :attribute debe tener :size.',
        'file'    => ' :attribute debe tener :size Kilobytes.',
        'string'  => ' :attribute debe tener :size caracteres.',
        'array'   => ' :attribute debe tener :size items.',
    ],
    'string'               => ' :attribute debe ser texto.',
    'timezone'             => ' :attribute debe ser una zona valida.',
    'unique'               => ' :attribute ya existe.',
    'uploaded'             => ' :attribute no se pudo cargar.',
    'url'                  => ' :attribute formato no es valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name  lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    |  following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
