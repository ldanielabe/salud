/*=============================================
Agregar artículo          
=============================================*/
$("#btnAgregarDirectorio").click(function(){

	$("#agregarDirectorio").toggle(400);
	$("#directorio").toggle(400);
})

/*=============================================
Subir Imagen a través del Input         
=============================================*/
$("#subirFoto").change(function(){

	imagen = this.files[0];

	//Validar tamaño de la imagen

	imagenSize = imagen.size;

	if(Number(imagenSize) > 2000000){

		$("#arrastreImagenDirectorio").before('<div class="alert alert-warning alerta text-center">El archivo excede el peso permitido, 200kb</div>')

	}

	else{

		$(".alerta").remove();

	}

	// Validar tipo de la imagen

	imagenType = imagen.type;

	if(imagenType == "image/jpeg" || imagenType == "image/png"){

		$(".alerta").remove();
	}

	else{

		$("#arrastreImagenDirectorio").before('<div class="alert alert-warning alerta text-center">El archivo debe ser formato JPG o PNG</div>')

	}

	/*=============================================
	Mostrar imagen con AJAX       
	=============================================*/
	if(Number(imagenSize) < 2000000 && imagenType == "image/jpeg" || imagenType == "image/png"){

	
	

	}

})

/*=============================================
Editar Artículo        
=============================================*/

$(".editarDirectorio").click(function(){

	id = $(this).parent().parent().attr("id");
	imagen = $("#"+id).children("img").attr("src");
	nombre = $("#"+id).children("h1").html();
	descripcion = $("#"+id).children("p").html();
	;

	$("#"+id).html('<span><input  id="cerrar" type="submit" class="btn btn-danger pull-right" value="Cerrar"></span> <form method="post" id="directorioEdit" enctype="multipart/form-data"><span><input  type="submit" class="btn btn-primary pull-right" value="Guardar"></span><div id="editarImagen"><input  type="file" id="subirNuevaFoto" class="btn btn-default"><div id="nuevaFoto"><span class="fa fa-times cambiarImagen"></span><img src="'+imagen+'" class="img-thumbnail" ></div></div><input type="text" value="'+nombre+'" name="editarNombre"  class="form-control"><textarea cols="30" rows="5" name="editarDescripcion" class="form-control">'+descripcion+'</textarea><input type="hidden" value="'+id+'" name="id"><input type="hidden" value="'+imagen+'" name="fotoAntigua"><hr></form>');

	$("#cerrar").click(function(){
		$("#directorioEdit").hide(); 
		$("#cerrar").hide(); 
		$("#bloqueDirectorio-"+id).show();
console.log("a");

	});
	

	$(".cambiarImagen").click(function(){
	
		$(this).hide();

		$("#subirNuevaFoto").show();
		$("#subirNuevaFoto").css({"width":"90%"});
		$("#nuevaFoto").html("");
		$("#subirNuevaFoto").attr("name","editarImagen");
		$("#subirNuevaFoto").attr("required",true);

		$("#subirNuevaFoto").change(function(){

			imagen = this.files[0];
			imagenSize = imagen.size;

			if(Number(imagenSize) > 2000000){

				$("#editarImagen").before('<div class="alert alert-warning alerta text-center">El archivo excede el peso permitido, 200kb</div>')

			}

			else{

				$(".alerta").remove();

			}

			imagenType = imagen.type;
			
			if(imagenType == "image/jpeg" || imagenType == "image/png"){

				$(".alerta").remove();

			}

			else{

				$("#editarImagen").before('<div class="alert alert-warning alerta text-center">El archivo debe ser formato JPG o PNG</div>')

			}

			if(Number(imagenSize) < 2000000 && imagenType == "image/jpeg" || imagenType == "image/png"){

				var datos = new FormData();

				datos.append("imagen", imagen);	

				$.ajax({
						url:"actualizarDirectorio",
						method: "POST",
						data: datos,
						cache: false,
						contentType: false,
						processData: false,
						beforeSend: function(){

							$("#nuevaFoto").html('<img src="views/images/status.gif" style="width:15%" id="status2">');

						},
						success: function(respuesta){
				
							$("#status2").remove();

							if(respuesta == 0){

								$("#editarImagen").before('<div class="alert alert-warning alerta text-center">La imagen es inferior a 800px * 400px</div>')
							
							}

							else{

								$("#nuevaFoto").html('<img src="'+respuesta.slice(6)+'" class="img-thumbnail">');

							}
							
						}

				})	

			}

		})

	})

})

/*=============================================
Ordenar Item Artículos
=============================================*/

var almacenarOrdenId = new Array();
var ordenItem = new Array();

$("#ordenarDirectorios").click(function(){

	$("#ordenarDirectorios").hide();
	$("#guardarOrdenDirectorios").show();

	$("#editarDirectorio").css({"cursor":"move"})
	$("#editarDirectorio span i").hide()
	$("#editarDirectorio button").hide()
	$("#editarDirectorio img").hide()
	$("#editarDirectorio p").hide()
	$("#editarDirectorio hr").hide()
	$("#editarDirectorio div").remove()
	$(".bloqueDirectorio h1").css({"font-size":"14px","position":"absolute","padding":"10px", "top":"-15px"})
	$(".bloqueDirectorio").css({"padding":"2px"})
	$("#editarDirectorio span").html('<i class="glyphicon glyphicon-move" style="padding:8px"></i>')

	$("body, html").animate({

		scrollTop:$("body").offset().top

	}, 500)

	$("#editarDirectorio").sortable({
		revert: true,
		connectWith: ".bloqueDirectorio",
		handle: ".handleArticle",
		stop: function(event){

			for(var i= 0; i < $("#editarDirectorio li").length; i++){

				almacenarOrdenId[i] = event.target.children[i].id;
				ordenItem[i]  =  i+1;

			}	
		}
	})

	$("#guardarOrdenDirectorios").click(function(){

		$("#ordenarDirectorios").show();
		$("#guardarOrdenDirectorios").hide();

		for(var i= 0; i < $("#editarDirectorio li").length; i++){

			var actualizarOrden = new FormData();
			actualizarOrden.append("actualizarOrdenDirectorios", almacenarOrdenId[i]);
			actualizarOrden.append("actualizarOrdenItem", ordenItem[i]);

			$.ajax({

				url:"views/ajax/gestorDirectorios.php",
				method: "POST",
				data: actualizarOrden,
				cache: false,
				contentType: false,
				processData: false,
				success: function(respuesta){

					$("#editarDirectorio").html(respuesta);

					swal({
						title: "¡OK!",
						text: "¡El orden se ha actualizado correctamente!",
						type: "success",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},
						function(isConfirm){
							if (isConfirm){
								window.location = "Directorios";
							}
						});


				}

			})


			
		}
	
	})

})