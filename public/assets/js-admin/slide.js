
if($("#columnasSlide").html() == 0){

	$("#columnasSlide").css({"height":"100px"});

}

else{

	$("#columnasSlide").css({"height":"auto"});

}

/*=====  Área de arrastre de imágenes  ======*/

/*=============================================
Subir Imagen
=============================================*/

/*=====  Subir Imagen  ======*/
/*=============================================
Soltar Imagen
=============================================*/

$('#slidefoto').on('blur', function (e) {
  
	

	$("#columnasSlide").css({"background":"white"})

	//var archivo = e.originalEvent.dataTransfer.files;
	var imagen = e.target.files[0];

	// Validar tamaño de la imagen
	var imagenSize = e.target.files[0].size;
	console.log(imagen.name);
	if(Number(imagenSize) > 2000000){
		
		$("#botonSlide").prop('disabled', true);
		$("#columnasSlide").before('<div class="alert alert-warning alerta text-center">El archivo excede el peso permitido, 200kb</div>')

	}else{
		$(".alerta").remove();
		$("#botonSlide").prop('disabled', false);
	}

	

	// Validar tipo de la imagen
	var imagenType = imagen.type;
	
	if(imagenType == "image/jpeg" || imagenType == "image/png"){

		$(".alerta").remove();
		$("#botonSlide").prop('disabled', false);
	}

	else{
		$("#botonSlide").prop('disabled', true);
		$("#columnasSlide").before('<div class="alert alert-warning alerta text-center">El archivo debe ser formato JPG o PNG</div>')

	}
	var uploadFile = e.target.files[0];
	
	var img = new Image();
	img.src = URL.createObjectURL(uploadFile);
	img.onload = function () {
		if (this.width.toFixed(0) != 1600 && this.height.toFixed(0) != 600) {
			$("#botonSlide").prop('disabled', true);
		$("#columnasSlide").before('<div class="alert alert-warning alerta text-center">Las medidas deben ser de 1600x600</div>')

		}else{
			$(".alerta").remove();
			$("#botonSlide").prop('disabled', false);
		}
	   
		
	};
	


	//Subir imagen al servidor
	if(Number(imagenSize) < 2000000 && imagenType == "image/jpeg" || imagenType == "image/png"){

		$("#botonSlide").prop('disabled', false);
		
		$(".alerta").remove();


	}




});
function validarImagen(obj){
    var uploadFile = obj.files[0];
    
        var img = new Image();
        img.onload = function () {
            if (this.width.toFixed(0) != 200 && this.height.toFixed(0) != 200) {
                alert('Las medidas deben ser: 200 * 200');
            }
           
            
        };
        img.src = URL.createObjectURL(uploadFile);
                   
}


/*=====  Soltar Imagen  ======*/

/*=============================================
Eliminar Item Slide
=============================================*/



$(".eliminarSlide").click(function(){

	if($(".eliminarSlide").length == 1){

		$("#columnasSlide").css({"height":"100px"});

	}

	idSlide = $(this).parent().attr("id");



	Swal.fire({
	title: 'Estas seguro?',
	text: "Quieres eliminar la imagen!",
	icon: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'si'
  }).then((result) => {
	if (result.value) {
	 
	  $.ajax({

		url: "/deleteSlide/"+idSlide,
		method: "get",
		cache: false,
		contentType: false,
		processData: false,
		success: function(respuesta){
		if(respuesta==1){

	$(this).parent().remove();
	$("#item"+idSlide).remove();

			Swal.fire(
				'Eliminado!',
				'Tu imagen ha sido eliminada con exito.',
				'success'
			  )
	    location.reload();
		}else if(respuesta==2){
			Swal.fire(
				'Error!',
				'Ha ocurrido un error.',
				'error'
			  )
		}else{
			Swal.fire(
				'Error!',
				'Ha ocurrido un error al eliminar la imagen.',
				'error'
			  )
		}
		
		
		}

	})
	}
  })



})


/*=====  Eliminar Item Slide  ======*/

/*=============================================
Editar Item Slide
=============================================*/

$(".editarSlide").click(function(){

	idSlide = $(this).parent().attr("id");
	rutaImagen = $(this).parent().children("img").attr("src");
	rutaTitulo = $(this).parent().children("h1").html();
	rutaDescripcion = $(this).parent().children("p").html();
	
	$(this).parent().html('<img src="'+rutaImagen+'" class="img-thumbnail"><input type="text" class="form-control" id="enviarTitulo" placeholder="Título" value="'+rutaTitulo+'"><textarea row="5" id="enviarDescripcion" class="form-control" placeholder="Descripción">'+rutaDescripcion+'</textarea><button class="btn btn-info pull-right" id="guardar'+idSlide+'" style="margin:10px">Guardar</button>');

	$("#guardar"+idSlide).click(function(){

		enviarId = idSlide.slice(4);

		enviarTitulo = $("#enviarTitulo").val();
		enviarDescripcion = $("#enviarDescripcion").val();

		var actualizarSlide = new FormData();

		actualizarSlide.append("enviarId",enviarId);
		actualizarSlide.append("enviarTitulo",enviarTitulo);
		actualizarSlide.append("enviarDescripcion",enviarDescripcion);

		$.ajax({
			url:"views/ajax/gestorSlide.php",
			method: "POST",
			data: actualizarSlide,
			cache: false,
			contentType: false,
			processData: false,
			dataType:"json",
			success: function(respuesta){
				
				$("#guardar"+idSlide).parent().html('<span class="fa fa-pencil editarSlide" style="background:blue"></span><img src="'+rutaImagen+'" style="float:left; margin-bottom:10px" width="80%"><h1>'+respuesta["titulo"]+'</h1><p>'+respuesta["descripcion"]+'</p>');

				swal({
						title: "¡OK!",
						text: "¡Se han guardado los cambios correctamente!",
						type: "success",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},
						function(isConfirm){
							if (isConfirm){
								window.location = "slide";
							}
						});

			}

		});



	})

})




