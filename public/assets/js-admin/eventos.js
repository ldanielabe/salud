/*=============================================
Agregar artículo          
=============================================*/
$("#btnAgregarArticulo").click(function(){

	$("#agregarArtículo").toggle(400);
	$("#evento").toggle(400);
})

/*=============================================
Subir Imagen a través del Input         
=============================================*/
$("#subirFoto").change(function(){

	imagen = this.files[0];

	//Validar tamaño de la imagen

	imagenSize = imagen.size;

	if(Number(imagenSize) > 2000000){

	
		md.showNotification('top','right','El archivo excede el peso permitido, 200kb');
		$("#botonSlide").prop('disabled', true);
	}

	else{

		$(".alerta").remove();
		$("#botonSlide").prop('disabled', false);
	}

	// Validar tipo de la imagen

	imagenType = imagen.type;

	if(imagenType == "image/jpeg" || imagenType == "image/png"){

		$(".alerta").remove();
		$("#botonSlide").prop('disabled', false);
	}

	else{

		md.showNotification('top','right','El archivo debe ser formato JPG o PNG');
		$("#botonSlide").prop('disabled', true);
	}

	/*=============================================
	Mostrar imagen con AJAX       
	=============================================*/
	if(Number(imagenSize) < 2000000 && imagenType == "image/jpeg" || imagenType == "image/png"){

		var datos = new FormData();

		datos.append("imagen", imagen);
		$("#botonSlide").prop('disabled', false);
	

	}

})

/*=============================================
Editar Artículo        
=============================================*/

$(".editarArticulo").click(function(){

	idArticulo = $(this).parent().parent().attr("id");
	rutaImagen = $("#"+idArticulo).children("img").attr("src");
	titulo = $("#"+idArticulo).children("h1").html();
	introduccion = $("#"+idArticulo).children("p").html();
	contenido = $("#"+idArticulo).children("input").val();

	$("#"+idArticulo).html('<span><input  id="cerrar" type="submit" class="btn btn-danger pull-right" value="Cerrar"></span> <form method="post" id="evento" enctype="multipart/form-data"><span><input  type="submit" class="btn btn-primary pull-right" value="Guardar"></span><div id="editarImagen"><input  type="file" id="subirNuevaFoto" class="btn btn-default"><div id="nuevaFoto"><span class="fa fa-times cambiarImagen"></span><img src="'+rutaImagen+'" class="img-thumbnail" ></div></div><input type="text" value="'+titulo+'" name="editarTitulo"  class="form-control"><textarea cols="30" rows="5" name="editarIntroduccion" class="form-control">'+introduccion+'</textarea><textarea name="editarContenido" id="editarContenido" cols="30" rows="10" class="form-control">'+contenido+'</textarea><input type="hidden" value="'+idArticulo+'" name="id"><input type="hidden" value="'+rutaImagen+'" name="fotoAntigua"><hr></form>');

	$("#cerrar").click(function(){
		$("#evento").hide(); 
console.log("a");

	});
	

	$(".cambiarImagen").click(function(){
	
		$(this).hide();

		$("#subirNuevaFoto").show();
		$("#subirNuevaFoto").css({"width":"90%"});
		$("#nuevaFoto").html("");
		$("#subirNuevaFoto").attr("name","editarImagen");
		$("#subirNuevaFoto").attr("required",true);

		$("#subirNuevaFoto").change(function(){

			imagen = this.files[0];
			imagenSize = imagen.size;

			if(Number(imagenSize) > 2000000){

				$("#editarImagen").before('<div class="alert alert-warning alerta text-center">El archivo excede el peso permitido, 200kb</div>')

			}

			else{

				$(".alerta").remove();

			}

			imagenType = imagen.type;
			
			if(imagenType == "image/jpeg" || imagenType == "image/png"){

				$(".alerta").remove();

			}

			else{

				$("#editarImagen").before('<div class="alert alert-warning alerta text-center">El archivo debe ser formato JPG o PNG</div>')

			}

			if(Number(imagenSize) < 2000000 && imagenType == "image/jpeg" || imagenType == "image/png"){

				var datos = new FormData();

				datos.append("imagen", imagen);	

				$.ajax({
						url:"updateEventos",
						method: "POST",
						data: datos,
						cache: false,
						contentType: false,
						processData: false,
						beforeSend: function(){

							$("#nuevaFoto").html('<img src="views/images/status.gif" style="width:15%" id="status2">');

						},
						success: function(respuesta){
				
							$("#status2").remove();

							if(respuesta == 0){

								$("#editarImagen").before('<div class="alert alert-warning alerta text-center">La imagen es inferior a 800px * 400px</div>')
							
							}

							else{

								$("#nuevaFoto").html('<img src="'+respuesta.slice(6)+'" class="img-thumbnail">');

							}
							
						}

				})	

			}

		})

	})

})

/*=============================================
Ordenar Item Artículos
=============================================*/

var almacenarOrdenId = new Array();
var ordenItem = new Array();

$("#ordenarArticulos").click(function(){

	$("#ordenarArticulos").hide();
	$("#guardarOrdenArticulos").show();

	$("#editarArticulo").css({"cursor":"move"})
	$("#editarArticulo span i").hide()
	$("#editarArticulo button").hide()
	$("#editarArticulo img").hide()
	$("#editarArticulo p").hide()
	$("#editarArticulo hr").hide()
	$("#editarArticulo div").remove()
	$(".bloqueArticulo h1").css({"font-size":"14px","position":"absolute","padding":"10px", "top":"-15px"})
	$(".bloqueArticulo").css({"padding":"2px"})
	$("#editarArticulo span").html('<i class="glyphicon glyphicon-move" style="padding:8px"></i>')

	$("body, html").animate({

		scrollTop:$("body").offset().top

	}, 500)

	$("#editarArticulo").sortable({
		revert: true,
		connectWith: ".bloqueArticulo",
		handle: ".handleArticle",
		stop: function(event){

			for(var i= 0; i < $("#editarArticulo li").length; i++){

				almacenarOrdenId[i] = event.target.children[i].id;
				ordenItem[i]  =  i+1;

			}	
		}
	})

	$("#guardarOrdenArticulos").click(function(){

		$("#ordenarArticulos").show();
		$("#guardarOrdenArticulos").hide();

		for(var i= 0; i < $("#editarArticulo li").length; i++){

			var actualizarOrden = new FormData();
			actualizarOrden.append("actualizarOrdenArticulos", almacenarOrdenId[i]);
			actualizarOrden.append("actualizarOrdenItem", ordenItem[i]);

		

			
		}
	
	})

})