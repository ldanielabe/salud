var label_practicas=['Medico','Rezandero'];
var datos_practicas=[0,0];
for (let index = 0; index < practicas_cuidado.length; index++) {
  if(index==0){
    label_practicas = [practicas_cuidado[index].practicas_cuidados];
    datos_practicas =[practicas_cuidado[index].cantidad];
  }else{
    let label2 = [practicas_cuidado[index].practicas_cuidados];
    let datos2 =[practicas_cuidado[index].cantidad];
    datos_practicas=datos_practicas.concat(datos2);
    label_practicas=label_practicas.concat(label2);
  }
  
}
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_practicas
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_practicas,
  position: 'top',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: ' practicas de cuidado '+practicas_colegio,
  floating: true,
  offsetY: 330,
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart2 = new ApexCharts(document.querySelector("#bar-chart2"), options);
mychart2.render();   
  
//---------------------------------------------

var label_cuidadores=['Madre','Padre'];
var datos_cuidadores=[0,0];
for (let index = 0; index < cuidadores.length; index++) {
  if(index==0){
    label_cuidadores = [cuidadores[index].cuidador];
    datos_cuidadores =[cuidadores[index].cantidad];
  }else{
    let label2 = [cuidadores[index].cuidador];
    let datos2 =[cuidadores[index].cantidad];
    datos_cuidadores=datos_cuidadores.concat(datos2);
    label_cuidadores=label_cuidadores.concat(label2);
  }
  
}

var options = {
  series: [{
    name: 'Cantidad',
  data: datos_cuidadores
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_cuidadores,
  position: 'top',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'F cuidadores del menor cuando decae la salud '+cuidadores_colegio,
  floating: true,
  offsetY: 330,
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
mychart3.render();   


//grafica fumadores-------------------------------------------------------------------



var label_fumadores=['si','no'];
var datos_fumadores=[0,0];

for (let index = 0; index < presencia_fumadores.length; index++) {
  if(index==0){
    label_fumadores = [presencia_fumadores[index].presencia_fumadores];
    datos_fumadores =[(presencia_fumadores[index].cantidad)];
  }else{
    let label2 = [presencia_fumadores[index].presencia_fumadores];
    let datos2 =[presencia_fumadores[index].cantidad];
    datos_fumadores=datos_fumadores.concat(datos2);
    label_fumadores=label_fumadores.concat(label2);
  }
  
}
var options = {
  series: datos_fumadores,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Fumadores hogar '+presencia_fumadores_colegio
},
labels: label_fumadores,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
piechart.render();

 
    
  //Grafica consumidores alcohol-----------------------------
 
  var label_alcoholicos=['si','no'];
  var datos_alcoholicos=[0,0];
  var cantidad_alcoholicos=0;
  for (let index = 0; index < presencia_alcoholicos.length; index++) {
    cantidad_alcoholicos+=presencia_alcoholicos[index].cantidad;
   
    
  }
  
  for (let index = 0; index < presencia_alcoholicos.length; index++) {
    if(index==0){
      label_alcoholicos = [presencia_alcoholicos[index].presencia_alcoholicos];
      datos_alcoholicos =[Math.round(((presencia_alcoholicos[index].cantidad)/cantidad_alcoholicos)*100)];
    }else{
      let label2 = [presencia_alcoholicos[index].presencia_alcoholicos];
      let datos2 =[Math.round(((presencia_alcoholicos[index].cantidad)/cantidad_alcoholicos)*100)];
      datos_alcoholicos=datos_alcoholicos.concat(datos2);
      label_alcoholicos=label_alcoholicos.concat(label2);
    }
    
  }
 
  
  var options = {
    series: datos_alcoholicos,
    chart: {
    width: '100%',
    type: 'pie',
  },
  plotOptions: {
    pie: {
      dataLabels: {
        offset: -5
      }
    }
  },
  title: {
    text: 'Consumidores alcohol entorno familiar '+presencia_alcoholicos_colegio
  },
  labels: label_alcoholicos,
  responsive: [{
    breakpoint: 280,
    options: {
      chart: {
        width: 100
      },
      legend: {
        show: false
      }
    }
  }]
  };
  var piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
  piechart2.render();

//-----------------------------------------------------

  function dominioUnoAlcoholicos(){
    var label_alcoholicos=['si','no'];
    var datos_alcoholicos=[0,0];
    var cantidad_alcoholicos=0;
    for (let index = 0; index < presencia_alcoholicos.length; index++) {
      cantidad_alcoholicos+=presencia_alcoholicos[index].cantidad;
     
      
    }
    
    for (let index = 0; index < presencia_alcoholicos.length; index++) {
      if(index==0){
        label_alcoholicos = [presencia_alcoholicos[index].presencia_alcoholicos];
        datos_alcoholicos =[Math.round(((presencia_alcoholicos[index].cantidad)/cantidad_alcoholicos)*100)];
      }else{
        let label2 = [presencia_alcoholicos[index].presencia_alcoholicos];
        let datos2 =[Math.round(((presencia_alcoholicos[index].cantidad)/cantidad_alcoholicos)*100)];
        datos_alcoholicos=datos_alcoholicos.concat(datos2);
        label_alcoholicos=label_alcoholicos.concat(label2);
      }
      
    }
    
   piechart2.destroy();
   var options = {
    series: datos_alcoholicos,
    chart: {
    width: '100%',
    type: 'pie',
  },
  plotOptions: {
    pie: {
      dataLabels: {
        offset: -5
      }
    }
  },
  title: {
    text: 'Consumidores alcohol entorno familiar '+presencia_alcoholicos_colegio
  },
  labels: label_alcoholicos,
  responsive: [{
    breakpoint: 280,
    options: {
      chart: {
        width: 100
      },
      legend: {
        show: false
      }
    }
  }]
  };
 piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
  piechart2.render();

  }

  function dominioUnoFumadores(){

    var label_fumadores=['si','no'];
    var datos_fumadores=[0,0];
    var cantidad_fumadores=0;
    for (let index = 0; index < presencia_fumadores.length; index++) {
      cantidad_fumadores+=presencia_fumadores[index].cantidad;
     
      
    }
    
    for (let index = 0; index < presencia_fumadores.length; index++) {
      if(index==0){
        label_fumadores = [presencia_fumadores[index].presencia_fumadores];
        datos_fumadores =[Math.round(((presencia_fumadores[index].cantidad)/cantidad_fumadores)*100)];
      }else{
        let label2 = [presencia_fumadores[index].presencia_fumadores];
        let datos2 =[Math.round(((presencia_fumadores[index].cantidad)/cantidad_fumadores)*100)];
        datos_fumadores=datos_fumadores.concat(datos2);
        label_fumadores=label_fumadores.concat(label2);
      }
      
    }
    piechart.destroy();
    var options = {
      series: datos_fumadores,
      chart: {
      width: '100%',
      type: 'pie',
    },
    plotOptions: {
      pie: {
        dataLabels: {
          offset: -5
        }
      }
    },
    title: {
      text: 'Fumadores en el hogar '+presencia_fumadores_colegio
    },
    labels: label_fumadores,
    responsive: [{
      breakpoint: 280,
      options: {
        chart: {
          width: 100
        },
        legend: {
          show: false
        }
      }
    }]
    };
   piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
    piechart.render();
  
  }

  //Grafica cuidadores
function dominioUnoCuidadores(){
  
  var label_cuidadores=['Madre','Padre'];
  var datos_cuidadores=[0,0];
  for (let index = 0; index < cuidadores.length; index++) {
    if(index==0){
      label_cuidadores = [cuidadores[index].cuidador];
      datos_cuidadores =[cuidadores[index].cantidad];
    }else{
      let label2 = [cuidadores[index].cuidador];
      let datos2 =[cuidadores[index].cantidad];
      datos_cuidadores=datos_cuidadores.concat(datos2);
      label_cuidadores=label_cuidadores.concat(label2);
    }
    
  }
  mychart3.destroy();
  var options = {
    series: [{
      name: 'Cantidad',
    data: datos_cuidadores
  }],
    chart: {
    height: 350,
    type: 'bar',
    events: {
      click: function(chart, w, e) {
        // console.log(chart, w, e)
      }
    }
  },
  colors: colors,
  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true
    }
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return val ;
    },
  },
  legend: {
    show: false
  },
  xaxis: {
    categories: label_cuidadores,
    position: 'top',
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      fill: {
        type: 'gradient',
        gradient: {
          colorFrom: '#D8E3F0',
          colorTo: '#BED1E6',
          stops: [0, 100],
          opacityFrom: 0.4,
          opacityTo: 0.5,
        }
      }
    },
    tooltip: {
      enabled: true,
    }
  },
  
  title: {
    text: 'Frecuencia cuidadores del menor cuando decae la salud '+cuidadores_colegio,
    floating: true,
    offsetY: 330,
    align: 'center',
    style: {
      color: '#444'
    }
  }
  };
  
  
 mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
  mychart3.render();   
  }
  function dominioUno(datos){
    label_practicas=['Medico','Rezandero'];
    datos_practicas=[0,0];
    for (let index = 0; index < practicas_cuidado.length; index++) {
      if(index==0){
        label_practicas = [practicas_cuidado[index].practicas_cuidados];
        datos_practicas =[practicas_cuidado[index].cantidad];
      }else{
        let label2 = [practicas_cuidado[index].practicas_cuidados];
        let datos2 =[practicas_cuidado[index].cantidad];
        datos_practicas=datos_practicas.concat(datos2);
        label_practicas=label_practicas.concat(label2);
      }
      
    }       
           mychart2.destroy();
           var options = {
            series: [{
              name: 'Cantidad',
            data: datos_practicas
          }],
            chart: {
            height: 350,
            type: 'bar',
            events: {
              click: function(chart, w, e) {
                // console.log(chart, w, e)
              }
            }
          },
          colors: colors,
          plotOptions: {
            bar: {
              columnWidth: '45%',
              distributed: true
            }
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
              return val ;
            },
          },
          legend: {
            show: false
          },
          xaxis: {
            categories: label_practicas,
            position: 'top',
            axisBorder: {
              show: false
            },
            axisTicks: {
              show: false
            },
            crosshairs: {
              fill: {
                type: 'gradient',
                gradient: {
                  colorFrom: '#D8E3F0',
                  colorTo: '#BED1E6',
                  stops: [0, 100],
                  opacityFrom: 0.4,
                  opacityTo: 0.5,
                }
              }
            },
            tooltip: {
              enabled: true,
            }
          },
          
          title: {
            text: ' practicas de cuidado '+practicas_colegio,
            floating: true,
            offsetY: 330,
            align: 'center',
            style: {
              color: '#444'
            }
          }
          };
          
          
          mychart2 = new ApexCharts(document.querySelector("#bar-chart2"), options);
          mychart2.render();  
       
          }
  
  