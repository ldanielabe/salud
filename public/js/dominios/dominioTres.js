
var label_alt_gastrointestinales=["vomito: "];
var datos_alt_gastrointestinales=[0];
for (let index = 0; index < alt_gastrointestinales.vomito.length; index++) {
  if(index==0){
    label_alt_gastrointestinales = ["vomito: "+alt_gastrointestinales.vomito[index].vomito];
    datos_alt_gastrointestinales =[alt_gastrointestinales.vomito[index].cantidad];
  }else{
    let label2 = ["vomito: "+alt_gastrointestinales.vomito[index].vomito];
    let datos2 =[alt_gastrointestinales.vomito[index].cantidad];
    datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
    label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
  }
  
}
for (let index = 0; index < alt_gastrointestinales.diarrea.length; index++) {
  
      let label2 = ["diarrea: "+alt_gastrointestinales.diarrea[index].diarrea];
      let datos2 =[alt_gastrointestinales.diarrea[index].cantidad];
      datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
      label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
    
    
  }
  for (let index = 0; index < alt_gastrointestinales.enuresis.length; index++) {
  
    let label2 = ["enuresis: "+alt_gastrointestinales.enuresis[index].enuresis];
    let datos2 =[alt_gastrointestinales.enuresis[index].cantidad];
    datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
    label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
  
  
}
for (let index = 0; index < alt_gastrointestinales.estrenimiento.length; index++) {
  
    let label2 = ["estrenimiento: "+alt_gastrointestinales.estrenimiento[index].estrenimiento];
    let datos2 =[alt_gastrointestinales.estrenimiento[index].cantidad];
    datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
    label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
  
  
}
for (let index = 0; index < alt_gastrointestinales.hematuria.length; index++) {
  
    let label2 = ["hematuria: "+alt_gastrointestinales.hematuria[index].hematuria];
    let datos2 =[alt_gastrointestinales.hematuria[index].cantidad];
    datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
    label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
  
  
}
for (let index = 0; index < alt_gastrointestinales.poliuria.length; index++) {
  
    let label2 = ["poliuria: "+alt_gastrointestinales.poliuria[index].poliuria];
    let datos2 =[alt_gastrointestinales.poliuria[index].cantidad];
    datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
    label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
  
  
}
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_alt_gastrointestinales
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_alt_gastrointestinales,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Frencuencia de alteraciones gastrointestinales y urinarias en los escolares  '+ alt_gastrointestinales.colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart9 = new ApexCharts(document.querySelector("#bar-chart9"), options);
mychart9.render();  



function dominioTresAlt(){

  
     label_alt_gastrointestinales=["vomito: "];
     datos_alt_gastrointestinales=[0];
    for (let index = 0; index < alt_gastrointestinales.vomito.length; index++) {
      if(index==0){
        label_alt_gastrointestinales = ["vomito: "+alt_gastrointestinales.vomito[index].vomito];
        datos_alt_gastrointestinales =[alt_gastrointestinales.vomito[index].cantidad];
      }else{
        let label2 = ["vomito: "+alt_gastrointestinales.vomito[index].vomito];
        let datos2 =[alt_gastrointestinales.vomito[index].cantidad];
        datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
        label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
      }
      
    }
    for (let index = 0; index < alt_gastrointestinales.diarrea.length; index++) {
      
          let label2 = ["diarrea: "+alt_gastrointestinales.diarrea[index].diarrea];
          let datos2 =[alt_gastrointestinales.diarrea[index].cantidad];
          datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
          label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
        
        
      }
      for (let index = 0; index < alt_gastrointestinales.enuresis.length; index++) {
      
        let label2 = ["enuresis: "+alt_gastrointestinales.enuresis[index].enuresis];
        let datos2 =[alt_gastrointestinales.enuresis[index].cantidad];
        datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
        label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
      
      
    }
    for (let index = 0; index < alt_gastrointestinales.estrenimiento.length; index++) {
      
        let label2 = ["estrenimiento: "+alt_gastrointestinales.estrenimiento[index].estrenimiento];
        let datos2 =[alt_gastrointestinales.estrenimiento[index].cantidad];
        datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
        label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
      
      
    }
    for (let index = 0; index < alt_gastrointestinales.hematuria.length; index++) {
      
        let label2 = ["hematuria: "+alt_gastrointestinales.hematuria[index].hematuria];
        let datos2 =[alt_gastrointestinales.hematuria[index].cantidad];
        datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
        label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
      
      
    }
    for (let index = 0; index < alt_gastrointestinales.poliuria.length; index++) {
      
        let label2 = ["poliuria: "+alt_gastrointestinales.poliuria[index].poliuria];
        let datos2 =[alt_gastrointestinales.poliuria[index].cantidad];
        datos_alt_gastrointestinales=datos_alt_gastrointestinales.concat(datos2);
        label_alt_gastrointestinales=label_alt_gastrointestinales.concat(label2);
      
      
    }
    mychart9.destroy();
    var options = {
      series: [{
        name: 'Cantidad',
      data: datos_alt_gastrointestinales
    }],
      chart: {
      height: 350,
      type: 'bar',
      events: {
        click: function(chart, w, e) {
          // console.log(chart, w, e)
        }
      }
    },
    colors: colors,
    plotOptions: {
      bar: {
        columnWidth: '45%',
        distributed: true
      }
    },
    dataLabels: {
      enabled: true,
      formatter: function (val) {
        return val ;
      },
    },
    legend: {
      show: false
    },
    xaxis: {
      categories: label_alt_gastrointestinales,
      position: 'bottom',
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      },
      crosshairs: {
        fill: {
          type: 'gradient',
          gradient: {
            colorFrom: '#D8E3F0',
            colorTo: '#BED1E6',
            stops: [0, 100],
            opacityFrom: 0.4,
            opacityTo: 0.5,
          }
        }
      },
      tooltip: {
        enabled: true,
      }
    },
    
    title: {
      text: 'Frencuencia de alteraciones gastrointestinales y urinarias en los escolares  '+ alt_gastrointestinales.colegio,
      floating: true,
     
      align: 'center',
      style: {
        color: '#444'
      }
    }
    };
    
    
   mychart9 = new ApexCharts(document.querySelector("#bar-chart9"), options);
    mychart9.render();  

//----------------------------------------------------------

var label_p=['si','no'];
var datos_p=[0,0];
var cantidad_c=0;
for (let index = 0; index < alt_gastrointestinales.practicas.length; index++) {
    cantidad_c+= alt_gastrointestinales.practicas[index].cantidad;
 
  
}


for (let index = 0; index < alt_gastrointestinales.practicas.length; index++) {
    if(index==0){
        if(alt_gastrointestinales.practicas[index].practicas_cuidado_escolar==1){
          label_p= ["Si"];
        }else{
          label_p= ["No"];
        }
     
        datos_p =[Math.round(((alt_gastrointestinales.practicas[index].cantidad)/cantidad_c)*100)];
    }else{
      if(alt_gastrointestinales.practicas[index].practicas_cuidado_escolar==1){
          label2= ["Si"];
        }else{
          label2= ["No"];
        }
      let datos2 =[Math.round(((alt_gastrointestinales.practicas[index].cantidad)/cantidad_c)*100)];
      datos_p=datos_p.concat(datos2);
      label_p=label_p.concat(label2);
    }
    
  }
  piechart3.destroy();
  var options = {
    series: datos_p,
    chart: {
    width: '100%',
    type: 'pie',
  },
  plotOptions: {
    pie: {
      dataLabels: {
        offset: -5
      }
    }
  },
  title: {
    text: ' Practicas de cuidado en el escolar, Total:'+cantidad_c
  },
  labels: label_p,
  responsive: [{
    breakpoint: 280,
    options: {
      chart: {
        width: 100
      },
      legend: {
        show: false
      }
    }
  }]
  };
   piechart3 = new ApexCharts(document.querySelector("#pie-chart3"), options);
  piechart3.render();


}