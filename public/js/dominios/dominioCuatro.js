
var label_horas=[""];
var datos_horas=[0];
for (let index = 0; index < horas.horas.length; index++) {
  if(index==0){
    label_horas = ["Horas: "+horas.horas[index].horas_sueno];
    datos_horas =[horas.horas[index].cantidad];
  }else{
    let label2 = ["Horas: "+horas.horas[index].horas_sueno];
    let datos2 =[horas.horas[index].cantidad];
    datos_horas=datos_horas.concat(datos2);
    label_horas=label_horas.concat(label2);
  }
  
}


var options = {
  series: [{
  name: 'cantidad',
  data: datos_horas

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_horas, 
  title: {
    text: 'Frecuencia de estudiantes según N° de horas que duerme al dia  '+ horas.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

var mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
mychart1.render();
//-------------------------------------------------------------------------------


var label_actividades=[""];
var datos_actividades=[0];
for (let index = 0; index < actividades.actividades.length; index++) {
  if(index==0){
    label_actividades = [actividades.actividades[index].actividades_recreativas];
    datos_actividades =[actividades.actividades[index].cantidad];
  }else{
    let label2 = [actividades.actividades[index].actividades_recreativas];
    let datos2 =[actividades.actividades[index].cantidad];
    datos_actividades=datos_actividades.concat(datos2);
    label_actividades=label_actividades.concat(label2);
  }
  
}

var options = {
  series: [{
  name: 'cantidad',
  data: datos_actividades

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_actividades, 
  title: {
    text: 'Frecuencia de actividades recreativas  '+ actividades.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

var mychart2 = new ApexCharts(document.querySelector("#bar-chart2"), options);
mychart2.render();


//-------------------------------------------------------------------------------

var label_alteraciones_columna=['si','no'];
var datos_alteraciones_columna=[0];
var cantidad_alteraciones_columna=0;
for (let index = 0; index < alteraciones_columna.alteraciones_columna.length; index++) {
  cantidad_alteraciones_columna+= alteraciones_columna.alteraciones_columna[index].cantidad;
 
  
}

for (let index = 0; index < alteraciones_columna.alteraciones_columna.length; index++) {
  if(index==0){
    label_alteraciones_columna = [alteraciones_columna.alteraciones_columna[index].alteraciones_columna];
    datos_alteraciones_columna =[Math.round(((alteraciones_columna.alteraciones_columna[index].cantidad)/cantidad_alteraciones_columna)*100)];
  }else{
    let label2 = [ alteraciones_columna.alteraciones_columna[index].alteraciones_columna];
    let datos2 =[Math.round((( alteraciones_columna.alteraciones_columna[index].cantidad)/cantidad_alteraciones_columna)*100)];
    datos_alteraciones_columna=datos_alteraciones_columna.concat(datos2);
    label_alteraciones_columna=label_alteraciones_columna.concat(label2);
  }
  
}


var options = {
  series: datos_alteraciones_columna,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alt en columna, '+alteraciones_columna.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_alteraciones_columna,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
piechart.render();

//-------------------------------------------------------------------------------

var label_redes_sociales=['si','no'];
var datos_redes_sociales=[0,0];
var cantidad_redes_sociales=0;
for (let index = 0; index < redes_sociales.redes_sociales.length; index++) {
  cantidad_redes_sociales+= redes_sociales.redes_sociales[index].cantidad;
 
  
}

for (let index = 0; index < redes_sociales.redes_sociales.length; index++) {
  if(index==0){
    label_redes_sociales = [redes_sociales.redes_sociales[index].redes_sociales];
    datos_redes_sociales =[Math.round(((redes_sociales.redes_sociales[index].cantidad)/cantidad_redes_sociales)*100)];
  }else{
    let label2 = [ redes_sociales.redes_sociales[index].redes_sociales];
    let datos2 =[Math.round((( redes_sociales.redes_sociales[index].cantidad)/cantidad_redes_sociales)*100)];
    datos_redes_sociales=datos_redes_sociales.concat(datos2);
    label_redes_sociales=label_redes_sociales.concat(label2);
  }
  
}

var options = {
  series: datos_redes_sociales,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: ' usa las redes sociales, '+redes_sociales.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_redes_sociales,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
piechart2.render();
//-----------------------------------------------------------------------------
var label_alteraciones=["habla"];
var datos_alteraciones=[0];
for (let index = 0; index < alteraciones.habla.length; index++) {
  if(index==0){
    label_alteraciones = ["Habla:"+alteraciones.habla[index].habla];
    datos_alteraciones =[alteraciones.habla[index].cantidad];
  }else{
    let label2 = ["Habla:"+alteraciones.habla[index].habla];
    let datos2 =[alteraciones.habla[index].cantidad];
    datos_alteraciones=datos_alteraciones.concat(datos2);
    label_alteraciones=label_alteraciones.concat(label2);
  }
  
}
for (let index = 0; index < alteraciones.visuales.length; index++) {
  
    let label2 = ["Visuales:"+alteraciones.visuales[index].visuales];
    let datos2 =[alteraciones.visuales[index].cantidad];
    datos_alteraciones=datos_alteraciones.concat(datos2);
    label_alteraciones=label_alteraciones.concat(label2);
  
  
}
for (let index = 0; index < alteraciones.auditivas.length; index++) {
  
  let label2 = ["Auditivas:"+alteraciones.auditivas[index].auditivas];
  let datos2 =[alteraciones.auditivas[index].cantidad];
  datos_alteraciones=datos_alteraciones.concat(datos2);
  label_alteraciones=label_alteraciones.concat(label2);


}



var options = {
  series: [{
  name: 'cantidad',
  data: datos_alteraciones

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_alteraciones, 
  title: {
    text: 'Alt en los escolares  '+ alteraciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

var mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
mychart3.render();

//-----------------------------------------------------------------------------
function dominioCincoAlteraciones(){

  var label_alteraciones=["habla"];
  var datos_alteraciones=[0];
  for (let index = 0; index < alteraciones.habla.length; index++) {
    if(index==0){
      label_alteraciones = ["Habla:"+alteraciones.habla[index].habla];
      datos_alteraciones =[alteraciones.habla[index].cantidad];
    }else{
      let label2 = ["Habla:"+alteraciones.habla[index].habla];
      let datos2 =[alteraciones.habla[index].cantidad];
      datos_alteraciones=datos_alteraciones.concat(datos2);
      label_alteraciones=label_alteraciones.concat(label2);
    }
    
  }
  for (let index = 0; index < alteraciones.visuales.length; index++) {
    
      let label2 = ["Visuales:"+alteraciones.visuales[index].visuales];
      let datos2 =[alteraciones.visuales[index].cantidad];
      datos_alteraciones=datos_alteraciones.concat(datos2);
      label_alteraciones=label_alteraciones.concat(label2);
    
    
  }
  for (let index = 0; index < alteraciones.auditivas.length; index++) {
    
    let label2 = ["Auditivas:"+alteraciones.auditivas[index].auditivas];
    let datos2 =[alteraciones.auditivas[index].cantidad];
    datos_alteraciones=datos_alteraciones.concat(datos2);
    label_alteraciones=label_alteraciones.concat(label2);
  
  
  }
  mychart3.destroy();
  var options = {
    series: [{
    name: 'cantidad',
    data: datos_alteraciones
  
  }],
    annotations: {
    points: [{
      x: 'Bananas',
      seriesIndex: 0,
      label: {
        borderColor: '#775DD0',
        offsetY: 0,
        style: {
          color: '#fff000',
          background: '#115DD1',
        },
        text: 'Bananas are good',
      }
    }]
  },
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
      endingShape: 'rounded'  
    }
  },
  dataLabels: {
    enabled: true
  },
  stroke: {
    width: 2
  },
  
  grid: {
    row: {
      colors: ['#fff', '#f2f2f2']
    }
  },
  xaxis: {
    labels: {
      rotate: -45
    },
    categories:label_alteraciones, 
    title: {
      text: 'Alt en los escolares  '+ alteraciones.colegio,
    floating: true,
    align: 'center',
    style: {
      color: '#444'
    }
  },
    tickPlacement: 'on'
  },
  yaxis: {
   
  },
  fill: {
    type: 'gradient',
    gradient: {
      shade: 'light',
      type: "horizontal",
      shadeIntensity: 0.25,
      gradientToColors: undefined,
      inverseColors: true,
      opacityFrom: 0.85,
      opacityTo: 0.85,
      stops: [50, 0, 100]
    },
  }
  };
  
   mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
  mychart3.render();



}



//-----------------------------------------------------------------------------
function dominioCuatroRedes(){

  var label_redes_sociales=['si','no'];
  var datos_redes_sociales=[0,0];
  var cantidad_redes_sociales=0;
  for (let index = 0; index < redes_sociales.redes_sociales.length; index++) {
    cantidad_redes_sociales+= redes_sociales.redes_sociales[index].cantidad;
   
    
  }
  
  for (let index = 0; index < redes_sociales.redes_sociales.length; index++) {
    if(index==0){
      label_redes_sociales = [redes_sociales.redes_sociales[index].redes_sociales];
      datos_redes_sociales =[Math.round(((redes_sociales.redes_sociales[index].cantidad)/cantidad_redes_sociales)*100)];
    }else{
      let label2 = [ redes_sociales.redes_sociales[index].redes_sociales];
      let datos2 =[Math.round((( redes_sociales.redes_sociales[index].cantidad)/cantidad_redes_sociales)*100)];
      datos_redes_sociales=datos_redes_sociales.concat(datos2);
      label_redes_sociales=label_redes_sociales.concat(label2);
    }
    
  }
  piechart2.destroy();
 
var options = {
  series: datos_redes_sociales,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: ' usa las redes sociales, '+redes_sociales.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_redes_sociales,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
piechart2.render();


}

//-----------------------------------------------------------------------------

var label_autopercepcion=[""];
var datos_autopercepcion=[0];
for (let index = 0; index < autopercepcion.describirse.length; index++) {
  if(index==0){
    label_autopercepcion = [autopercepcion.describirse[index].describirse];
    datos_autopercepcion =[autopercepcion.describirse[index].cantidad];
  }else{
    let label2 = [autopercepcion.describirse[index].describirse];
    let datos2 =[autopercepcion.describirse[index].cantidad];
    datos_autopercepcion=datos_autopercepcion.concat(datos2);
    label_autopercepcion=label_autopercepcion.concat(label2);
  }
  
}
for (let index = 0; index < autopercepcion.expresiones.length; index++) {
 
    let label2 = [autopercepcion.expresiones[index].expresiones];
    let datos2 =[autopercepcion.expresiones[index].cantidad];
    datos_autopercepcion=datos_autopercepcion.concat(datos2);
    label_autopercepcion=label_autopercepcion.concat(label2);
  
  
}

var options = {
  series: [{
  name: 'cantidad',
  data: datos_autopercepcion

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '20%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: 45
  },
  categories: label_autopercepcion, 
  title: {
    text: ' Descripcion de si mismo   '+ autopercepcion.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

var mychart4 = new ApexCharts(document.querySelector("#bar-chart4"), options);
mychart4.render();
//-------------------------------------------------------------------------------
function dominioSeis(){

  var label_autopercepcion=[""];
  var datos_autopercepcion=[0];
  for (let index = 0; index < autopercepcion.describirse.length; index++) {
    if(index==0){
      label_autopercepcion = ["El niño es capaz de describirse a si mismo:"+autopercepcion.describirse[index].describirse];
      datos_autopercepcion =[autopercepcion.describirse[index].cantidad];
    }else{
      let label2 = ["El niño es capaz de describirse a si mismo:"+autopercepcion.describirse[index].describirse];
      let datos2 =[autopercepcion.describirse[index].cantidad];
      datos_autopercepcion=datos_autopercepcion.concat(datos2);
      label_autopercepcion=label_autopercepcion.concat(label2);
    }
    
  }
  for (let index = 0; index < autopercepcion.expresiones.length; index++) {
   
      let label2 = ["Relata expresiones negativas hacia si mismo:"+autopercepcion.expresiones[index].expresiones];
      let datos2 =[autopercepcion.expresiones[index].cantidad];
      datos_autopercepcion=datos_autopercepcion.concat(datos2);
      label_autopercepcion=label_autopercepcion.concat(label2);
    
    
  }
  mychart4.destroy();
  
var options = {
  series: [{
  name: 'cantidad',
  data: datos_autopercepcion

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '20%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: 45
  },
  categories: label_autopercepcion, 
  title: {
    text: ' Descripcion de si mismo   '+ autopercepcion.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

 mychart4 = new ApexCharts(document.querySelector("#bar-chart4"), options);
mychart4.render();


}



//-------------------------------------------------------------------------------
function dominioCuatroAlteraciones(){

  var label_alteraciones_columna=['si','no'];
  var datos_alteraciones_columna=[0,0];
  var cantidad_alteraciones_columna=0;
  for (let index = 0; index < alteraciones_columna.alteraciones_columna.length; index++) {
    cantidad_alteraciones_columna+= alteraciones_columna.alteraciones_columna[index].cantidad;
   
    
  }
  
  for (let index = 0; index < alteraciones_columna.alteraciones_columna.length; index++) {
    if(index==0){
      label_alteraciones_columna = [alteraciones_columna.alteraciones_columna[index].alteraciones_columna];
      datos_alteraciones_columna =[Math.round(((alteraciones_columna.alteraciones_columna[index].cantidad)/cantidad_alteraciones_columna)*100)];
    }else{
      let label2 = [ alteraciones_columna.alteraciones_columna[index].alteraciones_columna];
      let datos2 =[Math.round((( alteraciones_columna.alteraciones_columna[index].cantidad)/cantidad_alteraciones_columna)*100)];
      datos_alteraciones_columna=datos_alteraciones_columna.concat(datos2);
      label_alteraciones_columna=label_alteraciones_columna.concat(label2);
    }
    
  }
  piechart.destroy();

  var options = {
    series: datos_alteraciones_columna,
    chart: {
    width: '100%',
    type: 'pie',
  },
  plotOptions: {
    pie: {
      dataLabels: {
        offset: -5
      }
    }
  },
  title: {
    text: 'Alt en columna, '+alteraciones_columna.colegio,
    floating: true,
    align: 'center',
    style: {
      color: '#444'
    }
  },
  labels: label_alteraciones_columna,
  responsive: [{
    breakpoint: 280,
    options: {
      chart: {
        width: 100
      },
      legend: {
        show: false
      }
    }
  }]
  };
 piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
  piechart.render();


}


//-------------------------------------------------------------------------------
function dominioCuatroActividades(){


  var label_actividades=[""];
  var datos_actividades=[0];
  for (let index = 0; index < actividades.actividades.length; index++) {
    if(index==0){
      label_actividades = [actividades.actividades[index].actividades_recreativas];
      datos_actividades =[actividades.actividades[index].cantidad];
    }else{
      let label2 = [actividades.actividades[index].actividades_recreativas];
      let datos2 =[actividades.actividades[index].cantidad];
      datos_actividades=datos_actividades.concat(datos2);
      label_actividades=label_actividades.concat(label2);
    }
    
  }
   mychart2.destroy();
   var options = {
    series: [{
    name: 'cantidad',
    data: datos_actividades
  
  }],
    annotations: {
    points: [{
      x: 'Bananas',
      seriesIndex: 0,
      label: {
        borderColor: '#775DD0',
        offsetY: 0,
        style: {
          color: '#fff000',
          background: '#115DD1',
        },
        text: 'Bananas are good',
      }
    }]
  },
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
      endingShape: 'rounded'  
    }
  },
  dataLabels: {
    enabled: true
  },
  stroke: {
    width: 2
  },
  
  grid: {
    row: {
      colors: ['#fff', '#f2f2f2']
    }
  },
  xaxis: {
    labels: {
      rotate: -45
    },
    categories:label_actividades, 
    title: {
      text: 'Frecuencia de actividades recreativas  '+ actividades.colegio,
    floating: true,
    align: 'center',
    style: {
      color: '#444'
    }
  },
    tickPlacement: 'on'
  },
  yaxis: {
   
  },
  fill: {
    type: 'gradient',
    gradient: {
      shade: 'light',
      type: "horizontal",
      shadeIntensity: 0.25,
      gradientToColors: undefined,
      inverseColors: true,
      opacityFrom: 0.85,
      opacityTo: 0.85,
      stops: [50, 0, 100]
    },
  }
  };
  
  mychart2 = new ApexCharts(document.querySelector("#bar-chart2"), options);
  mychart2.render();


  
}

//----------------------------------------------------------------------------------
function dominioCuatroHoras(){
  var label_horas=[""];
var datos_horas=[0];
for (let index = 0; index < horas.horas.length; index++) {
  if(index==0){
    label_horas = ["Horas: "+horas.horas[index].horas_sueno];
    datos_horas =[horas.horas[index].cantidad];
  }else{
    let label2 = ["Horas: "+horas.horas[index].horas_sueno];
    let datos2 =[horas.horas[index].cantidad];
    datos_horas=datos_horas.concat(datos2);
    label_horas=label_horas.concat(label2);
  }
  
}
mychart1.destroy();

var options = {
  series: [{
  name: 'cantidad',
  data: datos_horas

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_horas, 
  title: {
    text: 'Frecuencia de estudiantes según N° de horas que duerme al dia  '+ horas.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};
 mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
mychart1.render();
}

//-----------------------------------------------------------------
