var $primary = '#7367F0';
var $danger = '#EA5455';
var $warning = '#FF9F43';
var $info = '#0DCCE1';
var $primary_light = '#8F80F9';
var $warning_light = '#FFC085';
var $danger_light = '#f29292';
var $info_light = '#1edec5';
var $strok_color = '#b9c3cd';
var $label_color = '#e7eef7';
var $white = '#fff';


// DOMINIO 10 -  religion-bar start 

var label_religion=[""];
var datos_religion=[0];
for (let index = 0; index < religion.religion.length; index++) {
  if(index==0){
    label_religion = [religion.religion[index].religion];
    datos_religion =[religion.religion[index].cantidad];
  }else{
    let label2 = [religion.religion[index].religion];
    let datos2 =[religion.religion[index].cantidad];
    datos_religion=datos_religion.concat(datos2);
    label_religion=label_religion.concat(label2);
  }
  
}


var sessionChartoptions = {
  chart: {
    type: 'bar',
    height: 200,
    sparkline: { enabled: true },
    toolbar: { show: false },
  },
  states: {
    hover: {
      filter: 'none'
    }
  },
  colors: [$label_color, $label_color, $primary, $label_color, $label_color, $label_color],
  series: [{
    name: "# Estudiantes",
    data: datos_religion
  }],
  grid: {
    show: false,
    padding: {
      left: 0,
      right: 0
    }
  },

  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true,
      endingShape: 'rounded'
    }
  },
  tooltip: {
    x: { show: false }
  },
  xaxis: {
    type: 'text',
    categories:label_religion
    
  },



  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
      endingShape: 'rounded'  
    }
  },
  dataLabels: {
    enabled: false
  },
  
  stroke: {
    width: 2
  },

  
}

var sessionChart = new ApexCharts(
  document.querySelector("#religion-bar"),
  sessionChartoptions
);

sessionChart.render();

//religion-bar ends 


//--------------------------------------------------------------------
// DOMINIO ONCE -  carnet-pie

var label_carnet=['si','no'];
var datos_carnet=0;
var cantidad_carnet=0;
for (let index = 0; index < carnet.carnet.length; index++) {
cantidad_carnet+= carnet.carnet[index].cantidad;


}

for (let index = 0; index < carnet.carnet.length; index++) {
if(index==0){
    if(carnet.carnet[index].carnet==1){
        label_carnet= ["Si"];
    }else{
        label_carnet= ["No"];
    }
 
    datos_carnet =[Math.round(((carnet.carnet[index].cantidad)/cantidad_carnet)*100)];
}else{
  if(carnet.carnet[index].carnet==1){
      label2= ["Si"];
    }else{
      label2= ["No"];
    }
  let datos2 =[Math.round((( carnet.carnet[index].cantidad)/cantidad_carnet)*100)];
  datos_carnet=datos_carnet.concat(datos2);
  label_carnet=label_carnet.concat(label2);
}

}


var sessionChartoptions = {
  chart: {
    type: 'donut',
    height: 325,
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: false
  },
  series: datos_carnet,
  legend: { show: false },
  comparedResult: [2, -3, 8],
  labels: label_carnet,
  stroke: { width: 0 },
  colors: [$primary, $warning, $danger],
  fill: {
    type: 'gradient',
    gradient: {
      gradientToColors: [$primary_light, $warning_light, $danger_light]
    }
  }
}

var sessionChart = new ApexCharts(
  document.querySelector("#carnet-pie"),
  sessionChartoptions
);

sessionChart.render();


//-----------------------------------------------------------------------
// DOMINIO DOCE -  dolor-bar start


var label_dolor=[""];
var datos_dolor=[0];
for (let index = 0; index < dolor_valorizacion.dolor.length; index++) {
if(index==0){
if(dolor_valorizacion.dolor[index].dolor==1){
    label_dolor= ["Si"];
}else{
    label_dolor= ["No"];
}
datos_dolor =[dolor_valorizacion.dolor[index].cantidad];
}else{
if(dolor_valorizacion.dolor[index].dolor==1){
    label2= ["Si"];
}else{
    label2= ["No"];
}
let datos2 =[dolor_valorizacion.dolor[index].cantidad];
datos_dolor=datos_dolor.concat(datos2);
label_dolor=label_dolor.concat(label2);
}

}



var sessionChartoptions = {
  chart: {
    type: 'bar',
    height: 200,
    sparkline: { enabled: true },
    toolbar: { show: false },
  },
  states: {
    hover: {
      filter: 'none'
    }
  },
  colors: [$label_color, $label_color, $primary, $label_color, $label_color, $label_color],
  series: [{
    name: "# Estudiantes",
    data: datos_dolor
  }],
  grid: {
    show: false,
    padding: {
      left: 0,
      right: 0
    }
  },

  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true,
      endingShape: 'rounded'
    }
  },
  tooltip: {
    x: { show: false }
  },
  xaxis: {
    type: 'text',
    categories:label_dolor
    
  },



  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
      endingShape: 'rounded'  
    }
  },
  dataLabels: {
    enabled: false
  },
  
  stroke: {
    width: 2
  }
}

var sessionDolor = new ApexCharts(
  document.querySelector("#dolor-bar"),
  sessionChartoptions
);

sessionDolor.render();

//-----------------------------------------------------------------------


//DOMINIO TRECE - problemas-bar start 


var label_problemas=['si','no'];
var datos_problemas=0;
var cantidad_carnet=0;

for (let index = 0; index < problemas.problemas.length; index++) {
    cantidad_carnet+= problemas.problemas[index].cantidad;
 
  
}


for (let index = 0; index < problemas.problemas.length; index++) {
    if(index==0){
        if(problemas.problemas[index].problemas==1){
            label_problemas= ["Si"];
        }else{
            label_problemas= ["No"];
        }
     
        datos_problemas =[Math.round(((problemas.problemas[index].cantidad)/cantidad_carnet)*100)];
    }else{
      if(problemas.problemas[index].problemas==1){
          label2= ["Si"];
        }else{
          label2= ["No"];
        }
      let datos2 =[Math.round((( problemas.problemas[index].cantidad)/cantidad_carnet)*100)];
      datos_problemas=datos_problemas.concat(datos2);
      label_problemas=label_problemas.concat(label2);
    }
    
  }

var customerChartoptions = {
      chart: {
        type: 'pie',
        height: 330,
        dropShadow: {
          enabled: true,
          blur: 5,
          left: 1,
          top: 1,
          opacity: 0.2
        },
        toolbar: {
          show: false
        }
      },
      labels: label_problemas,
      series: datos_problemas,
      dataLabels: {
        enabled: false
      },
      legend: { show: false },
      stroke: {
        width: 5
      },
      colors: [$primary, $warning, $danger],
      fill: {
        type: 'gradient',
        gradient: {
          gradientToColors: [$primary_light, $warning_light, $danger_light]
        }  
      }
    }
  
    var customerChart = new ApexCharts(
      document.querySelector("#problemas-pie"),
      customerChartoptions
    );
  
    customerChart.render();
  

