
    var Message;
    var id_enviar;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
   
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };
        sendMessage = function (text,tipo) {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
         
            
            message_side =  tipo;
          
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
        //enviar mensaje
        $('.send_message').click(function (e) {
if(getMessageText()!=""){
            $.ajax({
                type: "GET",
                url: "/enviarMensaje/"+id_enviar+"/"+getMessageText(),
                success: function (data) {
                    sendMessage(getMessageText(),'left');
                } ,
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    md.showNotification('top','right','Error enviando mensajes');
                }          
            });
        }

            
        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                
                return sendMessage(getMessageText(),'left');
            }
        });
        
      
    



//------------------------------------------------------------
function doSearch()
{
    const tableReg = document.getElementById('datos');
    const searchText = document.getElementById('searchTerm').value.toLowerCase();
    let total = 0;

    // Recorremos todas las filas con contenido de la tabla
    for (let i = 1; i < tableReg.rows.length; i++) {
        // Si el td tiene la clase "noSearch" no se busca en su cntenido
        if (tableReg.rows[i].classList.contains("noSearch")) {
            continue;
        }

        let found = false;
        const cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        // Recorremos todas las celdas
        for (let j = 0; j < cellsOfRow.length && !found; j++) {
            const compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            // Buscamos el texto en el contenido de la celda
            if (searchText.length == 0 || compareWith.indexOf(searchText) > -1) {
                found = true;
                total++;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg.rows[i].style.display = 'none';
        }
    }

    // mostramos las coincidencias
    const lastTR=tableReg.rows[tableReg.rows.length-1];
    const td=lastTR.querySelector("td");
    lastTR.classList.remove("hide", "red");
    if (searchText == "") {
        lastTR.classList.add("hide");
    } else if (total) {
        td.innerHTML="Se ha encontrado "+total+" coincidencia"+((total>1)?"s":"");
    } else {
        lastTR.classList.add("red");
        td.innerHTML="No se han encontrado coincidencias";
    }
}
//---------------------------

function historialMensaje(id,nombre)
{
    
    id_enviar=id;
document.getElementById('nombre').innerHTML="Chat Con "+nombre;
$('.appeared').remove();
$.ajax({
    type: "GET",
    url: "/historialMensaje/"+id,
    data: {id:id},
    success: function (data) {
        var lista =data.historial.concat(data.historial2);

        var n, i, k, aux;
        n = lista.length;
        for (k = 1; k < n; k++) {
            for (i = 0; i < (n - k); i++) {
                if (lista[i].id > lista[i + 1].id) {
                    aux = lista[i];
                    lista[i] = lista[i + 1];
                    lista[i + 1] = aux;
                }
            }
        }
        for (let index = 0; index < lista.length; index++) {
            if(id!=lista[index].id_user){
                document.getElementById('me').innerHTML="";
                sendMessage(lista[index].mensaje,'left');
            }else{
                document.getElementById('me').innerHTML="";
                sendMessage(lista[index].mensaje,'right');
            }
        }
       
    }         
});
}
