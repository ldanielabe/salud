
var label_alteracion=['si','no'];
var datos_alteracion=[0,0];
var cantidad_alteracion=0;
for (let index = 0; index < relaciones.alteracion.length; index++) {
  cantidad_alteracion+= relaciones.alteracion[index].cantidad;
 
  
}

for (let index = 0; index < relaciones.alteracion.length; index++) {
  if(index==0){
      if(relaciones.alteracion[index].alteracion==1){
        label_alteracion= ["Si"];
      }else{
        label_alteracion= ["No"];
      }
   
    datos_alteracion =[Math.round(((relaciones.alteracion[index].cantidad)/cantidad_alteracion)*100)];
  }else{
    if(relaciones.alteracion[index].alteracion==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( relaciones.alteracion[index].cantidad)/cantidad_alteracion)*100)];
    datos_alteracion=datos_alteracion.concat(datos2);
    label_alteracion=label_alteracion.concat(label2);
  }
  
}

var options = {
  series: datos_alteracion,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alt de la conducta, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_alteracion,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
piechart.render();

//----------------------------------------------------------------------------
function dominioSieteRelaciones(){
    var label_alteracion=['si','no'];
var datos_alteracion=[0,0];
var cantidad_alteracion=0;
for (let index = 0; index < relaciones.alteracion.length; index++) {
  cantidad_alteracion+= relaciones.alteracion[index].cantidad;
 
  
}

for (let index = 0; index < relaciones.alteracion.length; index++) {
  if(index==0){
      if(relaciones.alteracion[index].alteracion==1){
        label_alteracion= ["Si"];
      }else{
        label_alteracion= ["No"];
      }
   
    datos_alteracion =[Math.round(((relaciones.alteracion[index].cantidad)/cantidad_alteracion)*100)];
  }else{
    if(relaciones.alteracion[index].alteracion==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( relaciones.alteracion[index].cantidad)/cantidad_alteracion)*100)];
    datos_alteracion=datos_alteracion.concat(datos2);
    label_alteracion=label_alteracion.concat(label2);
  }
  
}
piechart.destroy();

var options = {
  series: datos_alteracion,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alt de la conducta, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_alteracion,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
}; piechart = new ApexCharts(document.querySelector("#pie-chart"), options);
piechart.render();

}
//----------------------------------------------------------------------------
var label_tutores=[""];
var datos_tutores=[0];
for (let index = 0; index < relaciones.tutores.length; index++) {
  if(index==0){
    label_tutores = [relaciones.tutores[index].tutores];
    datos_tutores =[relaciones.tutores[index].cantidad];
  }else{
    let label2 = [relaciones.tutores[index].tutores];
    let datos2 =[relaciones.tutores[index].cantidad];
    datos_tutores=datos_tutores.concat(datos2);
    label_tutores=label_tutores.concat(label2);
  }
  
}

        
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_tutores
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
plotOptions: {
  bar: {
    columnWidth: '25%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_tutores,
  position: 'top',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: ' Tutores a cargo de los escolares   '+ relaciones.colegio,
  floating: true,
  offsetY: 330,
  align: 'center',
  style: {
    color: '#444'
  }
}
};

var mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
mychart1.render();

//--------------------------------------------------------------------
function dominioSieteTutores(){

  var label_tutores=[""];
  var datos_tutores=[0];
  for (let index = 0; index < relaciones.tutores.length; index++) {
    if(index==0){
      label_tutores = [relaciones.tutores[index].tutores];
      datos_tutores =[relaciones.tutores[index].cantidad];
    }else{
      let label2 = [relaciones.tutores[index].tutores];
      let datos2 =[relaciones.tutores[index].cantidad];
      datos_tutores=datos_tutores.concat(datos2);
      label_tutores=label_tutores.concat(label2);
    }
    
  }
  mychart1.destroy();
        
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_tutores
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
plotOptions: {
  bar: {
    columnWidth: '25%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_tutores,
  position: 'top',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: ' Tutores a cargo de los escolares   '+ relaciones.colegio,
  floating: true,
  offsetY: 330,
  align: 'center',
  style: {
    color: '#444'
  }
}
};

 mychart1 = new ApexCharts(document.querySelector("#bar-chart1"), options);
mychart1.render();

}
//--------------------------------------------------------------------------------
var label_signos_maltrato=['si','no'];
var datos_signos_maltrato=[0,0];
var cantidad_signos_maltrato=0;
for (let index = 0; index < relaciones.signos_maltrato.length; index++) {
  cantidad_signos_maltrato+= relaciones.signos_maltrato[index].cantidad;
 
  
}

for (let index = 0; index < relaciones.signos_maltrato.length; index++) {
  if(index==0){
      if(relaciones.signos_maltrato[index].signos_maltrato==1){
        label_signos_maltrato= ["Si"];
      }else{
        label_signos_maltrato= ["No"];
      }
   
      datos_signos_maltrato =[Math.round(((relaciones.signos_maltrato[index].cantidad)/cantidad_signos_maltrato)*100)];
  }else{
    if(relaciones.signos_maltrato[index].signos_maltrato==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( relaciones.signos_maltrato[index].cantidad)/cantidad_signos_maltrato)*100)];
    datos_signos_maltrato=datos_signos_maltrato.concat(datos2);
    label_signos_maltrato=label_signos_maltrato.concat(label2);
  }
  
}

var options = {
  series: datos_signos_maltrato,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alt de la conducta, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_signos_maltrato,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
piechart2.render();

//-----------------------------------------------------------------------------------
function dominioSieteMaltrato(){
  var label_signos_maltrato=['si','no'];
var datos_signos_maltrato=[0,0];
var cantidad_signos_maltrato=0;
for (let index = 0; index < relaciones.signos_maltrato.length; index++) {
  cantidad_signos_maltrato+= relaciones.signos_maltrato[index].cantidad;
 
  
}

for (let index = 0; index < relaciones.signos_maltrato.length; index++) {
  if(index==0){
      if(relaciones.signos_maltrato[index].signos_maltrato==1){
        label_signos_maltrato= ["Si"];
      }else{
        label_signos_maltrato= ["No"];
      }
   
      datos_signos_maltrato =[Math.round(((relaciones.signos_maltrato[index].cantidad)/cantidad_signos_maltrato)*100)];
  }else{
    if(relaciones.signos_maltrato[index].signos_maltrato==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( relaciones.signos_maltrato[index].cantidad)/cantidad_signos_maltrato)*100)];
    datos_signos_maltrato=datos_signos_maltrato.concat(datos2);
    label_signos_maltrato=label_signos_maltrato.concat(label2);
  }
  
}
piechart2.destroy();
 
var options = {
  series: datos_signos_maltrato,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alt de la conducta, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_signos_maltrato,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart2 = new ApexCharts(document.querySelector("#pie-chart2"), options);
piechart2.render();
}
//----------------------------------------------------------------------------------

//--------------------------------------------------------------------
var label_mecanismo_castigo=[""];
var datos_mecanismo_castigo=[0];
for (let index = 0; index < relaciones.mecanismo_castigo.length; index++) {
  if(index==0){
    label_mecanismo_castigo = [relaciones.mecanismo_castigo[index].mecanismo_castigo];
    datos_mecanismo_castigo =[relaciones.mecanismo_castigo[index].cantidad];
  }else{
    let label2 = [relaciones.mecanismo_castigo[index].mecanismo_castigo];
    let datos2 =[relaciones.mecanismo_castigo[index].cantidad];
    datos_mecanismo_castigo=datos_mecanismo_castigo.concat(datos2);
    label_mecanismo_castigo=label_mecanismo_castigo.concat(label2);
  }
  
}

var options = {
  series: [{
  data: datos_mecanismo_castigo
}],
  chart: {
  type: 'bar',
  height: 350
},
plotOptions: {
  bar: {
    horizontal: true,
  }
},
dataLabels: {
  enabled: false
},
xaxis: {
  categories: label_mecanismo_castigo,
},
title: {
  text: ' Mecanismo que utilizan los padres para corregir a sus hijos, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
};

var myBarChart = new ApexCharts(document.querySelector("#bar-chart2"), options);
myBarChart.render();

//-------------------------------------------------------------------------
function dominioSieteCastigo(){
  var label_mecanismo_castigo=[""];
var datos_mecanismo_castigo=[0];
for (let index = 0; index < relaciones.mecanismo_castigo.length; index++) {
  if(index==0){
    label_mecanismo_castigo = [relaciones.mecanismo_castigo[index].mecanismo_castigo];
    datos_mecanismo_castigo =[relaciones.mecanismo_castigo[index].cantidad];
  }else{
    let label2 = [relaciones.mecanismo_castigo[index].mecanismo_castigo];
    let datos2 =[relaciones.mecanismo_castigo[index].cantidad];
    datos_mecanismo_castigo=datos_mecanismo_castigo.concat(datos2);
    label_mecanismo_castigo=label_mecanismo_castigo.concat(label2);
  }
  
}


myBarChart.destroy();

var options = {
  series: [{
  data: datos_mecanismo_castigo
}],
  chart: {
  type: 'bar',
  height: 350
},
plotOptions: {
  bar: {
    horizontal: true,
  }
},
dataLabels: {
  enabled: false
},
xaxis: {
  categories: label_mecanismo_castigo,
},
title: {
  text: ' Mecanismo que utilizan los padres para corregir a sus hijos, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
};

 myBarChart = new ApexCharts(document.querySelector("#bar-chart2"), options);
myBarChart.render();
}
//-----------------------------------------------------------------------------
var label_aprobacion_castigo=[""];
var datos_aprobacion_castigo=[0];
for (let index = 0; index < relaciones.aprobacion_castigo.length; index++) {
  if(index==0){
    label_aprobacion_castigo = [relaciones.aprobacion_castigo[index].aprobacion_castigo];
    datos_aprobacion_castigo =[relaciones.aprobacion_castigo[index].cantidad];
  }else{
    let label2 = [relaciones.aprobacion_castigo[index].aprobacion_castigo];
    let datos2 =[relaciones.aprobacion_castigo[index].cantidad];
    datos_aprobacion_castigo=datos_aprobacion_castigo.concat(datos2);
    label_aprobacion_castigo=label_aprobacion_castigo.concat(label2);
  }
  
}

var options = {
  series: [{
  name: 'cantidad',
  data: datos_aprobacion_castigo

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_aprobacion_castigo, 
  title: {
    text: 'Esta de acuerdo con el castigo   '+ relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

var mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
mychart3.render();


//----------------------------------------------------------------------
function dominioSieteAprobacion(){
  var label_aprobacion_castigo=[""];
var datos_aprobacion_castigo=[0];
for (let index = 0; index < relaciones.aprobacion_castigo.length; index++) {
  if(index==0){
    label_aprobacion_castigo = [relaciones.aprobacion_castigo[index].aprobacion_castigo];
    datos_aprobacion_castigo =[relaciones.aprobacion_castigo[index].cantidad];
  }else{
    let label2 = [relaciones.aprobacion_castigo[index].aprobacion_castigo];
    let datos2 =[relaciones.aprobacion_castigo[index].cantidad];
    datos_aprobacion_castigo=datos_aprobacion_castigo.concat(datos2);
    label_aprobacion_castigo=label_aprobacion_castigo.concat(label2);
  }
  
}
mychart3.destroy();
var options = {
  series: [{
  name: 'cantidad',
  data: datos_aprobacion_castigo

}],
  annotations: {
  points: [{
    x: 'Bananas',
    seriesIndex: 0,
    label: {
      borderColor: '#775DD0',
      offsetY: 0,
      style: {
        color: '#fff000',
        background: '#115DD1',
      },
      text: 'Bananas are good',
    }
  }]
},
chart: {
  height: 350,
  type: 'bar',
},
plotOptions: {
  bar: {
    columnWidth: '50%',
    endingShape: 'rounded'  
  }
},
dataLabels: {
  enabled: true
},
stroke: {
  width: 2
},

grid: {
  row: {
    colors: ['#fff', '#f2f2f2']
  }
},
xaxis: {
  labels: {
    rotate: -45
  },
  categories:label_aprobacion_castigo, 
  title: {
    text: 'Esta de acuerdo con el castigo   '+ relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
  tickPlacement: 'on'
},
yaxis: {
 
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: "horizontal",
    shadeIntensity: 0.25,
    gradientToColors: undefined,
    inverseColors: true,
    opacityFrom: 0.85,
    opacityTo: 0.85,
    stops: [50, 0, 100]
  },
}
};

 mychart3 = new ApexCharts(document.querySelector("#bar-chart3"), options);
mychart3.render();
}
//------------------------------------
var label_relaciones_interpersonales=['Buena','Regular'];
var datos_relaciones_interpersonales=[0,0];
var cantidad_alteracion=0;
for (let index = 0; index < relaciones.relaciones_interpersonales.length; index++) {
  cantidad_alteracion+= relaciones.relaciones_interpersonales[index].cantidad;
 
  
}


for (let index = 0; index < relaciones.relaciones_interpersonales.length; index++) {
  if(index==0){
    label_relaciones_interpersonales = [relaciones.relaciones_interpersonales[index].relaciones_interpersonales];
    datos_relaciones_interpersonales =[Math.round(((relaciones.relaciones_interpersonales[index].cantidad)/cantidad_alteracion)*100)];
  }else{
    let label2 = [ relaciones.relaciones_interpersonales[index].relaciones_interpersonales];
    let datos2 =[Math.round(((relaciones.relaciones_interpersonales[index].cantidad)/cantidad_alteracion)*100)];
    datos_relaciones_interpersonales=datos_alteraciones_columna.concat(datos2);
    label_relaciones_interpersonales=label_relaciones_interpersonales.concat(label2);
  }
  
}


var options = {
  series: datos_relaciones_interpersonales,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alteraciones en columna, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_relaciones_interpersonales,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart3 = new ApexCharts(document.querySelector("#pie-chart3"), options);
piechart3.render();

//--------------------------------------------------------------------------
function dominioSieteAlteraciones(){
  var label_relaciones_interpersonales=['Buena','Regular'];
var datos_relaciones_interpersonales=[0,0];
var cantidad_alteracion=0;
for (let index = 0; index < relaciones.relaciones_interpersonales.length; index++) {
  cantidad_alteracion+= relaciones.relaciones_interpersonales[index].cantidad;
 
  
}


for (let index = 0; index < relaciones.relaciones_interpersonales.length; index++) {
  if(index==0){
    label_relaciones_interpersonales = [relaciones.relaciones_interpersonales[index].relaciones_interpersonales];
    datos_relaciones_interpersonales =[Math.round(((relaciones.relaciones_interpersonales[index].cantidad)/cantidad_alteracion)*100)];
  }else{
    let label2 = [ relaciones.relaciones_interpersonales[index].relaciones_interpersonales];
    let datos2 =[Math.round(((relaciones.relaciones_interpersonales[index].cantidad)/cantidad_alteracion)*100)];
    datos_relaciones_interpersonales=datos_alteraciones_columna.concat(datos2);
    label_relaciones_interpersonales=label_relaciones_interpersonales.concat(label2);
  }
  
}

piechart3.destroy();

var options = {
  series: datos_relaciones_interpersonales,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Alteraciones en columna, '+relaciones.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_relaciones_interpersonales,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart3 = new ApexCharts(document.querySelector("#pie-chart3"), options);
piechart3.render();
}
//----------------------------------------------------------------------Dominio8
var label_expresion=['si','no'];
var datos_expresion=[0,0];
var cantidad_expresion=0;
for (let index = 0; index < sexualidad.expresion.length; index++) {
  cantidad_expresion+= sexualidad.expresion[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.expresion.length; index++) {
  if(index==0){
      if(sexualidad.expresion[index].expresion==1){
        label_expresion= ["Si"];
      }else{
        label_expresion= ["No"];
      }
   
      datos_expresion =[Math.round(((sexualidad.expresion[index].cantidad)/cantidad_expresion)*100)];
  }else{
    if(sexualidad.expresion[index].expresion==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.expresion[index].cantidad)/cantidad_expresion)*100)];
    datos_expresion=datos_expresion.concat(datos2);
    label_expresion=label_expresion.concat(label2);
  }
  
}

var options = {
  series: datos_expresion,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Expresa su sexualidad, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_expresion,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart4 = new ApexCharts(document.querySelector("#pie-chart4"), options);
piechart4.render();


//-------------------------------------------
function dominioOchoExpresion(){
  var label_expresion=['si','no'];
var datos_expresion=[0,0];
var cantidad_expresion=0;
for (let index = 0; index < sexualidad.expresion.length; index++) {
  cantidad_expresion+= sexualidad.expresion[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.expresion.length; index++) {
  if(index==0){
      if(sexualidad.expresion[index].expresion==1){
        label_expresion= ["Si"];
      }else{
        label_expresion= ["No"];
      }
   
      datos_expresion =[Math.round(((sexualidad.expresion[index].cantidad)/cantidad_expresion)*100)];
  }else{
    if(sexualidad.expresion[index].expresion==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.expresion[index].cantidad)/cantidad_expresion)*100)];
    datos_expresion=datos_expresion.concat(datos2);
    label_expresion=label_expresion.concat(label2);
  }
  
}
piechart4.destroy();
var options = {
  series: datos_expresion,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Expresa su sexualidad, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_expresion,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart4 = new ApexCharts(document.querySelector("#pie-chart4"), options);
piechart4.render();


}
//------------------------------------
var label_cuida_su_cuerpo=['si','no'];
var datos_cuida_su_cuerpo=[0,0];
var cantidad_cuida_su_cuerpo=0;

for (let index = 0; index < sexualidad.cuida_su_cuerpo.length; index++) {
  cantidad_cuida_su_cuerpo+= sexualidad.cuida_su_cuerpo[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.cuida_su_cuerpo.length; index++) {
  if(index==0){
      if(sexualidad.cuida_su_cuerpo[index].cuida_su_cuerpo==1){
        label_cuida_su_cuerpo= ["Si"];
      }else{
        label_cuida_su_cuerpo= ["No"];
      }
   
      datos_cuida_su_cuerpo =[Math.round(((sexualidad.cuida_su_cuerpo[index].cantidad)/cantidad_cuida_su_cuerpo)*100)];
  }else{
    if(sexualidad.cuida_su_cuerpo[index].cuida_su_cuerpo==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.cuida_su_cuerpo[index].cantidad)/cantidad_cuida_su_cuerpo)*100)];
    datos_cuida_su_cuerpo=datos_cuida_su_cuerpo.concat(datos2);
    label_cuida_su_cuerpo=label_cuida_su_cuerpo.concat(label2);
  }
  
}
var options = {
  series: datos_cuida_su_cuerpo,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Cuidado de su cuerpo, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_cuida_su_cuerpo,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart5 = new ApexCharts(document.querySelector("#pie-chart5"), options);
piechart5.render();

//-----------------------------------------------------------------------------
function dominioOchoCuidado(){
  var label_cuida_su_cuerpo=['si','no'];
var datos_cuida_su_cuerpo=[0,0];
var cantidad_cuida_su_cuerpo=0;
for (let index = 0; index < sexualidad.cuida_su_cuerpo.length; index++) {
  cantidad_cuida_su_cuerpo+= sexualidad.cuida_su_cuerpo[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.cuida_su_cuerpo.length; index++) {
  if(index==0){
      if(sexualidad.cuida_su_cuerpo[index].cuida_su_cuerpo==1){
        label_cuida_su_cuerpo= ["Si"];
      }else{
        label_cuida_su_cuerpo= ["No"];
      }
   
      datos_cuida_su_cuerpo =[Math.round(((sexualidad.cuida_su_cuerpo[index].cantidad)/cantidad_cuida_su_cuerpo)*100)];
  }else{
    if(sexualidad.cuida_su_cuerpo[index].cuida_su_cuerpo==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.cuida_su_cuerpo[index].cantidad)/cantidad_cuida_su_cuerpo)*100)];
    datos_cuida_su_cuerpo=datos_cuida_su_cuerpo.concat(datos2);
    label_cuida_su_cuerpo=label_cuida_su_cuerpo.concat(label2);
  }
  
}
piechart5.destroy();
var options = {
  series: datos_cuida_su_cuerpo,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Cuidado de su cuerpo, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_cuida_su_cuerpo,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart5 = new ApexCharts(document.querySelector("#pie-chart5"), options);
piechart5.render();
}
//-------------------------------------------------------------------
var label_abuso_sexual=['si','no'];
var datos_abuso_sexual=[0,0];
var cantidad_abuso_sexual=0;

for (let index = 0; index < sexualidad.abuso_sexual.length; index++) {
  cantidad_abuso_sexual+= sexualidad.abuso_sexual[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.abuso_sexual.length; index++) {
  if(index==0){
      if(sexualidad.abuso_sexual[index].abuso_sexual==1){
        label_abuso_sexual= ["Si"];
      }else{
        label_abuso_sexual= ["No"];
      }
   
      datos_abuso_sexual =[Math.round(((sexualidad.abuso_sexual[index].cantidad)/cantidad_abuso_sexual)*100)];
  }else{
    if(sexualidad.abuso_sexual[index].abuso_sexual==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.abuso_sexual[index].cantidad)/cantidad_abuso_sexual)*100)];
    datos_abuso_sexual=datos_abuso_sexual.concat(datos2);
    label_abuso_sexual=label_abuso_sexual.concat(label2);
  }
  
}
var options = {
  series: datos_abuso_sexual,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Riesgo abuso sexual, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_abuso_sexual,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart6 = new ApexCharts(document.querySelector("#pie-chart6"), options);
piechart6.render();


//------------------------------------------------------------------------------
function dominioOchoRiesgo(){
  var label_abuso_sexual=['si','no'];
var datos_abuso_sexual=[0,0];
var cantidad_abuso_sexual=0;

for (let index = 0; index < sexualidad.abuso_sexual.length; index++) {
  cantidad_abuso_sexual+= sexualidad.abuso_sexual[index].cantidad;
 
  
}

for (let index = 0; index < sexualidad.abuso_sexual.length; index++) {
  if(index==0){
      if(sexualidad.abuso_sexual[index].abuso_sexual==1){
        label_abuso_sexual= ["Si"];
      }else{
        label_abuso_sexual= ["No"];
      }
   
      datos_abuso_sexual =[Math.round(((sexualidad.abuso_sexual[index].cantidad)/cantidad_abuso_sexual)*100)];
  }else{
    if(sexualidad.abuso_sexual[index].abuso_sexual==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( sexualidad.abuso_sexual[index].cantidad)/cantidad_abuso_sexual)*100)];
    datos_abuso_sexual=datos_abuso_sexual.concat(datos2);
    label_abuso_sexual=label_abuso_sexual.concat(label2);
  }
  
}
piechart6.destroy();
var options = {
  series: datos_abuso_sexual,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Riesgo abuso sexual, '+sexualidad.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_abuso_sexual,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart6 = new ApexCharts(document.querySelector("#pie-chart6"), options);
piechart6.render();
}
//----------------------------------------------------------------------------------------
var label_estres=['si','no'];
var datos_estres=[0,0];
var cantidad_estres=0;

for (let index = 0; index < estres.tiene_estres.length; index++) {
  cantidad_estres+= estres.tiene_estres[index].cantidad;
 
  
}

for (let index = 0; index < estres.tiene_estres.length; index++) {
  if(index==0){
      if(estres.tiene_estres[index].tiene_estres==1){
        label_estres= ["Si"];
      }else{
        label_estres= ["No"];
      }
   
      datos_estres =[Math.round(((estres.tiene_estres[index].cantidad)/cantidad_estres)*100)];
  }else{
    if(estres.tiene_estres[index].tiene_estres==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( estres.tiene_estres[index].cantidad)/cantidad_estres)*100)];
    datos_estres=datos_estres.concat(datos2);
    label_estres=label_estres.concat(label2);
  }
  
}
var options = {
  series: datos_estres,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Sentimientos de estrés, '+estres.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_estres,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
var piechart7 = new ApexCharts(document.querySelector("#pie-chart7"), options);
piechart7.render();

//--------------------------------------------------------------
function dominioNueveEstres(){
  var label_estres=['si','no'];
var datos_estres=[0,0];
var cantidad_estres=0;

for (let index = 0; index < estres.tiene_estres.length; index++) {
  cantidad_estres+= estres.tiene_estres[index].cantidad;
 
  
}

for (let index = 0; index < estres.tiene_estres.length; index++) {
  if(index==0){
      if(estres.tiene_estres[index].tiene_estres==1){
        label_estres= ["Si"];
      }else{
        label_estres= ["No"];
      }
   
      datos_estres =[Math.round(((estres.tiene_estres[index].cantidad)/cantidad_estres)*100)];
  }else{
    if(estres.tiene_estres[index].tiene_estres==1){
        label2= ["Si"];
      }else{
        label2= ["No"];
      }
    let datos2 =[Math.round((( estres.tiene_estres[index].cantidad)/cantidad_estres)*100)];
    datos_estres=datos_estres.concat(datos2);
    label_estres=label_estres.concat(label2);
  }
  
}
piechart7.destroy();
var options = {
  series: datos_estres,
  chart: {
  width: '100%',
  type: 'pie',
},
plotOptions: {
  pie: {
    dataLabels: {
      offset: -5
    }
  }
},
title: {
  text: 'Sentimientos de estrés, '+estres.colegio,
  floating: true,
  align: 'center',
  style: {
    color: '#444'
  }
},
labels: label_estres,
responsive: [{
  breakpoint: 280,
  options: {
    chart: {
      width: 100
    },
    legend: {
      show: false
    }
  }
}]
};
 piechart7 = new ApexCharts(document.querySelector("#pie-chart7"), options);
piechart7.render();
}
//----------------------dominio 10.-------------------------------------
