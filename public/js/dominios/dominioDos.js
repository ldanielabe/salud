var label_raciones_comida=["desayuno: "];
var datos_raciones_comida=[0];
for (let index = 0; index < desayuno.length; index++) {
  if(index==0){
    label_raciones_comida = ["desayuno: "+desayuno[index].desayuno];
    datos_raciones_comida =[desayuno[index].cantidad];
  }else{
    let label2 = ["desayuno: "+desayuno[index].desayuno];
    let datos2 =[desayuno[index].cantidad];
    datos_raciones_comida=datos_raciones_comida.concat(datos2);
    label_raciones_comida=label_raciones_comida.concat(label2);
  }
  
}
for (let index = 0; index < media_manana.length; index++) {
  
      let label2 = ["media mañana: "+media_manana[index].media_manana];
      let datos2 =[media_manana[index].cantidad];
      datos_raciones_comida=datos_raciones_comida.concat(datos2);
      label_raciones_comida=label_raciones_comida.concat(label2);
    
    
  }
  for (let index = 0; index < almuerzo.length; index++) {
  
    let label2 = ["almuerzo: "+almuerzo[index].almuerzo];
    let datos2 =[almuerzo[index].cantidad];
    datos_raciones_comida=datos_raciones_comida.concat(datos2);
    label_raciones_comida=label_raciones_comida.concat(label2);
  
  
}
for (let index = 0; index < onces.length; index++) {
  
    let label2 = ["onces: "+onces[index].onces];
    let datos2 =[onces[index].cantidad];
    datos_raciones_comida=datos_raciones_comida.concat(datos2);
    label_raciones_comida=label_raciones_comida.concat(label2);
  
  
}
for (let index = 0; index < cena.length; index++) {
  
    let label2 = ["cena: "+cena[index].cena];
    let datos2 =[cena[index].cantidad];
    datos_raciones_comida=datos_raciones_comida.concat(datos2);
    label_raciones_comida=label_raciones_comida.concat(label2);
  
  
}
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_raciones_comida
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_raciones_comida,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Frecuencia de raciones de comidas '+ raciones_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart4 = new ApexCharts(document.querySelector("#bar-chart4"), options);
mychart4.render();  



//--------------------------------------------------------------------
var label_estado=["Adecuado"];
var datos_estado=[0];
for (let index = 0; index < estado_nutricional.length; index++) {
  if(index==0){
    label_estado = [estado_nutricional[index].estado_nutricional];
    datos_estado =[estado_nutricional[index].cantidad];
  }else{
    let label2 = [estado_nutricional[index].estado_nutricional];
    let datos2 =[estado_nutricional[index].cantidad];
    datos_estado=datos_estado.concat(datos2);
    label_estado=label_estado.concat(label2);
  }
  
}



var options = {
  series: [{
    name: 'Cantidad',
  data: datos_estado
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_estado,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Problemas crecimiento, '+ estado_nutricional_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart5 = new ApexCharts(document.querySelector("#bar-chart5"), options);
mychart5.render();
//--------------------------------------------------------------

var label_anemia=["Tiene anemia"];
var datos_anemia=[0];
for (let index = 0; index < anemia.length; index++) {
  if(index==0){
    label_anemia = [anemia[index].anemia];
    datos_anemia =[anemia[index].cantidad];
  }else{
    let label2 = [anemia[index].anemia];
    let datos2 =[anemia[index].cantidad];
    datos_anemia=datos_anemia.concat(datos2);
    label_anemia=label_anemia.concat(label2);
  }
  
}


var options = {
  series: [{
    name: 'Cantidad',
  data: datos_anemia
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_anemia,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Frecuencia Anemia '+ anemia_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart5 = new ApexCharts(document.querySelector("#bar-chart6"), options);
mychart5.render();


//-------------------------------------------------------------------
var label_salud_oral=["gingivitis"];
var datos_salud_oral=[0];
for (let index = 0; index < gingivitis.length; index++) {
  if(index==0){
    label_salud_oral = ['gingivitis:'+gingivitis[index].gingivitis];
    datos_salud_oral =[gingivitis[index].cantidad];
  }else{
    let label2 = ['gingivitis:'+gingivitis[index].gingivitis];
    let datos2 =[gingivitis[index].cantidad];
    datos_salud_oral=datos_salud_oral.concat(datos2);
    label_salud_oral=label_salud_oral.concat(label2);
  }
  
}
for (let index = 0; index < caries.length; index++) {
  
    let label2 = ['caries:'+caries[index].caries];
    let datos2 =[caries[index].cantidad];
    datos_salud_oral=datos_salud_oral.concat(datos2);
    label_salud_oral=label_salud_oral.concat(label2);
  
  
}
for (let index = 0; index < dolor.length; index++) {
  
  let label2 = ['dolor:'+dolor[index].dolor];
  let datos2 =[dolor[index].cantidad];
  datos_salud_oral=datos_salud_oral.concat(datos2);
  label_salud_oral=label_salud_oral.concat(label2);


}

var options = {
  series: [{
    name: 'Cantidad',
  data: datos_salud_oral
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_salud_oral,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Salud oral en los escolares '+ salud_oral_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart6 = new ApexCharts(document.querySelector("#bar-chart7"), options);
mychart6.render();



//----------------------------------------------------------------

var label_cepillado=[""];
var datos_cepillado=[0];
for (let index = 0; index < cepillado.length; index++) {
  if(index==0){
    label_cepillado = [cepillado[index].frecuencia_cepillado+' vez'];
    datos_cepillado =[cepillado[index].cantidad];
  }else{
    let label2 = [cepillado[index].frecuencia_cepillado+' veces'];
    let datos2 =[cepillado[index].cantidad];
    datos_cepillado=datos_cepillado.concat(datos2);
    label_cepillado=label_cepillado.concat(label2);
  }
  
}

var options = {
  series: [{
    name: 'Cantidad',
  data: datos_cepillado
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_cepillado,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Frecuencia del cepillado al dia en los escolares '+ cepillado_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


var mychart8 = new ApexCharts(document.querySelector("#bar-chart8"), options);
mychart8.render();

//-------------------------------------------------------------------

function dominioDosCepillado(){
  var label_cepillado=[""];
  var datos_cepillado=[0];
  for (let index = 0; index < cepillado.length; index++) {
    if(index==0){
      label_cepillado = [cepillado[index].frecuencia_cepillado+' vez'];
      datos_cepillado =[cepillado[index].cantidad];
    }else{
      let label2 = [cepillado[index].frecuencia_cepillado+' veces'];
      let datos2 =[cepillado[index].cantidad];
      datos_cepillado=datos_cepillado.concat(datos2);
      label_cepillado=label_cepillado.concat(label2);
    }
    
  }
  
  mychart8.destroy();
  var options = {
    series: [{
      name: 'Cantidad',
    data: datos_cepillado
  }],
    chart: {
    height: 350,
    type: 'bar',
    events: {
      click: function(chart, w, e) {
        // console.log(chart, w, e)
      }
    }
  },
  colors: colors,
  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true
    }
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return val ;
    },
  },
  legend: {
    show: false
  },
  xaxis: {
    categories: label_cepillado,
    position: 'bottom',
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      fill: {
        type: 'gradient',
        gradient: {
          colorFrom: '#D8E3F0',
          colorTo: '#BED1E6',
          stops: [0, 100],
          opacityFrom: 0.4,
          opacityTo: 0.5,
        }
      }
    },
    tooltip: {
      enabled: true,
    }
  },
  
  title: {
    text: 'Frecuencia del cepillado al dia en los escolares '+ cepillado_colegio,
    floating: true,
   
    align: 'center',
    style: {
      color: '#444'
    }
  }
  };
  
  
  mychart8 = new ApexCharts(document.querySelector("#bar-chart8"), options);
  mychart8.render();





}



//-------------------------------------------------------------------
function dominioDosSaludOral(){

   label_salud_oral=["gingivitis"];
   datos_salud_oral=[0];
  for (let index = 0; index < gingivitis.length; index++) {
    if(index==0){
      label_salud_oral = ['gingivitis:'+gingivitis[index].gingivitis];
      datos_salud_oral =[gingivitis[index].cantidad];
    }else{
      let label2 = ['gingivitis:'+gingivitis[index].gingivitis];
      let datos2 =[gingivitis[index].cantidad];
      datos_salud_oral=datos_salud_oral.concat(datos2);
      label_salud_oral=label_salud_oral.concat(label2);
    }
    
  }
  for (let index = 0; index < caries.length; index++) {
    
      let label2 = ['caries:'+caries[index].caries];
      let datos2 =[caries[index].cantidad];
      datos_salud_oral=datos_salud_oral.concat(datos2);
      label_salud_oral=label_salud_oral.concat(label2);
    
    
  }
  for (let index = 0; index < dolor.length; index++) {
    
    let label2 = ['dolor:'+dolor[index].dolor];
    let datos2 =[dolor[index].cantidad];
    datos_salud_oral=datos_salud_oral.concat(datos2);
    label_salud_oral=label_salud_oral.concat(label2);
  
  
  }
  
  mychart6.destroy();
  
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_salud_oral
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_salud_oral,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'Salud oral en los escolares '+ salud_oral_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


 mychart6 = new ApexCharts(document.querySelector("#bar-chart7"), options);
mychart6.render();


}

//-----------------------------------------------------------------pag93
function dominioDosAnemia(){

   label_anemia=["Tiene anemia"];
   datos_anemia=[0];
  for (let index = 0; index < anemia.length; index++) {
    if(index==0){
      label_anemia = [anemia[index].anemia];
      datos_anemia =[anemia[index].cantidad];
    }else{
      let label2 = [anemia[index].anemia];
      let datos2 =[anemia[index].cantidad];
      datos_anemia=datos_anemia.concat(datos2);
      label_anemia=label_anemia.concat(label2);
    }
    
  }
  
  mychart5.destroy();
  
var options = {
  series: [{
    name: 'Cantidad',
  data: datos_anemia
}],
  chart: {
  height: 350,
  type: 'bar',
  events: {
    click: function(chart, w, e) {
      // console.log(chart, w, e)
    }
  }
},
colors: colors,
plotOptions: {
  bar: {
    columnWidth: '45%',
    distributed: true
  }
},
dataLabels: {
  enabled: true,
  formatter: function (val) {
    return val ;
  },
},
legend: {
  show: false
},
xaxis: {
  categories: label_anemia,
  position: 'bottom',
  axisBorder: {
    show: false
  },
  axisTicks: {
    show: false
  },
  crosshairs: {
    fill: {
      type: 'gradient',
      gradient: {
        colorFrom: '#D8E3F0',
        colorTo: '#BED1E6',
        stops: [0, 100],
        opacityFrom: 0.4,
        opacityTo: 0.5,
      }
    }
  },
  tooltip: {
    enabled: true,
  }
},

title: {
  text: 'F Anemia '+ anemia_colegio,
  floating: true,
 
  align: 'center',
  style: {
    color: '#444'
  }
}
};


 mychart5 = new ApexCharts(document.querySelector("#bar-chart6"), options);
mychart5.render();

}

//--------------------------------------------------------------
function dominioDosRaciones(){
   label_raciones_comida=["desayuno: "];
   datos_raciones_comida=[0];
  for (let index = 0; index < desayuno.length; index++) {
    if(index==0){
      label_raciones_comida = ["desayuno: "+desayuno[index].desayuno];
      datos_raciones_comida =[desayuno[index].cantidad];
    }else{
      let label2 = ["desayuno: "+desayuno[index].desayuno];
      let datos2 =[desayuno[index].cantidad];
      datos_raciones_comida=datos_raciones_comida.concat(datos2);
      label_raciones_comida=label_raciones_comida.concat(label2);
    }
    
  }
  for (let index = 0; index < media_manana.length; index++) {
    
        let label2 = ["media mañana: "+media_manana[index].media_manana];
        let datos2 =[media_manana[index].cantidad];
        datos_raciones_comida=datos_raciones_comida.concat(datos2);
        label_raciones_comida=label_raciones_comida.concat(label2);
      
      
    }
    for (let index = 0; index < almuerzo.length; index++) {
    
      let label2 = ["almuerzo: "+almuerzo[index].almuerzo];
      let datos2 =[almuerzo[index].cantidad];
      datos_raciones_comida=datos_raciones_comida.concat(datos2);
      label_raciones_comida=label_raciones_comida.concat(label2);
    
    
  }
  for (let index = 0; index < onces.length; index++) {
    
      let label2 = ["onces: "+onces[index].onces];
      let datos2 =[onces[index].cantidad];
      datos_raciones_comida=datos_raciones_comida.concat(datos2);
      label_raciones_comida=label_raciones_comida.concat(label2);
    
    
  }
  for (let index = 0; index < cena.length; index++) {
    
      let label2 = ["cena: "+cena[index].cena];
      let datos2 =[cena[index].cantidad];
      datos_raciones_comida=datos_raciones_comida.concat(datos2);
      label_raciones_comida=label_raciones_comida.concat(label2);
    
    
  }
  mychart4.destroy();
  var options = {
    series: [{
      name: 'Cantidad',
    data: datos_raciones_comida
  }],
    chart: {
    height: 350,
    type: 'bar',
    events: {
      click: function(chart, w, e) {
        // console.log(chart, w, e)
      }
    }
  },
  colors: colors,
  plotOptions: {
    bar: {
      columnWidth: '45%',
      distributed: true
    }
  },
  dataLabels: {
    enabled: true,
    formatter: function (val) {
      return val ;
    },
  },
  legend: {
    show: false
  },
  xaxis: {
    categories: label_raciones_comida,
    position: 'bottom',
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    crosshairs: {
      fill: {
        type: 'gradient',
        gradient: {
          colorFrom: '#D8E3F0',
          colorTo: '#BED1E6',
          stops: [0, 100],
          opacityFrom: 0.4,
          opacityTo: 0.5,
        }
      }
    },
    tooltip: {
      enabled: true,
    }
  },
  
  title: {
    text: 'Frecuencia de raciones de comidas '+ raciones_colegio,
    floating: true,
   
    align: 'center',
    style: {
      color: '#444'
    }
  }
  };
  
  
 mychart4 = new ApexCharts(document.querySelector("#bar-chart4"), options);
  mychart4.render();  
  




}


