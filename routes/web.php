<?php

Route::get('/ini', 'GestionPromocion\inicioController@inicio')->name('ini');
Route::get('/', function () {
    return redirect()->route('ini');
});
/*EDITAR PREGUNTAS Y RESPUESTAS FICHA SALUD - SUPER ADMIN*/

Route::group(['middleware' => 'superadmin'], function () {
    Route::get('/admin', 'GestionPromocion\inicioController@inicioAdmin')->name('admin')->middleware('auth');
    Route::get('editarFicha', 'GestionAtencion\FichaSaludController@index')->name('editarFicha')->middleware('auth');
    Route::get('/editarFicha/all/{dominio}/{prg}', 'GestionAtencion\FichaSaludController@all')->middleware('auth');
    Route::get('/editarFicha/{id}/{preguntas}/{tipo_input}/{respuestas}/{riesgo}/{id_pregunta}', 'GestionAtencion\FichaSaludController@guardar')->middleware('auth');
    Route::get('/editarPreguntas/{id}/{preguntas}', 'GestionAtencion\FichaSaludController@editarPreguntas')->middleware('auth');
    Route::get('/editarRespuestas/{respuestas}/{riesgo}/{id_rta}', 'GestionAtencion\FichaSaludController@editarRespuestas')->middleware('auth');
    Route::get('/agregarDominio', 'GestionAtencion\FichaSaludController@datagrid_rel')->name('agregarDominio')->middleware('auth');
    Route::get('/agregarDominio', 'GestionAtencion\FichaSaludController@datagrid')->name('agregarDominio')->middleware('auth');
    Route::post('/agregarDominio', 'GestionAtencion\FichaSaludController@datagrid');
    //slide
Route::get('/slide', 'GestionPromocion\inicioController@slide')->name('slide');

//noticias
Route::get('/noticias', 'GestionPromocion\inicioController@noticias')->name('noticias');

});
Route::get('/news', 'GestionPromocion\inicioController@noticiasPaginadas')->name('news');

Route::group(['middleware' => 'evaluador'], function () {
/*FORMULARIO FICHA SALUD - EVALUADOR*/
Route::get('/ficha_salud/{id}', 'EditarFichaController@editar');
Route::get('/nueva_ficha/{id}', 'EditarFichaController@crear')->name('nueva_ficha');
Route::get('/historialPDF/{id}', 'EditarFichaController@historialPDF');
Route::get('/fichaExport/{id}', 'EditarFichaController@export')->name('fichaExport'); 
Route::get('/crearFicha/{id}', 'EditarFichaController@crearFicha'); 
Route::get('/guardarObsGlobal/{value}/{id}', 'EditarFichaController@guardarObsGlobal'); 
Route::get('/updateCrecimiento/{id}/{altura}/{peso}/{crecimiento}', 'EditarFichaController@updateCrecimiento');
Route::get('/guardarObsDominio/{id_ficha}/{id_dominio}/{valor}/{valorizacion}', 'EditarFichaController@guardarObsDominio');
Route::get('/recordatorio/{id_colegio}', 'EditarFichaController@recordatorio');

Route::get('/updateObservaciones/{id}', 'EditarFichaController@updateObservaciones'); 

Route::get('/reportes/{id_colegio}', 'EditarFichaController@reportes'); 
Route::get('/reporteAjax/{activo}/{id_pregunta}/{id_colegio}', 'EditarFichaController@reporteAjax'); 
Route::get('/reporteAjaxDominio/{id_dominio}', 'EditarFichaController@reporteAjaxDominio'); 
Route::get('/generar_pdf_ficha_salud/{id}', 'EditarFichaController@generar_pdf_ficha_salud'); 
Route::get('/buscar_pdf_ficha_salud/{id}/{id_ficha}', 'EditarFichaController@buscar_pdf_ficha_salud'); 
});
Route::get('/recordatorioObservador/{id_colegio}', 'EditarFichaController@recordatorioObservador')->middleware('auth');
Route::get('/updateObservador/{id}', 'EditarFichaController@updateObservador');
Route::get('/guardarFicha/{id_s}/{id_p}/{d}', 'EditarFichaController@guardarFicha');

Route::get('/home', function () {
    Auth::guard('')->logout();
    return view('auth.login');
});
Auth::routes();
Route::get('/prueba', function () {
    return view('prueba');
});


Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'ResetPasswordController@reset');

Route::get('/administrador/dashboard', 'GestionPromocion\inicioController@inicioAdministrador')->name('/administrador/dashboard')->middleware('auth');
   
    Route::get('/deleteMensaje/{id}', 'GestionPromocion\inicioController@deleteMensaje')->middleware('auth');
    Route::get('/perfil', 'GestionUsuarios\PerfilController@perfilAdmin')->name('perfil')->middleware('auth');
    Route::post('/act_perfil', 'GestionUsuarios\PerfilController@actualizarPerfil')->name('act_perfil');
    Route::get('/acudiente/perfil', 'GestionUsuarios\PerfilController@perfilAcudiente')->name('acudiente/perfil')->middleware('auth');
    
//Prevencion

Route::get('agregarDocumentos', 'GestionPromocion\PromocionController@datagrid_rel')->middleware('auth');
Route::get('agregarDocumentos', 'GestionPromocion\PromocionController@datagrid')->middleware('auth');
Route::post('agregarDocumentos', 'GestionPromocion\PromocionController@datagrid');

Route::get('/promocion', 'GestionPromocion\PromocionController@documentos')->name('promocion');
Route::get('/documentos_interes', 'GestionPromocion\PromocionController@documentos_inicio')->name('documentos_interes');
Route::get('/noticiasInfo/{id}', 'GestionPromocion\inicioController@noticiasInfo')->name('noticiasInfo');

//-----------

Route::group(['middleware' => 'rector'], function () {
    //contactenos
    Route::get('/contactenos_rector', 'GestionPromocion\inicioController@contactenos_rector')->middleware('auth');

    /*Evento*/
    Route::post('/reg_evento', 'GestionPromocion\inicioController@registrarEventos')->name('reg_evento')->middleware('auth');
    Route::get('/eventos', 'GestionPromocion\inicioController@mostrarEventos')->name('eventos')->middleware('auth');
    Route::get('/deleteEvento/{id}', 'GestionPromocion\inicioController@deleteEvento')->middleware('auth');
    Route::post('/updateEventos/{id}', 'GestionPromocion\inicioController@updateEventos')->name('updateEventos')->middleware('auth');

    /*Directorio*/
    Route::get('/directorio', 'GestionPromocion\inicioController@mostrarDirectorio')->name('directorio')->middleware('auth');
    Route::post('/registrarDirectorio', 'GestionPromocion\inicioController@registrarDirectorio')->name('registrarDirectorio')->middleware('auth');
    Route::get('/eliminarDirectorio/{id}', 'GestionPromocion\inicioController@eliminarDirectorio')->middleware('auth');
    Route::post('/actualizarDirectorio/{id}', 'GestionPromocion\inicioController@actualizarDirectorio')->name('actualizarDirectorio')->middleware('auth');

    //slide
    Route::post('/aggSlide', 'GestionPromocion\inicioController@registrarSlide')->name('aggSlide')->middleware('auth');
    Route::get('/deleteSlide/{id}', 'GestionPromocion\inicioController@deleteSlide')->middleware('auth');

    //noticias
    Route::post('/reg_noticia', 'GestionPromocion\inicioController@registrarNoticia')->name('reg_noticia')->middleware('auth');
    Route::get('/deleteNoticia/{id}', 'GestionPromocion\inicioController@deleteNoticia')->middleware('auth');
    Route::post('/updateNoticias/{id}', 'GestionPromocion\inicioController@updateNoticia')->name('updateNoticias')->middleware('auth');

   
   

});

//contactenos
Route::post('/contactenos', 'GestionPromocion\inicioController@contactenos')->name('contactenos');
 /*Evento*/
 Route::get('/carrusel', 'GestionPromocion\inicioController@carrusel_eventos');
/*Directorio*/
Route::get('/carruselDirectorio', 'GestionPromocion\inicioController@carrusel_directorio');

Route::get('/carrusel_documentos/{id}', 'GestionPromocion\inicioController@carrusel_documentos');

Route::get('/eventoInfo/{id}', 'GestionPromocion\inicioController@eventoInfo');


//evaluador
Route::get('/antecedentesVista/{id}', 'GestionPrevencion\prevencionController@inicioEvaluador')->name('antecedentes')->middleware('auth');
Route::get('/reporteAntecedentesGrado/{inicio}/{fin}/{grado}/{id}', 'GestionPrevencion\prevencionController@reporteAntecedentesGrado')->middleware('auth');
Route::get('/antecedentesRango/{inicio}/{fin}/{id}', 'GestionPrevencion\prevencionController@antecedentesRango')->middleware('auth');
Route::get('/observador/{id}', 'GestionPrevencion\prevencionController@observador')->middleware('auth');
Route::get('/observadorAjax/{id}/{inicio}/{fin}/{grado}/{estado}', 'GestionPrevencion\prevencionController@observadorAjax')->middleware('auth');

Route::get('/observadorReporte/{id}', 'GestionPrevencion\prevencionController@observadorReporte')->middleware('auth');
Route::get('/observadorReporteAjax/{id}/{inicio}/{fin}/{grado}/{estado}/{falta}', 'GestionPrevencion\prevencionController@observadorReporteAjax')->middleware('auth');


Route::get('/perfilEvaluador', function () {
    
    return view('evaluador.perfil');
});
Route::get('/estadisticas/{id}', 'GestionPrevencion\prevencionController@graficas');
Route::post('/generar_pdf_graficas', 'GestionPrevencion\prevencionController@generar_pdf_graficas');
Route::get('/estadisticasAjax/{id_dominio}/{grado}/{id_colegio}', 'GestionPrevencion\prevencionController@estadisticasAjax');
Route::get('/estadisticasAjaxRango/{id_dominio}/{grado}/{id_colegio}/{inicio}/{fin}', 'GestionPrevencion\prevencionController@estadisticasAjaxRango');



//Rector
Route::group(['middleware' => 'rector'], function () {
Route::get('/rector/dashboard', 'GestionUsuarios\PerfilController@rectorInicio')->name('/rector/dashboard');
});
//Profesor
Route::group(['middleware' => 'profesor'], function () {
Route::get('/profesor/dashboard', 'GestionUsuarios\PerfilController@profesorInicio')->name('/profesor/dashboard');
});
//chat-------------------

Route::get('/historialMensaje/{id}', 'GestionPromocion\inicioController@historialMensaje')->name('historialMensaje');
Route::get('/enviarMensaje/{id}/{mensaje}', 'GestionPromocion\inicioController@enviarMensaje')->name('enviarMensaje');

Route::group(['middleware' => 'entidad'], function () {
Route::get('/autoridad/dashboard', 'GestionPromocion\inicioController@inicioAutoridad')->name('/autoridad/dashboard');
});


//--------------------------
Route::group(['middleware' => 'evaluador'], function () {
Route::get('administrador/{id}', 'AdministradorController@datagrid_rel');
Route::get('administrador/{id}', 'AdministradorController@datagrid');
Route::post('administrador/{id}', 'AdministradorController@datagrid');

Route::get('instituciones', 'AdministradorController@instituciones_datagrid_rel');
Route::get('instituciones', 'AdministradorController@instituciones_datagrid');
Route::post('instituciones', 'AdministradorController@instituciones_datagrid');

Route::get('estudiante/{id}', 'EstudianteController@datagrid_rel');
Route::get('estudiante/{id}', 'EstudianteController@datagrid');
Route::post('estudiante/{id}', 'EstudianteController@datagrid');

Route::get('profesores/{id}', 'ProfesoresController@datagrid_rel');
Route::get('profesores/{id}', 'ProfesoresController@datagrid');
Route::post('profesores/{id}', 'ProfesoresController@datagrid');

Route::get('rector/{id}', 'RectorController@datagrid_rel');
Route::get('rector/{id}', 'RectorController@datagrid');
Route::post('rector/{id}', 'RectorController@datagrid');

Route::get('acudiente/{id}', 'AcudienteController@datagrid_rel');
Route::get('acudiente/{id}', 'AcudienteController@datagrid');
Route::post('acudiente/{id}', 'AcudienteController@datagrid');

Route::get('autoridad', 'AutoridadController@datagrid_rel');
Route::get('autoridad', 'AutoridadController@datagrid');
Route::post('autoridad', 'AutoridadController@datagrid');

Route::get('evaluador/{id}', 'EvaluadorController@datagrid_rel');
Route::get('evaluador/{id}', 'EvaluadorController@datagrid');
Route::post('evaluador/{id}', 'EvaluadorController@datagrid');


Route::get('antecedentes/{id}', 'FichaSaludController@datagrid_rel');
Route::get('antecedentes/{id}', 'FichaSaludController@datagrid');
Route::post('antecedentes/{id}', 'FichaSaludController@datagrid');

Route::get('observacion/{id}', 'ObservacionesController@datagrid_rel');
Route::get('observacion/{id}', 'ObservacionesController@datagrid');
Route::post('observacion/{id}', 'ObservacionesController@datagrid');

Route::get('antecedentesTipo', 'AntecedentesTipoController@datagrid_rel');
Route::get('antecedentesTipo', 'AntecedentesTipoController@datagrid');
Route::post('antecedentesTipo', 'AntecedentesTipoController@datagrid');

Route::get('/subir', 'EstudianteController@subir');
Route::post('/subir', 'EstudianteController@subir')->name('subir');

});
/*F*O*R*M*U*L*A*R*I*O*S********************************************************************* */
Route::get('practicas_cuidado/{id}', 'FormulariosFichaController@datagrid_rel_practicas_cuidado');
Route::get('practicas_cuidado/{id}', 'FormulariosFichaController@datagrid_practicas_cuidado');
Route::post('practicas_cuidado/{id}', 'FormulariosFichaController@datagrid_practicas_cuidado');


Route::get('estado_higiene/{id}', 'FormulariosFichaController@datagrid_rel_estado_higiene');
Route::get('estado_higiene/{id}', 'FormulariosFichaController@datagrid_estado_higiene');
Route::post('estado_higiene/{id}', 'FormulariosFichaController@datagrid_estado_higiene');

Route::get('cuidadores/{id}', 'FormulariosFichaController@datagrid_rel_cuidadores');
Route::get('cuidadores/{id}', 'FormulariosFichaController@datagrid_cuidadores');
Route::post('cuidadores/{id}', 'FormulariosFichaController@datagrid_cuidadores');


Route::get('crecimiento/{id}', 'FormulariosFichaController@datagrid_rel');
Route::get('crecimiento/{id}', 'FormulariosFichaController@datagrid');
Route::post('crecimiento/{id}', 'FormulariosFichaController@datagrid');


Route::get('presencia_alcoholicos_fumadores/{id}', 'FormulariosFichaController@datagrid_rel_presencia_alcoholicos_fumadores');
Route::get('presencia_alcoholicos_fumadores/{id}', 'FormulariosFichaController@datagrid_presencia_alcoholicos_fumadores');
Route::post('presencia_alcoholicos_fumadores/{id}', 'FormulariosFichaController@datagrid_presencia_alcoholicos_fumadores');

Route::get('/raciones_comidas/{id}', 'FormulariosFichaController@datagrid_rel_raciones_comidas');
Route::get('/raciones_comidas/{id}', 'FormulariosFichaController@datagrid_raciones_comidas');
Route::post('/raciones_comidas/{id}', 'FormulariosFichaController@datagrid_raciones_comidas');

Route::get('/anemia/{id}', 'FormulariosFichaController@datagrid_rel_anemia');
Route::get('/anemia/{id}', 'FormulariosFichaController@datagrid_anemia');
Route::post('/anemia/{id}', 'FormulariosFichaController@datagrid_anemia');

Route::get('/salud_oral/{id}', 'FormulariosFichaController@datagrid_rel_salud_oral');
Route::get('/salud_oral/{id}', 'FormulariosFichaController@datagrid_salud_oral');
Route::post('/salud_oral/{id}', 'FormulariosFichaController@datagrid_salud_oral');

Route::get('alt_gastrointestinales_urinarias/{id}', 'FormulariosFichaController@datagrid_rel_alt_gastrointestinales_urinarias');
Route::get('alt_gastrointestinales_urinarias/{id}', 'FormulariosFichaController@datagrid_alt_gastrointestinales_urinarias');
Route::post('alt_gastrointestinales_urinarias/{id}', 'FormulariosFichaController@datagrid_alt_gastrointestinales_urinarias');


Route::get('actividad_reposo/{id}', 'FormulariosFichaController@datagrid_rel_actividad_reposo');
Route::get('actividad_reposo/{id}', 'FormulariosFichaController@datagrid_actividad_reposo');
Route::post('actividad_reposo/{id}', 'FormulariosFichaController@datagrid_actividad_reposo');

Route::get('alteraciones_escolares/{id}', 'FormulariosFichaController@datagrid_rel_alteraciones_escolares');
Route::get('alteraciones_escolares/{id}', 'FormulariosFichaController@datagrid_alteraciones_escolares');
Route::post('alteraciones_escolares/{id}', 'FormulariosFichaController@datagrid_alteraciones_escolares');


Route::get('autopercepcion/{id}', 'FormulariosFichaController@datagrid_rel_autopercepcion');
Route::get('autopercepcion/{id}', 'FormulariosFichaController@datagrid_autopercepcion');
Route::post('autopercepcion/{id}', 'FormulariosFichaController@datagrid_autopercepcion');


Route::get('rol_relaciones/{id}', 'FormulariosFichaController@datagrid_rel_rol_relaciones');
Route::get('rol_relaciones/{id}', 'FormulariosFichaController@datagrid_rol_relaciones');
Route::post('rol_relaciones/{id}', 'FormulariosFichaController@datagrid_rol_relaciones');

Route::get('sexualidad/{id}', 'FormulariosFichaController@datagrid_rel_sexualidad');
Route::get('sexualidad/{id}', 'FormulariosFichaController@datagrid_sexualidad');
Route::post('sexualidad/{id}', 'FormulariosFichaController@datagrid_sexualidad');


Route::get('estres/{id}', 'FormulariosFichaController@datagrid_rel_estres');
Route::get('estres/{id}', 'FormulariosFichaController@datagrid_estres');
Route::post('estres/{id}', 'FormulariosFichaController@datagrid_estres');

Route::get('religion/{id}', 'FormulariosFichaController@datagrid_rel_religion');
Route::get('religion/{id}', 'FormulariosFichaController@datagrid_religion');
Route::post('religion/{id}', 'FormulariosFichaController@datagrid_religion');

Route::get('carnet_vacunacion/{id}', 'FormulariosFichaController@datagrid_rel_carnet');
Route::get('carnet_vacunacion/{id}', 'FormulariosFichaController@datagrid_carnet');
Route::post('carnet_vacunacion/{id}', 'FormulariosFichaController@datagrid_carnet');



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
