-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-10-2020 a las 15:56:16
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `salud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedentes`
--

CREATE TABLE `antecedentes` (
  `id` int(11) NOT NULL,
  `antecedente` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `antecedentes`
--

INSERT INTO `antecedentes` (`id`, `antecedente`, `descripcion`) VALUES
(1, 'Patológicos', 'patologicos'),
(2, 'Quirúrgicos', 'quirurgicos'),
(3, 'Traumáticos', 'traumáticos'),
(4, 'Alergicos', 'alergicos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedentes_tipos`
--

CREATE TABLE `antecedentes_tipos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `id_antecedentes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `antecedentes_tipos`
--

INSERT INTO `antecedentes_tipos` (`id`, `tipo`, `id_antecedentes`) VALUES
(1, 'Amigdalitis', 1),
(2, 'Gastritis', 1),
(3, 'Arrimias Cardiacas', 1),
(4, 'Asma', 1),
(5, 'Calculos en los Riñones', 1),
(6, 'Colon Irritable', 1),
(7, 'Conjuntivitis', 1),
(8, 'Convulsiones', 1),
(9, 'Craneosinostosis', 1),
(10, 'Dengue', 1),
(11, 'Chicungunya', 1),
(12, 'Cataratas', 2),
(13, 'CX MS', 2),
(14, 'CX Corazon', 2),
(15, 'Cx Craneo', 2),
(16, 'Hernia Umbelical', 2),
(17, 'Objeto Extraño', 2),
(18, 'Oido sin Especificar', 2),
(19, 'Amigdalectomia', 2),
(20, 'Reconstruccion de Timpano', 2),
(21, 'Cx Clavicula', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactenos`
--

CREATE TABLE `contactenos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `institucion` varchar(200) NOT NULL,
  `mensaje` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contactenos`
--

INSERT INTO `contactenos` (`id`, `nombre`, `correo`, `institucion`, `mensaje`, `updated_at`, `created_at`) VALUES
(1, 'SAMI', 'samiarevalo85@gmail.com', 'Argelino Garzon', 'asasassas', '2020-05-09 06:47:54', '2020-05-09 06:47:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directorios`
--

CREATE TABLE `directorios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `directorios`
--

INSERT INTO `directorios` (`id`, `nombre`, `descripcion`, `imagen`, `id_user`, `updated_at`, `created_at`) VALUES
(1, 'UFPS', 'Institución Pública de Educación Superior, orientada al mejoramiento continuo y la calidad en los procesos de docencia, investigación y extensión', 'logoufps.png', 54, '2020-03-19 04:02:41', '2020-03-19 04:02:41'),
(2, 'UFPS', 'Institución Pública de Educación Superior, orientada al mejoramiento continuo y la calidad en los procesos de docencia, investigación y extensión', 'logoufps.png', 54, '2020-03-19 04:03:48', '2020-03-19 04:03:48'),
(3, 'UFPS', 'Institución Pública de Educación Superior, orientada al mejoramiento continuo y la calidad en los procesos de docencia, investigación y extensión', 'logoufps.png', 54, '2020-03-19 04:04:45', '2020-03-19 04:04:45'),
(4, 'UFPS', 'Institución Pública de Educación Superior, orientada al mejoramiento continuo y la calidad en los procesos de docencia, investigación y extensión', 'logoufps.png', 54, '2020-03-19 04:04:51', '2020-03-19 04:04:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dominios`
--

CREATE TABLE `dominios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `estado` varchar(50) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dominios`
--

INSERT INTO `dominios` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Promoción de la salud', NULL, 'Activo'),
(2, 'Nutrición', NULL, 'Activo'),
(3, 'Eliminación e intercambio', NULL, 'Activo'),
(4, 'Actividad reposo\r\n\r\n', NULL, 'Activo'),
(5, 'Percepción Cognición', NULL, 'Activo'),
(6, 'Autopercepción', NULL, 'Activo'),
(7, 'Rol Relaciones', NULL, 'Activo'),
(8, 'Sexualidad Reproducción', NULL, 'Activo'),
(9, 'Afrontamiento y tolerancia al estrés\r\n\r\n', NULL, 'Activo'),
(10, 'Principios Vitales', NULL, 'Activo'),
(11, 'Seguridad y protección', NULL, 'Activo'),
(12, 'Confort', NULL, 'Activo'),
(13, 'Crecimiento y desarrollo', NULL, 'Activo'),
(14, 'Seguridad', 'Se pregunta acerca de la seguridad del estudiante', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes`
--

CREATE TABLE `estudiantes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `documento` varchar(255) NOT NULL,
  `celular` varchar(255) NOT NULL,
  `fechaNacimiento` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `genero` varchar(15) NOT NULL,
  `grupo_sanguineo` varchar(255) NOT NULL,
  `peso` float DEFAULT NULL,
  `altura` float DEFAULT NULL,
  `estado_nutricional` varchar(255) NOT NULL,
  `barrio` varchar(255) NOT NULL,
  `grado` int(11) NOT NULL,
  `jornada` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `area_residencia` varchar(50) NOT NULL,
  `regimen_salud` varchar(50) NOT NULL,
  `id_institucion` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estudiantes`
--

INSERT INTO `estudiantes` (`id`, `nombre`, `apellidos`, `email`, `documento`, `celular`, `fechaNacimiento`, `direccion`, `genero`, `grupo_sanguineo`, `peso`, `altura`, `estado_nutricional`, `barrio`, `grado`, `jornada`, `imagen`, `password`, `area_residencia`, `regimen_salud`, `id_institucion`, `activo`) VALUES
(1, 'Emily Alejandra', 'Arias', 'emily@hotmail.com', '54894651', '310245451', '2010-04-07', 'Cll 28a no 16-89', 'Femenino', 'O+', 2, 4, 'Bajo Peso', 'Centro de Sardinata', 4, 'Mañana', 'IMG-20190821-WA0086.jpg', '$2y$10$Z.AyBijH0uBJvhTDFaEx1uKbN.aal2jsbLVSG1gUlX6WD4S2Ctkje', 'Urbano', 'Contributivo', 1, 1),
(2, 'Daniela', 'Espitia', 'lauridani1@gmail.com', '545454444', '3102546561', '2020-04-16', 'Cll 28a no 16-89', 'Femenino', 'O+', 2, 4, 'Adecuado', 'Centro de Sardinata', 11, 'Mañana', 'WhatsApp-Image-2020-04-01-at-12-05-32-PM-780e8.jpeg', '$2y$10$otiz7UExqHqeMFvK9qQ08OIA71wnnczJ/tbvS4pHYKkF2iYBxKgLS', 'Rural', 'Contributivo', 1, 1),
(3, 'Julian', 'Machado', 'juli@hotmail.com', '6548946522', '3104515478', '2020-04-01', 'Cll 28a no 16-89', 'Masculino', 'O-', 2, 4, 'Bajo Peso', 'Centro de Sardinata', 5, 'Mañana', 'holiiiii-0cd33.jpeg', '$2y$10$bTKOpyqXvy6IIeayfQtX/eYy/NNFaelun1LzWpqooFuCXQj.pTC/.', 'Urbano', 'Subsidiado ', 1, 1),
(4, 'Juan', 'Sardinata', 'estudiante@gmail.com', '1234567890', '3210456789', '2000-04-01', 'Cll 28a no 16-89', 'Masculino', 'O-', 2, 4, 'Bajo Peso', 'Centro de Sardinata', 1, 'Tarde', 'mujer-joven-estudiando-y-escribiendo-libros-cuadernos.jpg', 'G4QC4gGMUUegrVv', 'Urbano', 'Contributivo', 1, 1),
(5, 'Danna ', 'Zalamanca', 'dannaz@gmail.com', '1054896636', '3156995154', '2008-05-15', 'Cll 2a no 16-54', 'Femenino', 'O+', 50, 148, 'Adecuado', 'Centro de Sardinata', 7, 'Mañana', '', '$2y$10$x7llm/32rua3SC14laqUsu9Wpm69AOL4g/LaCtRIaSfNFzC1hHBMm', 'Rural', 'Contributivo', 2, 1),
(6, 'Nancy', 'Gongora', 'nancyg@gmail.com', '1985264488', '3169874556', '2008-02-11', 'Cll 10B no 16-89', 'Femenino', 'O-', 60, 152, 'Sobrepeso', 'Las Mercedes', 7, 'Mañana', '', '$2y$10$3ZlGGFYDu1Bi8zdD7Z8xVOwmPEtLc9m.LQLUDlVV.mnOL2eT/c94.', 'Urbano', 'Subsidiado', 2, 1),
(7, 'Paula', 'Godoy', 'paulag@gmail.com', '1659894854', '3216455465', '2007-12-13', 'Cll 28a no 16-89', 'Femenino', 'O+', 51, 150, 'Adecuado', 'Las Mercedes', 7, 'Mañana', '', '$2y$10$wKgkILLYedu5ckLoYgGxpunof5kGxWhP3.zC2Tcx9zUuMNPfzpYwe', 'Urbano', 'Subsidiado', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `introduccion` text DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `fecha` varchar(50) DEFAULT NULL,
  `hora` varchar(50) DEFAULT NULL,
  `lugar` varchar(250) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `ruta` varchar(250) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id`, `titulo`, `introduccion`, `contenido`, `fecha`, `hora`, `lugar`, `orden`, `ruta`, `id_user`, `updated_at`, `created_at`) VALUES
(5, '1ra Feria Empresarial de productos y servicios de mujeres creadoras de iniciativas productivas', '1ra Feria Empresarial de productos y servicios de mujeres creadoras de iniciativas productivas', '1ra Feria Empresarial de productos y servicios de mujeres creadoras de iniciativas productivas danu', NULL, NULL, NULL, NULL, '20170113_154724.jpg', 0, '2020-03-20 08:10:07', '2020-03-15 17:17:09'),
(6, 'Secretaría de Educación gestionó dispositivos para la prevención del coronavirus en colegios oficiales', 'En compañía de la Organización Panamericana de la Salud, el Instituto Departamental de Salud y la Oficina de Gestión del Riesgo Departamental se realizó el pasado 9 de marzo una visita a las ocho instituciones educativas oficiales de Villa del Rosario', 'Se determino la instalación de puntos de lavado de manos para cada una de ellas, con el objetivo de atender las medidas del Gobierno en cuanto al coronavirus.\r\n\r\nAdicionalmente, en una reciente reunión con Unicef y Project Hope también se gestionó la instalación de dispositivos para el lavado de manos en todos los colegios oficiales del Área Metropolitana de Cúcuta, así como el inicio de una campaña de prevención y autocuidado.\r\n\r\n“Esperamos que en los próximos días se comiencen a instalar estos dispositivos, con punto de agua y jabón, en cada establecimiento; mientras tanto, exhortamos a las comunidades educativas a seguir las recomendaciones descritas a través de la Circular No.41 en la que remitimos los cuidados que se deben tener según lo estipulado por el Ministerio de Educación y de Salud y Protección Social”, expresó la secretaria de Educación, Laura Cristina Cáceres Niño.\r\n\r\nLas siguientes son las recomendaciones de la circular conjunta remitida por el Gobierno Nacional:\r\n\r\nLavarse constantemente las manos con agua y jabón líquido.\r\nTaparse la boca y la nariz en el momento de toser o estornudar.\r\nTomar las medidas de protección necesarias ante cambios bruscos de temperatura.\r\nAbstenerse de asistir a la Institución Educativa si presenta gripa.\r\nMantener al día el esquema de vacunación de los niños y jóvenes.\r\nConservar limpias las superficies, juguetes y útiles escolares.\r\nFinalmente, desde la Secretaría de Educación Departamental se hace un llamado a la comunidad en general a mantener la calma y seguir las recomendaciones divulgadas a través de los canales oficiales.', NULL, NULL, NULL, NULL, 'circular prohibicion contratos.jpg', 0, '2020-03-16 23:39:14', '2020-03-16 23:39:14'),
(7, 'Niños y jóvenes infractores de la ley recibieron útiles escolares', 'Cuadernos, cartucheras, colores, lápices y morrales fueron entregados a cerca de 140 menores ', 'Cuadernos, cartucheras, colores, lápices y morrales fueron entregados a cerca de 140 menores vinculados al sistema de responsabilidad penal para adolescentes, para que puedan continuar sus estudios como todos los demás estudiantes del departamento.', NULL, NULL, NULL, NULL, 'KITS MENORES INFRACTORES.jpg', 0, '2020-03-16 23:40:33', '2020-03-16 23:40:33'),
(8, 'INS Colombia realizó su Rendición de Cuentas vigencia 2019', 'INS Colombia realizó su Rendición de Cuentas vigencia 2019', 'a directora del Instituto Nacional de Salud, Martha Lucía Ospina, entregó un balance de la entidad, que en el último año consolidó una efectiva respuesta contra el sarampión. Esfuerzo reconocido por la Organización Mundial de la Salud al mantener el estatus sanitario libre de sarampión para el país.\r\nLas cifras son evidentes, mientras países de la región como Brasil y Venezuela perdieron su estatus sanitario frente al sarampión. Colombia mitigó los efectos y terminó con cientos de casos, mientras sus países vecinos presentaron miles.\r\n\r\nDurante la audiencia de rendición de cuentas del Instituto, la directora destacó los esfuerzos realizados para fortalecer la investigación, con la reclasificación  de sus grupos de investigación por Colciencias en las categorías A1, A, B y C , contando con un total de 14 grupos reconocidos y con la elaboración de 206 documentos para la generación de conocimiento que comprenden artículos en las más prestigiosas revistas científicas, documentos técnicos, policy brief, entre otros.\r\n\r\nLa producción además de informes técnicos sobre malnutrición en Colombia, carga de enfermedad ambiental y la encuesta sobre el comportamiento de los colombianos frente a la decisión de compra de alimentos, generando insumos técnicos para políticas públicas y toma de decisiones.\r\n\r\nEl INS también enfocó sus esfuerzos en mejorar la capacidad de diagnóstico por laboratorio y desarrollar los sistemas de información para el análisis de datos para la toma de decisiones. \r\n\r\nLa Dirección resaltó además la respuesta del INS a ley de 1980 de 2019 por la cual se crea el Programa de Tamizaje Neonatal en Colombia , en el cual el Instituto es responsable de la realización de las pruebas diagnóstico de laboratorio para la detección temprana de enfermedades raras, la notificación de casos y la evaluación de alto desempeño a los laboratorios que realicen dicha prueba, entre otros.', NULL, NULL, NULL, NULL, 'circular prohibicion contratos.jpg', 0, '2020-03-17 00:01:13', '2020-03-17 00:01:13'),
(9, 'IV encuentro con secretarios de salud del país', 'IV encuentro con secretarios de salud del país', 'Durante el IV encuentro de secretarios de salid Departamentales y Distritales. La directora del Instituto Nacional de Salud, Martha Lucía Ospina, hizo un llamado a los secretarios de salud sobre la importancia de fortalecer sus capacidades de respuesta en un momento de emergencia, en gestionar el riesgo y fortalecer la vigilancia epidemiológica.\r\n\r\n\"Cada departamento debe tener la capacidad de responder al Sistema nacional de Vigilancia como lo hace Colombia de forma internacional con el Centro nacional de Enlace\", comentó Ospina.\r\n\r\nInstó a los secretarios para que revisen en sus departamentos: la gestión del riesgo, los componentes de vigilancia, la capacidad para gestionar los recursos en una emergencia, la respuesta inmediata de los laboratorios, entre otros.\r\n\r\n\"La entidad territorial es la líder de la gestión de la vigilancia epidemiológica. Hoy Bogotá, Valle del Cauca, Cundinamarca, meta, Nariño y Caldas, seguido de Norte de Santander, Quindío, barranquilla y Arauca, son las entidades territoriales con mejor evaluación de capacidades básicas de vigilancia y respuesta\", afirmó Martha Lucía Ospina.\r\n\r\nFinalmente, ofreció a los departamentos y distritos el Programa de Epidemiología de Campo\r\n\r\n– FETP, para capacitar al personal de las secretarias y así poder responder de forma temprana a los diversos eventos de interés de salud pública.', NULL, NULL, NULL, NULL, 'KITS MENORES INFRACTORES.jpg', 0, '2020-03-17 00:01:51', '2020-03-17 00:01:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha_saluds`
--

CREATE TABLE `ficha_saluds` (
  `id` int(11) NOT NULL,
  `detalle` varchar(200) DEFAULT NULL,
  `id_estudiante` int(11) NOT NULL,
  `id_antecedentes_tipo` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ficha_saluds`
--

INSERT INTO `ficha_saluds` (`id`, `detalle`, `id_estudiante`, `id_antecedentes_tipo`, `estado`, `updated_at`, `created_at`) VALUES
(1, 'se encontró amigdalitis en el joven', 1, 1, 0, '2020-04-17 16:51:02', '2020-04-17 16:51:02'),
(2, 'estar pendiente', 1, 1, 0, '2020-05-10 18:48:50', '2020-05-10 18:48:50'),
(3, 'Presenta calculos hace un mes', 1, 5, 0, '2020-05-10 18:49:42', '2020-05-10 18:49:42'),
(4, 'Tiene asma desde el primer año de nacimiento', 1, 4, 0, '2020-05-10 18:50:10', '2020-05-10 18:50:10'),
(5, 'tine desde los 6 años', 3, 1, 1, '2020-05-21 03:43:15', '2020-05-21 03:43:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulario_ficha_salud`
--

CREATE TABLE `formulario_ficha_salud` (
  `id` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `observaciones` text DEFAULT 'Ninguna',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `formulario_ficha_salud`
--

INSERT INTO `formulario_ficha_salud` (`id`, `id_estudiante`, `observaciones`, `created_at`, `updated_at`) VALUES
(51, 4, NULL, '2020-09-12 04:36:12', '2020-09-12 04:36:12'),
(52, 4, NULL, '2020-09-12 04:39:10', '2020-09-12 04:39:10'),
(53, 4, 'hola', '2020-09-12 04:41:41', '2020-09-12 04:41:41'),
(54, 2, NULL, '2020-09-16 01:45:26', '2020-09-16 01:45:26'),
(55, 2, NULL, '2020-09-16 01:55:46', '2020-09-16 01:55:46'),
(56, 2, NULL, '2020-09-16 01:56:58', '2020-09-16 01:56:58'),
(57, 2, NULL, '2020-09-16 01:59:46', '2020-09-16 01:59:46'),
(58, 2, NULL, '2020-09-16 02:02:04', '2020-09-16 02:02:04'),
(59, 2, NULL, '2020-09-16 02:03:49', '2020-09-16 02:03:49'),
(60, 2, NULL, '2020-09-16 02:04:37', '2020-09-16 02:04:37'),
(61, 2, NULL, '2020-09-16 02:06:34', '2020-09-16 02:06:34'),
(62, 2, NULL, '2020-09-16 02:08:02', '2020-09-16 02:08:02'),
(63, 2, NULL, '2020-09-16 02:44:00', '2020-09-16 02:44:00'),
(64, 4, 'observacion global ', '2020-09-16 13:18:54', '2020-09-16 13:18:54'),
(65, 3, 'global', '2020-09-16 13:23:29', '2020-09-16 13:23:29'),
(66, 1, 'Ninguna', '2020-09-17 14:53:01', '2020-09-17 14:53:01'),
(67, 3, 'Ninguna', '2020-09-18 22:52:06', '2020-09-18 22:52:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucions`
--

CREATE TABLE `institucions` (
  `id` int(11) NOT NULL,
  `nombre_insti` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` int(11) NOT NULL,
  `mision` varchar(500) NOT NULL,
  `vision` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `institucions`
--

INSERT INTO `institucions` (`id`, `nombre_insti`, `direccion`, `telefono`, `mision`, `vision`) VALUES
(1, 'Colegio Argelino Garzon', 'sardinata', 23123, '', ''),
(2, 'Colegio Nuestra Señora de las Mercedes', 'Calle 8 #4-70 Barrio San Francisco', 5665213, 'Trabajamos por la formación integral de niños, niñas y jóvenes con base en procesos, capacidades, valores y competencias para aprender a educarse, a ser, a obrar y emprender de manera autónoma, responsable, trascendente y participativa, dentro del marco de la defensa y promoción de los derechos humanos, la educación inclusiva, la conservación y protección del medio ambiente, la educación para la sexualidad  y construcción de ciudadanía, fundamentados en principios cristianos, valores éticos, mor', 'Posicionarnos  a 2015  como una de las mejores Instituciones Educativas oficiales  del departamento que sobresalga   en cobertura, calidad y accesibilidad; promoviendo el liderazgo, el desarrollo social, cultural y empresarial,  que motive   en nuestros  estudiantes  la capacidad  creativa y productiva,   para que  respondan  a  las necesidades personales y de la región.'),
(3, 'Alirio Vergel Pacheco ', 'Calle # Sardinata ', 8992611, '*******', '*********');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `area_residencia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`id`, `nombre`, `tipo`, `area_residencia`) VALUES
(1, 'San Francisco ', 'Barrio', 'Urbana'),
(2, 'El Llano', 'Barrio', 'Urbana'),
(3, 'La Perla', 'Barrio', 'Urbana'),
(4, 'Centenario', 'Barrio', 'Urbana'),
(5, 'Tamarindo', 'Barrio', 'Urbana'),
(6, 'Guajira', 'Barrio', 'Urbana'),
(7, 'Betania', 'Barrio', 'Urbana'),
(8, 'Poblado', 'Barrio', 'Urbana'),
(9, 'Botello Gómez', 'Barrio', 'Urbana'),
(10, 'La Victoria', 'Barrio', 'Urbana'),
(11, 'Pueblo Nuevo', 'Barrio', 'Urbana'),
(12, 'San Rafael', 'Barrio', 'Urbana'),
(13, 'Centro', 'Barrio', 'Urbana'),
(14, 'La Pesa', 'Barrio', 'Urbana'),
(15, 'Chicaros', 'Barrio', 'Urbana'),
(16, 'San Martin\r\n', 'Barrio', 'Urbana'),
(17, 'El Baho', 'Barrio', 'Urbana'),
(18, 'Vista Hermosa', 'Barrio', 'Urbana'),
(19, 'Jordán', 'Corregimiento Las Mercedes', 'Rural'),
(20, 'El Tagua', 'Corregimiento Las Mercedes', 'Rural'),
(21, 'Santa Cruz', 'Corregimiento Las Mercedes', 'Rural'),
(22, 'La Pita', 'Corregimiento Las Mercedes', 'Rural'),
(23, 'San Alejo', 'Corregimiento Las Mercedes', 'Rural'),
(24, 'Bella Vista', 'Corregimiento Las Mercedes', 'Rural'),
(25, 'Paramillo', 'Corregimiento Las Mercedes', 'Rural'),
(26, 'El Reposo', 'Corregimiento Las Mercedes', 'Rural'),
(27, 'Filo Real', 'Corregimiento Las Mercedes', 'Rural'),
(28, 'San Ramón', 'Corregimiento Las Mercedes', 'Rural'),
(29, 'El Recreo', 'Corregimiento Las Mercedes', 'Rural'),
(30, 'El Rodeo', 'Corregimiento Las Mercedes', 'Rural'),
(31, 'Encontrados', 'Corregimiento Las Mercedes', 'Rural'),
(32, 'San Gil', 'Corregimiento Las Mercedes', 'Rural'),
(33, 'Cartagena', 'Corregimiento Las Mercedes', 'Rural'),
(34, 'El Riecito', 'Corregimiento Las Mercedes', 'Rural'),
(35, 'La Libertad', 'Corregimiento Las Mercedes', 'Rural'),
(36, 'Miraflores', 'Corregimiento Las Mercedes', 'Rural'),
(37, 'El Cairo', 'Corregimiento Las Mercedes', 'Rural'),
(38, 'El Danto', 'Corregimiento Las Mercedes', 'Rural'),
(39, 'La Pradera', 'Corregimiento Las Mercedes', 'Rural'),
(40, 'La Reforma', 'Corregimiento Las Mercedes', 'Rural'),
(41, 'San Vicente', 'Corregimiento Las Mercedes', 'Rural'),
(42, 'El Placer', 'Corregimiento Las Mercedes', 'Rural'),
(43, 'Jordancito', 'Corregimiento Las Mercedes', 'Rural'),
(44, 'Los Ángeles', 'Corregimiento Las Mercedes', 'Rural'),
(45, 'Los Curos', 'Corregimiento Las Mercedes', 'Rural'),
(46, 'El Diamante', 'Corregimiento Las Mercedes', 'Rural'),
(47, 'Paramito', 'Corregimiento Las Mercedes', 'Rural'),
(48, 'Pailas', 'Corregimiento Las Mercedes', 'Rural'),
(49, 'Andalucía', 'Corregimiento Las Mercedes', 'Rural'),
(50, 'Santa María', 'Corregimiento Las Mercedes', 'Rural'),
(51, 'San Francisco', 'Corregimiento Las Mercedes', 'Rural'),
(52, 'Buenos Aires', 'Corregimiento Las Mercedes', 'Rural'),
(53, 'La Chapolita', 'Corregimiento Las Mercedes', 'Rural'),
(54, 'La Mesa', 'Corregimiento Las Mercedes', 'Rural'),
(55, 'El Comienzo', 'Corregimiento Las Mercedes', 'Rural'),
(56, 'Balcones', 'Corregimiento Las Mercedes', 'Rural'),
(57, 'San Juan', 'Corregimiento Las Mercedes', 'Rural'),
(58, 'La Barca', 'Corregimiento Las Mercedes', 'Rural'),
(59, 'Filipinas', 'Corregimiento Las Mercedes', 'Rural'),
(60, 'La Garita', 'Corregimiento Las Mercedes', 'Rural'),
(61, 'La Ceiba', 'Corregimiento Las Mercedes', 'Rural'),
(62, 'San Benito', 'Corregimiento Las Mercedes', 'Rural'),
(63, 'Las Mercedes', 'Corregimiento Las Mercedes', 'Rural'),
(64, 'San Joaquín', 'Corregimiento San Martín de Loba', 'Rural'),
(65, 'Remolino', 'Corregimiento San Martín de Loba', 'Rural'),
(66, 'Corinto', 'Corregimiento San Martín de Loba', 'Rural'),
(67, 'Maporita Berlín', 'Corregimiento San Martín de Loba', 'Rural'),
(68, 'El Treinta', 'Corregimiento San Martín de Loba', 'Rural'),
(69, 'Santa Ana', 'Corregimiento San Martín de Loba', 'Rural'),
(70, 'El Ecuador', 'Corregimiento San Martín de Loba', 'Rural'),
(71, 'El Porvenir', 'Corregimiento San Martín de Loba', 'Rural'),
(72, 'La Paz', 'Corregimiento San Martín de Loba', 'Rural'),
(73, 'Campo Lajas', 'Corregimiento San Martín de Loba', 'Rural'),
(74, 'Miraflores de San\r\nMartin', 'Corregimiento San Martín de Loba', 'Rural'),
(75, 'La Llanita', 'Corregimiento San Martín de Loba', 'Rural'),
(76, 'San Martin de Loba', 'Corregimiento San Martín de Loba', 'Rural'),
(77, 'Caldasia', 'Corregimiento El Carmen', 'Rural'),
(78, 'Fátima', 'Corregimiento El Carmen', 'Rural'),
(79, 'La Esmeralda', 'Corregimiento El Carmen', 'Rural'),
(80, 'Guayacanes', 'Corregimiento El Carmen', 'Rural'),
(81, 'Higuerón', 'Corregimiento El Carmen', 'Rural'),
(82, 'Llano Grande', 'Corregimiento El Carmen', 'Rural'),
(83, 'San Luis Bajo', 'Corregimiento El Carmen', 'Rural'),
(84, 'San Sebastián', 'Corregimiento El Carmen', 'Rural'),
(85, 'Santa Teresa', 'Corregimiento El Carmen', 'Rural'),
(86, 'Santa Clara', 'Corregimiento El Carmen', 'Rural'),
(87, 'El Carmen', 'Corregimiento El Carmen', 'Rural'),
(88, 'San Isidro', 'Corregimiento La Victoria', 'Rural'),
(89, 'La Trinidad', 'Corregimiento La Victoria', 'Rural'),
(90, 'Gallinetas', 'Corregimiento La Victoria', 'Rural'),
(91, 'Cascajal', 'Corregimiento La Victoria', 'Rural'),
(92, 'La Cristalina', 'Corregimiento La Victoria', 'Rural'),
(93, 'Berlín', 'Corregimiento La Victoria', 'Rural'),
(94, 'San José', 'Corregimiento La Victoria', 'Rural'),
(95, 'Balsamina', 'Corregimiento La Victoria', 'Rural'),
(96, 'Guadalupe', 'Corregimiento La Victoria', 'Rural'),
(97, 'Naranjales', 'Corregimiento La Victoria', 'Rural'),
(98, 'La Primavera', 'Corregimiento La Victoria', 'Rural'),
(99, 'Cascarillales', 'Corregimiento La Victoria', 'Rural'),
(100, 'La Forzosa', 'Corregimiento La Victoria', 'Rural'),
(101, 'Santa Clara', 'Corregimiento La Victoria', 'Rural'),
(102, 'Santa Rosa', 'Corregimiento La Victoria', 'Rural'),
(103, 'San Luis parte alta', 'Corregimiento La Victoria', 'Rural'),
(104, 'La Victoria', 'Corregimiento La Victoria', 'Rural'),
(105, 'San Miguel', 'Centro Sardinata', 'Urbana'),
(106, 'La Ceiba', 'Centro Sardinata', 'Urbana'),
(107, 'La Popa', 'Centro Sardinata', 'Urbana'),
(108, 'El Líbano', 'Centro Sardinata', 'Urbana'),
(109, 'Abejales', 'Centro Sardinata', 'Urbana'),
(110, 'San Antonio', 'Centro Sardinata', 'Urbana'),
(111, 'El Vesubio', 'Centro Sardinata', 'Urbana'),
(112, 'El Paramo', 'Centro Sardinata', 'Urbana'),
(113, 'La Potrera', 'Centro Sardinata', 'Urbana'),
(114, 'Puente Piedra', 'Centro Sardinata', 'Urbana'),
(115, 'El Bojoso', 'Centro Sardinata', 'Urbana'),
(116, 'Bocono', 'Centro Sardinata', 'Urbana'),
(117, 'San Roque', 'Centro Sardinata', 'Urbana'),
(118, 'Rubí', 'Centro Sardinata', 'Urbana'),
(119, 'Las Mesas', 'Centro Sardinata', 'Urbana'),
(120, 'La Pailona', 'Centro Sardinata', 'Urbana'),
(121, 'El Cerro', 'Centro Sardinata', 'Urbana'),
(122, 'Junín', 'Centro Sardinata', 'Urbana'),
(123, 'Valderrama', 'Centro Sardinata', 'Urbana'),
(124, 'El Guamo San Miguel', 'Centro Sardinata', 'Urbana'),
(125, 'Campo Rico', 'Centro Sardinata', 'Urbana'),
(126, 'El Guayabo', 'Centro Sardinata', 'Urbana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `leido` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `mensaje` text NOT NULL,
  `id_user_mensaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `id_user`, `leido`, `created_at`, `updated_at`, `mensaje`, `id_user_mensaje`) VALUES
(1, 54, 1, '2020-08-17 01:24:15', '2020-08-17 01:24:15', 'hola', 0),
(2, 54, 1, '2020-08-17 01:24:48', '2020-08-17 01:24:48', 'que mas', 0),
(3, 0, 1, '2020-08-17 01:24:48', '2020-08-19 14:00:51', 'bien y usted?', 54),
(4, 54, 1, '2020-08-18 12:53:54', '2020-08-18 12:53:54', 'jajaj', 0),
(5, 54, 1, '2020-08-18 12:58:26', '2020-08-18 12:58:26', 'Que mas', 0),
(6, 54, 1, '2020-08-18 12:58:26', '2020-08-18 12:58:26', 'hola', 0),
(7, 54, 1, '2020-08-18 12:58:27', '2020-08-18 12:58:27', 'le escribo para informar que quiero ver los informes', 0),
(8, 54, 1, '2020-08-18 12:58:27', '2020-08-18 12:58:27', 'gracias', 0),
(9, 54, 1, '2020-08-18 12:58:28', '2020-08-18 12:58:28', 'quedo atento para la atencion del estudiante que tiene calculos de octavo', 0),
(10, 54, 1, '2020-08-18 12:58:29', '2020-08-18 12:58:29', 'adios', 0),
(11, 54, 1, '2020-08-19 12:09:13', '2020-08-19 12:09:13', 'hola', 0),
(12, 54, 1, '2020-08-19 12:09:45', '2020-08-19 12:09:45', 'que mas', 0),
(13, 0, 1, '2020-08-19 01:24:48', '2020-08-19 14:00:51', 'funciona perfect ', 54),
(14, 54, 1, '2020-08-19 12:12:10', '2020-08-19 12:12:10', 'pa que vea', 0),
(15, 54, 1, '2020-08-19 12:12:33', '2020-08-19 12:12:33', 'hola Evaluador como esta', 49);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `ruta` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `contenido` text NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `ruta`, `titulo`, `contenido`, `fecha`, `id_user`, `updated_at`, `created_at`) VALUES
(1, 'ELECCION COMITE LABORAL.jpg', 'Secretaría de Educación gestionó dispositivos para la prevención del coronavirus en colegios oficiales', 'En compañía de la Organización Panamericana de la Salud, el Instituto Departamental de Salud y la Oficina de Gestión del Riesgo Departamental se realizó el pasado 9 de marzo una visita a las ocho instituciones educativas oficiales de Villa del Rosario, determinándose la instalación de puntos de lavado de manos para cada una de ellas, con el objetivo de atender las medidas del Gobierno en cuanto al coronavirus.\r\n\r\n \r\n\r\nAdicionalmente, en una reciente reunión con Unicef y Project Hope también se gestionó la instalación de dispositivos para el lavado de manos en todos los colegios oficiales del Área Metropolitana de Cúcuta, así como el inicio de una campaña de prevención y autocuidado.\r\n\r\n \r\n\r\n“Esperamos que en los próximos días se comiencen a instalar estos dispositivos, con punto de agua y jabón, en cada establecimiento; mientras tanto, exhortamos a las comunidades educativas a seguir las recomendaciones descritas a través de la Circular No.41 en la que remitimos los cuidados que se deben tener según lo estipulado por el Ministerio de Educación y de Salud y Protección Social”, expresó la secretaria de Educación, Laura Cristina Cáceres Niño.\r\n\r\n \r\n\r\nLas siguientes son las recomendaciones de la circular conjunta remitida por el Gobierno Nacional:\r\n\r\nLavarse constantemente las manos con agua y jabón líquido.\r\nTaparse la boca y la nariz en el momento de toser o estornudar.\r\nTomar las medidas de protección necesarias ante cambios bruscos de temperatura.\r\nAbstenerse de asistir a la Institución Educativa si presenta gripa.\r\nMantener al día el esquema de vacunación de los niños y jóvenes.\r\nConservar limpias las superficies, juguetes y útiles escolares.\r\nFinalmente, desde la Secretaría de Educación Departamental se hace un llamado a la comunidad en general a mantener la calma y seguir las recomendaciones divulgadas a través de los canales oficiales.', '22-03-2020 22:20:15', 0, '2020-03-23 03:20:16', '2020-03-23 03:20:16'),
(2, 'KITS MENORES INFRACTORES.jpg', 'Niños y jóvenes infractores de la ley recibieron útiles escolares', 'Cuadernos, cartucheras, colores, lápices y morrales fueron entregados a cerca de 140 menores vinculados al sistema de responsabilidad penal para adolescentes, para que puedan continuar sus estudios como todos los demás estudiantes del departamento.\r\n\r\n \r\n\r\nLos útiles escolares fueron donados por la Unicef y entregados por la secretaria de Educación, Laura Cristina Cáceres Niño, a los niños y jóvenes tras cumplida una visita al Centro de Formación Juvenil, ubicado en el municipio de Los Patios, en el cual se presta el servicio educativo bajo dirección de la Institución Educativa Patio Centro 2.\r\n\r\n \r\n\r\nSegún Cáceres Niño, además, se llevó a cabo una reunión que contó con la participación de la procuradora de Familia, Myriam Rozo Wilches, en la que los menores infractores de la ley expusieron sus necesidades en materia de educación. \r\n\r\n \r\n\r\nEn conclusión se acordó adelantar un plan a corto y mediano plazo sobre la designación de docentes, la implementación de un modelo educativo flexible multigrado y la incorporación en el Proyecto Educativo Institucional de los modelos educativos que se están empleando para la formación de los adolescentes. \r\n\r\n \r\n\r\nAdemás, desde la Secretaría de Educación se gestionará la dotación de libros, mesas plásticas, tableros acrílicos, ventiladores y otros.', '22-03-2020 22:21:07', 0, '2020-03-23 03:21:07', '2020-03-23 03:21:07'),
(3, 'circular prohibicion contratos.jpg', 'Mala salud dental afecta el desempeño escolar', '\"Las enfermedades odontológicas más comunes en los escolares son la caries dental, la gingivitis (inflamación de las encías) y malocusiones (alteración de la posición de los dientes y morfología de maxilares), los cuales son ocasionados por la placa bacteriana acumulada en los dientes\", afirma el doctor Roberto Laynes Almeida, Jefe del Departamento de Estomatología del Hospital Loayza.\r\n\r\nEl odontólogo refiere que, también, existen otros hábitos nocivos frecuentes en los escolares que afectan la salud bucal como morder objetos (uñas, lapiceros, borradores, juguetes), ‘chuparse’ los dedos o apoyar la mandíbula sobre la mano o el antebrazo por un tiempo prolongado, puesto que alteran la forma de los maxilares.\r\n\r\nPor ello, el especialista asegura que es importante que los escolares inicien las clases sin problemas dentales, puesto que estos pueden afectar su rendimiento académico. El número de veces que el menor deberá acudir al dentista estará determinado por la frecuencia del desarrollo de caries, forma y posición de los dientes, dieta alimenticia, higiene bucal, entre otros.\r\n\r\n\"El examen odontológico integral del menor consiste no solo en la revisión de los dientes, sino también, de todo el sistema estomatognático conformado por aquellos órganos, músculos, articulaciones y huesos que permiten que una persona pueda masticar, hablar, sonreír, pronunciar, besar, succionar, entre otros\", agrega.\r\n\r\nLONCHERA ESCOLAR COMO MEDIDA PREVENTIVA\r\n\r\nUna lonchera escolar saludable ayuda a reducir la aparición de problemas dentales. Por ello, esta debe estar conformada básicamente por productos naturales como frutas frescas, jugos naturales, refrescos caseros, sandwich de casa, entre otros.\r\n\r\n\"Los productos ricos en azúcares como galletas, toffees, chicles, gaseosas, jugos envasados, snacks, o todo aquello que contenga preservantes provocan daño en la salud bucal del menor, en vez de alimentarlo adecuadamente\", advierte el especialista.\r\n\r\nAsimismo, el odontólogo recomendó el consumo de frutas como la manzana o zanahoria, puesto que, dado su contextura fibrosa, actúan como un ‘cepillo natural’ en los dientes limpiándolos de la placa bacteriana. No obstante, ello no reemplaza el uso del cepillo, pasta dentífrica e hilo dental después de cada comida o refrigerio escolar.', '22-03-2020 22:27:02', 0, '2020-03-23 03:27:02', '2020-03-23 03:27:02'),
(4, '5c1ed16740d97.jpeg', 'Malnutrición escolar: una bomba de tiempo en Colombia', 'Los resultados de la Encuesta Nacional de Salud Escolar (Ense) y la de Tabaquismo en Jóvenes 2018 realizadas por el Ministerio de Salud y la Universidad del Valle desnudan, según el pediatra Vladimir Muñoz, preocupantes grietas en las políticas de salud pública orientadas a niños y adolescentes. Grietas que, “de no corregirse de manera inmediata, se traducirán en graves enfermedades con impactos sociales y económicos incalculables para el país”, dice el experto.', '22-03-2020 22:30:50', 0, '2020-03-23 03:30:50', '2020-03-23 03:30:50'),
(5, '5a84de52c65e4.jpeg', 'Sec. Educación ya está entregando bonos para alimentación escolar', 'Desde hoy, 01 de abril, la Secretaría de Educación comenzó a entregar los bonos redimibles de alimentación que cubren los refrigerios escolares. Hasta el momento más de 500 mil familias en Bogotá se han inscrito para recibir este apoyo. \r\n\r\nEste plan de atención ante la llegada de la pandemia al país busca garantizar que ningún estudiante en Bogotá pase hambre durante la cuarentena. Y aunque, desde el momento en el que los alumnos dejaron de asistir presencialmente a las clases, la entidad ha implementado diversos mecanismos para atender su alimentación, el programa de bonos redimibles busca que los padres no se tengan que movilizar por toda la ciudad y que no estén expuestos a aglomeraciones.', '02-04-2020 19:54:36', 0, '2020-04-03 00:54:37', '2020-04-03 00:54:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observaciones`
--

CREATE TABLE `observaciones` (
  `id` int(11) NOT NULL,
  `detalles` varchar(250) NOT NULL,
  `privado` tinyint(1) NOT NULL DEFAULT 0,
  `tipo_falta` varchar(250) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `observaciones`
--

INSERT INTO `observaciones` (`id`, `detalles`, `privado`, `tipo_falta`, `id_estudiante`, `created_at`, `update_at`) VALUES
(1, 'El dia de ayer 1 de octubre se peleo con un compañero de clase sin razon alguna', 1, 'Comportamiento', 3, '2020-10-02 18:58:23', '2020-10-02 18:58:23'),
(2, 'El dia de ayer 1 de octubre se peleo con un compañero de clase sin razon alguna', 1, 'Comportamiento', 2, '2020-10-02 18:58:23', '2020-10-02 18:58:23'),
(3, 'El dia de ayer 1 de octubre se peleo con un compañero de clase sin razon alguna', 1, 'Bullyng', 2, '2020-10-02 18:58:23', '2020-10-02 18:58:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observaciones_dominio_ficha`
--

CREATE TABLE `observaciones_dominio_ficha` (
  `id` int(11) NOT NULL,
  `descripcion` text DEFAULT 'Ninguna',
  `id_dominio` int(11) NOT NULL,
  `id_formulario_ficha_salud` int(11) NOT NULL,
  `resuelto` tinyint(1) NOT NULL DEFAULT 0,
  `valorizacion` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `observaciones_dominio_ficha`
--

INSERT INTO `observaciones_dominio_ficha` (`id`, `descripcion`, `id_dominio`, `id_formulario_ficha_salud`, `resuelto`, `valorizacion`, `created_at`, `update_at`) VALUES
(1, 'Presento desnutricion, se recomienda ir con el doctor, o investigar en la casa', 13, 53, 0, NULL, NULL, NULL),
(2, 'Presento desnutricion, se recomienda ir con el doctor, o investigar en la casa', 13, 53, 1, NULL, NULL, NULL),
(3, 'Presento desnutricion, se recomienda ir con el doctor, o investigar en la casa', 2, 53, 1, NULL, NULL, NULL),
(4, '', 1, 53, 1, '\"buena\"', '2020-09-16 01:33:19', '2020-09-16 01:33:19'),
(5, NULL, 1, 53, 0, '\"buena222\"', '2020-09-16 01:36:06', '2020-09-16 01:36:06'),
(6, NULL, 1, 54, 0, 'undefined', '2020-09-16 01:45:47', '2020-09-16 01:45:47'),
(7, 'ultima', 14, 61, 0, 'La valorización del Seguridad es Adecuada', '2020-09-16 02:06:48', '2020-09-16 02:06:48'),
(8, NULL, 1, 62, 0, 'La valorización del Promoción de la salud es Alterado', '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(9, NULL, 2, 62, 0, 'La valorización del Nutrición es Alterado', '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(10, NULL, 3, 62, 0, 'La valorización del Eliminación e intercambio es Alterado', '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(11, NULL, 6, 62, 0, 'La valorización del Autopercepción es Adecuada', '2020-09-16 02:08:16', '2020-09-16 02:08:16'),
(12, NULL, 9, 62, 0, 'La valorización del Afrontamiento y tolerancia al estrés es Alterado', '2020-09-16 02:08:16', '2020-09-16 02:08:16'),
(13, NULL, 10, 62, 0, 'La valorización del Principios Vitales es Adecuada', '2020-09-16 02:08:16', '2020-09-16 02:08:16'),
(14, NULL, 11, 62, 0, 'La valorización del Seguridad y protección es Alterado', '2020-09-16 02:08:16', '2020-09-16 02:08:16'),
(15, NULL, 12, 62, 0, 'La valorización del Confort es Alterado', '2020-09-16 02:08:17', '2020-09-16 02:08:17'),
(16, NULL, 13, 62, 0, 'La valorización del Crecimiento y desarrollo es Alterado', '2020-09-16 02:08:17', '2020-09-16 02:08:17'),
(17, NULL, 14, 62, 0, 'La valorización del Seguridad es Adecuada', '2020-09-16 02:08:17', '2020-09-16 02:08:17'),
(18, NULL, 1, 63, 0, 'La valorización del Promoción de la salud es Alterado', '2020-09-16 02:44:13', '2020-09-16 02:44:13'),
(19, NULL, 2, 63, 0, 'La valorización del Nutrición es Alterado', '2020-09-16 02:44:13', '2020-09-16 02:44:13'),
(20, NULL, 3, 63, 0, 'La valorización del Eliminación e intercambio es Alterado', '2020-09-16 02:44:13', '2020-09-16 02:44:13'),
(21, NULL, 6, 63, 0, 'La valorización del Autopercepción es Adecuada', '2020-09-16 02:44:13', '2020-09-16 02:44:13'),
(22, NULL, 9, 63, 0, 'La valorización del Afrontamiento y tolerancia al estrés es Alterado', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(23, NULL, 10, 63, 0, 'La valorización del Principios Vitales es Adecuada', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(24, NULL, 11, 63, 0, 'La valorización del Seguridad y protección es Alterado', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(25, NULL, 12, 63, 0, 'La valorización del Confort es Alterado', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(26, NULL, 13, 63, 0, 'La valorización del Crecimiento y desarrollo es Alterado', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(27, NULL, 14, 63, 0, 'La valorización del Seguridad es Adecuada', '2020-09-16 02:44:14', '2020-09-16 02:44:14'),
(28, NULL, 1, 64, 0, 'La valorización del Promoción de la salud es Alterado', '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(29, NULL, 2, 64, 0, 'La valorización del Nutrición es Alterado', '2020-09-16 13:19:07', '2020-09-16 13:19:07'),
(30, NULL, 3, 64, 0, 'La valorización del Eliminación e intercambio es Alterado', '2020-09-16 13:19:07', '2020-09-16 13:19:07'),
(31, NULL, 6, 64, 0, 'La valorización del Autopercepción es Adecuada', '2020-09-16 13:19:07', '2020-09-16 13:19:07'),
(32, NULL, 9, 64, 0, 'La valorización del Afrontamiento y tolerancia al estrés es Alterado', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(33, NULL, 10, 64, 0, 'La valorización del Principios Vitales es Adecuada', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(34, NULL, 11, 64, 0, 'La valorización del Seguridad y protección es Alterado', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(35, NULL, 12, 64, 0, 'La valorización del Confort es Alterado', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(36, NULL, 13, 64, 0, 'La valorización del Crecimiento y desarrollo es Alterado', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(37, 'ultima observacion', 14, 64, 0, 'La valorización del Seguridad es Adecuada', '2020-09-16 13:19:08', '2020-09-16 13:19:08'),
(38, NULL, 1, 65, 0, 'La valorización del Promoción de la salud es Alterado', '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(39, NULL, 2, 65, 0, 'La valorización del Nutrición es Alterado', '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(40, NULL, 3, 65, 0, 'La valorización del Eliminación e intercambio es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(41, NULL, 4, 65, 0, 'La valorización del Actividad reposo es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(42, NULL, 5, 65, 0, 'La valorización del Percepción Cognición es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(43, NULL, 6, 65, 0, 'La valorización del Autopercepción es Adecuada', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(44, NULL, 7, 65, 0, 'La valorización del Rol Relaciones es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(45, NULL, 8, 65, 0, 'La valorización del Sexualidad Reproducción es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(46, NULL, 9, 65, 0, 'La valorización del Afrontamiento y tolerancia al estrés es Alterado', '2020-09-16 13:23:42', '2020-09-16 13:23:42'),
(47, NULL, 10, 65, 0, 'La valorización del Principios Vitales es Adecuada', '2020-09-16 13:23:43', '2020-09-16 13:23:43'),
(48, NULL, 11, 65, 0, 'La valorización del Seguridad y protección es Alterado', '2020-09-16 13:23:43', '2020-09-16 13:23:43'),
(49, NULL, 12, 65, 0, 'La valorización del Confort es Alterado', '2020-09-16 13:23:43', '2020-09-16 13:23:43'),
(50, 'ultimo', 13, 65, 0, 'La valorización del Crecimiento y desarrollo es Alterado', '2020-09-16 13:23:43', '2020-09-16 13:23:43'),
(51, NULL, 14, 65, 0, 'La valorización del Seguridad es Adecuada', '2020-09-16 13:23:43', '2020-09-16 13:23:43'),
(52, 'Ninguna', 1, 67, 0, 'La valorización del dominio Promoción de la salud es Alterado', '2020-09-18 22:52:33', '2020-09-18 22:52:33'),
(53, 'Ninguna', 2, 67, 0, 'La valorización del dominio Nutrición es Alterado', '2020-09-18 22:52:33', '2020-09-18 22:52:33'),
(54, 'Ninguna', 3, 67, 0, 'La valorización del dominio Eliminación e intercambio es Alterado', '2020-09-18 22:52:34', '2020-09-18 22:52:34'),
(55, 'Ninguna', 4, 67, 0, 'La valorización del dominio Actividad reposo es Alterado', '2020-09-18 22:52:34', '2020-09-18 22:52:34'),
(56, 'Ninguna', 5, 67, 0, 'La valorización del dominio Percepción Cognición es Alterado', '2020-09-18 22:52:34', '2020-09-18 22:52:34'),
(57, 'Ninguna', 6, 67, 0, 'La valorización del dominio Autopercepción es Adecuada', '2020-09-18 22:52:34', '2020-09-18 22:52:34'),
(58, 'Ninguna', 7, 67, 0, 'La valorización del dominio Rol Relaciones es Alterado', '2020-09-18 22:52:34', '2020-09-18 22:52:34'),
(59, 'Ninguna', 8, 67, 0, 'La valorización del dominio Sexualidad Reproducción es Alterado', '2020-09-18 22:52:35', '2020-09-18 22:52:35'),
(60, 'Ninguna', 9, 67, 0, 'La valorización del dominio Afrontamiento y tolerancia al estrés es Alterado', '2020-09-18 22:52:35', '2020-09-18 22:52:35'),
(61, 'Ninguna', 10, 67, 0, 'La valorización del dominio Principios Vitales es Adecuada', '2020-09-18 22:52:35', '2020-09-18 22:52:35'),
(62, 'Ninguna', 11, 67, 0, 'La valorización del dominio Seguridad y protección es Alterado', '2020-09-18 22:52:35', '2020-09-18 22:52:35'),
(63, 'Ninguna', 12, 67, 0, 'La valorización del dominio Confort es Alterado', '2020-09-18 22:52:36', '2020-09-18 22:52:36'),
(64, 'Ninguna', 13, 67, 0, 'La valorización del dominio Crecimiento y desarrollo es Alterado', '2020-09-18 22:52:36', '2020-09-18 22:52:36'),
(65, 'Ninguna', 14, 67, 0, 'La valorización del dominio Seguridad es Adecuada', '2020-09-18 22:52:36', '2020-09-18 22:52:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('naydu@ufps.edu.co', '$2y$10$0Vd2yPCgmFm6kCv7bI1jfOkIsFzxW0np4F4/yxxveZLCMYKZ/u.FO', '2020-04-13 21:13:52'),
('naydu@ufps.edu.co', '$2y$10$0Vd2yPCgmFm6kCv7bI1jfOkIsFzxW0np4F4/yxxveZLCMYKZ/u.FO', '2020-04-13 21:13:52'),
('naydu@ufps.edu.co', '$2y$10$0Vd2yPCgmFm6kCv7bI1jfOkIsFzxW0np4F4/yxxveZLCMYKZ/u.FO', '2020-04-13 21:13:52'),
('naydu@ufps.edu.co', '$2y$10$0Vd2yPCgmFm6kCv7bI1jfOkIsFzxW0np4F4/yxxveZLCMYKZ/u.FO', '2020-04-13 21:13:52'),
('lauridani1@gmail.com', '$2y$10$U/vwtinHtKl7MEnIvGdO4eim2NgnUyyg4qAorgcERO7GVnMYQahoi', '2020-10-01 08:38:57'),
('samiyahiram@ufps.edu.co', '$2y$10$11Eal3Lp5N2I6C3y3YytnOfq1SgzI1GBf3Vov/yLFTqIi9JauSh.y', '2020-10-01 08:47:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id` int(11) NOT NULL,
  `pregunta` varchar(255) NOT NULL,
  `tipo_input` varchar(50) NOT NULL,
  `id_dominio` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `descripcion_grafica` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `pregunta`, `tipo_input`, `id_dominio`, `activo`, `descripcion_grafica`, `created_at`, `updated_at`) VALUES
(1, '¿Cual es el estado de los oidos', 'select', 1, 1, 'Frecuencia del estado de los oídos de los estudiantes', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(2, '¿Cual es el estado de los ojos?', 'select', 1, 1, 'Frecuencia del estado de los ojos de los estudiante', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(3, '¿Cual es el estado de las fosas nasales?', 'select', 1, 1, 'Frecuencia del estado de las fosas nasales de los estudiante', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(4, '¿Cual es el estado de la cavidad oral?', 'select', 1, 1, 'Frecuencia del estado de la cavidad oral de los estudiante', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(5, '¿Cual es el estado de las manos y pies ?', 'select', 1, 1, 'Frecuencia del estado de las manos y pies de los estudiante', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(6, '¿Cual es el estado la vestimenta?', 'select', 1, 1, 'Frecuencia del estado de la vestimenta de los estudiante', '2020-09-08 01:41:09', '2020-09-08 01:41:09'),
(7, '¿Vive con personas alcohólicos?', 'si_no', 1, 1, 'Frecuencia Presencia de alcohólicos \r\nen el hogar', '2020-09-08 01:43:22', '2020-09-08 01:43:22'),
(8, '¿Vive con personas que fuman', 'si_no', 1, 1, 'Frecuencia Presencia de fumadores\r\nen el hogar', '2020-09-08 01:43:22', '2020-09-08 01:43:22'),
(9, '¿Cuantas raciones recibe de desayuno?', 'select', 2, 1, 'Frecuencia de raciones de comidas al\r\ndia por estudiantes ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(10, '¿Cuantas raciones recibe de \r\nmedia mañana?', 'select', 2, 1, 'Frecuencia de raciones de comidas al\r\ndia por estudiantes ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(11, '¿Cuantas raciones recibe de almuerzo?', 'select', 2, 1, 'Frecuencia de raciones de comidas al\r\ndía por estudiantes ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(12, '¿Cuantas raciones recibe de onces?', 'select', 2, 1, 'Frecuencia de raciones de comidas al\r\ndía por estudiantes ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(13, '¿Cuantas raciones recibe de cena?', 'select', 2, 1, 'Frecuencia de raciones de comidas al\r\ndía por estudiantes ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(14, '¿Presenta anemia?', 'si_no', 2, 1, 'Frecuencia de la presencia\r\nde anemia', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(15, '¿Presenta gingivitis?', 'si_no', 2, 1, 'Salud Oral en los escolares ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(16, '¿Presenta caries', 'si_no', 2, 1, 'Salud Oral en los escolares ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(17, '¿Presenta dolor bucal?', 'si_no', 2, 1, 'Salud Oral en los escolares ', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(18, '¿Cuantas veces se cepilla al dia?', 'select', 2, 1, 'frecuencia de cepillado al día\r\nen los escolares \r\n', '2020-09-08 01:57:21', '2020-09-08 01:57:21'),
(19, '¿Presenta vómito?', 'si_no', 3, 1, 'frecuencia de alteraciones gastrointestinales en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(20, '¿Presenta diarrea?', 'si_no', 3, 1, 'frecuencia de alteraciones gastrointestinales en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(21, '¿Presenta estreñimiento?', 'si_no', 3, 1, 'frecuencia de alteraciones gastrointestinales en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(22, '¿Presenta poliuria?', 'si_no', 3, 1, 'frecuencia de alteraciones urinarias en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(23, '¿Presenta hematuria?', 'si_no', 3, 1, 'frecuencia de alteraciones urinarias en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(24, '¿Presenta enuresis?', 'si_no', 3, 1, 'frecuencia de alteraciones urinarias en los escolares ', '2020-09-08 03:41:52', '2020-09-08 03:41:52'),
(25, '¿Cuantas horas duerme?', 'select', 4, 1, 'Frecuencia de estudiantes\r\nsegún N° de horas que duerme al día', '2020-09-08 04:07:33', '2020-09-08 04:07:33'),
(26, '¿Que actividades recreativas desempeña?', 'multiselect', 4, 1, 'Frecuencia de actividades\r\nrecreativas', '2020-09-08 04:07:33', '2020-09-08 04:07:33'),
(27, '¿Usa redes sociales?', 'si_no', 4, 1, 'usa las redes sociales', '2020-09-08 04:07:33', '2020-09-08 04:07:33'),
(28, '¿Presenta alteraciones en la columna?', 'si_no', 4, 1, 'Presencia de alteraciones en\r\ncolumna', '2020-09-08 04:07:33', '2020-09-08 04:07:33'),
(29, '¿Presenta problemas de lenguaje?', 'si_no', 5, 1, 'presencia de alteraciones\r\nen los escolares', '2020-09-08 04:13:06', '2020-09-08 04:13:06'),
(30, '¿Presenta problemas auditivos?', 'si_no', 5, 1, 'presencia de alteraciones\r\nen los escolares', '2020-09-08 04:13:06', '2020-09-08 04:13:06'),
(31, '¿Presenta problemas visuales?', 'si_no', 5, 1, 'presencia de alteraciones\r\nen los escolares', '2020-09-08 04:13:06', '2020-09-08 04:13:06'),
(32, '¿Puede describirse por si solo?', 'select', 6, 1, 'Descripción de si mismo', '2020-09-08 04:15:35', '2020-09-08 04:15:35'),
(33, '¿Que expresiones tiene de si mismo?', 'multiselect', 6, 1, 'Descripción de si mismo', '2020-09-08 04:15:35', '2020-09-08 04:15:35'),
(34, ' ¿Presenta alteraciones de conducta?', 'si_no', 7, 1, 'presencia de alteraciones de la\r\nconducta', '2020-09-08 04:18:47', '2020-09-08 04:18:47'),
(35, '¿Cual es el parentesco del acudiente?', 'select', 7, 1, 'Tutores a cargo de\r\nlos escolares ', '2020-09-08 04:18:47', '2020-09-08 04:18:47'),
(36, '¿Presenta signos de maltrato?', 'si_no', 7, 1, 'presencia de signos de maltrato', '2020-09-08 04:18:47', '2020-09-08 04:18:47'),
(37, '¿Que castigos se le aplica?', 'multiselect', 7, 1, 'Mecanismo que utilizan los\r\npadres para corregir a sus hijos', '2020-09-08 04:18:47', '2020-09-08 04:18:47'),
(38, '¿Está de acuerdo con el castigo?', 'si_no', 7, 1, 'esta de acuerdo con el\r\ncastigo', '2020-09-08 04:21:48', '2020-09-08 04:21:48'),
(39, '¿Como son las relaciones interpersonales?', 'si_no', 7, 1, 'relacion interpersonal de los\r\nescolares', '2020-09-08 04:23:18', '2020-09-08 04:23:18'),
(40, '¿Como se expresa sobre la sexualidad?', 'si_no', 8, 1, 'Se expresa con naturalidad\r\nsobre su sexualidad', '2020-09-08 04:35:31', '2020-09-08 04:35:31'),
(41, '¿cuida su cuerpo?', 'si_no', 8, 1, 'Responde adecuadamente sobre\r\nel cuidado de su cuerpo', '2020-09-08 04:35:31', '2020-09-08 04:35:31'),
(42, '¿Presenta signos de abuso sexual?', 'si_no', 8, 1, 'Identifica los riesgo\r\nsobre el abuso sexual', '2020-09-08 04:35:31', '2020-09-08 04:35:31'),
(43, '¿Presenta estrés?', 'si_no', 9, 1, 'El niño refiere sentimientos de\r\nestrés', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(44, '¿Presenta trastorno de sueño?', 'si_no', 9, 1, 'Trastorno de sueño', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(45, '¿Presenta depresión?', 'si_no', 9, 1, 'Identificación de depresión en los escolares', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(46, '¿Sufre de ansiedad?', 'si_no', 9, 1, 'Identificación de ansiedad en los escolares', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(47, '¿Deja acumular las tareas?', 'si_no', 9, 1, 'Deja acumular tareas', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(48, '¿Presenta problemas de concentración?', 'si_no', 9, 1, 'Identificación de problemas de concentración en los escolares', '2020-09-08 05:00:02', '2020-09-08 05:00:02'),
(49, '¿Cual es la religion?', 'select', 10, 1, 'religión de los escolares', '2020-09-08 05:27:51', '2020-09-08 05:27:51'),
(50, '¿presento el carnet de vacunas?', 'si_no', 11, 1, 'fue entregado en carnet de vacunas\r\nen el colegio', '2020-09-08 05:27:51', '2020-09-08 05:27:51'),
(51, '¿Presenta algun dolor?', 'si_no', 12, 1, 'Presencia de dolores ', '2020-09-08 05:27:51', '2020-09-08 05:27:51'),
(52, '¿Presenta problemas de desarrollo?', 'si_no', 13, 1, 'Problemas de desarrollo ', '2020-09-08 05:27:51', '2020-09-08 05:27:51'),
(53, '¿Cuales son las practicas de cuidado?', 'select', 1, 1, 'Frecuencia de las prácticas de\r\ncuidado cuando el menor se enferma \r\n', '2020-09-08 13:32:30', '2020-09-08 13:32:30'),
(54, '¿Quien lo cuida cuando decae la salud?', 'select', 1, 1, 'Frecuencia cuidadores del menor cuando decae la\r\nsalud', '2020-09-08 13:32:30', '2020-09-08 13:32:30'),
(58, '¿El estudiante presenta algún problema de crecimiento?', 'crecimiento', 2, 1, 'Problemas de crecimiento', '2020-09-15 19:19:55', '2020-09-15 19:19:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prevencions`
--

CREATE TABLE `prevencions` (
  `id` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `contenido` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `url` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `privado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prevencions`
--

INSERT INTO `prevencions` (`id`, `tipo`, `titulo`, `contenido`, `updated_at`, `created_at`, `url`, `id_user`, `privado`) VALUES
(2, 1, 'prueba', 'asasas', '2020-09-15 20:00:12', '2020-09-15 20:00:12', 'future.pdf', 54, 0),
(3, 1, 'higiene en los ojos', 'Se explica....', '2020-09-30 13:40:01', '2020-09-30 13:40:01', '1.pdf', 54, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id` int(11) NOT NULL,
  `respuestas` varchar(255) NOT NULL,
  `id_preguntas` int(11) NOT NULL,
  `id_formulario_ficha_salud` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id`, `respuestas`, `id_preguntas`, `id_formulario_ficha_salud`, `created_at`, `updated_at`) VALUES
(199, 'Inadecuado', 1, 52, '2020-09-12 04:39:10', '2020-09-12 04:39:10'),
(200, 'Adecuado', 2, 52, '2020-09-12 04:39:11', '2020-09-12 04:39:11'),
(201, 'Adecuado', 3, 52, '2020-09-12 04:39:11', '2020-09-12 04:39:11'),
(202, 'Adecuado', 4, 52, '2020-09-12 04:39:11', '2020-09-12 04:39:11'),
(203, 'Inadecuado', 5, 52, '2020-09-12 04:39:12', '2020-09-12 04:39:12'),
(204, 'Inadecuado', 6, 52, '2020-09-12 04:39:12', '2020-09-12 04:39:12'),
(205, 'No', 7, 52, '2020-09-12 04:39:12', '2020-09-12 04:39:12'),
(206, 'Si', 8, 52, '2020-09-12 04:39:13', '2020-09-12 04:39:13'),
(207, 'Lo cuidan en la casa', 53, 52, '2020-09-12 04:39:13', '2020-09-12 04:39:13'),
(208, 'Cuidador', 54, 52, '2020-09-12 04:39:13', '2020-09-12 04:39:13'),
(209, 'Algunos días (menos de 6 días)', 9, 52, '2020-09-12 04:39:13', '2020-09-12 04:39:13'),
(210, 'Algunos días (menos de 6 días)', 10, 52, '2020-09-12 04:39:13', '2020-09-12 04:39:13'),
(211, 'Algunos días (menos de 6 días)', 11, 52, '2020-09-12 04:39:14', '2020-09-12 04:39:14'),
(212, 'Algunos días (menos de 6 días)', 12, 52, '2020-09-12 04:39:14', '2020-09-12 04:39:14'),
(213, 'Algunos días (menos de 6 días)', 13, 52, '2020-09-12 04:39:14', '2020-09-12 04:39:14'),
(214, 'No', 14, 52, '2020-09-12 04:39:14', '2020-09-12 04:39:14'),
(215, 'No', 15, 52, '2020-09-12 04:39:14', '2020-09-12 04:39:14'),
(216, 'Si', 16, 52, '2020-09-12 04:39:15', '2020-09-12 04:39:15'),
(217, 'Si', 17, 52, '2020-09-12 04:39:15', '2020-09-12 04:39:15'),
(218, '2 veces', 18, 52, '2020-09-12 04:39:15', '2020-09-12 04:39:15'),
(219, 'No', 19, 52, '2020-09-12 04:39:15', '2020-09-12 04:39:15'),
(220, 'No', 20, 52, '2020-09-12 04:39:16', '2020-09-12 04:39:16'),
(221, 'Si', 21, 52, '2020-09-12 04:39:16', '2020-09-12 04:39:16'),
(222, 'Si', 22, 52, '2020-09-12 04:39:16', '2020-09-12 04:39:16'),
(223, 'No', 23, 52, '2020-09-12 04:39:16', '2020-09-12 04:39:16'),
(224, 'No', 24, 52, '2020-09-12 04:39:16', '2020-09-12 04:39:16'),
(225, '6 Horas', 25, 52, '2020-09-12 04:39:17', '2020-09-12 04:39:17'),
(226, 'Manualidades', 26, 52, '2020-09-12 04:39:17', '2020-09-12 04:39:17'),
(227, 'Si', 27, 52, '2020-09-12 04:39:17', '2020-09-12 04:39:17'),
(228, 'Si', 28, 52, '2020-09-12 04:39:17', '2020-09-12 04:39:17'),
(229, 'No', 29, 52, '2020-09-12 04:39:18', '2020-09-12 04:39:18'),
(230, 'No', 30, 52, '2020-09-12 04:39:18', '2020-09-12 04:39:18'),
(231, 'No', 31, 52, '2020-09-12 04:39:18', '2020-09-12 04:39:18'),
(232, 'Relata expresiones negativas hacia sí mismo', 32, 52, '2020-09-12 04:39:18', '2020-09-12 04:39:18'),
(233, 'soy alegre', 33, 52, '2020-09-12 04:39:19', '2020-09-12 04:39:19'),
(234, 'No', 34, 52, '2020-09-12 04:39:19', '2020-09-12 04:39:19'),
(235, 'Padres', 35, 52, '2020-09-12 04:39:19', '2020-09-12 04:39:19'),
(236, 'Si', 36, 52, '2020-09-12 04:39:19', '2020-09-12 04:39:19'),
(237, 'No los dejan salir o jugar', 37, 52, '2020-09-12 04:39:19', '2020-09-12 04:39:19'),
(238, 'Si', 38, 52, '2020-09-12 04:39:20', '2020-09-12 04:39:20'),
(239, 'Si', 39, 52, '2020-09-12 04:39:20', '2020-09-12 04:39:20'),
(240, 'No', 40, 52, '2020-09-12 04:39:20', '2020-09-12 04:39:20'),
(241, 'Si', 41, 52, '2020-09-12 04:39:21', '2020-09-12 04:39:21'),
(242, 'Si', 42, 52, '2020-09-12 04:39:21', '2020-09-12 04:39:21'),
(243, 'No', 43, 52, '2020-09-12 04:39:21', '2020-09-12 04:39:21'),
(244, 'Si', 44, 52, '2020-09-12 04:39:21', '2020-09-12 04:39:21'),
(245, 'Si', 45, 52, '2020-09-12 04:39:21', '2020-09-12 04:39:21'),
(246, 'Si', 46, 52, '2020-09-12 04:39:22', '2020-09-12 04:39:22'),
(247, 'Si', 47, 52, '2020-09-12 04:39:22', '2020-09-12 04:39:22'),
(248, 'Si', 48, 52, '2020-09-12 04:39:22', '2020-09-12 04:39:22'),
(249, 'Cristiano', 49, 52, '2020-09-12 04:39:22', '2020-09-12 04:39:22'),
(250, 'No', 50, 52, '2020-09-12 04:39:23', '2020-09-12 04:39:23'),
(251, 'No', 51, 52, '2020-09-12 04:39:23', '2020-09-12 04:39:23'),
(252, 'No', 52, 52, '2020-09-12 04:39:23', '2020-09-12 04:39:23'),
(253, 'Inadecuado', 1, 53, '2020-09-12 04:41:42', '2020-09-12 04:41:42'),
(254, 'Adecuado', 2, 53, '2020-09-12 04:41:42', '2020-09-12 04:41:42'),
(255, 'Adecuado', 3, 53, '2020-09-12 04:41:42', '2020-09-12 04:41:42'),
(256, 'Adecuado', 4, 53, '2020-09-12 04:41:43', '2020-09-12 04:41:43'),
(257, 'Inadecuado', 5, 53, '2020-09-12 04:41:43', '2020-09-12 04:41:43'),
(258, 'Inadecuado', 6, 53, '2020-09-12 04:41:43', '2020-09-12 04:41:43'),
(259, 'No', 7, 53, '2020-09-12 04:41:44', '2020-09-12 04:41:44'),
(260, 'Si', 8, 53, '2020-09-12 04:41:44', '2020-09-12 04:41:44'),
(261, 'Lo cuidan en la casa', 53, 53, '2020-09-12 04:41:44', '2020-09-12 04:41:44'),
(262, 'Cuidador', 54, 53, '2020-09-12 04:41:45', '2020-09-12 04:41:45'),
(263, 'Algunos días (menos de 6 días)', 9, 53, '2020-09-12 04:41:45', '2020-09-12 04:41:45'),
(264, 'Algunos días (menos de 6 días)', 10, 53, '2020-09-12 04:41:45', '2020-09-12 04:41:45'),
(265, 'Algunos días (menos de 6 días)', 11, 53, '2020-09-12 04:41:45', '2020-09-12 04:41:45'),
(266, 'Algunos días (menos de 6 días)', 12, 53, '2020-09-12 04:41:46', '2020-09-12 04:41:46'),
(267, 'Algunos días (menos de 6 días)', 13, 53, '2020-09-12 04:41:46', '2020-09-12 04:41:46'),
(268, 'No', 14, 53, '2020-09-12 04:41:46', '2020-09-12 04:41:46'),
(269, 'No', 15, 53, '2020-09-12 04:41:46', '2020-09-12 04:41:46'),
(270, 'Si', 16, 53, '2020-09-12 04:41:46', '2020-09-12 04:41:46'),
(271, 'Si', 17, 53, '2020-09-12 04:41:47', '2020-09-12 04:41:47'),
(272, '2 veces', 18, 53, '2020-09-12 04:41:47', '2020-09-12 04:41:47'),
(273, 'No', 19, 53, '2020-09-12 04:41:47', '2020-09-12 04:41:47'),
(274, 'No', 20, 53, '2020-09-12 04:41:47', '2020-09-12 04:41:47'),
(275, 'Si', 21, 53, '2020-09-12 04:41:47', '2020-09-12 04:41:47'),
(276, 'Si', 22, 53, '2020-09-12 04:41:48', '2020-09-12 04:41:48'),
(277, 'No', 23, 53, '2020-09-12 04:41:48', '2020-09-12 04:41:48'),
(278, 'No', 24, 53, '2020-09-12 04:41:48', '2020-09-12 04:41:48'),
(279, '6 Horas', 25, 53, '2020-09-12 04:41:48', '2020-09-12 04:41:48'),
(280, 'Manualidades', 26, 53, '2020-09-12 04:41:49', '2020-09-12 04:41:49'),
(281, 'Si', 27, 53, '2020-09-12 04:41:49', '2020-09-12 04:41:49'),
(282, 'Si', 28, 53, '2020-09-12 04:41:49', '2020-09-12 04:41:49'),
(283, 'No', 29, 53, '2020-09-12 04:41:49', '2020-09-12 04:41:49'),
(284, 'No', 30, 53, '2020-09-12 04:41:50', '2020-09-12 04:41:50'),
(285, 'No', 31, 53, '2020-09-12 04:41:50', '2020-09-12 04:41:50'),
(286, 'Relata expresiones negativas hacia sí mismo', 32, 53, '2020-09-12 04:41:50', '2020-09-12 04:41:50'),
(287, 'soy alegre', 33, 53, '2020-09-12 04:41:50', '2020-09-12 04:41:50'),
(288, 'No', 34, 53, '2020-09-12 04:41:51', '2020-09-12 04:41:51'),
(289, 'Padres', 35, 53, '2020-09-12 04:41:51', '2020-09-12 04:41:51'),
(290, 'Si', 36, 53, '2020-09-12 04:41:51', '2020-09-12 04:41:51'),
(291, 'No los dejan salir o jugar', 37, 53, '2020-09-12 04:41:51', '2020-09-12 04:41:51'),
(292, 'Si', 38, 53, '2020-09-12 04:41:52', '2020-09-12 04:41:52'),
(293, 'Si', 39, 53, '2020-09-12 04:41:52', '2020-09-12 04:41:52'),
(294, 'No', 40, 53, '2020-09-12 04:41:52', '2020-09-12 04:41:52'),
(295, 'Si', 41, 53, '2020-09-12 04:41:52', '2020-09-12 04:41:52'),
(296, 'Si', 42, 53, '2020-09-12 04:41:52', '2020-09-12 04:41:52'),
(297, 'No', 43, 53, '2020-09-12 04:41:53', '2020-09-12 04:41:53'),
(298, 'Si', 44, 53, '2020-09-12 04:41:53', '2020-09-12 04:41:53'),
(299, 'Si', 45, 53, '2020-09-12 04:41:53', '2020-09-12 04:41:53'),
(300, 'Si', 46, 53, '2020-09-12 04:41:54', '2020-09-12 04:41:54'),
(301, 'Si', 47, 53, '2020-09-12 04:41:54', '2020-09-12 04:41:54'),
(302, 'Si', 48, 53, '2020-09-12 04:41:54', '2020-09-12 04:41:54'),
(303, 'Cristiano', 49, 53, '2020-09-12 04:41:54', '2020-09-12 04:41:54'),
(304, 'No', 50, 53, '2020-09-12 04:41:55', '2020-09-12 04:41:55'),
(305, 'No', 51, 53, '2020-09-12 04:41:55', '2020-09-12 04:41:55'),
(306, 'No', 52, 53, '2020-09-12 04:41:55', '2020-09-12 04:41:55'),
(307, 'Sucios', 1, 54, '2020-09-16 01:45:26', '2020-09-16 01:45:26'),
(308, 'Inadecuado', 2, 54, '2020-09-16 01:45:26', '2020-09-16 01:45:26'),
(309, 'Inadecuado', 6, 54, '2020-09-16 01:45:26', '2020-09-16 01:45:26'),
(310, 'Inadecuado', 4, 54, '2020-09-16 01:45:27', '2020-09-16 01:45:27'),
(311, 'Inadecuado', 3, 54, '2020-09-16 01:45:27', '2020-09-16 01:45:27'),
(312, 'Inadecuado', 5, 54, '2020-09-16 01:45:27', '2020-09-16 01:45:27'),
(313, 'Si', 7, 54, '2020-09-16 01:45:27', '2020-09-16 01:45:27'),
(314, 'Si', 8, 54, '2020-09-16 01:45:27', '2020-09-16 01:45:27'),
(315, 'Lo llevan al rezandero', 53, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(316, 'Tíos', 54, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(317, 'Casi nunca (1 o 2 días)', 9, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(318, 'Casi nunca (1 o 2 días)', 10, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(319, 'Casi nunca (1 o 2 días)', 11, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(320, 'Casi nunca (1 o 2 días)', 12, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(321, 'Casi nunca (1 o 2 días)', 13, 54, '2020-09-16 01:45:28', '2020-09-16 01:45:28'),
(322, 'Si', 14, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(323, 'Si', 15, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(324, 'Si', 16, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(325, 'Si', 17, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(326, '6 veces', 18, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(327, 'Sobrepeso grado II (Riesgo)', 58, 54, '2020-09-16 01:45:29', '2020-09-16 01:45:29'),
(328, 'Si', 19, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(329, 'Si', 20, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(330, 'Si', 21, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(331, 'Si', 22, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(332, 'Si', 23, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(333, 'Si', 24, 54, '2020-09-16 01:45:30', '2020-09-16 01:45:30'),
(334, '8 Horas', 25, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(335, 'Manualidades', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(336, 'Televisión', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(337, 'Leer', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(338, 'Escuchar música', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(339, 'Patinaje', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(340, 'Baloncesto', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(341, 'Estudiar', 26, 54, '2020-09-16 01:45:31', '2020-09-16 01:45:31'),
(342, 'Jugar', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(343, 'Fútbol', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(344, 'Celular', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(345, 'Bailar', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(346, 'Pintar', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(347, 'Dibujar', 26, 54, '2020-09-16 01:45:32', '2020-09-16 01:45:32'),
(348, 'Si', 27, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(349, 'Si', 28, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(350, 'Si', 29, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(351, 'Si', 30, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(352, 'Si', 31, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(353, 'Relata expresiones negativas hacia sí mismo', 32, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(354, 'soy alegre', 33, 54, '2020-09-16 01:45:33', '2020-09-16 01:45:33'),
(355, 'me gusta como soy', 33, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(356, 'amistoso(a)', 33, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(357, 'amoroso(a)', 33, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(358, 'feliz', 33, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(359, 'Ninguna', 33, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(360, 'Si', 34, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(361, 'Otros', 35, 54, '2020-09-16 01:45:34', '2020-09-16 01:45:34'),
(362, 'Si', 36, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(363, 'No los dejan salir o jugar', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(364, 'Solo lo regañan', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(365, 'Lo reprenden con golpes', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(366, 'Le quita lo que mas les gusta', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(367, 'No lo dejan ver tv o utilizar el celular', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(368, 'Lo ponen a hacer aseo', 37, 54, '2020-09-16 01:45:35', '2020-09-16 01:45:35'),
(369, 'No aplica', 37, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(370, 'Si', 38, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(371, 'Si', 39, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(372, 'Si', 40, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(373, 'Si', 41, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(374, 'Si', 42, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(375, 'Si', 43, 54, '2020-09-16 01:45:36', '2020-09-16 01:45:36'),
(376, 'Si', 44, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(377, 'Si', 45, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(378, 'Si', 46, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(379, 'Si', 47, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(380, 'Si', 48, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(381, 'Ninguna', 49, 54, '2020-09-16 01:45:37', '2020-09-16 01:45:37'),
(382, 'Si', 50, 54, '2020-09-16 01:45:38', '2020-09-16 01:45:38'),
(383, 'Si', 51, 54, '2020-09-16 01:45:38', '2020-09-16 01:45:38'),
(384, 'Si', 52, 54, '2020-09-16 01:45:38', '2020-09-16 01:45:38'),
(385, 'Sucios', 1, 55, '2020-09-16 01:55:47', '2020-09-16 01:55:47'),
(386, 'Inadecuado', 5, 55, '2020-09-16 01:55:47', '2020-09-16 01:55:47'),
(387, 'Inadecuado', 2, 55, '2020-09-16 01:55:47', '2020-09-16 01:55:47'),
(388, 'Inadecuado', 3, 55, '2020-09-16 01:55:47', '2020-09-16 01:55:47'),
(389, 'Inadecuado', 4, 55, '2020-09-16 01:55:47', '2020-09-16 01:55:47'),
(390, 'Inadecuado', 6, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(391, 'Si', 7, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(392, 'Si', 8, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(393, 'Lo llevan al rezandero', 53, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(394, 'Tíos', 54, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(395, 'Casi nunca (1 o 2 días)', 9, 55, '2020-09-16 01:55:48', '2020-09-16 01:55:48'),
(396, 'Casi nunca (1 o 2 días)', 10, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(397, 'Casi nunca (1 o 2 días)', 11, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(398, 'Casi nunca (1 o 2 días)', 12, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(399, 'Casi nunca (1 o 2 días)', 13, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(400, 'Si', 14, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(401, 'Si', 15, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(402, 'Si', 16, 55, '2020-09-16 01:55:49', '2020-09-16 01:55:49'),
(403, 'Si', 17, 55, '2020-09-16 01:55:50', '2020-09-16 01:55:50'),
(404, '6 veces', 18, 55, '2020-09-16 01:55:50', '2020-09-16 01:55:50'),
(405, 'Sobrepeso grado I (Riesgo)', 58, 55, '2020-09-16 01:55:50', '2020-09-16 01:55:50'),
(406, 'Si', 19, 55, '2020-09-16 01:55:50', '2020-09-16 01:55:50'),
(407, 'Si', 20, 55, '2020-09-16 01:55:50', '2020-09-16 01:55:50'),
(408, 'Si', 21, 55, '2020-09-16 01:55:51', '2020-09-16 01:55:51'),
(409, 'Si', 22, 55, '2020-09-16 01:55:51', '2020-09-16 01:55:51'),
(410, 'Si', 23, 55, '2020-09-16 01:55:51', '2020-09-16 01:55:51'),
(411, 'Si', 24, 55, '2020-09-16 01:55:51', '2020-09-16 01:55:51'),
(412, '8 Horas', 25, 55, '2020-09-16 01:55:51', '2020-09-16 01:55:51'),
(413, 'Manualidades', 26, 55, '2020-09-16 01:55:52', '2020-09-16 01:55:52'),
(414, 'Televisión', 26, 55, '2020-09-16 01:55:52', '2020-09-16 01:55:52'),
(415, 'Leer', 26, 55, '2020-09-16 01:55:52', '2020-09-16 01:55:52'),
(416, 'Escuchar música', 26, 55, '2020-09-16 01:55:52', '2020-09-16 01:55:52'),
(417, 'Sucios', 1, 56, '2020-09-16 01:56:59', '2020-09-16 01:56:59'),
(418, 'Inadecuado', 6, 56, '2020-09-16 01:56:59', '2020-09-16 01:56:59'),
(419, 'Inadecuado', 5, 56, '2020-09-16 01:56:59', '2020-09-16 01:56:59'),
(420, 'Inadecuado', 4, 56, '2020-09-16 01:56:59', '2020-09-16 01:56:59'),
(421, 'Inadecuado', 3, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(422, 'Inadecuado', 2, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(423, 'Si', 7, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(424, 'Si', 8, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(425, 'Lo llevan al rezandero', 53, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(426, 'Tíos', 54, 56, '2020-09-16 01:57:00', '2020-09-16 01:57:00'),
(427, 'Casi nunca (1 o 2 días)', 9, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(428, 'Casi nunca (1 o 2 días)', 10, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(429, 'Casi nunca (1 o 2 días)', 11, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(430, 'Casi nunca (1 o 2 días)', 12, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(431, 'Casi nunca (1 o 2 días)', 13, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(432, 'Si', 14, 56, '2020-09-16 01:57:01', '2020-09-16 01:57:01'),
(433, 'Si', 15, 56, '2020-09-16 01:57:02', '2020-09-16 01:57:02'),
(434, 'Si', 16, 56, '2020-09-16 01:57:02', '2020-09-16 01:57:02'),
(435, 'Si', 17, 56, '2020-09-16 01:57:02', '2020-09-16 01:57:02'),
(436, '6 veces', 18, 56, '2020-09-16 01:57:02', '2020-09-16 01:57:02'),
(437, 'Bajo peso', 58, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(438, 'Si', 19, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(439, 'Si', 20, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(440, 'Si', 21, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(441, 'Si', 22, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(442, 'Si', 23, 56, '2020-09-16 01:57:03', '2020-09-16 01:57:03'),
(443, 'Si', 24, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(444, '8 Horas', 25, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(445, 'Manualidades', 26, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(446, 'Televisión', 26, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(447, 'Leer', 26, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(448, 'Escuchar música', 26, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(449, 'Patinaje', 26, 56, '2020-09-16 01:57:04', '2020-09-16 01:57:04'),
(450, 'Baloncesto', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(451, 'Estudiar', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(452, 'Jugar', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(453, 'Fútbol', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(454, 'Celular', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(455, 'Bailar', 26, 56, '2020-09-16 01:57:05', '2020-09-16 01:57:05'),
(456, 'Pintar', 26, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(457, 'Dibujar', 26, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(458, 'Si', 27, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(459, 'Si', 28, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(460, 'Si', 29, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(461, 'Si', 30, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(462, 'Si', 31, 56, '2020-09-16 01:57:06', '2020-09-16 01:57:06'),
(463, 'Relata expresiones negativas hacia sí mismo', 32, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(464, 'soy alegre', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(465, 'me gusta como soy', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(466, 'amistoso(a)', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(467, 'amoroso(a)', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(468, 'feliz', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(469, 'Ninguna', 33, 56, '2020-09-16 01:57:07', '2020-09-16 01:57:07'),
(470, 'Si', 34, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(471, 'Otros', 35, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(472, 'Si', 36, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(473, 'No los dejan salir o jugar', 37, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(474, 'Solo lo regañan', 37, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(475, 'Lo reprenden con golpes', 37, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(476, 'Le quita lo que mas les gusta', 37, 56, '2020-09-16 01:57:08', '2020-09-16 01:57:08'),
(477, 'No lo dejan ver tv o utilizar el celular', 37, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(478, 'Lo ponen a hacer aseo', 37, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(479, 'No aplica', 37, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(480, 'Si', 38, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(481, 'Si', 39, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(482, 'Si', 40, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(483, 'Si', 41, 56, '2020-09-16 01:57:09', '2020-09-16 01:57:09'),
(484, 'Si', 42, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(485, 'Si', 43, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(486, 'Si', 44, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(487, 'Si', 45, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(488, 'Si', 46, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(489, 'Si', 47, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(490, 'Si', 48, 56, '2020-09-16 01:57:10', '2020-09-16 01:57:10'),
(491, 'Ninguna', 49, 56, '2020-09-16 01:57:11', '2020-09-16 01:57:11'),
(492, 'Si', 50, 56, '2020-09-16 01:57:11', '2020-09-16 01:57:11'),
(493, 'Si', 51, 56, '2020-09-16 01:57:11', '2020-09-16 01:57:11'),
(494, 'Si', 52, 56, '2020-09-16 01:57:11', '2020-09-16 01:57:11'),
(495, 'Sucios', 1, 57, '2020-09-16 01:59:47', '2020-09-16 01:59:47'),
(496, 'Inadecuado', 6, 57, '2020-09-16 01:59:47', '2020-09-16 01:59:47'),
(497, 'Inadecuado', 5, 57, '2020-09-16 01:59:47', '2020-09-16 01:59:47'),
(498, 'Inadecuado', 4, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(499, 'Inadecuado', 3, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(500, 'Inadecuado', 2, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(501, 'Si', 7, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(502, 'Si', 8, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(503, 'Lo llevan al rezandero', 53, 57, '2020-09-16 01:59:48', '2020-09-16 01:59:48'),
(504, 'Tíos', 54, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(505, 'Casi nunca (1 o 2 días)', 9, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(506, 'Casi nunca (1 o 2 días)', 10, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(507, 'Casi nunca (1 o 2 días)', 11, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(508, 'Casi nunca (1 o 2 días)', 12, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(509, 'Casi nunca (1 o 2 días)', 13, 57, '2020-09-16 01:59:49', '2020-09-16 01:59:49'),
(510, 'Si', 14, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(511, 'Si', 15, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(512, 'Si', 16, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(513, 'Si', 17, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(514, '6 veces', 18, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(515, 'Sobrepeso grado II (Riesgo)', 58, 57, '2020-09-16 01:59:50', '2020-09-16 01:59:50'),
(516, 'Si', 19, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(517, 'Si', 20, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(518, 'Si', 21, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(519, 'Si', 22, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(520, 'Si', 23, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(521, 'Si', 24, 57, '2020-09-16 01:59:51', '2020-09-16 01:59:51'),
(522, '8 Horas', 25, 57, '2020-09-16 01:59:52', '2020-09-16 01:59:52'),
(523, 'Manualidades', 26, 57, '2020-09-16 01:59:52', '2020-09-16 01:59:52'),
(524, 'Televisión', 26, 57, '2020-09-16 01:59:52', '2020-09-16 01:59:52'),
(525, 'Leer', 26, 57, '2020-09-16 01:59:52', '2020-09-16 01:59:52'),
(526, 'Escuchar música', 26, 57, '2020-09-16 01:59:52', '2020-09-16 01:59:52'),
(527, 'Patinaje', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(528, 'Baloncesto', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(529, 'Estudiar', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(530, 'Jugar', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(531, 'Fútbol', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(532, 'Celular', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(533, 'Bailar', 26, 57, '2020-09-16 01:59:53', '2020-09-16 01:59:53'),
(534, 'Pintar', 26, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(535, 'Dibujar', 26, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(536, 'Si', 27, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(537, 'Si', 28, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(538, 'Si', 29, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(539, 'Si', 30, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(540, 'Si', 31, 57, '2020-09-16 01:59:54', '2020-09-16 01:59:54'),
(541, 'Relata expresiones negativas hacia sí mismo', 32, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(542, 'soy alegre', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(543, 'me gusta como soy', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(544, 'amistoso(a)', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(545, 'amoroso(a)', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(546, 'feliz', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(547, 'Ninguna', 33, 57, '2020-09-16 01:59:55', '2020-09-16 01:59:55'),
(548, 'Si', 34, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(549, 'Otros', 35, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(550, 'Si', 36, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(551, 'No los dejan salir o jugar', 37, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(552, 'Solo lo regañan', 37, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(553, 'Lo reprenden con golpes', 37, 57, '2020-09-16 01:59:56', '2020-09-16 01:59:56'),
(554, 'Le quita lo que mas les gusta', 37, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(555, 'No lo dejan ver tv o utilizar el celular', 37, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(556, 'Lo ponen a hacer aseo', 37, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(557, 'No aplica', 37, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(558, 'Si', 38, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(559, 'Si', 39, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(560, 'Si', 40, 57, '2020-09-16 01:59:57', '2020-09-16 01:59:57'),
(561, 'Si', 41, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(562, 'Si', 42, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(563, 'Si', 43, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(564, 'Si', 44, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(565, 'Si', 45, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(566, 'Si', 46, 57, '2020-09-16 01:59:58', '2020-09-16 01:59:58'),
(567, 'Si', 47, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(568, 'Si', 48, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(569, 'Ninguna', 49, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(570, 'Si', 50, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(571, 'Si', 51, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(572, 'Si', 52, 57, '2020-09-16 01:59:59', '2020-09-16 01:59:59'),
(573, 'Sucios', 1, 58, '2020-09-16 02:02:04', '2020-09-16 02:02:04'),
(574, 'Inadecuado', 2, 58, '2020-09-16 02:02:05', '2020-09-16 02:02:05'),
(575, 'Inadecuado', 4, 58, '2020-09-16 02:02:05', '2020-09-16 02:02:05'),
(576, 'Inadecuado', 3, 58, '2020-09-16 02:02:05', '2020-09-16 02:02:05'),
(577, 'Inadecuado', 5, 58, '2020-09-16 02:02:05', '2020-09-16 02:02:05'),
(578, 'Inadecuado', 6, 58, '2020-09-16 02:02:06', '2020-09-16 02:02:06'),
(579, 'Si', 7, 58, '2020-09-16 02:02:06', '2020-09-16 02:02:06'),
(580, 'Si', 8, 58, '2020-09-16 02:02:06', '2020-09-16 02:02:06'),
(581, 'Lo llevan al rezandero', 53, 58, '2020-09-16 02:02:06', '2020-09-16 02:02:06'),
(582, 'Tíos', 54, 58, '2020-09-16 02:02:06', '2020-09-16 02:02:06'),
(583, 'Casi nunca (1 o 2 días)', 9, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(584, 'Casi nunca (1 o 2 días)', 10, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(585, 'Casi nunca (1 o 2 días)', 11, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(586, 'Casi nunca (1 o 2 días)', 12, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(587, 'Casi nunca (1 o 2 días)', 13, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(588, 'Si', 14, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(589, 'Si', 15, 58, '2020-09-16 02:02:07', '2020-09-16 02:02:07'),
(590, 'Si', 16, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(591, 'Si', 17, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(592, '6 veces', 18, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(593, 'Sobrepeso grado II (Riesgo)', 58, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(594, 'Si', 19, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(595, 'Si', 20, 58, '2020-09-16 02:02:08', '2020-09-16 02:02:08'),
(596, 'Si', 21, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(597, 'Si', 22, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(598, 'Si', 23, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(599, 'Si', 24, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(600, '8 Horas', 25, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(601, 'Manualidades', 26, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(602, 'Televisión', 26, 58, '2020-09-16 02:02:09', '2020-09-16 02:02:09'),
(603, 'Leer', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(604, 'Escuchar música', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(605, 'Patinaje', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(606, 'Baloncesto', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(607, 'Estudiar', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(608, 'Jugar', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(609, 'Fútbol', 26, 58, '2020-09-16 02:02:10', '2020-09-16 02:02:10'),
(610, 'Celular', 26, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(611, 'Bailar', 26, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(612, 'Pintar', 26, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(613, 'Dibujar', 26, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(614, 'Si', 27, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(615, 'Si', 28, 58, '2020-09-16 02:02:11', '2020-09-16 02:02:11'),
(616, 'Si', 29, 58, '2020-09-16 02:02:12', '2020-09-16 02:02:12'),
(617, 'Si', 30, 58, '2020-09-16 02:02:12', '2020-09-16 02:02:12'),
(618, 'Si', 31, 58, '2020-09-16 02:02:12', '2020-09-16 02:02:12'),
(619, 'Relata expresiones negativas hacia sí mismo', 32, 58, '2020-09-16 02:02:12', '2020-09-16 02:02:12'),
(620, 'soy alegre', 33, 58, '2020-09-16 02:02:12', '2020-09-16 02:02:12'),
(621, 'me gusta como soy', 33, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(622, 'amistoso(a)', 33, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(623, 'amoroso(a)', 33, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(624, 'feliz', 33, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(625, 'Ninguna', 33, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(626, 'Si', 34, 58, '2020-09-16 02:02:13', '2020-09-16 02:02:13'),
(627, 'Otros', 35, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(628, 'Si', 36, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(629, 'No los dejan salir o jugar', 37, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(630, 'Solo lo regañan', 37, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(631, 'Lo reprenden con golpes', 37, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(632, 'Le quita lo que mas les gusta', 37, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(633, 'No lo dejan ver tv o utilizar el celular', 37, 58, '2020-09-16 02:02:14', '2020-09-16 02:02:14'),
(634, 'Lo ponen a hacer aseo', 37, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(635, 'No aplica', 37, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(636, 'Si', 38, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(637, 'Si', 39, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(638, 'Si', 40, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(639, 'Si', 41, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(640, 'Si', 42, 58, '2020-09-16 02:02:15', '2020-09-16 02:02:15'),
(641, 'Si', 43, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(642, 'Si', 44, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(643, 'Si', 45, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(644, 'Si', 46, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(645, 'Si', 47, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(646, 'Si', 48, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(647, 'Ninguna', 49, 58, '2020-09-16 02:02:16', '2020-09-16 02:02:16'),
(648, 'Si', 50, 58, '2020-09-16 02:02:17', '2020-09-16 02:02:17'),
(649, 'Si', 51, 58, '2020-09-16 02:02:17', '2020-09-16 02:02:17'),
(650, 'Si', 52, 58, '2020-09-16 02:02:17', '2020-09-16 02:02:17'),
(651, 'Sucios', 1, 59, '2020-09-16 02:03:49', '2020-09-16 02:03:49'),
(652, 'Inadecuado', 6, 59, '2020-09-16 02:03:49', '2020-09-16 02:03:49'),
(653, 'Inadecuado', 5, 59, '2020-09-16 02:03:50', '2020-09-16 02:03:50'),
(654, 'Inadecuado', 4, 59, '2020-09-16 02:03:50', '2020-09-16 02:03:50'),
(655, 'Inadecuado', 3, 59, '2020-09-16 02:03:50', '2020-09-16 02:03:50'),
(656, 'Inadecuado', 2, 59, '2020-09-16 02:03:50', '2020-09-16 02:03:50'),
(657, 'Si', 7, 59, '2020-09-16 02:03:50', '2020-09-16 02:03:50'),
(658, 'Si', 8, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(659, 'Lo llevan al rezandero', 53, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(660, 'Tíos', 54, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(661, 'Casi nunca (1 o 2 días)', 9, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(662, 'Casi nunca (1 o 2 días)', 10, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(663, 'Casi nunca (1 o 2 días)', 11, 59, '2020-09-16 02:03:51', '2020-09-16 02:03:51'),
(664, 'Casi nunca (1 o 2 días)', 12, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(665, 'Casi nunca (1 o 2 días)', 13, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(666, 'Si', 14, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(667, 'Si', 15, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(668, 'Si', 16, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(669, 'Si', 17, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(670, '6 veces', 18, 59, '2020-09-16 02:03:52', '2020-09-16 02:03:52'),
(671, 'Sobrepeso grado I (Riesgo)', 58, 59, '2020-09-16 02:03:53', '2020-09-16 02:03:53'),
(672, 'Si', 19, 59, '2020-09-16 02:03:53', '2020-09-16 02:03:53'),
(673, 'Si', 20, 59, '2020-09-16 02:03:53', '2020-09-16 02:03:53'),
(674, 'Si', 21, 59, '2020-09-16 02:03:53', '2020-09-16 02:03:53'),
(675, 'Si', 22, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(676, 'Si', 23, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(677, 'Si', 24, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(678, '8 Horas', 25, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(679, 'Manualidades', 26, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(680, 'Televisión', 26, 59, '2020-09-16 02:03:54', '2020-09-16 02:03:54'),
(681, 'Leer', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(682, 'Escuchar música', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(683, 'Patinaje', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(684, 'Baloncesto', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(685, 'Estudiar', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(686, 'Jugar', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(687, 'Fútbol', 26, 59, '2020-09-16 02:03:55', '2020-09-16 02:03:55'),
(688, 'Celular', 26, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(689, 'Bailar', 26, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(690, 'Pintar', 26, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(691, 'Dibujar', 26, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(692, 'Si', 27, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(693, 'Si', 28, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(694, 'Si', 29, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(695, 'Si', 30, 59, '2020-09-16 02:03:56', '2020-09-16 02:03:56'),
(696, 'Si', 31, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(697, 'Relata expresiones negativas hacia sí mismo', 32, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(698, 'soy alegre', 33, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(699, 'me gusta como soy', 33, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(700, 'amistoso(a)', 33, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(701, 'amoroso(a)', 33, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(702, 'feliz', 33, 59, '2020-09-16 02:03:57', '2020-09-16 02:03:57'),
(703, 'Ninguna', 33, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(704, 'Si', 34, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(705, 'Otros', 35, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(706, 'Si', 36, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(707, 'No los dejan salir o jugar', 37, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(708, 'Solo lo regañan', 37, 59, '2020-09-16 02:03:58', '2020-09-16 02:03:58'),
(709, 'Lo reprenden con golpes', 37, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(710, 'Le quita lo que mas les gusta', 37, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(711, 'No lo dejan ver tv o utilizar el celular', 37, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(712, 'Lo ponen a hacer aseo', 37, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(713, 'No aplica', 37, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(714, 'Si', 38, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(715, 'Si', 39, 59, '2020-09-16 02:03:59', '2020-09-16 02:03:59'),
(716, 'Si', 40, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(717, 'Si', 41, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(718, 'Si', 42, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(719, 'Si', 43, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(720, 'Si', 44, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(721, 'Si', 45, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(722, 'Si', 46, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(723, 'Si', 47, 59, '2020-09-16 02:04:00', '2020-09-16 02:04:00'),
(724, 'Si', 48, 59, '2020-09-16 02:04:01', '2020-09-16 02:04:01'),
(725, 'Ninguna', 49, 59, '2020-09-16 02:04:01', '2020-09-16 02:04:01'),
(726, 'Si', 50, 59, '2020-09-16 02:04:01', '2020-09-16 02:04:01'),
(727, 'Si', 51, 59, '2020-09-16 02:04:01', '2020-09-16 02:04:01'),
(728, 'Si', 52, 59, '2020-09-16 02:04:01', '2020-09-16 02:04:01'),
(729, 'Sucios', 1, 60, '2020-09-16 02:04:38', '2020-09-16 02:04:38'),
(730, 'Inadecuado', 2, 60, '2020-09-16 02:04:38', '2020-09-16 02:04:38'),
(731, 'Si', 7, 60, '2020-09-16 02:04:38', '2020-09-16 02:04:38'),
(732, 'Inadecuado', 6, 60, '2020-09-16 02:04:38', '2020-09-16 02:04:38'),
(733, 'Inadecuado', 5, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(734, 'Inadecuado', 4, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(735, 'Inadecuado', 3, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(736, 'Si', 8, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(737, 'Lo llevan al rezandero', 53, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(738, 'Tíos', 54, 60, '2020-09-16 02:04:39', '2020-09-16 02:04:39'),
(739, 'Casi nunca (1 o 2 días)', 9, 60, '2020-09-16 02:04:40', '2020-09-16 02:04:40'),
(740, 'Casi nunca (1 o 2 días)', 10, 60, '2020-09-16 02:04:40', '2020-09-16 02:04:40'),
(741, 'Casi nunca (1 o 2 días)', 11, 60, '2020-09-16 02:04:40', '2020-09-16 02:04:40'),
(742, 'Casi nunca (1 o 2 días)', 12, 60, '2020-09-16 02:04:40', '2020-09-16 02:04:40'),
(743, 'Casi nunca (1 o 2 días)', 13, 60, '2020-09-16 02:04:40', '2020-09-16 02:04:40'),
(744, 'Si', 14, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(745, 'Si', 15, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(746, 'Si', 16, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(747, 'Si', 17, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(748, '6 veces', 18, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(749, 'Sobrepeso grado II (Riesgo)', 58, 60, '2020-09-16 02:04:41', '2020-09-16 02:04:41'),
(750, 'Si', 19, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(751, 'Si', 20, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(752, 'Si', 21, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(753, 'Si', 22, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(754, 'Si', 23, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(755, 'Si', 24, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(756, '8 Horas', 25, 60, '2020-09-16 02:04:42', '2020-09-16 02:04:42'),
(757, 'Manualidades', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(758, 'Televisión', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(759, 'Leer', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(760, 'Escuchar música', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(761, 'Patinaje', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(762, 'Baloncesto', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(763, 'Estudiar', 26, 60, '2020-09-16 02:04:43', '2020-09-16 02:04:43'),
(764, 'Jugar', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(765, 'Fútbol', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(766, 'Celular', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(767, 'Bailar', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(768, 'Pintar', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(769, 'Dibujar', 26, 60, '2020-09-16 02:04:44', '2020-09-16 02:04:44'),
(770, 'Si', 27, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(771, 'Si', 28, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(772, 'Si', 29, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(773, 'Si', 30, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(774, 'Si', 31, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(775, 'Relata expresiones negativas hacia sí mismo', 32, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(776, 'soy alegre', 33, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(777, 'me gusta como soy', 33, 60, '2020-09-16 02:04:45', '2020-09-16 02:04:45'),
(778, 'amistoso(a)', 33, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(779, 'amoroso(a)', 33, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(780, 'feliz', 33, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(781, 'Ninguna', 33, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(782, 'Si', 34, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(783, 'Otros', 35, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(784, 'Si', 36, 60, '2020-09-16 02:04:46', '2020-09-16 02:04:46'),
(785, 'No los dejan salir o jugar', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(786, 'Solo lo regañan', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(787, 'Lo reprenden con golpes', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(788, 'Le quita lo que mas les gusta', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(789, 'No lo dejan ver tv o utilizar el celular', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(790, 'Lo ponen a hacer aseo', 37, 60, '2020-09-16 02:04:47', '2020-09-16 02:04:47'),
(791, 'No aplica', 37, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(792, 'Si', 38, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(793, 'Si', 39, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(794, 'Si', 40, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(795, 'Si', 41, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(796, 'Si', 42, 60, '2020-09-16 02:04:48', '2020-09-16 02:04:48'),
(797, 'Si', 43, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(798, 'Si', 44, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(799, 'Si', 45, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(800, 'Si', 46, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(801, 'Si', 47, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(802, 'Si', 48, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(803, 'Ninguna', 49, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(804, 'Si', 50, 60, '2020-09-16 02:04:49', '2020-09-16 02:04:49'),
(805, 'Si', 51, 60, '2020-09-16 02:04:50', '2020-09-16 02:04:50'),
(806, 'Si', 52, 60, '2020-09-16 02:04:50', '2020-09-16 02:04:50'),
(807, 'Sucios', 1, 61, '2020-09-16 02:06:34', '2020-09-16 02:06:34'),
(808, 'Inadecuado', 5, 61, '2020-09-16 02:06:34', '2020-09-16 02:06:34'),
(809, 'Inadecuado', 4, 61, '2020-09-16 02:06:35', '2020-09-16 02:06:35'),
(810, 'Inadecuado', 3, 61, '2020-09-16 02:06:35', '2020-09-16 02:06:35'),
(811, 'Inadecuado', 2, 61, '2020-09-16 02:06:35', '2020-09-16 02:06:35'),
(812, 'Inadecuado', 6, 61, '2020-09-16 02:06:35', '2020-09-16 02:06:35'),
(813, 'Si', 7, 61, '2020-09-16 02:06:35', '2020-09-16 02:06:35'),
(814, 'Si', 8, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(815, 'Lo llevan al rezandero', 53, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(816, 'Tíos', 54, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(817, 'Casi nunca (1 o 2 días)', 9, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(818, 'Casi nunca (1 o 2 días)', 10, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(819, 'Casi nunca (1 o 2 días)', 11, 61, '2020-09-16 02:06:36', '2020-09-16 02:06:36'),
(820, 'Casi nunca (1 o 2 días)', 12, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(821, 'Casi nunca (1 o 2 días)', 13, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(822, 'Si', 14, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(823, 'Si', 15, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(824, 'Si', 16, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(825, 'Si', 17, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(826, '6 veces', 18, 61, '2020-09-16 02:06:37', '2020-09-16 02:06:37'),
(827, 'Peso normal', 58, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(828, 'Si', 19, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(829, 'Si', 20, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(830, 'Si', 21, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(831, 'Si', 22, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(832, 'Si', 23, 61, '2020-09-16 02:06:38', '2020-09-16 02:06:38'),
(833, 'Si', 24, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(834, '8 Horas', 25, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(835, 'Manualidades', 26, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(836, 'Televisión', 26, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(837, 'Leer', 26, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(838, 'Escuchar música', 26, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(839, 'Patinaje', 26, 61, '2020-09-16 02:06:39', '2020-09-16 02:06:39'),
(840, 'Baloncesto', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(841, 'Estudiar', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(842, 'Jugar', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(843, 'Fútbol', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(844, 'Celular', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(845, 'Bailar', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(846, 'Pintar', 26, 61, '2020-09-16 02:06:40', '2020-09-16 02:06:40'),
(847, 'Dibujar', 26, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(848, 'Si', 27, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(849, 'Si', 28, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(850, 'Si', 29, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(851, 'Si', 30, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(852, 'Si', 31, 61, '2020-09-16 02:06:41', '2020-09-16 02:06:41'),
(853, 'Relata expresiones negativas hacia sí mismo', 32, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(854, 'soy alegre', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(855, 'me gusta como soy', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(856, 'amistoso(a)', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(857, 'amoroso(a)', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(858, 'feliz', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(859, 'Ninguna', 33, 61, '2020-09-16 02:06:42', '2020-09-16 02:06:42'),
(860, 'Si', 34, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(861, 'Otros', 35, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(862, 'Si', 36, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(863, 'No los dejan salir o jugar', 37, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(864, 'Solo lo regañan', 37, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(865, 'Lo reprenden con golpes', 37, 61, '2020-09-16 02:06:43', '2020-09-16 02:06:43'),
(866, 'Le quita lo que mas les gusta', 37, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(867, 'No lo dejan ver tv o utilizar el celular', 37, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(868, 'Lo ponen a hacer aseo', 37, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(869, 'No aplica', 37, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(870, 'Si', 38, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(871, 'Si', 39, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(872, 'Si', 40, 61, '2020-09-16 02:06:44', '2020-09-16 02:06:44'),
(873, 'Si', 41, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(874, 'Si', 42, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(875, 'Si', 43, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(876, 'Si', 44, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(877, 'Si', 45, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(878, 'Si', 46, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(879, 'Si', 47, 61, '2020-09-16 02:06:45', '2020-09-16 02:06:45'),
(880, 'Si', 48, 61, '2020-09-16 02:06:46', '2020-09-16 02:06:46'),
(881, 'Ninguna', 49, 61, '2020-09-16 02:06:46', '2020-09-16 02:06:46'),
(882, 'Si', 50, 61, '2020-09-16 02:06:46', '2020-09-16 02:06:46'),
(883, 'Si', 51, 61, '2020-09-16 02:06:46', '2020-09-16 02:06:46'),
(884, 'Si', 52, 61, '2020-09-16 02:06:46', '2020-09-16 02:06:46'),
(885, 'Sucios', 1, 62, '2020-09-16 02:08:02', '2020-09-16 02:08:02'),
(886, 'Inadecuado', 6, 62, '2020-09-16 02:08:03', '2020-09-16 02:08:03'),
(887, 'Inadecuado', 5, 62, '2020-09-16 02:08:03', '2020-09-16 02:08:03'),
(888, 'Inadecuado', 4, 62, '2020-09-16 02:08:03', '2020-09-16 02:08:03'),
(889, 'Inadecuado', 3, 62, '2020-09-16 02:08:03', '2020-09-16 02:08:03'),
(890, 'Inadecuado', 2, 62, '2020-09-16 02:08:03', '2020-09-16 02:08:03'),
(891, 'Si', 7, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04'),
(892, 'Si', 8, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04'),
(893, 'Lo llevan al rezandero', 53, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04'),
(894, 'Tíos', 54, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04'),
(895, 'Casi nunca (1 o 2 días)', 9, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04');
INSERT INTO `respuestas` (`id`, `respuestas`, `id_preguntas`, `id_formulario_ficha_salud`, `created_at`, `updated_at`) VALUES
(896, 'Casi nunca (1 o 2 días)', 10, 62, '2020-09-16 02:08:04', '2020-09-16 02:08:04'),
(897, 'Casi nunca (1 o 2 días)', 11, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(898, 'Casi nunca (1 o 2 días)', 12, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(899, 'Casi nunca (1 o 2 días)', 13, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(900, 'Si', 14, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(901, 'Si', 15, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(902, 'Si', 16, 62, '2020-09-16 02:08:05', '2020-09-16 02:08:05'),
(903, 'Si', 17, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(904, '6 veces', 18, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(905, 'Sobrepeso grado I (Riesgo)', 58, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(906, 'Si', 19, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(907, 'Si', 20, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(908, 'Si', 21, 62, '2020-09-16 02:08:06', '2020-09-16 02:08:06'),
(909, 'Si', 22, 62, '2020-09-16 02:08:07', '2020-09-16 02:08:07'),
(910, 'Si', 23, 62, '2020-09-16 02:08:07', '2020-09-16 02:08:07'),
(911, 'Si', 24, 62, '2020-09-16 02:08:07', '2020-09-16 02:08:07'),
(912, '8 Horas', 25, 62, '2020-09-16 02:08:07', '2020-09-16 02:08:07'),
(913, 'Manualidades', 26, 62, '2020-09-16 02:08:07', '2020-09-16 02:08:07'),
(914, 'Televisión', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(915, 'Leer', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(916, 'Escuchar música', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(917, 'Patinaje', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(918, 'Baloncesto', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(919, 'Estudiar', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(920, 'Jugar', 26, 62, '2020-09-16 02:08:08', '2020-09-16 02:08:08'),
(921, 'Fútbol', 26, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(922, 'Celular', 26, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(923, 'Bailar', 26, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(924, 'Pintar', 26, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(925, 'Dibujar', 26, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(926, 'Si', 27, 62, '2020-09-16 02:08:09', '2020-09-16 02:08:09'),
(927, 'Si', 28, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(928, 'Si', 29, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(929, 'Si', 30, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(930, 'Si', 31, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(931, 'Relata expresiones negativas hacia sí mismo', 32, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(932, 'soy alegre', 33, 62, '2020-09-16 02:08:10', '2020-09-16 02:08:10'),
(933, 'me gusta como soy', 33, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(934, 'amistoso(a)', 33, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(935, 'amoroso(a)', 33, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(936, 'feliz', 33, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(937, 'Ninguna', 33, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(938, 'Si', 34, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(939, 'Otros', 35, 62, '2020-09-16 02:08:11', '2020-09-16 02:08:11'),
(940, 'Si', 36, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(941, 'No los dejan salir o jugar', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(942, 'Solo lo regañan', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(943, 'Lo reprenden con golpes', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(944, 'Le quita lo que mas les gusta', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(945, 'No lo dejan ver tv o utilizar el celular', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(946, 'Lo ponen a hacer aseo', 37, 62, '2020-09-16 02:08:12', '2020-09-16 02:08:12'),
(947, 'No aplica', 37, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(948, 'Si', 38, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(949, 'Si', 39, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(950, 'Si', 40, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(951, 'Si', 41, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(952, 'Si', 42, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(953, 'Si', 43, 62, '2020-09-16 02:08:13', '2020-09-16 02:08:13'),
(954, 'Si', 44, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(955, 'Si', 45, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(956, 'Si', 46, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(957, 'Si', 47, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(958, 'Si', 48, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(959, 'Ninguna', 49, 62, '2020-09-16 02:08:14', '2020-09-16 02:08:14'),
(960, 'Si', 50, 62, '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(961, 'Si', 51, 62, '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(962, 'Si', 52, 62, '2020-09-16 02:08:15', '2020-09-16 02:08:15'),
(963, 'Sucios', 1, 63, '2020-09-16 02:44:00', '2020-09-16 02:44:00'),
(964, 'Inadecuado', 6, 63, '2020-09-16 02:44:00', '2020-09-16 02:44:00'),
(965, 'Inadecuado', 5, 63, '2020-09-16 02:44:01', '2020-09-16 02:44:01'),
(966, 'Inadecuado', 4, 63, '2020-09-16 02:44:01', '2020-09-16 02:44:01'),
(967, 'Inadecuado', 3, 63, '2020-09-16 02:44:01', '2020-09-16 02:44:01'),
(968, 'Inadecuado', 2, 63, '2020-09-16 02:44:01', '2020-09-16 02:44:01'),
(969, 'Si', 7, 63, '2020-09-16 02:44:01', '2020-09-16 02:44:01'),
(970, 'Si', 8, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(971, 'Lo llevan al rezandero', 53, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(972, 'Tíos', 54, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(973, 'Casi nunca (1 o 2 días)', 9, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(974, 'Casi nunca (1 o 2 días)', 10, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(975, 'Casi nunca (1 o 2 días)', 11, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(976, 'Casi nunca (1 o 2 días)', 12, 63, '2020-09-16 02:44:02', '2020-09-16 02:44:02'),
(977, 'Casi nunca (1 o 2 días)', 13, 63, '2020-09-16 02:44:03', '2020-09-16 02:44:03'),
(978, 'Si', 14, 63, '2020-09-16 02:44:03', '2020-09-16 02:44:03'),
(979, 'Si', 15, 63, '2020-09-16 02:44:03', '2020-09-16 02:44:03'),
(980, 'Si', 16, 63, '2020-09-16 02:44:03', '2020-09-16 02:44:03'),
(981, 'Si', 17, 63, '2020-09-16 02:44:03', '2020-09-16 02:44:03'),
(982, '6 veces', 18, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(983, 'Sobrepeso grado I (Riesgo)', 58, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(984, 'Si', 19, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(985, 'Si', 20, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(986, 'Si', 21, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(987, 'Si', 22, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(988, 'Si', 23, 63, '2020-09-16 02:44:04', '2020-09-16 02:44:04'),
(989, 'Si', 24, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(990, '8 Horas', 25, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(991, 'Manualidades', 26, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(992, 'Televisión', 26, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(993, 'Leer', 26, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(994, 'Escuchar música', 26, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(995, 'Patinaje', 26, 63, '2020-09-16 02:44:05', '2020-09-16 02:44:05'),
(996, 'Baloncesto', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(997, 'Estudiar', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(998, 'Jugar', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(999, 'Fútbol', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(1000, 'Celular', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(1001, 'Bailar', 26, 63, '2020-09-16 02:44:06', '2020-09-16 02:44:06'),
(1002, 'Pintar', 26, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1003, 'Dibujar', 26, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1004, 'Si', 27, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1005, 'Si', 28, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1006, 'Si', 29, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1007, 'Si', 30, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1008, 'Si', 31, 63, '2020-09-16 02:44:07', '2020-09-16 02:44:07'),
(1009, 'Relata expresiones negativas hacia sí mismo', 32, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1010, 'soy alegre', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1011, 'me gusta como soy', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1012, 'amistoso(a)', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1013, 'amoroso(a)', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1014, 'feliz', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1015, 'Ninguna', 33, 63, '2020-09-16 02:44:08', '2020-09-16 02:44:08'),
(1016, 'Si', 34, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1017, 'Otros', 35, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1018, 'Si', 36, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1019, 'No los dejan salir o jugar', 37, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1020, 'Solo lo regañan', 37, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1021, 'Lo reprenden con golpes', 37, 63, '2020-09-16 02:44:09', '2020-09-16 02:44:09'),
(1022, 'Le quita lo que mas les gusta', 37, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1023, 'No lo dejan ver tv o utilizar el celular', 37, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1024, 'Lo ponen a hacer aseo', 37, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1025, 'No aplica', 37, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1026, 'Si', 38, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1027, 'Si', 39, 63, '2020-09-16 02:44:10', '2020-09-16 02:44:10'),
(1028, 'Si', 40, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1029, 'Si', 41, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1030, 'Si', 42, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1031, 'Si', 43, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1032, 'Si', 44, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1033, 'Si', 45, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1034, 'Si', 46, 63, '2020-09-16 02:44:11', '2020-09-16 02:44:11'),
(1035, 'Si', 47, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1036, 'Si', 48, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1037, 'Ninguna', 49, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1038, 'Si', 50, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1039, 'Si', 51, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1040, 'Si', 52, 63, '2020-09-16 02:44:12', '2020-09-16 02:44:12'),
(1041, 'Sucios', 1, 64, '2020-09-16 13:18:55', '2020-09-16 13:18:55'),
(1042, 'Inadecuado', 2, 64, '2020-09-16 13:18:55', '2020-09-16 13:18:55'),
(1043, 'Inadecuado', 3, 64, '2020-09-16 13:18:55', '2020-09-16 13:18:55'),
(1044, 'Inadecuado', 4, 64, '2020-09-16 13:18:55', '2020-09-16 13:18:55'),
(1045, 'Inadecuado', 5, 64, '2020-09-16 13:18:56', '2020-09-16 13:18:56'),
(1046, 'Inadecuado', 6, 64, '2020-09-16 13:18:56', '2020-09-16 13:18:56'),
(1047, 'Si', 7, 64, '2020-09-16 13:18:56', '2020-09-16 13:18:56'),
(1048, 'Si', 8, 64, '2020-09-16 13:18:56', '2020-09-16 13:18:56'),
(1049, 'Lo llevan al rezandero', 53, 64, '2020-09-16 13:18:56', '2020-09-16 13:18:56'),
(1050, 'Tíos', 54, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1051, 'Casi nunca (1 o 2 días)', 9, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1052, 'Casi nunca (1 o 2 días)', 10, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1053, 'Casi nunca (1 o 2 días)', 11, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1054, 'Casi nunca (1 o 2 días)', 12, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1055, 'Casi nunca (1 o 2 días)', 13, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1056, 'Si', 14, 64, '2020-09-16 13:18:57', '2020-09-16 13:18:57'),
(1057, 'Si', 15, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1058, 'Si', 16, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1059, 'Si', 17, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1060, '6 veces', 18, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1061, 'Sobrepeso grado I (Riesgo)', 58, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1062, 'Si', 19, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1063, 'Si', 20, 64, '2020-09-16 13:18:58', '2020-09-16 13:18:58'),
(1064, 'Si', 21, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1065, 'Si', 22, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1066, 'Si', 23, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1067, 'Si', 24, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1068, '8 Horas', 25, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1069, 'Manualidades', 26, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1070, 'Televisión', 26, 64, '2020-09-16 13:18:59', '2020-09-16 13:18:59'),
(1071, 'Leer', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1072, 'Escuchar música', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1073, 'Patinaje', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1074, 'Baloncesto', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1075, 'Estudiar', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1076, 'Jugar', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1077, 'Fútbol', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1078, 'Celular', 26, 64, '2020-09-16 13:19:00', '2020-09-16 13:19:00'),
(1079, 'Bailar', 26, 64, '2020-09-16 13:19:01', '2020-09-16 13:19:01'),
(1080, 'Pintar', 26, 64, '2020-09-16 13:19:01', '2020-09-16 13:19:01'),
(1081, 'Dibujar', 26, 64, '2020-09-16 13:19:01', '2020-09-16 13:19:01'),
(1082, 'Si', 27, 64, '2020-09-16 13:19:01', '2020-09-16 13:19:01'),
(1083, 'Si', 28, 64, '2020-09-16 13:19:01', '2020-09-16 13:19:01'),
(1084, 'Si', 29, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1085, 'Si', 30, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1086, 'Si', 31, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1087, 'Relata expresiones negativas hacia sí mismo', 32, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1088, 'soy alegre', 33, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1089, 'me gusta como soy', 33, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1090, 'amistoso(a)', 33, 64, '2020-09-16 13:19:02', '2020-09-16 13:19:02'),
(1091, 'amoroso(a)', 33, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1092, 'feliz', 33, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1093, 'Ninguna', 33, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1094, 'Si', 34, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1095, 'Otros', 35, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1096, 'Si', 36, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1097, 'No los dejan salir o jugar', 37, 64, '2020-09-16 13:19:03', '2020-09-16 13:19:03'),
(1098, 'Solo lo regañan', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1099, 'Lo reprenden con golpes', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1100, 'Le quita lo que mas les gusta', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1101, 'No lo dejan ver tv o utilizar el celular', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1102, 'Lo ponen a hacer aseo', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1103, 'No aplica', 37, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1104, 'Si', 38, 64, '2020-09-16 13:19:04', '2020-09-16 13:19:04'),
(1105, 'Si', 39, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1106, 'Si', 40, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1107, 'Si', 41, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1108, 'Si', 42, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1109, 'Si', 43, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1110, 'Si', 44, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1111, 'Si', 45, 64, '2020-09-16 13:19:05', '2020-09-16 13:19:05'),
(1112, 'Si', 46, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1113, 'Si', 47, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1114, 'Si', 48, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1115, 'Ninguna', 49, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1116, 'Si', 50, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1117, 'Si', 51, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1118, 'Si', 52, 64, '2020-09-16 13:19:06', '2020-09-16 13:19:06'),
(1119, 'Sucios', 1, 65, '2020-09-16 13:23:30', '2020-09-16 13:23:30'),
(1120, 'Inadecuado', 2, 65, '2020-09-16 13:23:30', '2020-09-16 13:23:30'),
(1121, 'Inadecuado', 3, 65, '2020-09-16 13:23:30', '2020-09-16 13:23:30'),
(1122, 'Inadecuado', 4, 65, '2020-09-16 13:23:31', '2020-09-16 13:23:31'),
(1123, 'Inadecuado', 5, 65, '2020-09-16 13:23:31', '2020-09-16 13:23:31'),
(1124, 'Inadecuado', 6, 65, '2020-09-16 13:23:31', '2020-09-16 13:23:31'),
(1125, 'Si', 7, 65, '2020-09-16 13:23:31', '2020-09-16 13:23:31'),
(1126, 'Si', 8, 65, '2020-09-16 13:23:31', '2020-09-16 13:23:31'),
(1127, 'Lo llevan al rezandero', 53, 65, '2020-09-16 13:23:32', '2020-09-16 13:23:32'),
(1128, 'Tíos', 54, 65, '2020-09-16 13:23:32', '2020-09-16 13:23:32'),
(1129, 'Casi nunca (1 o 2 días)', 9, 65, '2020-09-16 13:23:32', '2020-09-16 13:23:32'),
(1130, 'Casi nunca (1 o 2 días)', 10, 65, '2020-09-16 13:23:32', '2020-09-16 13:23:32'),
(1131, 'Casi nunca (1 o 2 días)', 11, 65, '2020-09-16 13:23:32', '2020-09-16 13:23:32'),
(1132, 'Casi nunca (1 o 2 días)', 12, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1133, 'Casi nunca (1 o 2 días)', 13, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1134, 'Si', 14, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1135, 'Si', 15, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1136, 'Si', 16, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1137, 'Si', 17, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1138, '6 veces', 18, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1139, 'Peso normal', 58, 65, '2020-09-16 13:23:33', '2020-09-16 13:23:33'),
(1140, 'Si', 19, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1141, 'Si', 20, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1142, 'Si', 21, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1143, 'Si', 22, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1144, 'Si', 23, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1145, 'Si', 24, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1146, '8 Horas', 25, 65, '2020-09-16 13:23:34', '2020-09-16 13:23:34'),
(1147, 'Manualidades', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1148, 'Televisión', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1149, 'Leer', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1150, 'Escuchar música', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1151, 'Patinaje', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1152, 'Baloncesto', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1153, 'Estudiar', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1154, 'Jugar', 26, 65, '2020-09-16 13:23:35', '2020-09-16 13:23:35'),
(1155, 'Fútbol', 26, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1156, 'Celular', 26, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1157, 'Bailar', 26, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1158, 'Pintar', 26, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1159, 'Dibujar', 26, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1160, 'Si', 27, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1161, 'Si', 28, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1162, 'Si', 29, 65, '2020-09-16 13:23:36', '2020-09-16 13:23:36'),
(1163, 'Si', 30, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1164, 'Si', 31, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1165, 'Relata expresiones negativas hacia sí mismo', 32, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1166, 'soy alegre', 33, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1167, 'me gusta como soy', 33, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1168, 'amistoso(a)', 33, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1169, 'amoroso(a)', 33, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1170, 'feliz', 33, 65, '2020-09-16 13:23:37', '2020-09-16 13:23:37'),
(1171, 'Ninguna', 33, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1172, 'Si', 34, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1173, 'Otros', 35, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1174, 'Si', 36, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1175, 'No los dejan salir o jugar', 37, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1176, 'Solo lo regañan', 37, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1177, 'Lo reprenden con golpes', 37, 65, '2020-09-16 13:23:38', '2020-09-16 13:23:38'),
(1178, 'Le quita lo que mas les gusta', 37, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1179, 'No lo dejan ver tv o utilizar el celular', 37, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1180, 'Lo ponen a hacer aseo', 37, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1181, 'No aplica', 37, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1182, 'Si', 38, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1183, 'Si', 39, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1184, 'Si', 40, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1185, 'Si', 41, 65, '2020-09-16 13:23:39', '2020-09-16 13:23:39'),
(1186, 'Si', 42, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1187, 'Si', 43, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1188, 'Si', 44, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1189, 'Si', 45, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1190, 'Si', 46, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1191, 'Si', 47, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1192, 'Si', 48, 65, '2020-09-16 13:23:40', '2020-09-16 13:23:40'),
(1193, 'Ninguna', 49, 65, '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(1194, 'Si', 50, 65, '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(1195, 'Si', 51, 65, '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(1196, 'Si', 52, 65, '2020-09-16 13:23:41', '2020-09-16 13:23:41'),
(1197, 'Sucios', 1, 66, '2020-09-17 14:53:02', '2020-09-17 14:53:02'),
(1198, 'Inadecuado', 5, 66, '2020-09-17 14:53:02', '2020-09-17 14:53:02'),
(1199, 'Inadecuado', 4, 66, '2020-09-17 14:53:02', '2020-09-17 14:53:02'),
(1200, 'Inadecuado', 3, 66, '2020-09-17 14:53:02', '2020-09-17 14:53:02'),
(1201, 'Adecuado', 2, 66, '2020-09-17 14:53:03', '2020-09-17 14:53:03'),
(1202, 'Adecuado', 6, 66, '2020-09-17 14:53:03', '2020-09-17 14:53:03'),
(1203, 'Si', 7, 66, '2020-09-17 14:53:03', '2020-09-17 14:53:03'),
(1204, 'Si', 8, 66, '2020-09-17 14:53:03', '2020-09-17 14:53:03'),
(1205, 'Lo llevan al rezandero', 53, 66, '2020-09-17 14:53:03', '2020-09-17 14:53:03'),
(1206, 'Tíos', 54, 66, '2020-09-17 14:53:04', '2020-09-17 14:53:04'),
(1207, 'Casi nunca (1 o 2 días)', 9, 66, '2020-09-17 14:53:04', '2020-09-17 14:53:04'),
(1208, 'Casi nunca (1 o 2 días)', 10, 66, '2020-09-17 14:53:04', '2020-09-17 14:53:04'),
(1209, 'Casi nunca (1 o 2 días)', 11, 66, '2020-09-17 14:53:04', '2020-09-17 14:53:04'),
(1210, 'Casi nunca (1 o 2 días)', 12, 66, '2020-09-17 14:53:04', '2020-09-17 14:53:04'),
(1211, 'Casi nunca (1 o 2 días)', 13, 66, '2020-09-17 14:53:05', '2020-09-17 14:53:05'),
(1212, 'Si', 14, 66, '2020-09-17 14:53:05', '2020-09-17 14:53:05'),
(1213, 'Si', 15, 66, '2020-09-17 14:53:05', '2020-09-17 14:53:05'),
(1214, 'Si', 16, 66, '2020-09-17 14:53:05', '2020-09-17 14:53:05'),
(1215, 'Si', 17, 66, '2020-09-17 14:53:06', '2020-09-17 14:53:06'),
(1216, '6 veces', 18, 66, '2020-09-17 14:53:06', '2020-09-17 14:53:06'),
(1217, 'Peso normal', 58, 66, '2020-09-17 14:53:06', '2020-09-17 14:53:06'),
(1218, 'Si', 19, 66, '2020-09-17 14:53:07', '2020-09-17 14:53:07'),
(1219, 'Si', 20, 66, '2020-09-17 14:53:07', '2020-09-17 14:53:07'),
(1220, 'Si', 21, 66, '2020-09-17 14:53:07', '2020-09-17 14:53:07'),
(1221, 'Si', 22, 66, '2020-09-17 14:53:08', '2020-09-17 14:53:08'),
(1222, 'Si', 23, 66, '2020-09-17 14:53:08', '2020-09-17 14:53:08'),
(1223, 'Si', 24, 66, '2020-09-17 14:53:08', '2020-09-17 14:53:08'),
(1224, '8 Horas', 25, 66, '2020-09-17 14:53:08', '2020-09-17 14:53:08'),
(1225, 'Manualidades', 26, 66, '2020-09-17 14:53:09', '2020-09-17 14:53:09'),
(1226, 'Televisión', 26, 66, '2020-09-17 14:53:09', '2020-09-17 14:53:09'),
(1227, 'Sucios', 1, 67, '2020-09-18 22:52:07', '2020-09-18 22:52:07'),
(1228, 'Inadecuado', 2, 67, '2020-09-18 22:52:07', '2020-09-18 22:52:07'),
(1229, 'Inadecuado', 3, 67, '2020-09-18 22:52:07', '2020-09-18 22:52:07'),
(1230, 'Inadecuado', 4, 67, '2020-09-18 22:52:08', '2020-09-18 22:52:08'),
(1231, 'Inadecuado', 5, 67, '2020-09-18 22:52:08', '2020-09-18 22:52:08'),
(1232, 'Inadecuado', 6, 67, '2020-09-18 22:52:08', '2020-09-18 22:52:08'),
(1233, 'Si', 7, 67, '2020-09-18 22:52:09', '2020-09-18 22:52:09'),
(1234, 'Si', 8, 67, '2020-09-18 22:52:09', '2020-09-18 22:52:09'),
(1235, 'Lo llevan al rezandero', 53, 67, '2020-09-18 22:52:09', '2020-09-18 22:52:09'),
(1236, 'Tíos', 54, 67, '2020-09-18 22:52:10', '2020-09-18 22:52:10'),
(1237, 'Casi nunca (1 o 2 días)', 9, 67, '2020-09-18 22:52:10', '2020-09-18 22:52:10'),
(1238, 'Casi nunca (1 o 2 días)', 10, 67, '2020-09-18 22:52:11', '2020-09-18 22:52:11'),
(1239, 'Casi nunca (1 o 2 días)', 11, 67, '2020-09-18 22:52:11', '2020-09-18 22:52:11'),
(1240, 'Casi nunca (1 o 2 días)', 12, 67, '2020-09-18 22:52:11', '2020-09-18 22:52:11'),
(1241, 'Casi nunca (1 o 2 días)', 13, 67, '2020-09-18 22:52:12', '2020-09-18 22:52:12'),
(1242, 'Si', 14, 67, '2020-09-18 22:52:12', '2020-09-18 22:52:12'),
(1243, 'Si', 15, 67, '2020-09-18 22:52:12', '2020-09-18 22:52:12'),
(1244, 'Si', 16, 67, '2020-09-18 22:52:13', '2020-09-18 22:52:13'),
(1245, 'Si', 17, 67, '2020-09-18 22:52:13', '2020-09-18 22:52:13'),
(1246, '6 veces', 18, 67, '2020-09-18 22:52:13', '2020-09-18 22:52:13'),
(1247, 'Obesidad de tipo I (Riesgo moderado)', 58, 67, '2020-09-18 22:52:14', '2020-09-18 22:52:14'),
(1248, 'Si', 19, 67, '2020-09-18 22:52:14', '2020-09-18 22:52:14'),
(1249, 'Si', 20, 67, '2020-09-18 22:52:15', '2020-09-18 22:52:15'),
(1250, 'Si', 21, 67, '2020-09-18 22:52:15', '2020-09-18 22:52:15'),
(1251, 'Si', 22, 67, '2020-09-18 22:52:15', '2020-09-18 22:52:15'),
(1252, 'Si', 23, 67, '2020-09-18 22:52:16', '2020-09-18 22:52:16'),
(1253, 'Si', 24, 67, '2020-09-18 22:52:16', '2020-09-18 22:52:16'),
(1254, '8 Horas', 25, 67, '2020-09-18 22:52:16', '2020-09-18 22:52:16'),
(1255, 'Manualidades', 26, 67, '2020-09-18 22:52:17', '2020-09-18 22:52:17'),
(1256, 'Televisión', 26, 67, '2020-09-18 22:52:17', '2020-09-18 22:52:17'),
(1257, 'Leer', 26, 67, '2020-09-18 22:52:17', '2020-09-18 22:52:17'),
(1258, 'Escuchar música', 26, 67, '2020-09-18 22:52:18', '2020-09-18 22:52:18'),
(1259, 'Patinaje', 26, 67, '2020-09-18 22:52:18', '2020-09-18 22:52:18'),
(1260, 'Baloncesto', 26, 67, '2020-09-18 22:52:18', '2020-09-18 22:52:18'),
(1261, 'Estudiar', 26, 67, '2020-09-18 22:52:19', '2020-09-18 22:52:19'),
(1262, 'Jugar', 26, 67, '2020-09-18 22:52:19', '2020-09-18 22:52:19'),
(1263, 'Fútbol', 26, 67, '2020-09-18 22:52:19', '2020-09-18 22:52:19'),
(1264, 'Celular', 26, 67, '2020-09-18 22:52:20', '2020-09-18 22:52:20'),
(1265, 'Bailar', 26, 67, '2020-09-18 22:52:20', '2020-09-18 22:52:20'),
(1266, 'Pintar', 26, 67, '2020-09-18 22:52:20', '2020-09-18 22:52:20'),
(1267, 'Dibujar', 26, 67, '2020-09-18 22:52:21', '2020-09-18 22:52:21'),
(1268, 'Si', 27, 67, '2020-09-18 22:52:21', '2020-09-18 22:52:21'),
(1269, 'Si', 28, 67, '2020-09-18 22:52:21', '2020-09-18 22:52:21'),
(1270, 'Si', 29, 67, '2020-09-18 22:52:22', '2020-09-18 22:52:22'),
(1271, 'Si', 30, 67, '2020-09-18 22:52:22', '2020-09-18 22:52:22'),
(1272, 'Si', 31, 67, '2020-09-18 22:52:22', '2020-09-18 22:52:22'),
(1273, 'Relata expresiones negativas hacia sí mismo', 32, 67, '2020-09-18 22:52:23', '2020-09-18 22:52:23'),
(1274, 'soy alegre', 33, 67, '2020-09-18 22:52:23', '2020-09-18 22:52:23'),
(1275, 'me gusta como soy', 33, 67, '2020-09-18 22:52:23', '2020-09-18 22:52:23'),
(1276, 'amistoso(a)', 33, 67, '2020-09-18 22:52:23', '2020-09-18 22:52:23'),
(1277, 'amoroso(a)', 33, 67, '2020-09-18 22:52:24', '2020-09-18 22:52:24'),
(1278, 'feliz', 33, 67, '2020-09-18 22:52:24', '2020-09-18 22:52:24'),
(1279, 'Ninguna', 33, 67, '2020-09-18 22:52:24', '2020-09-18 22:52:24'),
(1280, 'Si', 34, 67, '2020-09-18 22:52:25', '2020-09-18 22:52:25'),
(1281, 'Otros', 35, 67, '2020-09-18 22:52:25', '2020-09-18 22:52:25'),
(1282, 'Si', 36, 67, '2020-09-18 22:52:25', '2020-09-18 22:52:25'),
(1283, 'No los dejan salir o jugar', 37, 67, '2020-09-18 22:52:26', '2020-09-18 22:52:26'),
(1284, 'Solo lo regañan', 37, 67, '2020-09-18 22:52:26', '2020-09-18 22:52:26'),
(1285, 'Lo reprenden con golpes', 37, 67, '2020-09-18 22:52:26', '2020-09-18 22:52:26'),
(1286, 'Le quita lo que mas les gusta', 37, 67, '2020-09-18 22:52:27', '2020-09-18 22:52:27'),
(1287, 'No lo dejan ver tv o utilizar el celular', 37, 67, '2020-09-18 22:52:27', '2020-09-18 22:52:27'),
(1288, 'Lo ponen a hacer aseo', 37, 67, '2020-09-18 22:52:27', '2020-09-18 22:52:27'),
(1289, 'No aplica', 37, 67, '2020-09-18 22:52:28', '2020-09-18 22:52:28'),
(1290, 'Si', 38, 67, '2020-09-18 22:52:28', '2020-09-18 22:52:28'),
(1291, 'Si', 39, 67, '2020-09-18 22:52:28', '2020-09-18 22:52:28'),
(1292, 'Si', 40, 67, '2020-09-18 22:52:29', '2020-09-18 22:52:29'),
(1293, 'Si', 41, 67, '2020-09-18 22:52:29', '2020-09-18 22:52:29'),
(1294, 'Si', 42, 67, '2020-09-18 22:52:29', '2020-09-18 22:52:29'),
(1295, 'Si', 43, 67, '2020-09-18 22:52:30', '2020-09-18 22:52:30'),
(1296, 'Si', 44, 67, '2020-09-18 22:52:30', '2020-09-18 22:52:30'),
(1297, 'Si', 45, 67, '2020-09-18 22:52:30', '2020-09-18 22:52:30'),
(1298, 'Si', 46, 67, '2020-09-18 22:52:31', '2020-09-18 22:52:31'),
(1299, 'Si', 47, 67, '2020-09-18 22:52:31', '2020-09-18 22:52:31'),
(1300, 'Si', 48, 67, '2020-09-18 22:52:31', '2020-09-18 22:52:31'),
(1301, 'Ninguna', 49, 67, '2020-09-18 22:52:32', '2020-09-18 22:52:32'),
(1302, 'Si', 50, 67, '2020-09-18 22:52:32', '2020-09-18 22:52:32'),
(1303, 'Si', 51, 67, '2020-09-18 22:52:32', '2020-09-18 22:52:32'),
(1304, 'Si', 52, 67, '2020-09-18 22:52:33', '2020-09-18 22:52:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_select_dinamico`
--

CREATE TABLE `respuestas_select_dinamico` (
  `id` int(11) NOT NULL,
  `respuesta` varchar(255) NOT NULL,
  `id_preguntas` int(11) NOT NULL,
  `riesgo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `respuestas_select_dinamico`
--

INSERT INTO `respuestas_select_dinamico` (`id`, `respuesta`, `id_preguntas`, `riesgo`, `created_at`, `update_at`) VALUES
(1, 'Acuden al médico', 53, 0, '2020-09-08 13:34:08', '2020-09-08 13:34:08'),
(2, 'Lo cuidan en la casa', 53, 0, '2020-09-08 13:34:08', '2020-09-08 13:34:08'),
(3, 'Lo llevan al rezandero', 53, 0, '2020-09-08 13:34:08', '2020-09-08 13:34:08'),
(4, 'Cuidador', 54, 0, '2020-09-08 13:37:14', '2020-09-08 13:37:14'),
(5, 'Papá', 54, 0, '2020-09-08 13:37:14', '2020-09-08 13:37:14'),
(6, 'Mamá', 54, 0, '2020-09-08 13:37:14', '2020-09-08 13:37:14'),
(7, 'Abuelos', 54, 0, '2020-09-08 13:37:14', '2020-09-08 13:37:14'),
(8, 'Tíos', 54, 0, '2020-09-08 13:37:14', '2020-09-08 13:37:14'),
(9, 'Todos los días', 9, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(10, 'Algunos días (menos de 6 días)', 9, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(11, 'Casi nunca (1 o 2 días)', 9, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(12, 'Todos los días', 10, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(13, 'Algunos días (menos de 6 días)', 10, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(14, 'Casi nunca (1 o 2 días)', 10, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(15, 'Todos los días', 11, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(16, 'Algunos días (menos de 6 días)', 11, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(17, 'Casi nunca (1 o 2 días)', 11, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(18, 'Todos los días', 12, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(19, 'Algunos días (menos de 6 días)', 12, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(20, 'Casi nunca (1 o 2 días)', 12, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(21, 'Todos los días', 13, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(22, 'Algunos días (menos de 6 días)', 13, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(23, 'Casi nunca (1 o 2 días)', 13, 0, '2020-09-08 13:43:59', '2020-09-08 13:43:59'),
(24, '1 vez', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(25, '2 veces', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(26, '3 veces', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(27, '4 veces', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(28, '5 veces', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(29, '6 veces', 18, 0, '2020-09-08 14:00:30', '2020-09-08 14:00:30'),
(30, '3 Horas', 25, 0, '2020-09-08 14:08:12', '2020-09-08 14:08:12'),
(31, '6 Horas', 25, 0, '2020-09-08 14:08:12', '2020-09-08 14:08:12'),
(32, '7 Horas', 25, 0, '2020-09-08 14:08:12', '2020-09-08 14:08:12'),
(33, '8 Horas', 25, 0, '2020-09-08 14:08:12', '2020-09-08 14:08:12'),
(34, 'Manualidades', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(35, 'Televisión', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(36, 'Leer', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(37, 'Escuchar música', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(38, 'Patinaje', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(39, 'Baloncesto', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(40, 'Estudiar', 26, 0, '2020-09-08 14:11:04', '2020-09-08 14:11:04'),
(41, 'Jugar', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(42, 'Fútbol', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(43, 'Celular', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(44, 'Bailar', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(45, 'Pintar', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(46, 'Dibujar', 26, 0, '2020-09-08 14:12:25', '2020-09-08 14:12:25'),
(47, 'El niño es capaz de describirse a sí mismo', 32, 0, '2020-09-08 14:35:22', '2020-09-08 14:35:22'),
(48, 'Relata expresiones negativas hacia sí mismo', 32, 0, '2020-09-08 14:35:22', '2020-09-08 14:35:22'),
(49, 'soy alegre', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(50, 'me gusta como soy', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(51, 'amistoso(a)', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(52, 'amoroso(a)', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(53, 'feliz', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(54, 'Ninguna', 33, 0, '2020-09-08 14:37:24', '2020-09-08 14:37:24'),
(55, 'Padres', 35, 0, '2020-09-08 14:44:39', '2020-09-08 14:44:39'),
(56, 'Abuelos', 35, 0, '2020-09-08 14:44:39', '2020-09-08 14:44:39'),
(57, 'Tios', 35, 0, '2020-09-08 14:44:39', '2020-09-08 14:44:39'),
(58, 'Otros', 35, 0, '2020-09-08 14:44:39', '2020-09-08 14:44:39'),
(59, 'No los dejan salir o jugar', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(60, 'Solo lo regañan', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(61, 'Lo reprenden con golpes', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(62, 'Le quita lo que mas les gusta', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(63, 'No lo dejan ver tv o utilizar el celular', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(64, 'Lo ponen a hacer aseo', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(65, 'No aplica', 37, 0, '2020-09-08 14:59:31', '2020-09-08 14:59:31'),
(66, 'Catolico', 49, 0, '2020-09-08 15:03:19', '2020-09-08 15:03:19'),
(67, 'Cristiano', 49, 0, '2020-09-08 15:03:19', '2020-09-08 15:03:19'),
(68, 'Testigo de Jeova', 49, 0, '2020-09-08 15:03:19', '2020-09-08 15:03:19'),
(69, 'Ninguna', 49, 0, '2020-09-08 15:03:19', '2020-09-08 15:03:19'),
(71, 'Adecuado', 1, 0, '2020-09-08 18:19:50', '2020-09-08 18:19:50'),
(72, 'Inadecuado', 1, 1, '2020-09-08 18:19:50', '2020-09-08 18:19:50'),
(73, 'Adecuado', 2, 0, '2020-09-08 18:20:04', '2020-09-08 18:20:04'),
(74, 'Inadecuado', 2, 1, '2020-09-08 18:20:04', '2020-09-08 18:20:04'),
(75, 'Adecuado', 3, 0, '2020-09-08 18:20:21', '2020-09-08 18:20:21'),
(76, 'Inadecuado', 3, 1, '2020-09-08 18:20:21', '2020-09-08 18:20:21'),
(77, 'Adecuado', 4, 0, '2020-09-08 18:20:30', '2020-09-08 18:20:30'),
(78, 'Inadecuado', 4, 1, '2020-09-08 18:20:30', '2020-09-08 18:20:30'),
(79, 'Adecuado', 5, 0, '2020-09-08 18:20:37', '2020-09-08 18:20:37'),
(80, 'Inadecuado', 5, 1, '2020-09-08 18:20:37', '2020-09-08 18:20:37'),
(81, 'Adecuado', 6, 0, '2020-09-08 18:20:44', '2020-09-08 18:20:44'),
(82, 'Inadecuado', 6, 1, '2020-09-08 18:20:44', '2020-09-08 18:20:44'),
(83, 'Sucios', 1, 0, '2020-09-13 03:02:12', '2020-09-13 03:02:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rols`
--

CREATE TABLE `rols` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rols`
--

INSERT INTO `rols` (`id`, `descripcion`) VALUES
(1, 'super_administrador'),
(2, 'profesor'),
(3, 'rector'),
(4, 'evaluador'),
(5, 'autoridad'),
(6, 'acudiente'),
(7, 'administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `ruta` varchar(200) DEFAULT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slides`
--

INSERT INTO `slides` (`id`, `ruta`, `titulo`, `descripcion`, `url`, `orden`, `id_user`, `updated_at`, `created_at`) VALUES
(1, 'slider1.jpg', 'Colegio Nuestra Señora de las Mercedes ', 'SARDINATA', NULL, NULL, 0, '2020-03-22 05:49:14', '2020-03-22 05:49:14'),
(2, 'slider2.jpg', 'Sardinata', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut a', NULL, NULL, 0, '2020-03-22 05:49:23', '2020-03-22 05:49:23'),
(3, 'slider3.png', 'Sardinata', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut a', NULL, NULL, 0, '2020-03-22 05:49:32', '2020-03-22 05:49:32'),
(4, 'slider4.jpg', 'CoronaVirus', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.', 'http://www.sednortedesantander.gov.co/images/documentos/normatividad/Cobertura/2020/Anexo2%20Circular%20No.10%20Circular%20Externa.pdf', NULL, 0, '2020-03-22 05:49:41', '2020-03-22 05:49:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `documento` varchar(11) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `id_institucion` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `online` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `apellidos`, `email`, `password`, `documento`, `celular`, `id_rol`, `id_estudiante`, `id_institucion`, `remember_token`, `created_at`, `updated_at`, `online`) VALUES
(0, 'Laura Daniela', 'Buitrago', 'lauridani1@gmail.com', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '123456', '1234567891', 1, NULL, 3, 'dUsN4a17QyPHMHA4tvwSXvcKbTpY4J9VYYZpFMtGP31nHwFTTqnNXp3QcGM1', NULL, '2020-10-07 18:36:49', 0),
(28, 'admin', 'ufps', 'admin@gmail.com', '123456', '1645641', '2156516', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'Felipe', 'Bueno', 'felipe@gmail.com', '$2y$10$Ssj5S13PYh4HL8dq.o1kCO8GtWyXsQ9T3n1lq9xSEJaW4Nm7rRGz2', '656565232', '3112454685', 6, NULL, 1, NULL, NULL, NULL, NULL),
(30, 'Naydu', 'Perez', 'naydu@ufps.edu.co', '$2y$10$ALpK/CzWDD62A9Hme1AYFextUPYrOFWs9c.i1FxpY6bHfRrqd/sCy', '2147483647', '315484565', 2, NULL, 2, NULL, NULL, NULL, NULL),
(31, 'Pedro', 'Lemus', 'pedrolemus@gmail.com', '$2y$10$geMHLlODxOb/4qjT03prSOIoticCya.JcCVhIIKe.89znfGeLiDTq', '1649845646', '311587771', 3, NULL, 2, NULL, NULL, NULL, NULL),
(33, 'Profesor', 'Sardinata', 'profesor@gmail.com', '$2y$10$b8U6DHled8yayjzY9Dy4p.nBXxlRlvXdeFw/RJpbNMre/BLBl.u7.', '1654562156', '3254879512', 2, NULL, 2, NULL, NULL, NULL, NULL),
(34, 'Daniel', 'Guitarrero', 'danielguitarrero@wposs.com', '$2y$10$yZVLOUDgZZgo54BI/POweeVdO3nsi8EJsKDjtw//MsfBQenyIwXOW', '78545546', '2147483647', 2, NULL, 1, NULL, NULL, NULL, NULL),
(35, 'Felipe', 'ufps', 'felipe@gmail.com', '$2y$10$2kBcjF3PCnQ3eKAGvyOcD.GahMAJY/syb//tQDkenAWQ3P4Q1EfBC', '4985483647', '3515154848', 6, NULL, 1, NULL, NULL, NULL, NULL),
(36, 'Julian', 'Bueno', 'juli@hotmail.com', '$2y$10$cNc1H6kx19Jhms8mY2tMY.LMftVCTdMj1klqkgfKMTLdLJHXnjk.S', '1093154545', '3159898595', 6, NULL, 1, NULL, NULL, NULL, NULL),
(37, 'Esteban', 'Espina', 'esteban@gmail.com', '$2y$10$2eb4i9ASyj9hWWaWHwvPWe3Wk6IBO6px5KQOFkn7WpwsGOCpdsamu', '1095562626', '3158974984', 6, NULL, 1, NULL, NULL, NULL, NULL),
(38, 'Daniela', 'Zamora', 'daniela@wposs.com', '$2y$10$nKlDUFw5mm1OxCOKHHY.VuQBzTIgavXeyophwOvuGfoCUzE1yh3RW', '2147483647', '2147483647', 2, NULL, 1, NULL, NULL, NULL, NULL),
(39, 'Juliana', 'ufps', 'julia@hotmail.com', '$2y$10$A1xnvZ3EuleknzHSOUna..QNv74n3HB/tjXMkSEfFcXco/ERb5goO', '576786', '3213456', 2, NULL, 2, NULL, NULL, NULL, NULL),
(40, 'Felipe ', 'ramses', 'ramses@gmail.com', '$2y$10$KU0x3AWn9bkRX.krIjIFeeCmaltrmsuv95J.3kj4fk6A.cG9jB7/u', '2147483647', '2131', 2, NULL, 1, NULL, NULL, NULL, NULL),
(41, 'sergio', 'buitrago', 'sebuitrago@gmail.com', '$2y$10$S.l8oE3vIhJBNOAH0L1UgOhZm1COIZ.TepsVaKvgaDaix385KCVTi', '46564556', '456', 2, NULL, 1, NULL, NULL, NULL, NULL),
(42, 'Julia', 'ramona', 'juliaramona@hotmail.com', '$2y$10$bYuNzVpkVP4MBTU5j/AHvO/J6hEkLHdYRJ/xV6Ag2xak6R0tbs1JK', '54546', '546412', 2, NULL, 1, NULL, NULL, NULL, NULL),
(43, 'dany', 'contreras', 'dany@ufps.edu.co', '$2y$10$hqPWye4r17QlPj/VttvCd.YekOB2aKqxNbHPh1zUnfgkQM0lE7jmK', '2147483647', '5754', 2, NULL, 1, NULL, NULL, NULL, NULL),
(44, 'Laura', 'Moros', 'lauramoros@gmail.com', '$2y$10$HIPLqEmGmZ/FigvZlRc4MOiPGDizWVh8c/P/FgB0Hmc4WQl5bHXNS', '544645654', '546546', 2, NULL, 1, NULL, NULL, NULL, NULL),
(45, 'Argelio', 'Espina', 'argelio@gmail.com', '$2y$10$HIPLqEmGmZ/FigvZlRc4MOiPGDizWVh8c/P/FgB0Hmc4WQl5bHXNS', '2147483647', '2147483647', 2, NULL, 1, NULL, NULL, NULL, NULL),
(46, 'Felipe', 'Ramos', 'feliperamos@gmail.com', '$2y$10$HIPLqEmGmZ/FigvZlRc4MOiPGDizWVh8c/P/FgB0Hmc4WQl5bHXNS', '454556', '54632', 2, NULL, 1, NULL, NULL, NULL, NULL),
(47, 'Jaime', 'Rosa', 'jaime@gmail.com', '123456', '123456789', '12345675', 2, NULL, 1, NULL, NULL, NULL, NULL),
(48, 'Mario ', 'Granado', 'rector@gmail.com', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '000000000', '3147483644', 3, NULL, 1, 'ts9PB8kZyjVdnTNATMoQUzOGBg7AamSikqy8qWRAXzbCB7LiGa2GRlNhXIkH', NULL, '2020-10-05 00:18:33', 0),
(49, 'Evaluador', 'Sardinata', 'evaluador@gmail.com', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '1147483647', '314525258', 4, NULL, 1, 've2RAp7tVhAA07QCz2f1calHDzaRpBeALRfHCYXVuZAPDtyrW2qBFyyajwxJ', NULL, '2020-10-05 00:18:43', 1),
(50, 'Evaluador2', 'Sardinata', 'evaluador2@gmail.com', '$2y$10$Tuo5SHaeLF1jdlaXIVZIhO7ijYIFXE2PGabrqNeJUh.ZzIfCY6KJG', '1235467800', '3157483647', 4, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Autoridad', 'Sardinata', 'autoridad@gmail.com', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '1090548485', '3145896959', 5, NULL, 1, 'iAWTwglckvWcPxFD2CePsaxXrfCv0Y9JK95EkVHMx9LB7kGXbHoJFpW9z1zB', NULL, '2020-10-07 18:37:39', 0),
(52, 'Autoridad2', 'Sardinata', 'autoridad2@gmail.com', '$2y$10$uPTrYkFx9rNnXMQ06dtAnu3Wbo.gcoIW/S58yu5slmkSj4FmtyD16', '1098955512', '3156585599', 5, NULL, 1, NULL, NULL, NULL, NULL),
(53, 'sami yahir', 'arevalo montes', 'samiyahiram@ufps.edu.co', '$2y$10$0f8je.aalBR29/YTmeRAy.rUHXg5FEHFBwU3sMHTobkFhIRZmChvm', '1111111111', '1111111111', 2, NULL, 2, NULL, NULL, NULL, NULL),
(54, 'Alcaldia Sardinata', NULL, 'admin@ufps.edu.co', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '0000000000', '00000002', 1, NULL, 1, '3kxmMkXCvOe1hNCTuPnnGK7bkFRNFZ9BSjyRCiSNAmdJHxKOTpunzlhDSP0P', NULL, '2020-10-04 22:10:53', 0),
(55, 'juan', 'perez', 'juan@ufps.edu.co', '$2y$10$ZvJ.H2eZk2prB88Spi0hPuR58LUZNS3RDtqcv7wW/BMtlxWTR73La', '0000000001', '000000000', 4, NULL, 3, NULL, NULL, '2020-09-27 21:44:57', 1),
(56, 'ICBF', 'Ninguno', 'icbf@gmail.com', '$2y$10$7ReZZTrEJQ3fs6V5JYause1quIeMd8ZMMrhuEm4Wk5Qlmy3tbQrGi', '1000000000', '6572391', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Maria del Pilar', 'Pezotti Lemus', 'adminpilar@ufps.edu.co', '$2y$10$0hK/L4irUpLe6UqLFwUEJOoec5mRqtXJ.ChAT7QjvkftkrpqEwK3m', '6352656656', '3216549879', 7, NULL, 1, NULL, NULL, NULL, NULL),
(58, 'Maria del Pilar', 'Rojas', 'acudiente@gmail.com', '$2y$10$bGfzddwJB4Mu3ln6lzU3eOoz/Hx/FCgdf5sYZLEmW7vMx9YS4UQC6', '1654165465', '3123165465', 6, 1, NULL, 'dYpoUEiUUkt2mCaQvs3h5XMsICsl70fzTli25TEhQbQyEce7LVPPuYp2kCYi', NULL, '2020-10-06 01:01:03', 0),
(59, 'Pedro', 'Perez', 'acudiente2@gmail.com', '$2y$10$zxdWqhCfk.fh7F9KAT3nd.h35Ykl2HL4Mlqw7zNwvDkbF3pVbbZtK', '3216546546', '3216549874', 6, 4, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valorizacion`
--

CREATE TABLE `valorizacion` (
  `id` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `visitas`
--

INSERT INTO `visitas` (`id`, `created_at`, `updated_at`, `cantidad`, `fecha`) VALUES
(8, '2020-03-30 23:59:56', '2020-03-31 01:01:47', 40, '2020-03-30'),
(9, '2020-03-31 22:14:29', '2020-03-31 22:14:29', 10, '2020-03-03'),
(10, '2020-09-08 01:10:11', '2020-09-09 01:10:17', 100, '2020-02-11'),
(11, '2020-03-31 22:14:29', '2020-03-31 23:01:15', 10, '2020-03-31'),
(12, '2020-04-02 20:43:10', '2020-04-03 03:15:29', 19, '2020-04-02'),
(13, '2020-04-08 05:11:18', '2020-04-08 05:23:35', 9, '2020-04-08'),
(14, '2020-04-09 20:58:19', '2020-04-10 01:26:10', 3, '2020-04-09'),
(15, '2020-04-11 19:17:41', '2020-04-11 22:42:41', 11, '2020-04-11'),
(16, '2020-04-13 21:01:37', '2020-04-13 21:15:09', 5, '2020-04-13'),
(17, '2020-04-18 00:15:18', '2020-04-18 00:15:26', 2, '2020-04-17'),
(18, '2020-04-19 01:18:22', '2020-04-19 03:37:39', 32, '2020-04-18'),
(19, '2020-04-20 23:03:42', '2020-04-21 04:16:55', 24, '2020-04-20'),
(20, '2020-04-21 06:40:11', '2020-04-21 06:40:18', 2, '2020-04-21'),
(21, '2020-05-01 00:51:46', '2020-05-01 01:06:25', 11, '2020-04-30'),
(22, '2020-05-06 07:35:40', '2020-05-06 08:38:20', 6, '2020-05-06'),
(23, '2020-05-07 06:37:17', '2020-05-07 07:02:23', 12, '2020-05-07'),
(24, '2020-05-08 07:06:53', '2020-05-08 08:03:01', 15, '2020-05-08'),
(25, '2020-05-09 05:03:33', '2020-05-09 06:48:27', 78, '2020-05-09'),
(26, '2020-05-10 06:19:38', '2020-05-10 22:43:18', 19, '2020-05-10'),
(27, '2020-05-11 21:58:30', '2020-05-11 21:58:30', 1, '2020-05-11'),
(28, '2020-05-17 19:38:33', '2020-05-17 23:21:23', 132, '2020-05-17'),
(29, '2020-05-18 18:39:11', '2020-05-18 20:35:37', 3, '2020-05-18'),
(30, '2020-05-19 07:29:22', '2020-05-19 07:29:22', 1, '2020-05-19'),
(31, '2020-05-21 07:54:16', '2020-05-21 20:50:02', 2, '2020-05-21'),
(32, '2020-05-22 06:38:21', '2020-05-22 23:40:14', 3, '2020-05-22'),
(33, '2020-05-23 07:09:56', '2020-05-24 02:21:51', 2, '2020-05-23'),
(34, '2020-05-27 09:16:38', '2020-05-27 10:02:25', 2, '2020-05-27'),
(35, '2020-05-28 09:37:30', '2020-05-28 22:02:16', 4, '2020-05-28'),
(36, '2020-05-29 06:00:44', '2020-05-29 06:00:44', 1, '2020-05-29'),
(37, '2020-05-30 05:55:03', '2020-05-30 05:55:36', 2, '2020-05-30'),
(38, '2020-06-02 07:40:39', '2020-06-02 10:51:38', 2, '2020-06-02'),
(39, '2020-06-05 06:35:20', '2020-06-05 06:35:20', 1, '2020-06-05'),
(40, '2020-06-12 08:44:45', '2020-06-12 09:44:58', 3, '2020-06-12'),
(41, '2020-07-02 08:58:22', '2020-07-02 08:58:22', 1, '2020-07-02'),
(42, '2020-07-17 20:31:12', '2020-07-17 20:31:12', 1, '2020-07-17'),
(43, '2020-07-23 00:17:19', '2020-07-23 00:17:19', 1, '2020-07-22'),
(44, '2020-07-24 08:16:56', '2020-07-24 11:08:17', 8, '2020-07-24'),
(45, '2020-07-28 19:25:24', '2020-07-28 19:26:21', 3, '2020-07-28'),
(46, '2020-07-30 23:49:45', '2020-07-31 00:46:18', 3, '2020-07-30'),
(47, '2020-07-31 21:39:57', '2020-07-31 21:39:59', 2, '2020-07-31'),
(48, '2020-08-01 13:31:04', '2020-08-01 15:26:15', 2, '2020-08-01'),
(49, '2020-08-04 11:58:45', '2020-08-05 08:45:18', 7, '2020-08-04'),
(50, '2020-08-05 12:55:30', '2020-08-05 12:55:30', 1, '2020-08-05'),
(51, '2020-08-06 12:17:32', '2020-08-07 08:26:09', 8, '2020-08-06'),
(52, '2020-08-07 19:49:10', '2020-08-07 20:28:50', 3, '2020-08-07'),
(53, '2020-08-09 09:02:58', '2020-08-09 09:11:48', 3, '2020-08-08'),
(54, '2020-08-09 13:06:40', '2020-08-10 08:41:05', 2, '2020-08-09'),
(55, '2020-08-11 06:32:15', '2020-08-11 08:23:02', 2, '2020-08-10'),
(56, '2020-08-11 11:32:12', '2020-08-11 18:59:37', 3, '2020-08-11'),
(57, '2020-08-12 15:51:07', '2020-08-13 05:38:41', 2, '2020-08-12'),
(58, '2020-08-14 08:38:29', '2020-08-14 10:07:11', 3, '2020-08-13'),
(59, '2020-08-14 18:45:13', '2020-08-14 19:07:32', 2, '2020-08-14'),
(60, '2020-08-15 12:54:56', '2020-08-16 10:46:07', 4, '2020-08-15'),
(61, '2020-08-16 12:11:00', '2020-08-16 17:49:32', 6, '2020-08-16'),
(62, '2020-08-17 11:47:09', '2020-08-18 08:21:47', 2, '2020-08-17'),
(63, '2020-08-18 12:11:57', '2020-08-18 12:11:57', 1, '2020-08-18'),
(64, '2020-08-19 11:56:45', '2020-08-19 11:56:45', 1, '2020-08-19'),
(65, '2020-09-05 01:46:45', '2020-09-05 02:01:48', 2, '2020-09-04'),
(66, '2020-09-05 19:14:21', '2020-09-06 02:51:28', 6, '2020-09-05'),
(67, '2020-09-06 17:51:06', '2020-09-06 17:51:06', 1, '2020-09-06'),
(68, '2020-09-08 05:26:47', '2020-09-09 00:08:31', 3, '2020-09-08'),
(69, '2020-09-09 18:31:18', '2020-09-09 18:31:18', 1, '2020-09-09'),
(70, '2020-09-10 17:56:17', '2020-09-10 19:54:09', 2, '2020-09-10'),
(71, '2020-09-11 06:42:17', '2020-09-12 00:11:24', 5, '2020-09-11'),
(72, '2020-09-12 07:45:18', '2020-09-12 17:49:18', 4, '2020-09-12'),
(73, '2020-09-13 07:18:25', '2020-09-13 07:18:25', 1, '2020-09-13'),
(74, '2020-09-15 05:56:44', '2020-09-16 01:05:48', 7, '2020-09-15'),
(75, '2020-09-16 06:15:38', '2020-09-17 01:28:44', 4, '2020-09-16'),
(76, '2020-09-17 18:57:09', '2020-09-17 23:32:29', 2, '2020-09-17'),
(77, '2020-09-18 06:37:38', '2020-09-19 03:51:27', 5, '2020-09-18'),
(78, '2020-09-22 07:03:40', '2020-09-22 07:03:40', 1, '2020-09-22'),
(79, '2020-09-27 21:33:01', '2020-09-27 21:42:49', 2, '2020-09-27'),
(80, '2020-09-29 21:24:18', '2020-09-30 01:56:42', 10, '2020-09-29'),
(81, '2020-09-30 18:35:15', '2020-10-01 01:40:54', 45, '2020-09-30'),
(82, '2020-10-01 08:09:31', '2020-10-02 01:31:39', 11, '2020-10-01'),
(83, '2020-10-02 07:17:23', '2020-10-02 23:22:54', 18, '2020-10-02'),
(84, '2020-10-04 01:44:42', '2020-10-04 02:03:12', 8, '2020-10-03'),
(85, '2020-10-04 07:44:14', '2020-10-05 00:18:33', 5, '2020-10-04'),
(86, '2020-10-05 18:39:49', '2020-10-06 01:01:03', 8, '2020-10-05'),
(87, '2020-10-06 22:01:48', '2020-10-06 22:15:24', 2, '2020-10-06'),
(88, '2020-10-07 18:30:57', '2020-10-07 23:06:07', 8, '2020-10-07');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `antecedentes`
--
ALTER TABLE `antecedentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `antecedentes_tipos`
--
ALTER TABLE `antecedentes_tipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `antecedentes_tipo_ibfk_1` (`id_antecedentes`);

--
-- Indices de la tabla `contactenos`
--
ALTER TABLE `contactenos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `directorios`
--
ALTER TABLE `directorios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `dominios`
--
ALTER TABLE `dominios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_institucion` (`id_institucion`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `ficha_saluds`
--
ALTER TABLE `ficha_saluds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_antecedentes_tipo` (`id_antecedentes_tipo`),
  ADD KEY `ficha_saluds_ibfk_1` (`id_estudiante`);

--
-- Indices de la tabla `formulario_ficha_salud`
--
ALTER TABLE `formulario_ficha_salud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `institucions`
--
ALTER TABLE `institucions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_user_mensaje` (`id_user_mensaje`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indices de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `observaciones_dominio_ficha`
--
ALTER TABLE `observaciones_dominio_ficha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `observaciones_dominio_ficha_ibfk_1` (`id_dominio`),
  ADD KEY `id_formulario_ficha_salud` (`id_formulario_ficha_salud`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dominio` (`id_dominio`);

--
-- Indices de la tabla `prevencions`
--
ALTER TABLE `prevencions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `tipo` (`tipo`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_preguntas` (`id_preguntas`),
  ADD KEY `id_formulario_ficha_salud` (`id_formulario_ficha_salud`);

--
-- Indices de la tabla `respuestas_select_dinamico`
--
ALTER TABLE `respuestas_select_dinamico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_preguntas` (`id_preguntas`);

--
-- Indices de la tabla `rols`
--
ALTER TABLE `rols`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_institucion` (`id_institucion`),
  ADD KEY `id_rol` (`id_rol`) USING BTREE,
  ADD KEY `id_estudiante` (`id_estudiante`);

--
-- Indices de la tabla `valorizacion`
--
ALTER TABLE `valorizacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `antecedentes`
--
ALTER TABLE `antecedentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `antecedentes_tipos`
--
ALTER TABLE `antecedentes_tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `contactenos`
--
ALTER TABLE `contactenos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `directorios`
--
ALTER TABLE `directorios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `dominios`
--
ALTER TABLE `dominios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `ficha_saluds`
--
ALTER TABLE `ficha_saluds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `formulario_ficha_salud`
--
ALTER TABLE `formulario_ficha_salud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `institucions`
--
ALTER TABLE `institucions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `observaciones_dominio_ficha`
--
ALTER TABLE `observaciones_dominio_ficha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `prevencions`
--
ALTER TABLE `prevencions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1305;

--
-- AUTO_INCREMENT de la tabla `respuestas_select_dinamico`
--
ALTER TABLE `respuestas_select_dinamico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT de la tabla `rols`
--
ALTER TABLE `rols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `valorizacion`
--
ALTER TABLE `valorizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `antecedentes_tipos`
--
ALTER TABLE `antecedentes_tipos`
  ADD CONSTRAINT `antecedentes_tipos_ibfk_1` FOREIGN KEY (`id_antecedentes`) REFERENCES `antecedentes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `directorios`
--
ALTER TABLE `directorios`
  ADD CONSTRAINT `directorios_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  ADD CONSTRAINT `estudiantes_ibfk_1` FOREIGN KEY (`id_institucion`) REFERENCES `institucions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `eventos_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ficha_saluds`
--
ALTER TABLE `ficha_saluds`
  ADD CONSTRAINT `ficha_saluds_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiantes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_saluds_ibfk_2` FOREIGN KEY (`id_antecedentes_tipo`) REFERENCES `antecedentes_tipos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `formulario_ficha_salud`
--
ALTER TABLE `formulario_ficha_salud`
  ADD CONSTRAINT `formulario_ficha_salud_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiantes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `mensajes_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `mensajes_ibfk_2` FOREIGN KEY (`id_user_mensaje`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticias_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `observaciones`
--
ALTER TABLE `observaciones`
  ADD CONSTRAINT `observaciones_ibfk_1` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiantes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `observaciones_dominio_ficha`
--
ALTER TABLE `observaciones_dominio_ficha`
  ADD CONSTRAINT `observaciones_dominio_ficha_ibfk_1` FOREIGN KEY (`id_dominio`) REFERENCES `dominios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `observaciones_dominio_ficha_ibfk_2` FOREIGN KEY (`id_formulario_ficha_salud`) REFERENCES `formulario_ficha_salud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD CONSTRAINT `preguntas_ibfk_1` FOREIGN KEY (`id_dominio`) REFERENCES `dominios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prevencions`
--
ALTER TABLE `prevencions`
  ADD CONSTRAINT `prevencions_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `prevencions_ibfk_3` FOREIGN KEY (`tipo`) REFERENCES `dominios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD CONSTRAINT `respuestas_ibfk_1` FOREIGN KEY (`id_preguntas`) REFERENCES `preguntas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `respuestas_ibfk_2` FOREIGN KEY (`id_formulario_ficha_salud`) REFERENCES `formulario_ficha_salud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuestas_select_dinamico`
--
ALTER TABLE `respuestas_select_dinamico`
  ADD CONSTRAINT `respuestas_select_dinamico_ibfk_1` FOREIGN KEY (`id_preguntas`) REFERENCES `preguntas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_institucion`) REFERENCES `institucions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `rols` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`id_estudiante`) REFERENCES `estudiantes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
